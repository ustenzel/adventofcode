import Data.Char
import Data.List
import Data.Maybe

parse_mul :: String -> Maybe (Int,Int)
parse_mul ('m':'u':'l':'(':s)
    = do
        (x,',':t) <- Just $ span isDigit s
        (y,')':_) <- Just $ span isDigit t
        pure (read x, read y)
parse_mul _ = Nothing

mainA = print . sum . map (uncurry (*)) . mapMaybe parse_mul . tails =<< getContents


parse_do ('m':'u':'l':'(':s)
      | (x,',':t) <- span isDigit s
      , (y,')':u) <- span isDigit t  =  (read x, read y) : parse_do u

parse_do ('d':'o':'n':'\'':'t':'(':')':s)  =  parse_don't s
parse_do (_:s) = parse_do s
parse_do [] = []

parse_don't ('d':'o':'(':')':s)  =  parse_do s
parse_don't (_:s) = parse_don't s
parse_don't [] = []

main = print . sum . map (uncurry (*)) . parse_do =<< getContents
