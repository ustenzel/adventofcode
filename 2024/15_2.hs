import Control.Monad
import Data.Array.IO
import Data.Array.Unboxed
import Data.List

main = do
    (raw, _:moves) <- break null . lines <$> getContents

    let height = length raw
        width  = length (head raw) * 2
        p0:[]  = [ (x,y) | (y,l) <- zip [0..] raw, (x,'@') <- zip [0,2..] l ]

        xform '@' = ".."
        xform '.' = ".."
        xform '#' = "##"
        xform 'O' = "[]"

    grid <- newListArray ((0,0),(width-1,height-1)) $
            concat $ transpose $ map (concatMap xform) raw
            :: IO (IOUArray (Int,Int) Char)

    foldM (apply_move grid) p0 (concat moves)
    getAssocs grid >>= print . foldl' (\acc ((x,y),c) -> if c == '[' then acc + 100 * y + x else acc) 0


apply_move grid (x,y) dir = case dir of
    '>'  ->  pushH   1  (x+1)
    '<'  ->  pushH (-1) (x-1)
    'v'  ->  pushV   1  [] [(x,y+1)]
    '^'  ->  pushV (-1) [] [(x,y-1)]

  where
    pushH d x1 = do
        c1 <- readArray grid (x1,y)
        case c1 of '#' -> pure (x,y)
                   '[' -> pushH d (x1+d)
                   ']' -> pushH d (x1+d)
                   '.' -> (x+d,y) <$ forM_ [x1,x1-d..x+d]
                                (\i -> readArray grid (i-d,y) >>= writeArray grid (i,y))

    pushV d to_move to_check = do
        cs <- mapM (readArray grid) to_check
        case () of
            _ | all (=='.') cs -> (x,y+d) <$ forM_ (nub $ to_check ++ to_move)
                                        (\(i,j) -> readArray grid (i,j-d) >>= writeArray grid (i,j) >>
                                                   writeArray grid (i,j-d) '.')                             -- apply updates, move
              | any (=='#') cs -> pure (x,y)                                                                -- hit some wall, can't move
              | otherwise      -> pushV d (to_check ++ to_move) to_check'                                   -- more to push
                where
                    to_check' = [ p | (c,(i,j)) <- zip cs to_check
                                    , p <- case c of '[' -> [ (i,j+d), (i+1,j+d) ]
                                                     ']' -> [ (i-1,j+d), (i,j+d) ]
                                                     _   -> [] ]

show_grid grid (u,v) = do
    ((0,0),(w,h)) <- getBounds grid
    forM_ [0..h] $ \y ->
        mapM (\x -> if (x,y) == (u,v) then pure '@' else readArray grid (x,y)) [0..w] >>= putStrLn
    putStrLn []
