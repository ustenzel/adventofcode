import Control.Monad        ( foldM, forM )
import Data.Array.IO
import Data.List            ( sort )
import Data.Maybe           ( catMaybes )

main = do
    raw <- lines <$> getContents
    let width = length (head raw)
        height = length raw

    grid <- newListArray ((0,0),(height+1,width+1)) $
            replicate (width+2) '-' ++
            concatMap (\l -> '-' : l ++ "-") raw ++
            replicate (width+2) '-'
            :: IO (IOUArray (Int,Int) Char)

    rgns <- catMaybes <$> find_regions height width grid

    print . sum . map (\(_,a,u) -> a * length u) $ rgns         -- part 1
    print . sum . map (\(_,a,u) -> a * segments u) $ rgns       -- part 2

data Dir = N | E | S | W deriving Show

find_regions :: Int -> Int -> IOUArray (Int,Int) Char -> IO [Maybe (Char,Int,[(Int,Int,Dir)])]
find_regions h w grid =
    forM (range ((1,1),(h,w))) $ \p -> do
        c <- readArray grid p
        if 'A' <= c && c <= 'Z' then do
            (a,u) <- flood c '+' (0,[]) p
            flood '+' '-' (0,[]) p
            pure $ Just (c,a,u)
          else
            pure Nothing

  where
    flood c d (a0,u0) p@(y,x) = do
            writeArray grid p d
            foldM (\(!a,!u) b@(y',x',dir) -> do
                        v <- readArray grid (y',x')
                        if v == c then flood c d (a,u) (y',x')
                          else if v == d then pure (a,u)
                          else pure (a,b:u)
                  ) (a0+1,u0) [ (y-1,x,N),(y+1,x,S),(y,x-1,W),(y,x+1,E) ]

segments :: [(Int,Int,Dir)] -> Int
segments fs = sum $ map (count_segs . sort) [ [ (y,x) | (y,x,N) <- fs ], [ (y,x) | (y,x,S) <- fs ]
                                            , [ (x,y) | (y,x,E) <- fs ] , [ (x,y) | (y,x,W) <- fs ] ]
  where
    count_segs ((a,b):(c,d):xs)
        | a == c && b+1 == d  =      count_segs ((c,d):xs)
        | otherwise           =  1 + count_segs ((c,d):xs)
    count_segs [_] = 1
    count_segs [ ] = 0

