import Data.Char
import Data.List
import Data.Ratio

data Machine = Machine !Integer !Integer !Integer !Integer !Integer !Integer
  deriving Show

parse :: String -> [Machine]
parse = go . map (map read . words . map clean) . lines
  where
    go :: [[Integer]] -> [Machine]
    go ([ax,ay]:[bx,by]:[cx,cy]:ls) = Machine ax ay bx by cx cy : go (dropWhile null ls)
    go [] = []

    clean c = if isDigit c then c else ' '

solve :: Machine -> (Rational, Rational)
solve (Machine a d  b e  c f)  =  ( (c*e - b*f) % (a*e - b*d)
                                  , (a*f - c*d) % (a*e - b*d) )

tokens (a,b) | denominator a == 1 && denominator b == 1 = 3 * numerator a + numerator b
tokens _ = 0

move :: Machine -> Machine
move (Machine a b c d e f) = Machine a b c d (10000000000000+e) (10000000000000+f)

main = do
    ms <- parse <$> getContents
    print . sum . map tokens $ map solve ms                 -- part 1
    print . sum . map tokens . map solve $ map move ms      -- part 2


