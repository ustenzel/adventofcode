import Data.Char
import Data.List
import Data.Array.Unboxed
import Control.Monad

data Robot = R !Int !Int !Int !Int
  deriving Show

parse :: String -> [Robot]
parse = map ((\[x,y,u,v] -> R x y u v) . map read . words . map clean) . lines
  where
    clean c = if isDigit c || c == '-' then c else ' '

move n (R x y u v) = R x' y' u v
  where
    x' = mod (x + n*u) 101
    y' = mod (y + n*v) 103

count (a,b,c,d) (R x y _ _)
    | x < hx && y < hy = (a+1,b,c,d)
    | x < hx && y > hy = (a,b+1,c,d)
    | x > hx && y < hy = (a,b,c+1,d)
    | x > hx && y > hy = (a,b,c,d+1)
    | otherwise        = (a,b,c,d)
  where
    hx = 50
    hy = 51

vis :: Int -> [Robot] -> IO ()
vis n rs = do mapM_ (\y -> putStrLn $ [ arr ! (x,y) | x <- [0..100] ]) [0..102]
              putStrLn $ shows n "    "
  where
    arr = accumArray (const id) ' ' ((0,0),(100,102)) [ ((x,y),'#') | R x y _ _ <- rs ]
          :: UArray (Int,Int) Char

safety_margin = (\(a,b,c,d) -> a*b*c*d) . foldl' count (0,0,0,0)

mean :: [Robot] -> (Double,Double)
mean rs = ( fromIntegral (sum [ x | R x _ _ _ <- rs ]) / fromIntegral (length rs)
          , fromIntegral (sum [ y | R _ y _ _ <- rs ]) / fromIntegral (length rs) )

variance :: [Robot] -> (Double, Double)
variance rs = ( (sum [ (fromIntegral x-mx)^2 | R x _ _ _ <- rs ]) / fromIntegral (length rs)
              , (sum [ (fromIntegral y-my)^2 | R _ y _ _ <- rs ]) / fromIntegral (length rs) )
  where
    (mx,my) = mean rs

main = do
    rs <- parse <$> getContents
    print . safety_margin . map (move 100) $ rs

    let (n,_):_ = filter (\(_,(u,v)) -> u < 400 && v < 400) $ map (\l -> (l, variance $ map (move l) rs)) [0..]
    vis n $ map (move n) rs
