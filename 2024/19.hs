import           Data.Array
import qualified Data.ByteString.Char8 as S
import           Data.List

main :: IO ()
main = do
    towels0:_:patterns <- S.lines <$> S.getContents
    let towels = S.words $ S.map (\c -> if c==',' then ' ' else c) towels0
    print . length $ filter (is_possible towels) patterns           -- part 1
    print . sum $ map (ways_possible towels) patterns               -- part 2

is_possible :: [S.ByteString] -> S.ByteString -> Bool
is_possible towels pattern = tab ! 0
  where
    tab = listArray (0,S.length pattern) $
            [ or [ tab ! j | t <- towels
                           , t `S.isPrefixOf` S.drop i pattern
                           , let j = S.length t + i ]
            | i <- [0..S.length pattern-1]
            ] ++ [ True ]

ways_possible :: [S.ByteString] -> S.ByteString -> Int
ways_possible towels pattern = tab ! 0
  where
    tab = listArray (0,S.length pattern) $
            [ sum [ tab ! j | t <- towels
                            , t `S.isPrefixOf` S.drop i pattern
                            , let j = S.length t + i ]
            | i <- [0..S.length pattern-1]
            ] ++ [ 1 ]

