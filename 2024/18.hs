import           AoC.Dijkstra
import           Control.Monad
import           Data.Array.Unboxed
import           Data.Maybe
import           Data.Ord               ( Down(..) )
import qualified Data.Set               as S

read_drop :: String -> (Int,Int)
read_drop s = head [ (p,q)
                   | (p,',':t) <- reads s
                   , (q,"") <- reads t ]

limits = (70,70)

main = do
    drops <- map read_drop . lines <$> getContents

    let grid = accumArray min maxBound ((0,0), limits) $ zip drops [1..]
               :: UArray (Int,Int) Int

    mapM_ print $ bfs1 1024 grid limits (0,0)

    -- 'Down' because a lower limit is better.  'min' because the
    -- earlier obstacle determines the limit for the whole path.
    let neighbors (Down l) (x,y) = [ (Down (min l (grid ! p')), p')
                                   | p' <- [ (x+1,y), (x-1,y), (x,y-1), (x,y+1) ]
                                   , bounds grid `inRange` p' ]

    forM_ (dijkstra neighbors (== limits) [(Down maxBound, (0,0))]) $ \(Down ix, _) ->
        let (x,y) = drops !! (ix-1)
        in putStrLn $ shows x "," ++ show y


bfs1 lim grid dest = go 0 S.empty . S.singleton
  where
    go !n !closed !open
        | S.null open          = Nothing
        | dest `S.member` open = Just n
        | otherwise = go (n+1) (S.union closed open) $ S.fromList
                        [ p'
                        | (x,y) <- S.toList open
                        , p' <- [ (x+1,y), (x-1,y), (x,y-1), (x,y+1) ]
                        , bounds grid `inRange` p'
                        , grid ! p' > lim
                        , not $ p' `S.member` open
                        , not $ p' `S.member` closed ]


