import Data.Array.Unboxed
import qualified Data.Set as S

main = do
    raw <- lines <$> getContents
    let grid :: UArray (Int,Int) Char
        grid = listArray ((1,1),(length raw, length (head raw))) (concat raw)

    let [p0] = [ p | (p,'^') <- assocs grid ]
        d0   = (-1,0)

    let path = S.fromList $ map fst $ walk grid p0 d0
    print $ S.size path                                     -- part I

    print $ length $ filter has_loop $ map (\p -> walk (grid // [(p,'#')]) p0 d0)
                   $ S.toList (S.delete p0 path)            -- part II


walk grid p@(y,x) d@(v,u)
    | not $ bounds grid `inRange` p'  =  [(p,d)]
    | grid ! p' /= '#'  =  (p,d) : walk grid p' d
    | otherwise         =  walk grid p d'
  where
    p' = (y+v,x+u)
    d' = (u,-v)

has_loop = go S.empty
  where
    go _ [] = False
    go s (x:xs) | x `S.member` s = True
                | otherwise      = go (S.insert x s) xs



