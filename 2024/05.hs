import Data.Char
import Data.List
import qualified Data.Set as S

parse ls = case break (all isSpace) ls of
    (u,_:v) -> (S.fromList $ map parse_rule u, map (head . parse_update) v)

parse_rule :: String -> (Int,Int)
parse_rule s = head [ (a,b) | (a,'|':t) <- reads s, (b,"") <- reads t ]

parse_update :: String -> [[Int]]
parse_update s = [ x:xs | (x,t) <- reads s
                        , xs <- case t of [] -> [[]] ; ',':u -> parse_update u ; _ -> [] ]

valid :: S.Set (Int,Int) -> [Int] -> Bool
valid rules xs = and [ not $ (b,a) `S.member` rules
                     | a:as <- tails xs
                     , b <- as ]

middle xs = xs !! div (length xs) 2

repair rules = sortBy (?)
  where
    a ? b | (a,b) `S.member` rules = LT
          | (b,a) `S.member` rules = GT
          | otherwise              = EQ

main = do
    (rules, updates) <- parse . lines <$> getContents

    -- part 1
    print . sum . map middle $ filter (valid rules) updates

    -- part 2
    print . sum . map middle . map (repair rules) $ filter (not . valid rules) updates

