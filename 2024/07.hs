parse :: String -> [Int]
parse = map read . map (filter (/=':')) . words

main = do
    eqns <- map parse . lines <$> getContents
    print . sum $ map (try_solve opsA) eqns
    print . sum $ map (try_solve opsB) eqns

try_solve ops (y:xs) = if any (==y) $ values ops xs then y else 0

values ops (x:xs) = go [x] xs
  where
    go acc [    ] = acc
    go acc (y:ys) = go (concatMap (\op -> map (`op` y) acc) ops) ys

opsA = [(+), (*)]
opsB = [(+), (*), (.||.)]

x .||. y = x * m + y
  where
    m:_ = filter (>y) $ iterate (*10) 10


