import           AoC.BronKerbosch
import           Data.Bits
import           Data.Char
import           Data.List
import qualified Data.Map.Strict as M
import           Data.Ord
import qualified Data.Set        as S

parse :: String -> [(String, S.Set String)]
parse [a,b,'-',c,d] = [(u,S.singleton v),(v,S.singleton u)]
  where
    u = [a,b]
    v = [c,d]

find_triplets m = [ (a,b,c)
                  | (a,bs) <- M.toList m
                  , b <- S.toList bs
                  , a < b
                  , c <- S.toList $ M.findWithDefault S.empty b m
                  , b < c
                  , c `S.member` bs ]

has_tee (a,b,c) = h a || h b || h c
  where
    h ('t':_) = True
    h _       = False


main = do
    net <- M.fromListWith S.union . concatMap parse . lines <$> getContents

    print . length . filter has_tee $ find_triplets net                         -- part I

    let neighbors p = M.findWithDefault S.empty p net
    putStrLn . intercalate "," . S.toList . maximumBy (comparing S.size) $      -- part II
        bronKerbosch (M.keysSet net) neighbors (greedyPivot neighbors)


