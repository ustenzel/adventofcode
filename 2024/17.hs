import Data.Bits
import Data.Char
import Data.List
import Data.Maybe
import qualified Data.Vector.Unboxed as U

type Program = U.Vector Int

parse :: String -> (Int,Int,Int,Program)
parse s = case map (map read . words . map clean) $ lines s of
    [[a], [b], [c], [], ps] -> (a,b,c, U.fromList ps)
    foo -> error $ show foo
  where
    clean c = if isDigit c then c else ' '

data VM = VM { reg_a :: !Int
             , reg_b :: !Int
             , reg_c :: !Int
             , reg_ip :: !Int }
  deriving Show

step :: Program -> VM -> Maybe (Maybe Int, VM)
step prog vm
    | reg_ip vm >= U.length prog - 1  =  Nothing
    | otherwise = Just $ case prog U.! reg_ip vm of
        0 -> -- adv, actually a bit shift
            (Nothing, vm { reg_a = reg_a vm `shiftR` combo, reg_ip = reg_ip vm + 2 })

        6 -> -- bdv, actually a bit shift
            (Nothing, vm { reg_b = reg_a vm `shiftR` combo, reg_ip = reg_ip vm + 2 })

        7 -> -- cdv, actually a bit shift
            (Nothing, vm { reg_c = reg_a vm `shiftR` combo, reg_ip = reg_ip vm + 2 })

        1 -> -- bxl, an xor
            (Nothing, vm { reg_b = reg_b vm `xor` literal, reg_ip = reg_ip vm + 2 })

        2 -> -- bst
            (Nothing, vm { reg_b = mod combo 8, reg_ip = reg_ip vm + 2 })

        3 -> -- jnz
            (Nothing, vm { reg_ip = if reg_a vm == 0 then reg_ip vm + 2 else literal })

        4 -> -- bxc
            (Nothing, vm { reg_b = reg_b vm `xor` reg_c vm, reg_ip = reg_ip vm + 2 })

        5 -> -- out
            (Just (mod combo 8), vm { reg_ip = reg_ip vm + 2 })
  where
    literal = prog U.! (reg_ip vm + 1)
    combo = case literal of 4 -> reg_a vm
                            5 -> reg_b vm
                            6 -> reg_c vm
                            7 -> error "can't happen"
                            _ -> literal

run_with :: (Int,Int,Int, Program) -> [Int]
run_with (a,b,c,prog) = catMaybes $ unfoldr (step prog) (VM a b c 0)

main = do
    (a,b,c,prog) <- parse <$> getContents
    putStrLn . intercalate "," . map show $ run_with (a,b,c,prog)       -- part 1
    print . minimum $ search b c prog                                   -- part 2


-- bst A               B := A mod 8
-- bxl 1               B := B xor 1
-- cdv B               C := A >> B
-- adv 3               A := A >> 3
-- bxl 4               B := B xor 4
-- bxc                 B := B xor C
-- out B               out B
-- jnz 0               if A goto 0

-- An input less than 2^3n will produce at most n items of output.  We
-- can search from the end, one output number at a time.  If there was
-- only solution at each stage, that would be a search space of only
-- 7*16 programs.  It is more, because there can be multiple
-- intermediate solution.  It's still cheap enough and finishes
-- practically instantly.

search :: Int -> Int -> Program -> [Int]
search b c prog = go $ U.toList prog
  where
    go [] = [0]
    go (x:xs) = [ a
                | i <- go xs
                , j <- [0..7]
                , let a = shiftL i 3 .|. j
                , run_with (a,b,c,prog) == (x:xs) ]

