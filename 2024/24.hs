import           Control.Monad
import           Data.Bits          ( shiftL )
import           Data.List
import qualified Data.Map.Lazy      as M
import           System.Environment ( getArgs )
import           Text.Printf

parse_value :: String -> (String, Bool)
parse_value s = case words (map clean s) of
    [k,v] -> (k, if v == "1" then True else False)
  where
    clean c = if c == ':' then ' ' else c

eval_gate :: M.Map String Bool -> String -> (String, Bool)
eval_gate m s = case words s of
    [x,op,y,_,z] -> (z, f op a b)
      where
        Just a = M.lookup x m
        Just b = M.lookup y m
        f "AND" = (&&)
        f "OR" = (||)
        f "XOR" = (/=)


main = do
    [op] <- getArgs
    (values, _:gates) <- break null . lines <$> getContents

    case op of
        "1" -> -- part 1; cheap
               let bits = M.fromList $ map parse_value values ++ map (eval_gate bits) gates
               in print $ sum [ shiftL (fromEnum v) i | ('z':num, v) <- M.toList bits, (i,"") <- reads num ]

        "dot" -> do -- produce a dot file for visualization
                    putStrLn "strict graph foo {"
                    forM_ (map words gates) $ \[x,op,y,_,z] -> do
                        printf "%s [label=\"%s %s\"] ;\n" z z op
                        printf "%s -- %s ;\n" x z
                        printf "%s -- %s ;\n" y z
                    putStrLn "}"

        "2" -> main2 gates



-- This isn't working code.  It's half working crap left behind by a
-- very manual process that involved flipping back and forth between
-- code, experiment, and a "dot" visualization.
main2 gates = do
    let net = M.fromList $ map (label_gate net) gates

    let report w u@(Unk {}) = printf "Could not label %s: %s\n" w (show u)
        report w@('z':id) u@(Z i) | read id == i = pure ()
        report w@('z':id) u = printf "Mislabeled %s: %s\n" w (show u)
        report _ _   = pure ()

    mapM_ (uncurry report) $ M.toList net
    forM_ (M.toList net) $ \(w,l) -> case l of
        -- zi is mislabeled X.  So who was labeled X?  Swap those below.
        -- Works three time.
        Z  6 -> print (w, Z 6)
        Z 13 -> print (w, Z 13)
        Z 38 -> print (w, Z 38)
        _ -> pure ()

    -- remaining mislabels involve A25.  are A25 and B25 exchanged?
    -- found by inspection of the the dot graph.
    putStrLn . intercalate "," $ sort [ "z13", "gmh", "z06", "jmq", "z38", "qrh", "rqf", "cbd" ]





-- Try to find a meaningful label for a wire.
label_gate :: M.Map String Label -> String -> (String, Label)
label_gate m s = case words s of
    [x,op,y,_,z] -> (do_swap z, guess_label (f op) a b)
      where
        a = label_of m x
        b = label_of m y

        f "AND" = And
        f "OR"  = Or
        f "XOR" = Xor

        do_swap "gmh" = "z13"
        do_swap "z13" = "gmh"
        do_swap "jmq" = "z06"
        do_swap "z06" = "jmq"
        do_swap "qrh" = "z38"
        do_swap "z38" = "qrh"
        do_swap "cbd" = "rqf"
        do_swap "rqf" = "cbd"
        do_swap w = w

data Op = And | Or | Xor deriving Show
data Label = X Int | Y Int | A Int | B Int | C (Maybe Int) | D Int | Z Int | Unk Op Label Label deriving Show

label_of :: M.Map String Label -> String -> Label
label_of m ('x':id) = X (read id)
label_of m ('y':id) = Y (read id)
label_of m w        = m M.! w


-- | Tries to identify what gates "do".  This is crude, but yields just
-- six gates with which something is wrong.
guess_label :: Op -> Label -> Label -> Label

-- there should be an AND of xi and yi, call it ai
guess_label And (X i) (Y j) | i == j  =  A i
guess_label And (Y i) (X j) | i == j  =  A i

-- there should be an XOR of xi and yi, call it bi
guess_label Xor (X i) (Y j) | i == j  =  B i
guess_label Xor (Y i) (X j) | i == j  =  B i

-- zi should be an XOR of bi and c{i-1}, but we assume the carry is the
-- correct carry without checking
guess_label Xor (B i) (C _)  =  Z i
guess_label Xor (C _) (B j)  =  Z j

-- there should be an AND of bi and c{i-1}, but we assume the carry is
-- the correct carry without checking; call it di
guess_label And (B i) (C _)  =  D i
guess_label And (C _) (B j)  =  D j

-- there should be an OR of ai and di, call it ci
guess_label Or (A i) (D j) | i == j  =  C (Just i)
guess_label Or (D i) (A j) | i == j  =  C (Just i)

-- The ci are the only OR gates
guess_label Or _ _  =  C Nothing
guess_label op x y  =  Unk op x y


