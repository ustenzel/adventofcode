import Data.List

type T a = (a,a,a)

main = do
    lns <- lines <$> getContents

    -- part 1
    print . sum . map count . (\xs -> xs ++ map reverse xs) $
        lns ++ transpose lns ++ diags lns ++ diags (map reverse lns)

    -- part 2
    let triples = zipWith3 zip3  lns (drop 1 lns) (drop 2 lns)

        squares :: [[T (T Char)]]
        squares = map (\ts -> zip3 ts (drop 1 ts) (drop 2 ts)) triples

    print $ length $ filter has_cross $ concat squares

count ('X':'M':'A':'S':s) = 1 + count ('S':s)
count (_:s) = count s
count [   ] = 0

diags lns = transpose (zipWith drop [0..] lns) ++
            transpose (zipWith drop [1..] (transpose lns))

has_cross ((a,_,c),(_,e,_),(g,_,i)) = e == 'A' && good a i && good c g
  where
    good 'M' 'S' = True
    good 'S' 'M' = True
    good  _   _  = False
