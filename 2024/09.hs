import qualified Data.Sequence as Z
import Data.Char
import Data.Sequence ( Seq((:|>), (:<|)) )

file_ids !i (x:y:zs) = (x, Just i) : (y, Nothing) : file_ids (i+1) zs
file_ids !i [x] = [(x, Just i)]
file_ids  _ [ ] = [ ]

frag :: Z.Seq (Int, Maybe Int) -> [Int]
frag ((l,Just i) :<| z) = replicate l i ++ frag z
frag ((l,Nothing) :<| z) = case z of
    z' :|> (m,Nothing) -> frag $ (l,Nothing) :<| z'
    z' :|> (m, Just j)
        | l < m -> replicate l j ++ frag (z' :|> (m-l, Just j))
        | m < l -> replicate m j ++ frag ((l-m, Nothing) :<| z')
        | True  -> replicate l j ++ frag z'
    Z.Empty -> []
frag Z.Empty = []

csum :: [Int] -> Int
csum = sum . zipWith (*) [0..]

defrag :: Z.Seq (Int, Maybe Int) -> Z.Seq (Int, Maybe Int)
defrag (z :|> (l, Nothing)) = defrag z :|> (l,Nothing)
defrag (z :|> (l, Just  i)) = try_move l i Z.Empty z
defrag  Z.Empty             = Z.Empty

try_move :: Int -> Int -> Z.Seq (Int, Maybe Int) -> Z.Seq (Int, Maybe Int) -> Z.Seq (Int, Maybe Int)
try_move l i acc ((m,Nothing) :<| z) | l <= m     =  defrag (acc <> ((l,Just i) :<| (m-l,Nothing) :<| z)) :|> (l,Nothing)
                                     | otherwise  =  try_move l i (acc :|> (m,Nothing)) z
try_move l i acc ((m,Just j) :<| z)  =  try_move l i (acc :|> (m,Just j)) z
try_move l i acc Z.Empty  =  defrag acc :|> (l, Just i)

unpack :: Z.Seq (Int, Maybe Int) -> [Int]
unpack ((l, Just  i) :<| z) = replicate l i ++ unpack z
unpack ((l, Nothing) :<| z) = replicate l 0 ++ unpack z
unpack Z.Empty              = []

main = do
    dmap <- Z.fromList . file_ids 0 . map digitToInt . filter isDigit <$> getContents
    print . csum $ frag dmap
    print . csum . unpack $ defrag dmap
