import Control.Monad
import Data.Array.IO
import Data.Array.Unboxed
import Data.List

main = do
    (raw, _:moves) <- break null . lines <$> getContents

    let height = length raw
        width  = length (head raw)
        p0:[]  = [ (x,y) | (y,l) <- zip [0..] raw, (x,'@') <- zip [0..] l ]

    grid <- newListArray ((0,0),(width-1,height-1)) $
            map (\c -> if c == '@' then '.' else c) $ concat $ transpose raw
            :: IO (IOUArray (Int,Int) Char)

    pe <- foldM (apply_move grid) p0 (concat moves)

    getAssocs grid >>= print . foldl' (\acc ((x,y),c) -> if c == 'O' then acc + 100 * y + x else acc) 0


apply_move grid (x,y) dir = do
    cnext <- readArray grid (x+u,y+v)
    case cnext of '#' -> pure (x,y)
                  '.' -> pure (x+u,y+v)
                  'O' -> push (x+u,y+v)
  where
    (u,v) = case dir of '^' -> (0,-1) ; 'v' -> (0,1) ; '<' -> (-1,0) ; '>' -> (1,0)

    push (x1,y1) = do
        c1 <- readArray grid (x1,y1)
        case c1 of '#' -> pure (x,y)
                   'O' -> push (x1+u,y1+v)
                   '.' -> do writeArray grid (x1,y1) 'O'
                             writeArray grid (x+u,y+v) '.'
                             pure (x+u,y+v)

