import Data.Either
import Data.List

chunks [] = []
chunks ss = case break null ss of (u,v) -> u : chunks (drop 1 v)

-- keys are left, locks are right
parse_block :: [String] -> Either [Int] [Int]
parse_block ss@(s0:_)
    = (if all (=='#') s0 then Right else Left) $
      map length $ map (filter (=='#')) $ transpose ss

main = do
    (keys, locks) <- partitionEithers . map parse_block . chunks . lines <$> getContents

    print $ length [ ()
                   | k <- keys
                   , l <- locks
                   , all (<=7) $ zipWith (+) k l ]
