import Control.Monad
import Data.Array.IO
import Data.Array.Unboxed
import Data.Array.Unsafe
import Data.List
import qualified Data.Set as S

main = do
    raw <- lines <$> getContents
    let width = length (head raw)
        height = length raw
        start:_ = [ (x,y) | (y,l) <- zip [1..] raw, (x,'S') <- zip [1..] l ]
        end:_   = [ (x,y) | (y,l) <- zip [1..] raw, (x,'E') <- zip [1..] l ]

        enc '#' = -2
        enc _   = -1

    grid <- newListArray ((1,1),(width,height)) $ map enc $ concat $ transpose raw :: IO (IOUArray (Int,Int) Int)
    flood grid end
    grid <- unsafeFreeze grid :: IO (UArray (Int,Int) Int)

    let straight_cost = grid ! start

    -- part 1
    do  let solutions         = search 2 grid start
            good_cheats       = [ straight_cost-n | (c,n) <- solutions, n < straight_cost ]
            very_good_cheats  = [ straight_cost-n | (c,n) <- solutions, straight_cost-n >= 100 ]

        -- print $ map (\g -> (length g, head g)) $ group $ sort good_cheats       -- verify example
        print $ length very_good_cheats                                            -- part 1 result

    -- part 2
    do  let solutions         = search 20 grid start
            good_cheats       = [ straight_cost-n | (c,n) <- solutions, straight_cost-n >= 50 ]
            very_good_cheats  = [ straight_cost-n | (c,n) <- solutions, straight_cost-n >= 100 ]

        -- print $ map (\g -> (length g, head g)) $ group $ sort good_cheats       -- verify example
        print $ length very_good_cheats                                            -- part 2 result


flood :: IOUArray (Int,Int) Int -> (Int,Int) -> IO ()
flood grid p0 = do
    bnds <- getBounds grid
    let go !d [    ] = pure ()
        go !d fringe = do
            mapM_ (\p -> writeArray grid p d) fringe
            filterM (\p -> (== (-1)) <$> readArray grid p)
                [ p' | (x,y) <- fringe
                     , p' <- [(x+1,y),(x-1,y),(x,y-1),(x,y+1)]
                     , bnds `inRange` p' ]
                >>= go (d+1)
    go 0 [p0]

search ctime grid = go 0 S.empty . S.singleton
  where
    go !n !closed !open | S.null open = []
                        | otherwise = arrivals ++
                                      go (n+1) (S.union closed open) (S.fromList open')
      where
        arrivals = [ (c,d) | (x,y) <- S.toList open
                           , x' <- [x-ctime..x+ctime]
                           , y' <- [y-ctime..y+ctime]
                           , let k = abs (x'-x) + abs (y'-y)
                           , k <= ctime
                           , bounds grid `inRange` (x',y')
                           , grid ! (x',y') /= -2
                           , let c = ((x,y),(x',y'))
                           , let d = n + k + (grid ! (x',y')) ]

        open' = [ p' | (x,y) <- S.toList open
                     , p' <- [(x+1,y),(x-1,y),(x,y-1),(x,y+1)]
                     , bounds grid `inRange` p'
                     , grid ! p' /= (-2)
                     , not $ p' `S.member` closed
                     , not $ p' `S.member` open ]
