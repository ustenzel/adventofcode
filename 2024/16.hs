import           AoC.Dijkstra
import           Control.Monad
import           Data.Array.Unboxed
import           Data.List
import           Data.Monoid
import qualified Data.Set        as S

main = do
    raw <- lines <$> getContents
    let height = length raw
        width  = length (head raw)
        grid = listArray ((1,1),(width,height)) $ concat $ transpose raw :: UArray (Int,Int) Char

        [p0] = [ p | (p,'S') <- assocs grid ]

        finished (p,_) = grid ! p == 'E'
        neighbors (Cost k0 ts) ((x,y),(u,v)) = [ (Cost k (S.insert (x',y') ts),((x',y'),(u',v')))
                                               | (x',y',u',v',k) <- [(x+u,y+v,u,v,k0+1), (x,y,v,-u,k0+1000),(x,y,-v,u,k0+1000)]
                                               , grid ! (x',y') /= '#' ]

    forM_ (dijkstra' (?) neighbors finished [(Cost 0 (S.singleton p0), (p0,(1,0)))]) $ \(Cost k tiles, _) -> do
        print k                                                     -- part 1, cheapest path
        print $ S.size tiles                                        -- part 2, unique tiles in all paths


-- Custom cost type, consists of the actual cost, which is an integer as
-- usual, and the set of tiles traveled.
data Cost = Cost !Int (S.Set (Int,Int))

instance Eq  Cost where Cost a _     ==    Cost b _  =  a == b
instance Ord Cost where Cost a _ `compare` Cost b _  =  compare a b

Cost a ts1 ? Cost b ts2 | a == b  =  Cost a $ S.union ts1 ts2
