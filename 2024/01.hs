import Data.List ( transpose )

merge [] _ = []
merge _ [] = []
merge (x:xs) ((y,z):ys)
    | x < y = merge xs ((y,z):ys)
    | x > y = merge (x:xs) ys
    | True  = x * z : merge xs ys

main = do
    [ls,rs] <- map sort . transpose . map (map read . words) . lines <$> getContents

    print $ sum $ map abs $ zipWith (-) ls rs
    print $ sum $ merge ls $ map (\g -> (head g, length g)) $ group rs
