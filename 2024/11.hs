import Data.MemoTrie

main :: IO ()
main = do
    stones <- map read . words <$> getContents
    print $ length $ iterate (concatMap blink) stones !! 25     -- part 1, brute force
    print $ sum $ map (`becomes_after` 25) stones               -- part 1, memoized
    print $ sum $ map (`becomes_after` 75) stones               -- part 2

blink :: Int -> [Int]
blink 0 = [1]
blink x = go 10 10 100
  where
    go b l h | x < l = [ x * 2024 ]
                | x < h = [ div x b, mod x b ]
                | True  = go (10*b) (100*l) (100*h)

-- Stone nr 'a' after 'n' blinks turns into @a `becomes_after` n@ stones.
becomes_after :: Int -> Int -> Int
becomes_after a n = untrie the_trie (a,n)

the_trie :: (Int,Int) :->: Int
the_trie = trie $ \(a,n) -> if n == 0 then 1 else sum (map (`becomes_after` (n-1)) (blink a))
