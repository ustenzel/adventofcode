import Control.Applicative
import Control.Monad
import Data.Char
import Data.List
import Data.Maybe
import qualified Data.Map.Lazy as M

pos_numpad :: Char -> (Int,Int)
pos_numpad 'A' = (2,3)
pos_numpad '0' = (1,3)
pos_numpad '1' = (0,2)
pos_numpad '2' = (1,2)
pos_numpad '3' = (2,2)
pos_numpad '4' = (0,1)
pos_numpad '5' = (1,1)
pos_numpad '6' = (2,1)
pos_numpad '7' = (0,0)
pos_numpad '8' = (1,0)
pos_numpad '9' = (2,0)

-- There are at most two paths that can ever make sense:  move horizontally
-- first or move vertically first.  I can't see how they lead to any
-- difference, but they do, so we need to try both.
move_numpad :: Char -> Char -> [String]
move_numpad a b
    | x1 == 0 && y2 == 3  =  [ h ++ v ++ "A" ]
    | x2 == 0 && y1 == 3  =  [ v ++ h ++ "A" ]
    | otherwise           =  [ h ++ v ++ "A", v ++ h ++ "A" ]
  where
    (x1,y1) = pos_numpad a
    (x2,y2) = pos_numpad b
    h = replicate (x2-x1) '>' ++ replicate (x1-x2) '<'
    v = replicate (y1-y2) '^' ++ replicate (y2-y1) 'v'


pos_dirpad :: Char -> (Int,Int)
pos_dirpad 'A' = (2,0)
pos_dirpad '^' = (1,0)
pos_dirpad '<' = (0,1)
pos_dirpad 'v' = (1,1)
pos_dirpad '>' = (2,1)

-- There are again up to two paths for the directional pads.
move_dirpad :: Char -> Char -> [String]
move_dirpad a b
    | x2 == 0 && y1 == 0  =  [ v ++ h ++ "A" ]
    | x1 == 0 && y2 == 0  =  [ h ++ v ++ "A" ]
    | otherwise           =  [ h ++ v ++ "A", v ++ h ++ "A" ]
  where
    (x1,y1) = pos_dirpad a
    (x2,y2) = pos_dirpad b
    h = replicate (x2-x1) '>' ++ replicate (x1-x2) '<'
    v = replicate (y1-y2) '^' ++ replicate (y2-y1) 'v'


main :: IO ()
main = do
    inp <- lines <$> getContents
    let go n = print . sum . map (\l -> read (filter isDigit l) * sum (zipWith (min_cost_final n) ('A':l) l)) $ inp

    go 2        -- part 1
    go 25       -- part 2


-- 'tab ! (a,b,n)' is the minimum cost of robot n moving from a to b and
-- entering a b.  Robot 0 is the human.
tab :: M.Map (Char, Char, Int) Int
tab = M.fromList $ [ ((a,b,0), 1)
                   | a <- "<v>^A"
                   , b <- "<v>^A" ] ++

                   [ ((a,b,n), min_cost n a b)
                   | a <- "<v>^A"
                   , b <- "<v>^A"
                   , n <- [1..25] ]


min_cost       n a b = minimum $ map (\s -> sum $ zipWith (\u v -> tab M.! (u,v,n-1)) ('A':s) s) $ move_dirpad a b
min_cost_final n a b = minimum $ map (\s -> sum $ zipWith (\u v -> tab M.! (u,v,n)) ('A':s) s) $ move_numpad a b

