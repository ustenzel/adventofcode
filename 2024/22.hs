import           Data.Bits
import qualified Data.IntMap.Strict as M

step :: Int -> Int
step n = k
  where
    m = (shiftL n 6 `xor` n) .&. 0xFFFFFF
    l = (shiftR m 5 `xor` m) .&. 0xFFFFFF
    k = (shiftL l 11 `xor` l) .&. 0xFFFFFF


main :: IO ()
main = do
    input <- map read . lines <$> getContents
    print . sum $ map ((!! 2000) . iterate step) input          -- part 1
    print . maximum . M.unionsWith (+) $ map nbananas input     -- part 2


nbananas :: Int -> M.IntMap Int
nbananas = M.fromListWith (const id) . go . map (`mod` 10) . take 2001 . iterate step
  where
    go (a:b:c:d:e:xs) = ( key (b-a) (c-b) (d-c) (e-d), e ) : go (b:c:d:e:xs)
    go _              = []

    key a b c d = shiftL (a+10) 24 .|. shiftL (b+10) 16 .|. shiftL (c+10) 8 .|. (d+10)
