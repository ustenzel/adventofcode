import           Data.Function
import           Data.List
import           Data.Ord
import qualified Data.Set as S

main = do
    raw <- lines <$> getContents

    let width = length (head raw)
        height = length raw

        as = [ ((x,y),c)
             | (y,l) <- zip [1..] raw
             , (x,c) <- zip [1..] l
             , or [ 'a' <= c && c <= 'z'
                  , 'A' <= c && c <= 'Z'
                  , '0' <= c && c <= '9' ] ]

        on_grid (x,y) = 1 <= x && x <= width && 1 <= y && y <= height

    print . S.size . S.fromList . filter on_grid . concatMap antinodes . groupBy ((==) `on` snd) $ sortBy (comparing snd) as
    print . S.size . S.fromList . concatMap (linesWithin on_grid) . groupBy ((==) `on` snd) $ sortBy (comparing snd) as

antinodes as = [ n | (x,y):ps <- tails $ map fst as
                   , (u,v) <- ps
                   , n <- [ (2*u-x,2*v-y), (2*x-u, 2*y-v) ] ]

linesWithin g as = [ n | (x,y):ps <- tails $ map fst as
                       , (u,v) <- ps
                       , let dx = u-x
                       , let dy = v-y
                       , let s = dx `div` gcd dx dy
                       , let t = dy `div` gcd dx dy
                       , n <- takeWhile g (iterate (plus (s,t)) (x,y)) ++
                              takeWhile g (iterate (plus (-s,-t)) (x,y)) ]

(x,y) `plus` (u,v) = (x+u,y+v)

