import Data.Array.Unboxed
import Data.Char
import qualified Data.Set as S

main = do
    raw <- lines <$> getContents
    let width = length (head raw)
        height = length raw
        grid = listArray ((1,1),(height,width)) . map digitToInt $
               concat raw :: UArray (Int,Int) Int

        heads = [ p | (p,0) <- assocs grid ]

    print . sum $ map (score grid 1 . S.singleton) heads        -- part 1
    print . sum $ map (rating grid 1 . pure) heads              -- part 2

score grid 10 ps = length ps
score grid  i ps = score grid (i+1) $ S.fromList
                        [ (y',x')
                        | (y,x) <- S.toList ps
                        , (y',x') <- [(y+1,x),(y-1,x),(y,x+1),(y,x-1)]
                        , bounds grid `inRange` (y',x')
                        , grid ! (y',x') == i ]

rating grid 10 ps = length ps
rating grid  i ps = rating grid (i+1)
                        [ (y',x')
                        | (y,x) <- ps
                        , (y',x') <- [(y+1,x),(y-1,x),(y,x+1),(y,x-1)]
                        , bounds grid `inRange` (y',x')
                        , grid ! (y',x') == i ]
