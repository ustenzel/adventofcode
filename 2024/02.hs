import Data.List ( inits, tails )

mainA = print . length . filter is_safe . map (map read . words) . lines =<< getContents
main  = print . length . filter is_nearly_safe . map (map read . words) . lines =<< getContents

is_safe :: [Int] -> Bool
is_safe xs = all (between (-3) (-1)) ds || all (between 1 3) ds
  where
    ds = zipWith (-) xs (tail xs)

between a b x = a <= x && x <= b

is_nearly_safe xs = any is_safe (zipWith (++) (inits xs) (tails (tail xs)))
