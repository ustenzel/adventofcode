#! /bin/sh

touch .ghc.environment.x86_64-linux-9.4.8
cabal install --lib --force-reinstalls \
    --package-env ./.ghc.environment.x86_64-linux-9.4.8 \
    array bytestring containers MemoTrie unordered-containers text vector

