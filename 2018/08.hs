{-# LANGUAGE BangPatterns #-}
import Prelude ()
import BasePrelude

data T = T [T] [Int] deriving Show

pTree :: [Int] -> (T, [Int])
pTree (nc:nm:xs) = let (cs,ys) = pTrees nc xs
                       (ms,zs) = splitAt nm ys
                   in (T cs ms, zs)

pTrees :: Int -> [Int] -> ([T], [Int])
pTrees 0 xs = ([], xs)
pTrees n xs = let (t,ys) = pTree xs
                  (ts,zs) = pTrees (n-1) ys
              in (t:ts,zs)

sum_meta :: Int -> T -> Int
sum_meta !acc (T cs ms) = foldl' sum_meta (acc + sum ms) cs

tree_val :: T -> Int
tree_val (T [] ms) = sum ms
tree_val (T cs ms) = sum $ map (deref cs_vals) ms
    where
        cs_vals = map tree_val cs
        deref   _    0 = 0
        deref [    ] _ = 0
        deref (x:xs) 1 = x
        deref (x:xs) n = deref xs (n-1)


main = do
    (t,[]) <- pTree . map read . words <$> getContents
    print $ sum_meta 0 t
    print $ tree_val t
