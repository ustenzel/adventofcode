{-# LANGUAGE BangPatterns #-}
import Prelude ()
import BasePrelude
import Control.Monad.State.Strict

import qualified Data.Attoparsec.ByteString.Char8 as A
import qualified Data.ByteString.Char8 as B
import qualified Data.Set as S

pMain, pAlt, pCat, pAtom :: A.Parser Expr
pMain = A.char '^' *> pAlt <* A.char '$'

pAlt = do e <- pCat
          A.option e (Or e <$ A.char '|' <*> A.option (Tip B.empty) pAlt)

pCat = do e <- pAtom
          A.option e (Cat e <$> pCat)

pAtom = A.choice [ A.char '(' *> pAlt <* A.char ')'
                 , Tip <$> A.takeWhile1 (A.inClass "NESW") ]
                 -- , pure $ Tip B.empty ]


data Expr = Tip B.ByteString | Or Expr Expr | Cat Expr Expr
    deriving Show

-- Haha, tried to cheat, but nope, the longest path described by the
-- regex is not the answer.  The real problem has a shortcut.  No way
-- around building a graph, then.
longest (Tip   s) = B.length s
longest (Or  a b) = longest a `max` longest b
longest (Cat a b) = longest a + longest b

data Dir = N | E | S | W deriving (Eq, Ord, Show)

-- Walks an expression from (x,y), opening doors along the way, returns
-- a set of the positions reached.
mkMap :: Int -> Int -> Expr -> State (S.Set (Int,Int,Dir)) (S.Set (Int,Int))
mkMap x y (Tip s) | B.null s         =  return $ S.singleton (x,y)
                  | B.head s == 'N'  =  modify (S.insert (x,y,S) . S.insert (x,y-1,N)) >> mkMap x (y-1) (Tip (B.tail s))
                  | B.head s == 'E'  =  modify (S.insert (x,y,W) . S.insert (x+1,y,E)) >> mkMap (x+1) y (Tip (B.tail s))
                  | B.head s == 'S'  =  modify (S.insert (x,y,N) . S.insert (x,y+1,S)) >> mkMap x (y+1) (Tip (B.tail s))
                  | B.head s == 'W'  =  modify (S.insert (x,y,E) . S.insert (x-1,y,W)) >> mkMap (x-1) y (Tip (B.tail s))

mkMap x y (Cat a b) = mkMap x y a >>= foldM (\ !acc (u,v) -> S.union acc <$> mkMap u v b) S.empty . S.toList
mkMap x y (Or  a b) = S.union <$> mkMap x y a <*> mkMap x y b


directions = [ (N,0,-1), (E,1,0), (S,0,1), (W,-1,0) ]

type RS = S.Set (Int,Int)

furthest :: S.Set (Int,Int,Dir) -> Int -> RS -> RS -> [RS]
furthest mm d closed open
    | S.null open' = [closed']
    | otherwise    = closed' : furthest mm (d+1) closed' open'
  where
    closed' = S.union closed open
    open' = S.fromList [ (x+u,y+v)
                       | (x,y) <- S.toList open
                       , (dir,u,v) <- directions
                       , (x+u,y+v,dir) `S.member` mm ]
            `S.difference` closed'

main = do expr <- either error return . A.parseOnly pMain =<< B.getContents
          let mm = execState (mkMap 0 0 expr) S.empty
          -- print expr
          print $ longest expr
          -- print $ normalize expr []
          print $ S.size mm
          -- print mm
          -- print $ furthest mm 0 S.empty (S.singleton (0,0))
          let closedsets = furthest mm 0 S.empty (S.singleton (0,0))
          print $ length closedsets
          print $ S.size (last closedsets)
          print $ S.size (last closedsets) - S.size (closedsets !! 999)

