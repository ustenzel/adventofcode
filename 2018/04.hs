{-# LANGUAGE OverloadedStrings, BangPatterns #-}
import Prelude ()
import BasePrelude

import qualified Data.ByteString.Char8 as B
import qualified Data.IntMap.Strict as I

type Log = I.IntMap [Int]

scan :: Int -> Log -> [B.ByteString] -> Log
scan ng lg [    ] = lg
scan ng lg (l:ls) = case B.words l of
    _:_:"Guard":gid:_ -> case B.readInt (B.tail gid) of
                            Just (!i,"") -> scan i lg ls
                            _ -> error $ "Huh(1)? " ++ show l
    _:t:_ | "00:" `B.isPrefixOf` t -> case B.readInt (B.drop 3 t) of
                            Just (!i,"]") -> scan ng (I.insertWith (++) ng [i] lg) ls
                            _ -> error $ "Huh(2)? " ++ show l
    _ -> error $ "Huh(0)? " ++ show l


total_time :: Int -> [Int] -> Int
total_time !t (a:b:xs) = total_time (t+a-b) xs
total_time !t [      ] = t

minutes :: [Int] -> I.IntMap Int
minutes = I.unionsWith (+) . go
  where
    go (a:b:xs) = I.fromList [ (t,1) | t <- [b..a-1] ] : go xs
    go [      ] = []

main = do
    logbook <- scan undefined I.empty . sort . B.lines <$> B.getContents
    mapM_ print $ I.toList logbook
    putStrLn []
    mapM_ print $ I.toList $ I.map (total_time 0) logbook
    putStrLn []
    let (tt,g,ts) = maximum $ map (\(g,ts) -> (total_time 0 ts, g, ts)) $ I.toList logbook
    print (g, tt, ts)
    print $ minutes ts
    let (_,m) = maximum $ map (\(a,b) -> (b,a)) $ I.toList $ minutes ts
    print (g,m,g*m)
    putStrLn []
    let stats = concatMap (\(g,ts) -> map (\(a,b) -> (b,g,a)) $ I.toList $ minutes ts) $ I.toList logbook
    let (_,g',m') = maximum stats
    print $ g' * m'
