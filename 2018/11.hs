import Control.Monad
import Data.Array.ST
import Data.Array.Unboxed

serial = 4842

score x y = ((((x+10) * y + serial) * (x+10)) `mod` 1000) `div` 100 - 5

-- I feel dumb, because I didn't see it.  Making an unboxed grid of
-- partial sums is twice as fast as sliding stuff across a grid of
-- scores trying to compute the partial sums on the fly.
-- It's simpler, too.
grid :: UArray (Int,Int) Int
grid = runSTUArray (do
    arr <- newArray_ ((0,0),(300,300))
    forM_ (range ((1,1),(300,300))) $ \(x,y) ->
        writeArray arr (x,y) =<<
            if x == 0 || y == 0 then return 0 else
            liftM3 (\a b c -> a + b - c + score x y)
                   (readArray arr (x-1,y)) (readArray arr (x,y-1)) (readArray arr (x-1,y-1))
    return arr)

-- (29,(20,83))
-- (96,(237,281,10)), 14s, now 5s


square x y sz = grid ! (x+sz,y+sz) - grid ! (x,y+sz) - grid ! (x+sz,y) + grid ! (x,y)

partA = maximum [ ( square x y 3, x+1, y+1 )
                | x <- [0..297], y <- [0..297] ]

partB = maximum [ ( square x y s, x+1, y+1, s )
                | s <- [0..300], x <- [0..300-s], y <- [0..300-s] ]

main = do print partA
          print partB
          -- print $ maximum $ concatMap pwr_grid' [1..300]
