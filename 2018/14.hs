{-# LANGUAGE BangPatterns #-}
import Prelude ()
import BasePrelude
import qualified Data.Sequence as Z
import Data.Sequence ((|>))

roll = 3 : 7 : go (Z.fromList [3,7]) 0 1
  where
    go !rcps !cur1 !cur2 = new_digits ++ go rcps' new1 new2
      where
        sum_score = Z.index rcps cur1 + Z.index rcps cur2
        new_digits | sum_score < 10 = [ sum_score ]
                   | otherwise      = [ div sum_score 10, mod sum_score 10 ]
        rcps' = foldl (|>) rcps new_digits
        new1 = (cur1 + 1 + Z.index rcps cur1) `mod` Z.length rcps'
        new2 = (cur2 + 1 + Z.index rcps cur2) `mod` Z.length rcps'


{- pretty (rcps, cur1, cur2) = Z.foldMapWithIndex
    (\i v -> if i == cur1 then "(" ++ show v ++ ")" else
             if i == cur2 then "[" ++ show v ++ "]" else
                               " " ++ show v ++ " ") rcps -}

indexOf !i needle haystack
    | needle `isPrefixOf` haystack = i
    | otherwise                    = indexOf (succ i) needle (tail haystack)

main = do [needle] <- getArgs
          putStrLn . map intToDigit . take 10 . drop (read needle) $ roll
          print . indexOf 0 (map digitToInt needle) $ roll
