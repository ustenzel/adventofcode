import Prelude ()
import BasePrelude hiding ( Space )
import Control.Monad.State.Strict

import qualified Data.Map.Strict as M
import qualified Data.Set        as S

type Pos = (Int,Int)

type Grid = M.Map Pos MBeast

data MBeast = Wall | Space | Beast Beast deriving (Show, Eq)

data Species = Elf | Goblin deriving (Show, Eq)

data Beast = Beast_ { species :: Species, health :: Int }
  deriving (Show, Eq)

isBeast p (Beast b) = p b
isBeast p _ = False

initHealth = 200
initAttack = 3

readGrid :: [String] -> Grid
readGrid ss = M.fromList grid
  where
    h = length ss
    w = length (head ss)
    grid = [ ((y,x), case c of
                '#' -> Wall
                '.' -> Space
                'E' -> Beast (Beast_ Elf initHealth)
                'G' -> Beast (Beast_ Goblin initHealth))
           | (y,s) <- zip [0..] ss
           , (x,c) <- zip [0..] s ]

adjacent :: Pos -> [Pos]
adjacent (y0,x0) = [(y0-1,x0), (y0+1,x0), (y0,x0-1), (y0,x0+1)]

runBattle :: Int -> Grid -> [Grid]
runBattle m g = case runState (aRound m) g of
    (done, g') -> g' : if done then [] else runBattle m g'

aRound :: Int -> State Grid Bool
aRound m = go . M.filter (isBeast (const True)) =<< get
  where
    go :: Grid -> State Grid Bool
    go togo =
      case M.minViewWithKey togo of
        Nothing -> return False        -- everyone moved, round is over
        Just ((p,Beast b), togo') -> do
          targets <- gets $ M.filter (isBeast ((/= species b) . species))
          grid <- get
          let in_range = S.fromList [ p' | (p0,_) <- M.toList targets
                                    , p' <- adjacent p0
                                    , M.lookup p' grid == Just Space || p == p']

          case find_path grid p in_range of
            _ | M.null targets       -> return True     -- battle is over
            _ | S.member p in_range  -> att m p togo' >>= go
            Nothing                  -> go togo'
            Just p'                  -> move p p' >> att m p' togo' >>= go


-- @find_path grid pos dests@ finds all shortest paths from @pos@ to any
-- point in @dests@ and return the first position to step to, breaking
-- ties in reading order.  (Dijkstra's, then pick minimum?)
find_path :: Grid -> Pos -> S.Set Pos -> Maybe Pos
find_path grid p = go S.empty
  where
    go :: S.Set Pos -> S.Set Pos -> Maybe Pos
    go closed open | S.null open = Nothing
    go closed open =
        case sort [ p' | p' <- S.toList open, p `elem` adjacent p' ] of
            ps@(p':_) -> Just p'
            [  ] -> go (S.union closed open) open'
              where open' = flip S.difference closed $
                            S.filter (\p' -> M.lookup p' grid == Just Space) $
                            foldl' S.union S.empty $
                            S.map (S.fromList . adjacent) open

-- Move something.  Doesn't test if the move is legal.
move :: Pos -> Pos -> State Grid ()
move p p' | p' `elem` adjacent p
    = state $ \grid ->
        let Just b = M.lookup p grid
        in ((), M.insert p Space $ M.insert p' b $ grid)

-- @att p togo@ performs an attack from pos p and updates both the grid
-- and the "to go" list (in case a beast is killed).
att :: Int -> Pos -> Grid -> State Grid Grid
att m p0 togo = state $ \grid ->
    let Just (Beast (Beast_ spc _)) = M.lookup p0 grid
        targets = [ (p,b) | p <- adjacent p0
                          , Just (Beast b) <- [M.lookup p grid]
                          , species b /= spc ]
        (p,b) = minimumBy (comparing $ (health . snd) &&& fst) targets
        pwr = case spc of Elf -> m ; Goblin -> initAttack
    in if null targets then (togo, grid)
       else if health b - pwr <= 0 then (M.delete p togo, M.insert p Space grid)
       else (togo, M.insert p (Beast b { health = health b - pwr }) grid)

printGrid :: Grid -> IO ()
printGrid grid = do
    putStrLn $ unlines [ [ case M.lookup (y,x) grid of
                                Just Wall -> '#'
                                Just (Beast (Beast_ Elf _)) -> 'E'
                                Just (Beast (Beast_ Goblin _)) -> 'G'
                                _ -> '.'
                         | x <- [0..w] ]
                       | y <- [0..h] ]
    mapM_ print $ [ (p,b) | (p, Beast b) <- M.toList grid ]
    putStrLn []
  where
    ((h,w),_) = M.findMax grid

outcome n [  g ] = n * sum [ health b | (_, Beast b) <- M.toList g ]
outcome n (_:gs) = outcome (n+1) gs

elf_dies :: Int -> Grid -> Bool
elf_dies m g = case runState (aRound m) g of
    (done, g') | num_elves g > num_elves g' -> True
               | done                       -> False
               | otherwise                  -> elf_dies m g'
  where
    num_elves x = length [ () | (_, Beast (Beast_ Elf _)) <- M.toList x ]


main = do grid0 <- readGrid . map (takeWhile (not . isSpace)) . lines <$> getContents

          -- partA:
          print $ outcome 0 $ runBattle 3 grid0

          -- partB:
          let m : _ = [ m | m <- [4..], not (elf_dies m grid0) ]
          print $ outcome 0 $ runBattle m grid0







