{-# LANGUAGE BangPatterns, OverloadedStrings #-}
import Prelude ()
import BasePrelude

import qualified Data.Attoparsec.ByteString.Char8 as A
import qualified Data.ByteString.Char8 as B
import qualified Data.IntMap.Strict as M
import qualified Data.Set as S
import qualified Data.Vector as V

data Army = Immunesys | Infection deriving (Show, Eq)

data Attack = Slashing | Fire | Radiation | Bludgeoning | Cold deriving (Show, Eq, Ord)

data Group = Group { units      :: !Int
                   , hitpts     :: !Int
                   , weak_immu  :: (S.Set Attack, S.Set Attack)
                   , damage     :: !Int
                   , attack     :: !Attack
                   , initiative :: !Int
                   , army       :: !Army }
  deriving (Show, Eq)

pGroup = Group <$> A.decimal
               <*  A.string " units each with "
               <*> A.decimal
               <*  A.string " hit points "
               <*> A.option (S.empty,S.empty) (A.char '(' *> pWeakImmu <* A.char ')')
               <*  A.skipSpace
               <*  A.string "with an attack that does "
               <*> A.decimal
               <*  A.skipSpace
               <*> pAttack
               <*  A.string " damage at initiative "
               <*> A.decimal
               <*  A.endOfInput

pAttack = A.choice [ Slashing    <$ A.string "slashing"
                   , Fire        <$ A.string "fire"
                   , Radiation   <$ A.string "radiation"
                   , Bludgeoning <$ A.string "bludgeoning"
                   , Cold        <$ A.string "cold" ]

pWeakImmu =          (,) <$> pWeak <*> A.option S.empty (A.char ';' *> A.skipSpace *> pImmu)
            <|> flip (,) <$> pImmu <*> A.option S.empty (A.char ';' *> A.skipSpace *> pWeak)

pWeak = A.string "weak to " *> pAttList
pImmu = A.string "immune to " *> pAttList

pAttList = S.fromList <$> pAttack `A.sepBy1` (A.char ',' *> A.skipSpace)


parse_input :: B.ByteString -> [Group]
parse_input s = go undefined $ map (id &&& A.parseOnly pGroup) $ B.lines s
  where
     go a ((l,_):ls) | l == "Immune System:"  =  go Immunesys ls
     go a ((l,_):ls) | l == "Infection:"      =  go Infection ls
     go a ((l,_):ls) | l == ""                =  go a ls
     go a ((_,Right g):ls) = g a : go a ls
     go a ((l,Left  e):ls) = error $ "Shit: " ++ show e ++ " in " ++ show l
     go _ [] = []


eff_power :: Group -> Int
eff_power = liftA2 (*) units damage

-- "Everyone picks a target."  We'll keep the groups in a map, so we
-- can identify them by number.

target_selection :: M.IntMap Group -> [( Group, ( Int, Int ))]
target_selection gs0 = select S.empty $ reverse $ sortOn ((eff_power &&& initiative) . snd) $ M.toList gs0
  where
    select _     [        ] = [ ]
    select taken ((j,g):gs) =
        case sort [ ((-pot_dmg, -eff_power tgt, -initiative tgt), i)
                  | (i,tgt) <- M.toList gs0
                  , army g /= army tgt
                  , not $ S.member i taken
                  , not $ S.member (attack g) (snd (weak_immu tgt))
                  , let pot_dmg = if S.member (attack g) (fst (weak_immu tgt)) then 2 else 1
                  ] of
            [     ] -> select taken gs
            (_,i):_ -> (g,(j,i)) : select (S.insert i taken) gs

fight :: M.IntMap Group -> M.IntMap Group
fight gs0 = go gs0 $ map snd $ sortOn (negate . initiative . fst) $ target_selection gs0
  where
    go gs ((i,j):ijs) = case (M.lookup i gs, M.lookup j gs) of
        (Just g1, Just g2) -> let dmg = if S.member (attack g1) (snd (weak_immu g2)) then 0
                                        else if S.member (attack g1) (fst (weak_immu g2))
                                        then 2 * eff_power g1 else eff_power g1
                                  kills = dmg `div` hitpts g2
                                  gs' | units g2 > kills = M.insert j (g2 { units = units g2 - kills }) gs
                                      | otherwise        = M.delete j gs
                              in go gs' ijs
        _ -> go gs ijs
    go gs [] = gs

is_decided gs = all ((==) Immunesys . army) gs || all ((==) Infection . army) gs

outcome (g:g':gs) | is_decided g = (sum $ fmap units $ M.filter ((==) Immunesys . army) g,
                                    sum $ fmap units $ M.filter ((==) Infection . army) g,
                                    army . snd $ M.findMin g)
                  | g == g'      = (sum $ fmap units $ M.filter ((==) Immunesys . army) g,
                                    sum $ fmap units $ M.filter ((==) Infection . army) g,
                                    Infection)
                  | otherwise    = outcome (g':gs)

boost b = fmap (\g -> g { damage = damage g + if army g == Immunesys then b else 0 })

main = do gs <- M.fromList . zip [0..] . parse_input <$> B.getContents
          print $ result 0 gs       -- partA
          print $ search gs 0 4000  -- partB, 4000 is a wild guess
          -- (clean solution:  exponential search followed by binary)

result bst gs = outcome $ iterate fight $ boost bst gs

search gs l h
    | trace (show (l,h,r)) False = undefined
    | l == h         = (u,v,r)
    | r == Infection = search gs (m+1) h
    | otherwise      = search gs l m
  where
    m     = (l + h) `div` 2
    (u,v,r) = result m gs
