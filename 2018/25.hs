{-# LANGUAGE BangPatterns #-}
import Prelude ()
import BasePrelude

import qualified Data.Set as S

parse [x,y,z,w] = (x,y,z,w)
clean c = if c == ',' then ' ' else c

add_pt p = go (S.singleton p)
  where
    go acc (c:cs)
        | any (close p) c = go (S.union acc c) cs
        | otherwise       = c : go acc cs
    go acc [    ]         = [ acc ]

    close (x,y,z,w) (a,b,c,d) =
        abs (x-a) + abs (y-b) + abs (z-c) + abs (w-d) <= 3


main = do pts <- map (parse . map read . words) . lines . map clean <$> getContents
          let grps = foldl' (flip add_pt) [] pts
          print $ length grps
