{-# LANGUAGE BangPatterns #-}
import Prelude ()
import BasePrelude hiding ( left, right )

import qualified Data.Vector.Unboxed as V

data Board = B [Int64] [Int64]

instance Show Board where
    show (B ml (m:mr)) = unwords $ map show (reverse ml) ++ ["("++show m++")"] ++ map show mr
    show (B ml [    ]) = show (B [] (reverse ml))


board0 = B [] [0]

-- place :: Int -> Board -> (Int, Board)
place i b
    | i `mod` 23 == 0
        = case left 7 b of
            B ml (j:mr) -> ( i+j, B ml mr )
    | otherwise
        = case right 2 b of
            B ml mr -> ( 0, B ml (i:mr))

left 0 b = b
left n (B [] mr) = left n (B (reverse mr) [])
left n (B (m:ml) mr) = left (n-1) (B ml (m:mr))

right 0 b = b
right n (B ml []) = right n (B [] (reverse ml))
right n (B ml (m:mr)) = right (n-1) (B (m:ml) mr)

-- List of scores from moves.
-- play :: [Int]
play = go 1 board0
    where
        go !i b = case place i b of (s,b') -> s : go (i+1) b'

score num_players num_moves =
    V.accum (+) (V.replicate num_players 0) $
    zip (cycle [0..num_players-1]) (take num_moves play)

-- (A) 416 players; last marble is worth 71617 points
-- (B) 416 players; last marble is worth 7161700 points

main = do
    [np,nm] <- getArgs
    print $ V.maximum $ score (read np) (read nm)
