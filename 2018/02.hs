import Data.List

main1 =
    print . cksum .
    map (map length . group . sort) . lines =<< getContents

cksum xs = count 2 * count 3
 where
    count k = length $ filter (any (== k)) xs

main = do
    xs <- lines <$> getContents
    print [ map fst $ filter (uncurry (==)) $ zip x y
          | x:ys <- tails xs
          , y <- ys
          , 1 == sum (zipWith (\a b -> if a /= b then 1 else 0) x y) ]
