{-# LANGUAGE TypeOperators #-}
import BasePrelude
import Prelude ()

import Data.MemoTrie
import qualified Data.Set as S
import qualified Data.Map.Strict as M
import qualified Data.PQueue.Min as Q

geology :: (Int,Int) -> Int
geology = memo $ \(x,y) ->
    if (x,y) == (0,0) then 0 else
    if (x,y) == target then 0 else
    if x == 0 then y * 48271 else
    if y == 0 then x * 16807 else
    erosion (x-1) y * erosion x (y-1)

erosion x y = (geology (x,y) + depth) `mod` 20183
typ x y = erosion x y `mod` 3

-- input:
depth = 3198
target = (12,757)

-- depth = 510
-- target = (10,10)


partA = sum [ typ x y | (x,y) <- range ((0,0),target) ]

partB = print . head . filter (\(_,(x,y,t)) -> t == Torch && (x,y) == target) $
        dijkstra S.empty (Q.singleton (0,(0,0,Torch)))

main = partB


data Tool = None | Torch | Gear deriving (Eq, Ord, Show)

dijkstra :: S.Set (Int,Int,Tool) -> Q.MinQueue (Int,(Int,Int,Tool)) -> [(Int,(Int,Int,Tool))]
dijkstra closed open
    | S.member node closed = dijkstra closed open_tail
    | otherwise = (cost,node) : dijkstra (S.insert node closed) open'
  where
    ((cost, node), open_tail) = Q.deleteFindMin open

    open' = foldl' (\q cn -> Q.insert cn q) open_tail $
                [ (cost+c, n)
                | (c,n) <- step node
                , not $ S.member n closed ]

step :: (Int,Int,Tool) -> [(Int, (Int,Int,Tool))]
step (x,y,t) = filter (legal . snd) $ [ (7,(x,y,t')) | t' <- [None, Torch, Gear], t /= t' ]
                                   ++ [ (1,(x+u,y+v,t)) | (u,v) <- [(1,0),(0,1),(-1,0),(0,-1)]
                                                        , x+u >= 0, y+v >= 0 ]

legal :: (Int,Int,Tool) -> Bool
legal (x,y,t) = case (t, typ x y) of
    (None,  0) -> False
    (Torch, 1) -> False
    (Gear,  2) -> False
    _          -> True



