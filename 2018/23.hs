{-# LANGUAGE BangPatterns #-}
import           AoC.BronKerbosch
import           Data.Char
import           Data.List
import           Data.Ord
import qualified Data.Set as S
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U

type Bot = (Int,Int,Int,Int)

parse_bot :: String -> Bot
parse_bot s = case map read $ words $ map clean s of
    [x,y,z,r] -> (x, y, z, r)
  where
    clean c = if isDigit c || c == '-' then c else ' '

maiA :: IO ()
maiA = do bots <- map parse_bot . lines <$> getContents
          let botx = maximumBy (comparing (\(_,_,_,r) -> r)) bots
          print $ length $ filter (in_range botx) bots

in_range :: Bot -> Bot -> Bool
in_range (x,y,z,r) (x',y',z',_) =
    abs (x-x') + abs (y-y') + abs (z-z') <= r

overlaps :: Bot -> Bot -> Bool
overlaps (x,y,z,r) (x',y',z',r') =
    abs (x-x') + abs (y-y') + abs (z-z') <= r+r'

all_common :: [Bot] -> ((Int,Int,Int,Int),(Int,Int,Int,Int))
all_common = foldl' (\c n -> narrow c (to_slabs n)) (minBound, maxBound)
  where
    -- A range is the intersection of four slabs.  Decompose into
    to_slabs (x,y,z,r) = ((  x+y+z-r, x+y-z-r, x-y+z-r, -x+y+z-r )
                         ,(  x+y+z+r, x+y-z+r, x-y+z+r, -x+y+z+r ))

    narrow ((al,bl,cl,dl), (ah,bh,ch,dh))
           ((rl,sl,tl,ul), (rh,sh,th,uh))
        = ((max al rl, max bl sl, max cl tl, max dl ul)
          ,(min ah rh, min bh sh, min ch th, min dh uh))

-- Part B:
--
-- Enumerating all points in the volume is not feasible, there are
-- approximately 10^24 of them.  Enumerating all points that are on the
-- surface of at least three ranges is still 1.5*10^10 points, still not
-- feasible.
--
-- Idea from the reddit:  if all possible pairs of octahedrons from a
-- set overlap, they all have some common overlap.  (Easy to see:  each
-- octahedron defines a minimum and a maximum coordinate in four
-- cardinal directions.  In one direction, it's easy to see, because
-- it's just intervals overlapping.)  In the graph where two nanobots
-- are connected if and only their ranges overlap, find the largest
-- clique (Bron–Kerbosch algorithm), compute the common overlap for the
-- clique (an irregular, but axis aligned octahedron), select the point
-- closest to the origin.

main = do !bots <- U.fromList . map parse_bot . lines <$> getContents

          -- Build the overlap graph, run Bron-Kerbosch on it.  The
          -- first clique is returned right away, and happened to be
          -- the largest (977 nodes).  With greedy pivoting, it finds
          -- 62 maximal cliques in about two seconds.  (This proves
          -- that 977 is indeed largest, by a huge margin.)  This is
          -- freaking fast for an algorithm with O(3^(n/3)) worst case
          -- complexity!)

          let all_nodes = S.fromList [0 .. U.length bots -1]
              neighbors = V.generate (U.length bots) $ \i ->
                          S.fromList [ j | j <- [0 .. U.length bots -1]
                                     , i /= j
                                     , overlaps (bots U.! i) (bots U.! j) ]

              maxclique = maximumBy (comparing S.size) $ bronKerbosch all_nodes (neighbors V.!) (greedyPivot (neighbors V.!))
              good_rgn  = all_common $ map (bots U.!) $ S.toList maxclique

          print good_rgn

{- It remains to pick the point in the overlap closest to origin, but
   that's easily done manually.  I got

(88894457,9463157,37435763,41995532)
(88894457,9463157,37435763,41995537)

which resolves to

             x+y+z  = 88894457
             x+y-z  =  9463157
             x-y+z  = 37435763
41995532 <= -x+y+z <= 41995537

and has the solution

x = 23449460
y = 25729347
z = 39715650

which gives the answer

x+y+z = 88894457     -}
