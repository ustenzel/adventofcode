{-# LANGUAGE BangPatterns #-}
import Prelude ()
import BasePrelude
import System.IO

import Data.Array

data V2 = V2 !Int !Int deriving (Eq, Ord, Ix, Show)

instance Semigroup V2 where
    V2 a b <> V2 x y = V2 (a+x) (b+y)

readArr ls = listArray (V2 0 0, V2 (h-1) (w-1)) $ concat ls
  where
    w = length $ head ls
    h = length ls

evolve arr = listArray (bounds arr) [ step (arr ! p) (neighbors p) | p <- range (bounds arr) ]
  where
    step '.' (t,_) = if t >= 3 then '|' else '.'
    step '|' (_,l) = if l >= 3 then '#' else '|'
    step '#' (t,l) = if l >= 1 && t >= 1 then '#' else '.'

    neighbors p = ( sum [ 1 | o  <- offsets, inRange (bounds arr) (p <> o), arr ! (p <> o) == '|' ]
                  , sum [ 1 | o  <- offsets, inRange (bounds arr) (p <> o), arr ! (p <> o) == '#' ] )

    offsets = [ V2 (-1) (-1), V2 (-1) 0, V2 (-1) 1
              , V2 0 (-1), V2 0 1
              , V2 1 (-1), V2 1 0, V2 1 1 ]

mainA = do gen0 <- readArr . lines <$> getContents
           let genN = head . drop 10 $ iterate evolve gen0
           let t = length $ filter (=='|') $ elems genN
           let l = length $ filter (=='#') $ elems genN
           print (t,l,t*l)

-- Looked at output, it's periodic with period 28 after about 500
-- generations.  Since 1_000_000_000 = 20 mod 28, report the result for
-- generation 20*28 + 20 = 580.
mainB = do gen0 <- readArr . lines <$> getContents
           let count = foldl' (\(!t,!l) c -> if c == '|' then (t+1,l) else if c == '#' then (t,l+1) else (t,l)) (0,0) . elems
               val (t,l) = (t,l,t*l)
           mapM_ print . zip [0..] $ map (val . count) $ iterate evolve gen0

-- Text mode animation
main = do gen0 <- readArr . lines <$> getContents
          putStrLn "\27[1;1H\27[2J"
          mapM_ print_raw $ iterate evolve gen0

print_raw arr = do
    putStrLn $ "\27[1;1H" ++ unlines [ [ arr ! V2 y x | x <- [0..w] ] | y <- [0..h] ]
    threadDelay 20000
  where
    (V2 0 0, V2 h w) = bounds arr


