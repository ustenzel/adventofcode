import Prelude ()
import BasePrelude

import qualified Data.Map.Strict as M
import qualified Data.Set as S

-- Mostest stupidest input format so far...
read1 :: [String] -> (Char,Char)
read1 [_,[a],_,_,_,_,_,[b],_,_] = (b,a)
read1 foo = error $ show foo

unroll_deps m
    | M.null m = []
    | otherwise = next : unroll_deps (M.map (S.delete next) (M.delete next m))
  where
    next = fst $ M.findMin $ M.filter S.null m

main = do
    deps <- foldl' (\m (x,y) -> M.insertWith S.union x (S.singleton y) $
                                M.insertWith S.union y S.empty m) M.empty .
            map (read1 . words) . lines <$> getContents

    print deps
    print $ unroll_deps deps
    putStrLn []

    print $ schedule 60 5 deps


-- Schedule 5 workers.  Pick first available job as before, assign a
-- worker, remember when it will be finished.  If no worker or job is
-- available, jump forward in time, mark job as finished, keep going.

schedule tc n = go 0 []
  where
    go t [  ] queue | M.null queue = t
    go t jobs queue = case M.lookupMin $ M.filter S.null queue of
        _ | length jobs == n  ->  wait t jobs queue       -- everyone busy:  wait
        Nothing               ->  wait t jobs queue       -- not job ready:  wait
        Just (next,_)         ->  go t ((t+tc+ord next - ord '@', next) : jobs) (M.delete next queue)

    wait t jobs queue = case sort jobs of
        (t',next) : jobs' -> go t' jobs' (M.map (S.delete next) queue)



