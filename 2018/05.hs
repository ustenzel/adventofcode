import Prelude ()
import BasePrelude

import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C

react :: [Word8] -> [Word8] -> [Word8]
react l [        ] = reverse l
react l [    r   ] = reverse (r:l)
react l (r1:r2:rs)
    | xor r1 r2 == 32 = reactL l rs
    | otherwise       = react (r1:l) (r2:rs)

reactL (l:ls) (r:rs) | xor l r == 32 = reactL ls rs
reactL ls rs = react ls rs

is_letter c = (c>='A' && c<='Z') || (c>='a'&&c<='z')

main = do
    inp <- C.filter is_letter <$> B.getContents
    print $ length $ react [] $ B.unpack inp
    putStrLn []
    print $ minimum  [ (length $ react [] $ B.unpack $ B.filter (/=c) $ B.filter (/= (32+c)) inp, c)
                     | c <- take 26 [65..] ]
