import Prelude ()
import BasePrelude

import qualified Data.ByteString.Char8 as B
import qualified Data.IntMap.Strict as I

read2 s = case B.readInt s of
    Just (x,t) -> case B.readInt (B.drop 2 t) of
        Just (y,_) -> (x,y)

dist :: (Int,Int) -> (Int,Int) -> Int
dist (x,y) (u,v) = abs (u-x) + abs (v-y)

closest :: (Int,Int) -> [(Int,Int)] -> Int
closest x ys =
    case sort [ (dist x y, i) | (y,i) <- zip ys [(1::Int)..] ] of
        (d,i) : (d',_) : _ -> if d < d' then i else 0

main1 = do
    ps <- map read2 . B.lines <$> B.getContents
    let l = minimum [ x | (x,_) <- ps ]
        r = maximum [ x | (x,_) <- ps ]
        t = minimum [ y | (_,y) <- ps ]
        b = maximum [ y | (_,y) <- ps ]

    let fm = foldl' (\m p -> I.insertWith (+) (closest p ps) (1::Int) m) I.empty
                    [ (x,y) | x <- [l..r], y <- [t..b] ]

    putStrLn []
    sequence_ [ printf "%c: %d\n" (chr $ 64 + i) a | (i,a) <- I.toList fm ]

    let mm = foldl' (\m p -> I.delete (closest p ps) m) fm $
                    [ (x,y) | x <- [l..r], y <- [t,b] ] ++
                    [ (x,y) | x <- [l,r], y <- [t..b] ]

    putStrLn []
    sequence_ [ printf "%c: %d\n" (chr $ 64 + i) a | (i,a) <- I.toList mm ]

    print $ maximum mm


main = do
    ps <- map read2 . B.lines <$> B.getContents
    let l = minimum [ x | (x,_) <- ps ]
        r = maximum [ x | (x,_) <- ps ]
        t = minimum [ y | (_,y) <- ps ]
        b = maximum [ y | (_,y) <- ps ]

    print $ length $ filter (\p -> sum (map (dist p) ps) < 10000)
                            [ (x,y) | x <- [l..r], y <- [t..b] ]

