import qualified Data.IntSet as I

main1 = print . sum . map rd . lines =<< getContents

rd :: String -> Int
rd ('+':s) = read s
rd ('-':s) = negate $ read s

main = do
    xs <- cycle . map rd . lines <$> getContents
    print $ scan I.empty 0 xs

scan s f (x:xs) =
    if I.member f s then f
    else scan (I.insert f s) (f+x) xs


