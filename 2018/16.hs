import Prelude ()
import BasePrelude

import qualified Data.Map.Strict as M

data Regs = Regs !Int !Int !Int !Int deriving (Eq, Show)

get :: Regs -> Int -> Int
get (Regs a b c d) 0 = a
get (Regs a b c d) 1 = b
get (Regs a b c d) 2 = c
get (Regs a b c d) 3 = d

set :: Regs -> Int -> Int -> Regs
set (Regs a b c d) 0 x = Regs x b c d
set (Regs a b c d) 1 x = Regs a x c d
set (Regs a b c d) 2 x = Regs a b x d
set (Regs a b c d) 3 x = Regs a b c x

type Ops = [( Int, Int -> Int -> Int -> Regs -> Regs )]

ops :: Ops
ops = zip [0..]
    [ \a b c rs -> set rs c $ (get rs a) + (get rs b)                     -- addr
    , \a b c rs -> set rs c $ (get rs a) + b                              -- addi
    , \a b c rs -> set rs c $ (get rs a) * (get rs b)                     -- mulr
    , \a b c rs -> set rs c $ (get rs a) * b                              -- muli
    , \a b c rs -> set rs c $ (get rs a) .&. (get rs b)                   -- banr
    , \a b c rs -> set rs c $ (get rs a) .&. b                            -- bani
    , \a b c rs -> set rs c $ (get rs a) .|. (get rs b)                   -- borr
    , \a b c rs -> set rs c $ (get rs a) .|. b                            -- bori
    , \a b c rs -> set rs c $  get rs a                                   -- setr
    , \a b c rs -> set rs c a                                             -- seti
    , \a b c rs -> set rs c $ if a > get rs b then 1 else 0               -- gtir
    , \a b c rs -> set rs c $ if get rs a > b then 1 else 0               -- gtri
    , \a b c rs -> set rs c $ if get rs a > get rs b then 1 else 0        -- gtrr
    , \a b c rs -> set rs c $ if a == get rs b then 1 else 0              -- eqir
    , \a b c rs -> set rs c $ if get rs a == b then 1 else 0              -- eqri
    , \a b c rs -> set rs c $ if get rs a == get rs b then 1 else 0 ]     -- eqrr

type Insn = ( Int, Int, Int, Int )
type Sample = ( Insn, Regs, Regs )

parse :: [[String]] -> ( [Sample], [Insn] )
parse ( ["Before", x0, x1, x2, x3] : [op, a, b, c] : ["After", y0, y1, y2, y3] : [] : ls )
    = let rs0 = Regs (read x0) (read x1) (read x2) (read x3)
          rs1 = Regs (read y0) (read y1) (read y2) (read y3)
          insn = (read op, read a, read b, read c)
          (samples, prog) = parse ls
      in ( (insn, rs0, rs1) : samples, prog )
parse ( [] : ls ) = parse ls
parse ls = ( [], map (\[op,a,b,c] -> (read op, read a, read b, read c)) ls )

test_sample :: Ops -> Sample -> Ops
test_sample opmap ( (_, a, b, c), rs0, rs1 ) = filter (\(_,f) -> f a b c rs0 == rs1) opmap

refine :: M.Map Int Ops -> Sample -> M.Map Int Ops
refine opmap smp@( (op, _, _, _), _, _ ) = M.adjust (flip test_sample smp) op opmap

resolveAll :: M.Map Int Ops -> M.Map Int Ops
resolveAll m | M.map (map fst) m == M.map (map fst) m' = m
             | otherwise                               = resolveAll m'
  where m' = resolve m


resolve :: M.Map Int Ops -> M.Map Int Ops
resolve opmap = let resolved = [ nr | (_, [(nr,_)]) <- M.toList opmap ]

                    resolve1 :: Ops -> Ops
                    resolve1 [x] = [x]
                    resolve1 fns = filter (\(nr,_) -> not $ nr `elem` resolved) fns

                in M.map resolve1 opmap


mainA = do
    ( samples, prog ) <- parse . map words . lines . filter (liftA2 (||) isAlphaNum isSpace) <$> getContents
    print . length . filter ((>=3) . length) . map (test_sample ops) $ samples

main = do
    ( samples, prog ) <- parse . map words . lines . filter (liftA2 (||) isAlphaNum isSpace) <$> getContents

    print . length . filter ((>=3) . length) . map (test_sample ops) $ samples

    let opmap0 = M.fromList [ (i,ops) | i <- [0..15] ]
    let opmap = M.map (snd . head) $ resolveAll $ foldl' refine opmap0 samples

    print $ foldl' (\rs (op,a,b,c) -> fromJust (M.lookup op opmap) a b c rs) (Regs 0 0 0 0) prog
