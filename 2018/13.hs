import Prelude ()
import BasePrelude
import Data.Array.Unboxed
import qualified Data.Map as M

data Dir = N | E | S | W deriving Show
data Mem = L | G | R deriving Show

type Carts = [(( Int, Int ), ( Dir, Mem ))]
type Grid = UArray (Int,Int) Char

read_tracks :: [String] -> (Grid, Carts)
read_tracks lns = (grid, carts)
  where
    width = length $ head lns
    height = length lns
    grid = listArray ((0,0),(height-1,width-1)) $
                map (\c -> if c == '>' then '-' else
                           if c == '<' then '-' else
                           if c == '^' then '|' else
                           if c == 'v' then '|' else c) $
                concat lns
    carts = catMaybes [ if c == '>' then Just ((y,x),(E,L)) else
                        if c == '<' then Just ((y,x),(W,L)) else
                        if c == '^' then Just ((y,x),(N,L)) else
                        if c == 'v' then Just ((y,x),(S,L)) else
                        Nothing
                      | (y,ln) <- zip [0..] lns
                      , (x,c)  <- zip [0..] ln ]

tick :: Grid -> Carts -> Either (Int,Int) Carts
tick grid carts = run (M.fromList carts) carts
  where
    run mp [      ] = Right $ M.toList mp
    run mp (ct:cts) | isNothing (M.lookup (fst ct) mp) = run mp cts
    run mp (ct:cts) = let ct' = move ct
                      in case M.lookup (fst ct') mp of
                            -- Just _  -> Left (fst ct')
                            Just _  -> run (M.delete (fst ct') $ M.delete (fst ct) mp) cts
                            Nothing -> run (M.insert (fst ct') (snd ct') $ M.delete (fst ct) mp) cts

    move ((y,x),(N,m)) = case grid ! (y-1,x) of '|'  -> ((y-1,x),(N,m))
                                                '/'  -> ((y-1,x),(E,m))
                                                '\\' -> ((y-1,x),(W,m))
                                                '+'  -> case m of L -> ((y-1,x),(W,G))
                                                                  G -> ((y-1,x),(N,R))
                                                                  R -> ((y-1,x),(E,L))

    move ((y,x),(E,m)) = case grid ! (y,x+1) of '-'  -> ((y,x+1),(E,m))
                                                '/'  -> ((y,x+1),(N,m))
                                                '\\' -> ((y,x+1),(S,m))
                                                '+'  -> case m of L -> ((y,x+1),(N,G))
                                                                  G -> ((y,x+1),(E,R))
                                                                  R -> ((y,x+1),(S,L))
                                                c -> error $ show c

    move ((y,x),(S,m)) = case grid ! (y+1,x) of '|'  -> ((y+1,x),(S,m))
                                                '/'  -> ((y+1,x),(W,m))
                                                '\\' -> ((y+1,x),(E,m))
                                                '+'  -> case m of L -> ((y+1,x),(E,G))
                                                                  G -> ((y+1,x),(S,R))
                                                                  R -> ((y+1,x),(W,L))

    move ((y,x),(W,m)) = case grid ! (y,x-1) of '-'  -> ((y,x-1),(W,m))
                                                '/'  -> ((y,x-1),(S,m))
                                                '\\' -> ((y,x-1),(N,m))
                                                '+'  -> case m of L -> ((y,x-1),(S,G))
                                                                  G -> ((y,x-1),(W,R))
                                                                  R -> ((y,x-1),(N,L))

pogo grid carts = either id (pogo grid) $ tick grid carts

pogoB grid carts =
    case tick grid carts of
        Right [cart] -> fst cart
        Right carts' -> pogoB grid carts'

main = do
    (grid, carts) <- read_tracks . lines <$> getContents
    mapM_ print carts
    putStrLn []
    -- let Right carts' = tick grid carts
    -- mapM_ print $ carts'

    putStrLn []
    print $ pogoB grid carts

