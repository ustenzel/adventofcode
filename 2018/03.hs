import Control.Monad
import Data.Array.IO
import Data.List ( tails )
import Data.Maybe

import qualified Data.Attoparsec.ByteString.Char8 as A
import qualified Data.ByteString.Char8 as B

data Rect = Rect { rid, x1, y1, x2, y2 :: !Int } deriving Show

read_one :: A.Parser Rect
read_one = rect <$ A.char '#' <*> A.decimal <* A.skipSpace
                <* A.char '@' <*  A.skipSpace <*> A.decimal
                <* A.char ',' <*> A.decimal
                <* A.char ':' <* A.skipSpace <*> A.decimal
                <* A.char 'x' <*> A.decimal
  where
    rect i x y w h = Rect i x y (w+x) (h+y)

intersection :: Rect -> Rect -> Maybe Rect
intersection a b = do (x,u) <- overlap (x1 a) (x2 a) (x1 b) (x2 b)
                      (y,v) <- overlap (y1 a) (y2 a) (y1 b) (y2 b)
                      return $ Rect (rid a) x y u v

overlap x u y v | y < u && x < v = Just (max x y, min u v)
                | otherwise      = Nothing

union1 :: Rect -> Rect -> Maybe [Rect]
union1 a b | x1 a > x1 b   =  union1 b a
           | x2 a <= x1 b  =  Nothing
           | y2 a <= y1 b  =  Nothing
           | y2 b <= y1 a  =  Nothing
           | otherwise = Just $ rect (x1 a) (y1 a) (x1 b) (y2 a)
                              $ rect (x1 b) (y1 a `min` y1 b) (x2 a) (y2 a `max` y2 b)
                              $ rect (x2 a) (y1 b) (x2 b) (y2 b) []
  where
    rect x y u v | x == u || y == v = id
                 | otherwise        = (Rect (rid a) x y u v :)

unionN :: Rect -> [Rect] -> [Rect]
unionN x [    ] = [x]
unionN x (y:ys) =
    case union1 x y of
        Nothing -> y : unionN x ys
        Just xs -> union2 xs ys

union2 :: [Rect] -> [Rect] -> [Rect]
union2 [    ] ys = ys
union2 (x:xs) ys = union2 xs (unionN x ys)

unions :: [Rect] -> [Rect]
unions = foldr unionN []

area :: Rect -> Int
area r = (x2 r - x1 r) * (y2 r - y1 r)

mai1 = do rs <- mapM (either error return . A.parseOnly read_one) . B.lines =<< B.getContents
          print $ length rs -- mapM_ print rs
          putStrLn []
          let os = catMaybes [ intersection a b | a:as <- tails rs, b <- as ]
          print $ length os -- mapM_ print os
          putStrLn []
          let ar = unions os
          print $ length ar -- mapM_ print $ ar
          print $ sum $ map area ar

fill :: IOUArray (Int,Int) Int -> Rect -> IO ()
fill grid r = forM_ [x1 r .. x2 r -1] $ \x ->
              forM_ [y1 r .. y2 r -1] $ \y ->
                  readArray grid (x,y) >>= writeArray grid (x,y) . succ

mai2 = do rs <- mapM (either error return . A.parseOnly read_one) . B.lines =<< B.getContents
          grid <- newArray ((0,0),(1000,1000)) 0
          mapM_ (fill grid) rs
          print . length . filter (>1) =<< getElems grid

main = do rs <- mapM (either error return . A.parseOnly read_one) . B.lines =<< B.getContents
          -- mapM_ print rs
          putStrLn [ ]
          print [ a | (a,bs) <- splits [] rs, all (not . intersects a) bs ]

intersects a b = x2 a > x1 b && x2 b > x1 a && y2 a > y1 b && y2 b > y1 a

splits  _ [] = []
splits bs (a:as) = (a,as++bs) : splits (a:bs) as
