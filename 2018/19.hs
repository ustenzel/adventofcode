import Data.Bits
import qualified Data.Map.Strict as M
import qualified Data.Vector as V

data Regs = Regs !Int !Int !Int !Int !Int !Int deriving (Eq, Show)

get :: Regs -> Int -> Int
get (Regs a b c d e f) 0 = a
get (Regs a b c d e f) 1 = b
get (Regs a b c d e f) 2 = c
get (Regs a b c d e f) 3 = d
get (Regs a b c d e f) 4 = e
get (Regs a b c d e f) 5 = f

set :: Regs -> Int -> Int -> Regs
set (Regs a b c d e f) 0 x = Regs x b c d e f
set (Regs a b c d e f) 1 x = Regs a x c d e f
set (Regs a b c d e f) 2 x = Regs a b x d e f
set (Regs a b c d e f) 3 x = Regs a b c x e f
set (Regs a b c d e f) 4 x = Regs a b c d x f
set (Regs a b c d e f) 5 x = Regs a b c d e x

op :: String -> Int -> Int -> Int -> Regs -> Regs
op "addr" a b c rs  =  set rs c $ (get rs a) + (get rs b)                    -- addr
op "addi" a b c rs  =  set rs c $ (get rs a) + b                             -- addi
op "mulr" a b c rs  =  set rs c $ (get rs a) * (get rs b)                    -- mulr
op "muli" a b c rs  =  set rs c $ (get rs a) * b                             -- muli
op "banr" a b c rs  =  set rs c $ (get rs a) .&. (get rs b)                  -- banr
op "bani" a b c rs  =  set rs c $ (get rs a) .&. b                           -- bani
op "borr" a b c rs  =  set rs c $ (get rs a) .|. (get rs b)                  -- borr
op "bori" a b c rs  =  set rs c $ (get rs a) .|. b                           -- bori
op "setr" a b c rs  =  set rs c $  get rs a                                  -- setr
op "seti" a b c rs  =  set rs c a                                            -- seti
op "gtir" a b c rs  =  set rs c $ if a > get rs b then 1 else 0              -- gtir
op "gtri" a b c rs  =  set rs c $ if get rs a > b then 1 else 0              -- gtri
op "gtrr" a b c rs  =  set rs c $ if get rs a > get rs b then 1 else 0       -- gtrr
op "eqir" a b c rs  =  set rs c $ if a == get rs b then 1 else 0             -- eqir
op "eqri" a b c rs  =  set rs c $ if get rs a == b then 1 else 0             -- eqri
op "eqrr" a b c rs  =  set rs c $ if get rs a == get rs b then 1 else 0      -- eqrr


run :: Int -> V.Vector (Regs -> Regs) -> Regs -> Regs
run ipreg prog rs =
    case prog V.!? get rs ipreg of
        Just insn -> let rs' = insn rs in run ipreg prog (set rs' ipreg $ get rs' ipreg + 1)
        Nothing   -> rs

read_hdr ["#ip",x] = read x
read_insn [opcode, a, b, c] = op opcode (read a) (read b) (read c)

main = do
    raw_hdr : raw_prg <- map words . lines <$> getContents
    let ipreg = read_hdr raw_hdr
        prog  = V.fromList $ map read_insn raw_prg

    -- Part A
    print $ run ipreg prog (Regs 0 0 0 0 0 0)
    -- Part B: won't terminate in this lifetime
    -- print $ run ipreg prog (Regs 1 0 0 0 0 0)


-- For part B: reverse engineer program, it "computes the sum of all
-- numbers that divide C" after initializing C to something big.  It
-- does 'trial multiplication', so it's slow.  This is quick enough:

partB c = sum [ b | b <- [1..c], c `mod` b == 0 ]
mainB = partB  10551376
