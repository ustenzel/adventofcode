{-# LANGUAGE BangPatterns #-}
import BasePrelude
import Prelude ()

import qualified Data.Set as S

-- This is what the body of the outer loop does:
hasho f = hashi (f .|. 65536) 3935295

hashi e f
    | e < 256 = f2
    | otherwise = hashi (shiftR e 8) f2
  where
    f1 = (f + e .&. 255) .&. 16777215
    f2 = (f1 * 65899) .&. 16777215

partA = hasho (0 :: Word)

-- Find first repeated value; answer is the one /before/ that.
partB seen a (b:bs)
    | b `S.member` seen = a
    | otherwise = partB (S.insert b seen) b bs

main = do print partA
          print $ partB S.empty 0 $ iterate hasho (0::Word)


{- Notes from reverse engineering the assembly code:

#ip 1
     5	F = 0                       seti 0 2 5
        {
     6      E = F | 65536               bori 5 65536 4
     7      F = 3935295                 seti 3935295 1 5
            while(1) {
     8
     9          F = F + (E & 255)           addr 5 2 5
    10          F = F & 16777215            bani 5 16777215 5
    11          F = F * 65899               muli 5 65899 5
    12          F = F & 16777215            bani 5 16777215 5
    13          if( 256 > E ) goto 28       gtir 256 4 2
    14                                      addr 2 1 1
    15                                      addi 1 1 1
    16                                      seti 27 1 1
    17                                      seti 0 5 2

    18                                          addi 2 1 3
    19                                          muli 3 256 3
    20                                          gtrr 3 4 3
    21                                                      addr 3 1 1
    22                                          addi 1 1 1
    23                                          seti 25 0 1
    24                                          addi 2 1 2
    25                                      seti 17 7 1
    26          E = E >> 8                  setr 2 2 4
    27      }                           seti 7 6 1
    28  } while( F/=A ) ;               eqrr 5 0 2
    29                                  addr 2 1 1
    30	                            seti 5 4 1
-}
