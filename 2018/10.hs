{-# LANGUAGE BangPatterns #-}
import Prelude ()
import BasePrelude

import Data.Array.Unboxed

data P = P !Int64 !Int64 !Int64 !Int64

read1 s = case map read $ words $ filter is_good $ map uncomma s of
    [x,y,u,v] -> P x y u v
    _ -> error (show s)
  where
    is_good c = isDigit c || isSpace c || c == '-'
    uncomma c = if c == ',' then ' ' else c

step (P x y u v) = P (x+u) (y+v) u v

bbox ps = ( ( minimum [ x | P x _ _ _ <- ps ]
            , minimum [ y | P _ y _ _ <- ps ] )
          , ( maximum [ x | P x _ _ _ <- ps ]
            , maximum [ y | P _ y _ _ <- ps ] ) )

area ((l,t),(r,b)) = fromIntegral (r-l) * fromIntegral (b-t)

at_min_area !i (a:b:as) | area (fst a) > area (fst b) = at_min_area (i+1) (b:as)
at_min_area !i (a:_) = (i,a)

main = do
    ps <- map read1 . lines <$> getContents

    let anim = iterate (map step) ps
    let (tm, (bbx@((l,t),(r,b)), image)) = at_min_area 0 $ map (bbox &&& id) anim

    print tm
    print bbx

    let disp :: UArray (Int64,Int64) Char
        disp = accumArray (const id) ' ' bbx [ ((x,y),'#') | P x y _ _ <- image ]
    forM_ [t..b] $ \x -> do
        forM_ [l..r] $ \y -> putChar (disp ! (y,x))
        putStrLn []

