{-# LANGUAGE BangPatterns, FlexibleContexts #-}
import BasePrelude
import Data.Array.IO
import Prelude ()

import qualified Data.Set as S

read1 :: [String] -> [V2]
read1 [ "x", x, "y", y0, y1 ] = [ V2 (read x) y | y <- [read y0 .. read y1] ]
read1 [ "y", y, "x", x0, x1 ] = [ V2 x (read y) | x <- [read x0 .. read x1] ]

clean c = if isAlphaNum c then c else ' '

inj :: Tile -> Word8
inj = fromIntegral . fromEnum

out :: Word8 -> Tile
out = toEnum . fromIntegral

data V2 = V2 !Int !Int deriving (Show, Eq, Ord, Ix)

data Tile = Sand | Clay | Wet | Flooded deriving (Enum, Eq, Show)

type Soil = IOUArray V2 Word8

flood :: V2 -> Soil -> IO ()
flood (V2 !x !y) !soil = do
    bnds <- getBounds soil
    when (bnds `inRange` V2 x (y+1)) $ do
        down <- readArray soil (V2 x (y+1))
        case out down of
            Wet     -> return ()            -- been here, nothing to do   ???
            Sand    -> make_wet soil (V2 x (y+1))  >> flood (V2 x (y+1)) soil
            Clay    -> spread soil (V2 x y)
            Flooded -> spread soil (V2 x y)

make_wet soil p = do
    x <- readArray soil p
    case out x of
        Sand -> writeArray soil p (inj Wet)
        Wet  -> return ()
        _ -> error "can't wet!"

make_flooded soil p = do
    x <- readArray soil p
    case out x of
        Sand -> writeArray soil p (inj Flooded)
        Wet  -> writeArray soil p (inj Flooded)
        Flooded -> return ()
        _ -> error "can't flood!"


spread :: Soil -> V2 -> IO ()
spread !soil (V2 !x !y) = do
    lft <- look (-1) soil (V2 x y)
    rgt <- look   1  soil (V2 x y)
    case (lft, rgt) of
        -- two drains
        (Left xl, Left xr) -> do sequence_ [ make_wet soil (V2 x' y)  | x' <- [xl..xr] ]
                                 flood (V2 xl y) soil
                                 flood (V2 xr y) soil
        -- left drain, right block
        (Left xl, Right xr) -> do sequence_ [ make_wet soil (V2 x' y)  | x' <- [xl..xr] ]
                                  flood (V2 xl y) soil
        -- left block, right drain
        (Right xl, Left xr) -> do sequence_ [ make_wet soil (V2 x' y)  | x' <- [xl..xr] ]
                                  flood (V2 xr y) soil
        -- both blocked
        (Right xl, Right xr) -> do sequence_ [ make_flooded soil (V2 x' y)  | x' <- [xl..xr] ]
                                   spread soil (V2 x (y-1))

-- Left is a drain, Right is a wall.  Either way, value is last
-- coordinate that gets wet.
look :: Int -> Soil -> V2 -> IO (Either Int Int)
look !d !soil (V2 !x !y) = do
    next <- readArray soil (V2 (x+d) y)
    down <- readArray soil (V2 (x+d) (y+1))
    case (out next, out down) of
        (Clay,    _) -> return $ Right x
        (Flooded, _) -> return $ Right x
        (_,    Clay) -> look d soil (V2 (x+d) y)
        (_, Flooded) -> look d soil (V2 (x+d) y)
        (_,       _) -> return $ Left (x+d)

isWet Sand    = False
isWet Clay    = False
isWet Flooded = True
isWet Wet     = True

isFlooded Flooded = True
isFlooded _ = False

main :: IO ()
main = do raw <- concatMap (read1 . words . map clean) . lines <$> getContents
          let !miny = minimum [ y | V2 x y <- raw ]
              !maxy = maximum [ y | V2 x y <- raw ]
              !minx = minimum [ x | V2 x y <- raw ] - 1
              !maxx = maximum [ x | V2 x y <- raw ] + 1
          print (minx,maxx,maxy)

          soil <- newArray (V2 minx 0, V2 maxx maxy) (inj Sand)
          sequence_ [ writeArray soil p (inj Clay) | p <- raw ]
          -- dump soil
          flood (V2 500 0) soil     -- spring is at (500,0)
          -- dump soil
          foldM (\ !a p -> bool a (succ a) . isWet . out <$> readArray soil p) 0
                (range (V2 minx miny, V2 maxx maxy))
            >>= print

          foldM (\ !a p -> bool a (succ a) . isFlooded . out <$> readArray soil p) 0
                (range (V2 minx miny, V2 maxx maxy))
            >>= print


dump :: Soil -> IO ()
dump s = do
    (V2 x1 y1, V2 x2 y2) <- getBounds s
    forM_ [y1..y2] $ \y -> do
        forM_ [x1..x2] $ \x -> do
            putChar . toChar x y . out =<< readArray s (V2 x y)
        putStrLn []
    putStrLn []
  where
    toChar 500 0 _ = '+'
    toChar _ _ Sand = '.'
    toChar _ _ Clay = '#'
    toChar _ _ Wet  = '|'
    toChar _ _ Flooded = '~'

