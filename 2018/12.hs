{-# LANGUAGE LambdaCase #-}
import Text.Printf
import qualified Data.Map.Strict as M

step tab (pots,off) = (drop l pots'', off+2-l)
  where
    pots' = map (\k -> M.findWithDefault '.' k tab) $ roll 5 $ "...." ++ pots ++ "...."
    pots'' = reverse $ dropWhile (=='.') $ reverse pots'
    l = length $ takeWhile (=='.') pots''

    roll n s = case splitAt n s of (l,r) | length l == n -> l : roll n (tail s) ; _ -> []

cksum (pots,off) = sum $ zipWith (\c i -> if c == '#' then i else 0) pots [-off ..]

main = do
    header:_:rest <- map words . lines <$> getContents
    let ini = header !! 2
        tab = M.fromList $ concatMap (\case [a,b,[c]] -> [(a,c)] ; _ -> []) $ rest

    mapM_ (\(g,(s,o)) -> printf "%d: %d; %s (%d)\n" g (cksum (s,o)) s o) $
          zip [0::Int ..] $ iterate (step tab) (ini,0)
