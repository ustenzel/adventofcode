{-# LANGUAGE BangPatterns #-}
import BasePrelude
import Prelude ()
import Intcode hiding (S)

import qualified Data.Set as S

splits :: [MWord] -> [[MWord]]
splits [] = []
splits ws = case break (== 10) ws of
    (l,r) -> l : splits (drop 1 r)


shift_l, shift_r, shift_u, shift_d :: [[Bool]] -> [[Bool]]
shift_l = map (drop 1)
shift_r = map (False :)
shift_u = drop 1
shift_d = (:) (repeat False)

data Dir = N | E | S | W deriving Show

main :: IO ()
main = do
    prg <- readProg <$> readFile "i17.txt"

    let grid :: [[MWord]]
        grid = splits $ pogo [] (vm prg)

    -- partA
    let gridA :: [[Bool]]
        gridA = map (map (\c -> c /= 46)) grid

    print . sum . zipWith (\y -> sum . zipWith (\x -> bool (0::Int) (x*y)) [0..]) [0..] $
        zipWith5 (zipWith5 (\a b c d e -> a && b && c && d && e))
                 gridA (shift_l gridA) (shift_r gridA) (shift_u gridA) (shift_d gridA)

    -- partB
    let [(xr,yr,dr)] = [ (x,y,d)
                       | (y,l) <- zip [0..] grid
                       , (x,c) <- zip [0..] l
                       , d <- case chr (fromIntegral c) of '^' -> [N] ; 'v' -> [S] ; '<' -> [W] ; '>' -> [E] ; _ -> [] ]

    -- this prints a program which is too long
    partB (S.fromList [ (x,y) | (y,l) <- zip [0..] grid, (x,35) <- zip [0..] l ])
          xr yr dr (vm $ 2 : tail prg)

    -- partB interactive
    partBtest (vm $ 2 : tail prg)

partB :: S.Set (Int,Int) -> Int -> Int -> Dir -> P Identity () -> IO ()
partB grid x0 y0 d0 p = do
    print (x0,y0,d0)
    print grid
    putStrLn $ walk (x0,y0) d0
  where
    walk p d | step d p `S.member` grid = walk' 0 p d
             | step (leftof d) p `S.member` grid = "L," ++ walk' 0 p (leftof d)
             | step (rightof d) p `S.member` grid = "R," ++ walk' 0 p (rightof d)
             | otherwise = ""

    walk' n p d | step d p `S.member` grid = walk' (n+1) (step d p) d
                | otherwise = shows n "," ++ walk p d

leftof N = W
leftof E = N
leftof S = E
leftof W = S

rightof N = E
rightof E = S
rightof S = W
rightof W = N

step N (x,y) = (x,y-1)
step E (x,y) = (x+1,y)
step S (x,y) = (x,y+1)
step W (x,y) = (x-1,y)

partBtest vm = do
    let out = pogo (map (fromIntegral . ord) inp) vm
    -- putStrLn $ {-nice $-} map (chr . fromIntegral) $ init out
    print $ last out
  where
    -- too long
    -- "L,6,R,12,R,8,R,8,R,12,L,12,R,8,R,12,L,12,L,6,R,12,R,8,R,12,L,12,L,4,L,4,L,6,R,12,R,8,R,12,L,12,L,4,L,4,L,6,R,12,R,8,R,12,L,12,L,4,L,4,R,8,R,12,L,12"
    inp = unlines [ "A,B,B,A,C,A,C,A,C,B"
                  , "L,6,R,12,R,8"
                  , "R,8,R,12,L,12"
                  , "R,12,L,12,L,4,L,4"
                  , "n" ]

    nice :: String -> String
    nice ('\n':'\n':s) = "\n\27[1;1H" ++ nice s
    nice (c:s) = c : nice s
    nice [] = []
