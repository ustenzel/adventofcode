main = do
    ms <- map read . lines <$> readFile "input1.txt"
    print $ sum $ map f ms
    print $ sum $ map f2 ms

f x = div x 3 - 2

f2 = sum . takeWhile (> 0) . iterate f . f
