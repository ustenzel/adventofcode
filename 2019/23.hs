{-# LANGUAGE BangPatterns #-}
import Intcode
import BasePrelude
import Prelude ()

import qualified Data.Set as S
import qualified Data.Sequence as Z

main = do
    prg <- readProg <$> readFile "i23.txt"

    let xs = run (-1,-1) 0 (Z.fromList [ C (vm prg) [i] [] | i <- [0..49] ])

    -- part A
    print $ foldr (either const (const id)) undefined xs

    -- part B
    let lp s (Left  x:xs)                  = lp s xs
        lp s (Right x:xs) | x `S.member` s = x
                          | otherwise      = lp (S.insert x s) xs
    print $ lp S.empty xs

data C m = C { c_prg :: P m ()
             , c_que :: [MWord]
             , c_rqu :: [MWord] }

run :: (MWord,MWord) -> Int -> Z.Seq (C Identity) -> [Either MWord MWord]
run nat i cs = case cs Z.!? i of
    Nothing | all idle cs  -> Right (snd nat) : (run nat 0 $ Z.adjust (\(C p [] []) -> C p [fst nat, snd nat] []) 0 cs)
            | otherwise    -> run nat 0 cs
    Just (C (M   m) fq rq) -> run nat i (Z.update i (C (runIdentity m) fq rq) cs)
    Just (C (I   f) fq rq) ->
        case fq of
            (w:ws) -> kont (C (f w) ws rq)
            [    ] -> case reverse rq of
                (w:ws) -> kont (C (f w) ws [])
                [    ] -> kont (C (f (-1)) [] [])
      where
        kont c = run nat (i+1) (Z.update i c cs)

    Just (C (O addr p1) fq rq) -> send1 p1
      where
        send1   (M    m) = send1 (runIdentity m)
        send1   (O x p2) = send2 x p2
        send2 x (M    m) = send2 x (runIdentity m)
        send2 x (O y p3)
            | addr == 255 = Left y : (run (x,y) (i+1) $ Z.update i (C p3 fq rq) cs)
            | otherwise   = run nat (i+1) $
                            Z.adjust (\(C  _ fq' rq') -> C p3 fq' rq') i $
                            Z.adjust (\(C p' fq' rq') -> C p' fq' (y:x:rq')) (fromIntegral addr) cs

    -- Just (S   x) -- shouldn't happen
  where
    idle (C (I _) [] []) = True
    idle _               = False
