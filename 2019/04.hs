import Data.List

low = "171309"
high = "643603"

n_inc 0 _ = [[]]
n_inc n m = [ i:is | i <- [m..'9'], is <- n_inc (n-1) i ]

pwds = [ pwd | pwd <- n_inc 6 '0', low <= pwd, pwd <= high ]

has_double (a:b:_) | a == b = True
has_double (a:as) = has_double as
has_double [] = False

has_exactly_double = any (==2) . map length . group

main = do print $ length $ filter has_double pwds
          print $ length $ filter has_exactly_double pwds
