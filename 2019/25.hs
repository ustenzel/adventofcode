import Intcode
import Data.Char

main = do
    prg <- readProg <$> readFile "i25.txt"
    print $ length prg

    run1 statics (vm prg)


run1 (c:cs) (I   f) = run1 cs (f (fromIntegral (ord c)))
run1 [    ] (I   f) = run (I f)
run1    cs  (O o k) = putChar (chr $ fromIntegral o) >> run1 cs k
run1    cs  (M   m) = m >>= run1 cs
run1     _  (S   _) = pure ()

run (I   f) = getChar >>= run . f . fromIntegral . ord
run (O o k) = putChar (chr $ fromIntegral o) >> run k
run (M   m) = m >>= run
run (S   _) = pure ()


statics = unlines $
    [ "north"
    , "north"
    , "north"
    , "take mutex"
    , "south"
    , "south"
    , "east"
    , "north"
    , "take loom"
    , "south"
    , "west"
    , "south"
    , "east"
    , "take semiconductor"
    , "east"
    , "take ornament"
    , "west"
    , "west"
    , "west"
    , "west"
    , "south"
    , "east"
    , "take asterisk"
    , "north"
    , "take wreath"
    , "south"
    , "west"
    , "north"
    , "take sand"
    , "north"
    , "take dark matter"
    , "east"
    , "inv"

    , "drop ornament"                  -- too heavy
    -- , "drop loom"                   -- needed
    , "drop dark matter"               -- too heavy
    -- , "drop wreath"                 -- needed
    , "drop asterisk"                  -- too heavy

    -- , "drop mutex"           -- in
    , "drop semiconductor"      -- out
    -- , "drop sand" ]          -- in
    , "east" ]


