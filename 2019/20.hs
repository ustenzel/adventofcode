{-# LANGUAGE BangPatterns #-}
import Data.List
import Data.Ord
import qualified Data.Map.Strict as M
import qualified Data.Set as S

ex1 = [ "         A           "
      , "         A           "
      , "  #######.#########  "
      , "  #######.........#  "
      , "  #######.#######.#  "
      , "  #######.#######.#  "
      , "  #######.#######.#  "
      , "  #####  B    ###.#  "
      , "BC...##  C    ###.#  "
      , "  ##.##       ###.#  "
      , "  ##...DE  F  ###.#  "
      , "  #####    G  ###.#  "
      , "  #########.#####.#  "
      , "DE..#######...###.#  "
      , "  #.#########.###.#  "
      , "FG..#########.....#  "
      , "  ###########.#####  "
      , "             Z       "
      , "             Z       " ]

ex2 = [ "             Z L X W       C                 "
      , "             Z P Q B       K                 "
      , "  ###########.#.#.#.#######.###############  "
      , "  #...#.......#.#.......#.#.......#.#.#...#  "
      , "  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  "
      , "  #.#...#.#.#...#.#.#...#...#...#.#.......#  "
      , "  #.###.#######.###.###.#.###.###.#.#######  "
      , "  #...#.......#.#...#...#.............#...#  "
      , "  #.#########.#######.#.#######.#######.###  "
      , "  #...#.#    F       R I       Z    #.#.#.#  "
      , "  #.###.#    D       E C       H    #.#.#.#  "
      , "  #.#...#                           #...#.#  "
      , "  #.###.#                           #.###.#  "
      , "  #.#....OA                       WB..#.#..ZH"
      , "  #.###.#                           #.#.#.#  "
      , "CJ......#                           #.....#  "
      , "  #######                           #######  "
      , "  #.#....CK                         #......IC"
      , "  #.###.#                           #.###.#  "
      , "  #.....#                           #...#.#  "
      , "  ###.###                           #.#.#.#  "
      , "XF....#.#                         RF..#.#.#  "
      , "  #####.#                           #######  "
      , "  #......CJ                       NM..#...#  "
      , "  ###.#.#                           #.###.#  "
      , "RE....#.#                           #......RF"
      , "  ###.###        X   X       L      #.#.#.#  "
      , "  #.....#        F   Q       P      #.#.#.#  "
      , "  ###.###########.###.#######.#########.###  "
      , "  #.....#...#.....#.......#...#.....#.#...#  "
      , "  #####.#.###.#######.#######.###.###.#.#.#  "
      , "  #.......#.......#.#.#.#.#...#...#...#.#.#  "
      , "  #####.###.#####.#.#.#.#.###.###.#.###.###  "
      , "  #.......#.....#.#...#...............#...#  "
      , "  #############.#.#.###.###################  "
      , "               A O F   N                     "
      , "               A A D   M                     " ]

main = do
    raw <- lines <$> readFile "i20.txt"
    -- let raw = ex2

    let !xdim = maximum $ map length raw
        !ydim = length raw

    let portals = sort $ filter (\(a,b,_,_) -> 'A' <= a && a <= 'Z' && 'A' <= b && b <= 'Z') $
                      [ (a,b,x+2,y) | (l,!y) <- zip raw [0..]
                                    , (a:b:'.':_,!x) <- zip (tails l) [0..] ] ++
                      [ (a,b,x  ,y) | (l,!y) <- zip raw [0..]
                                    , ('.':a:b:_,!x) <- zip (tails l) [0..] ] ++
                      [ (a,b,x  ,y) | (l,!x) <- zip (transpose raw) [0..]
                                    , ('.':a:b:_,!y) <- zip (tails l) [0..] ] ++
                      [ (a,b,x,y+2) | (l,!x) <- zip (transpose raw) [0..]
                                    , (a:b:'.':_,!y) <- zip (tails l) [0..] ]

    let ('A','A',x_beg,y_beg) = head portals
        ('Z','Z',x_end,y_end) = last portals

    let maze_map = M.fromListWith (++) $
                    ( map fst $
                      filter (\(_,(a,b)) -> a == '.' && b == '.') $
                      concat $
                      concat $
                      zipWith6
                        (\ !y -> zipWith6
                            (\ !x c n e s w ->
                                [ (((x,y), [(x+1,y)]), (c,e))
                                , (((x,y), [(x,y+1)]), (c,s))
                                , (((x,y), [(x-1,y)]), (c,w))
                                , (((x,y), [(x,y-1)]), (c,n)) ]
                            )
                            [0::Int ..]
                        )
                        [0::Int ..]
                        raw
                        ([]:raw)
                        (map (drop 1) raw)
                        (drop 1 raw)
                        (map (' ':) raw) )

    let plain_portal_map = M.fromList $ concat [ [((x,y),(u,v)), ((u,v),(x,y))]
                                               | ( (a,b,x,y), (c,d,u,v) ) <- zip portals (tail portals)
                                               , a == c && b == d ]

    -- print $ portals
    -- putStrLn []
    -- print $ maze_map

    -- part A
    let !mazeA = M.unionWith (++) maze_map (M.map pure plain_portal_map)
    print $ bfs (flip (M.findWithDefault []) mazeA) (x_end, y_end) (x_beg, y_beg)


    -- part B
    let rec_portal_map = flip M.mapWithKey plain_portal_map $
                            \(x,y) (u,v) -> (u,v, if u == 2 || v == 2 || u == xdim - 3 || v == ydim - 3
                                                  then 1 else -1 :: Int)
    -- print (xdim,ydim)
    -- mapM_ print $ M.toList rec_portal_map

    let nbrsB :: (Int,Int,Int) -> [(Int,Int,Int)]
        nbrsB (x,y,lv) = (case M.lookup (x,y) rec_portal_map of
                            Just (u,v,d) | lv+d >= 0 -> (:) (u,v,lv+d)
                            otherwise                -> id)
                         $ map (\(u,v) -> (u,v,lv)) (M.findWithDefault [] (x,y) maze_map)

    print $ bfs nbrsB (x_end, y_end, 0) (x_beg, y_beg, 0)


bfs :: Ord a => (a -> [a]) -> a -> a -> ([a],Int)
bfs neighbors goal ini = go 0 M.empty $ M.singleton ini []
  where
    go dist closed open
        = case M.lookup goal open of
            Just trace          -> (reverse trace, dist)
            _ | M.null open     ->  error "WTF?"
              | otherwise       ->  go (dist+1) closed' (M.difference open' closed')
          where
            closed' = M.union closed open
            open'   = M.fromList [ (spot', spot' : trace)
                                 | (spot, trace) <- M.toList open
                                 , spot' <- neighbors spot ]

