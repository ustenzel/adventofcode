import Intcode

main :: IO ()
main = do
    z <- readProg <$> readFile "input5.txt"
    print $ pogo [1] (vm z) -- part A
    print $ pogo [5] (vm z) -- part B
