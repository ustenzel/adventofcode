{-# LANGUAGE BangPatterns #-}
import Data.List
import Data.Char
import qualified Data.Vector.Unboxed as U

phase :: U.Vector Int -> U.Vector Int
phase ds = U.generate (U.length ds) ((`mod` 10) . abs . dig . (+1))
  where
    dig !n = go2 0 (n-1) 0
      where
        go2 !a !i !j | i+j >= U.length ds = a
                     | j == n             = go3 a (i+n+n) 0
                     | otherwise          = go2 (a + ds `U.unsafeIndex` (i+j)) i (j+1)

        go3 !a !i !j | i+j >= U.length ds = a
                     | j == n             = go2 a (i+n+n) 0
                     | otherwise          = go3 (a - ds `U.unsafeIndex` (i+j)) i (j+1)

main = do
    raw <- map digitToInt . filter isDigit <$> readFile "i16.txt"
    -- part A: easy and quick
    -- putStrLn . map intToDigit . U.toList . U.take 8 . head . drop 100 $ iterate phase $ U.fromList raw

    -- part B would take forever
    -- print . map intToDigit . U.toList . U.take 8 . head . iterate phase . U.fromList . concat $ replicate 10000 raw

    -- part B:  as the index is bigger than the length of the input
    -- signal, the pattern reduces to a sum over a tail part of the signal:
    -- y_i = sum_{j:=i}^l x_j

    -- let's just compute the tail part.  Also do it in reverse, because
    -- then it's just partial sums.
    let off = foldl' (\a d -> 10*a + d) 0 $ take 7 raw
    let the_tail = U.fromListN (10000 * length raw - off) $ cycle (reverse raw)
    putStrLn . map intToDigit . U.toList . U.take 8 . U.reverse . head . drop 100 $ iterate partial_sums the_tail


partial_sums = U.map ((`mod` 10) . abs) . U.postscanl' (+) 0
