{-# LANGUAGE DeriveFunctor, DeriveFoldable #-}

import Data.Functor.Identity
import Intcode
import qualified Data.HashMap.Strict as M

main = do
    prg <- readProg <$> readFile "input11.txt"

    let go (T x y c ts) = do putStr $ "\27[" ++ show (20-y) ++ ";" ++ show (x+40) ++ "H" ++
                                      if c == 0 then " " else "\27[7m \27[0m"
                             go ts
        go (E r)        = pure r

    putStr "\27[1;1H\27[J"
    picA <- go $ paint M.empty 0 0 0 1 (vm prg)
    print $ M.size picA

    -- part A
    -- print $ M.size $ runIdentity $ paint M.empty 0 0 0 1 (vm prg)

    -- part B
    -- let pic = runIdentity $ paint (M.singleton (0,0) 1) 0 0 0 1 (vm prg)
    -- printPic pic

    {- putStr "\27[1;1H\27[J"

    pic <- go $ paint (M.singleton (0,0) 1) 0 0 0 1 (vm prg)
    let ym = minimum $ map snd $ M.keys pic
    putStr $ "\27[" ++ show (4-ym) ++ ";1H" -}


printPic pic = do
    putStrLn $ unlines $ reverse
        [ concat [ if c == 0 then " " else "\27[7m \27[0m"
                 | x <- [xl..xh]
                 , let c = M.lookupDefault 0 (x,y) pic ]
        | y <- [yl..yh] ]
  where
    xl = minimum $ map fst $ M.keys pic
    xh = maximum $ map fst $ M.keys pic
    yl = minimum $ map snd $ M.keys pic
    yh = maximum $ map snd $ M.keys pic


data Trace r = T Int Int Int (Trace r) | E r deriving (Functor, Foldable)

paint :: M.HashMap (Int,Int) Int -> Int -> Int -> Int -> Int -> P Identity r -> Trace (M.HashMap (Int,Int) Int)
paint pic x y u v = p1
  where
    p1 (I f) = p2 (f . fromIntegral $ M.lookupDefault 0 (x,y) pic)
    p1 (M k) = p1 (runIdentity k)
    p1 (S _) = E pic

    p2 (M   k) = p2 (runIdentity k)
    p2 (O o k) = T x y (fromIntegral o) $ p3 (M.insert (x,y) (fromIntegral o) pic) k

    p3 p (M   k) = p3 p (runIdentity k)
    p3 p (O 0 k) = paint p (x-v) (y+u) (-v) u k -- turn left, move forward
    p3 p (O 1 k) = paint p (x+v) (y-u) v (-u) k -- turn right, move forward


