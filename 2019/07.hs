import Data.List
import Intcode

main :: IO ()
main = do
    prg <- readProg <$> readFile "input7.txt"

    print $ maximum $ map (pogo [0] . chain) $ permutations $ map (amp prg) [0..4]
    print $ maximum $ map (pogo [] . knot [0] . chain) $ permutations $ map (amp prg) [5..9]


amp :: Monad m => [MWord] -> MWord -> P m ()
amp prg phase = go $ vm prg
  where
    go (I   f) = f phase
    go (O o p) = O o (go p)
    go (S   x) = S x
    go (M   m) = M (fmap go m)

knot :: Monad m => [MWord] -> P m r -> P m r
knot (i:is) (S   r) = O i (knot is (S r))
knot [    ] (S   r) = S r
knot is     (O o p) = knot (is ++ [o]) p
knot (i:is) (I   f) = knot is (f i)
knot [    ] (I   _) = error "acausal feedback loop"
knot is     (M   m) = M (knot is <$> m)

