import Intcode
import Data.Bool

main = do
    prg <- readProg <$> readFile "i19.txt"

    let get x y  =  pogo [x,y] (vm prg) == [1]

    -- part A: take a look
    putStrLn $ unlines $
        [ [ bool '.' '#' $ get x y | x <- [0..49] ] | y <- [0..49] ]

    -- part A: answer
    print $ length [ () | x <- [0..49], y <- [0..49], get x y ]

    -- partB: looking for (X,Y) such that
    -- (X+100,Y) is 0,
    -- (X+99,Y) is 1,
    -- (X,Y+99) is 1,

    -- start somewhere safe: Y == 100.  Find the right edge.
    let x0 : _ = [ x | x <- [0..], get x 100 && not (get (x+1) 100) ]

    -- walk along the right edge
    let loop x y
            | get x y && get (x-99) (y+99)  =  (x-99) * 10000 + y       -- other corner is good, we found it
            | get (x+1) y                   =  loop (x+1) y             -- move right if that spot is still good
            | otherwise                     =  loop x (y+1)             -- else move down

    print $ loop x0 100

