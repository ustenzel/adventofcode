import BasePrelude
import Prelude ()

splits n s | null s = []
splits n s = case splitAt n s of (l,r) -> l : splits n r

-- 25 pixels wide and 6 pixels tall
-- layer has 150 chars
main = do
    layers <- splits 150 . filter isDigit <$> readFile "input8.txt"

    -- part A
    print $ uncurry (*) $
        (length . filter (=='1') &&& length . filter (=='2')) $
        minimumBy (comparing $ length . filter (=='0')) layers

    -- part B
    mapM_ putStrLn $ splits 25 $
        map (\ps -> case dropWhile (=='2') ps of '0':_ -> ' ' ; _ -> '*') $
        transpose layers
