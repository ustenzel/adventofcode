import BasePrelude
import Prelude ()
import qualified Data.Map as M

main = do
    inp <- map (fmap (drop 1) . break (== ')')) . lines <$> readFile "input6.txt"

    -- part A, counting direct and indirect orbits
    let m = M.fromList [ (y, 1 + M.findWithDefault 0 x m) | (x,y) <- inp ]
    print $ sum m

    -- part B
    -- map every object to the distance from YOU and from SAN, then
    -- merge and print minimum
    let Nothing ? b = b
        a ? Nothing = a
        Just a ? Just b = Just (min a b)

    let m_you = M.fromListWith (?) $ ("YOU",Just (-1)) : [ (x, (+1) <$> join (M.lookup y m_you)) | (x,y) <- inp ]
    let m_san = M.fromListWith (?) $ ("SAN",Just (-1)) : [ (x, (+1) <$> join (M.lookup y m_san)) | (x,y) <- inp ]

    print $ minimum $ catMaybes $ M.elems $ M.intersectionWith (liftA2 (+)) m_you m_san


