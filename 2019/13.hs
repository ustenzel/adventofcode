import Control.Monad
import Data.Foldable
import Intcode
import System.IO
import Control.Monad.Trans.Class
import Control.Monad.Trans.State.Strict
import qualified Data.HashMap.Strict as M

main = do
    prg <- readProg <$> readFile "i13.txt"

    let scr = mkScreen M.empty $ pogo [] (vm prg)

    let display   m = putStr m >> hFlush stdout
        nodisplay _ = pure ()

    -- play the game, because it looks cool
    g <- play display $ vm (2 : tail prg)

    -- part A: count blocks
    print $ foldl' (\n t -> n + fromEnum (t == 2)) 0 scr

    -- part B: final score
    print $ g_score g

mkScreen m (x:y:z:xs) = mkScreen (M.insert (x,y) z m) xs
mkScreen m [        ] = m

data G = G { p_ball   :: MWord
           , p_paddle :: MWord
           , g_score  :: MWord
           , obuf     :: [MWord] }
  deriving Show


play :: (String -> IO ()) -> P IO r -> IO G
play disp p = do
    clrscr
    g <- execStateT (go p) (G 0 0 0 [])
    gotoXY 0 22
    disp []
    pure g
  where
    go (I   f) = paddle >>= go . f
    go (O o k) = put1 o >> go k
    go (M   m) = lift m >>= go
    go (S   r) = pure r

    put1 w = do g <- get
                case obuf g of
                    (y:x:os) -> do put g { obuf = [] }
                                   output x y w
                    os       -> do put g { obuf = w : os }

    output (-1) 0 z = do lift $ gotoXY 0 0
                         lift $ clreol
                         lift $ disp (show z)
                         modify $ \g -> g { g_score = z }
    output   x  y z = do lift $ gotoXY x (y+1)
                         lift $ disp (display z)
                         when (z==3) $ modify $ \g -> g { p_paddle = x }
                         when (z==4) $ modify $ \g -> g { p_ball   = x }

    display 0 = " "
    display 1 = "\27[7m \27[0m"
    display 2 = "#"
    display 3 = "_"
    display 4 = "*"

    paddle = gets $ \g -> signum (p_ball g - p_paddle g)

    clrscr = disp "\27[1;1H\27[J"
    clreol = disp "\27[K"
    gotoXY x y = disp $ "\27[" ++ show (1+y) ++ ";" ++ show (1+x) ++ "H"

