#! /bin/sh

ghc --make -O2 d21.hs && ./d21 << EOF
-- okay, I cheated.  I couldn't come up with this
-- (D && !(A && B && (C || !H)))
NOT H T
OR C T
AND B T
AND A T
NOT T J
AND D J
RUN

EOF
exit 0
