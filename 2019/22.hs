import BasePrelude
import Prelude ()
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Unboxed.Mutable as W

partA ln cmds =
        foldl' app_shuf (U.generate ln id) . map words $ cmds

-- main = readFile "i22.txt" >>=
            -- print . U.elemIndex 2019 . partA 10007 . lines


app_shuf v ["deal","with","increment",n]  =
    U.create (do w <- W.new (U.length v)
                 forM_ [0 .. U.length v - 1] $ \i ->
                    W.write w ((i * read n) `mod` U.length v) (v U.! i)
                 pure w)

app_shuf v ["deal","into","new","stack"]  =  U.reverse v

app_shuf v ["cut",n]  =  case U.splitAt i v of (l,r) -> r U.++ l
  where
    i = if read n >= 0 then read n else U.length v + read n

ex1 = [ "deal with increment 7" ]
      -- , "deal into new stack"
      -- , "deal into new stack" ]
      --  Result: 0 3 6 9 2 5 8 1 4 7

ex2 = [ "deal with increment 7"
      , "deal with increment 9"
      , "cut -2" ]
      -- Result: 6 3 0 7 4 1 8 5 2 9

ex3 = [ "deal into new stack"
      , "cut -2"
      , "deal with increment 7"
      , "cut 8"
      , "cut -4"
      , "deal with increment 7"
      , "cut 3"
      , "deal with increment 9"
      , "deal with increment 3"
      , "cut -1" ]
      -- Result: 9 2 5 8 1 4 7 0 3 6


-- Affine map.  AM a b maps position p to a*p+b modulo the deck size.
data AM = AM !Integer !Integer deriving Show

-- Maps compose, given a modulus.  Composition is associative (exersize
-- for advanced students).
compose :: Integer -> AM -> AM -> AM
compose m (AM a b) (AM c d) = AM (mod (a*c) m) (mod (b*c+d) m)

from_shuf ["deal","with","increment",n]  =  AM (read n) 0
from_shuf ["deal","into","new","stack"]  =  AM (-1) (-1)
from_shuf ["cut",n]                      =  AM 1 (- read n)


main = do
    -- part A
    readFile "i22.txt" >>= print . (\(AM a b) -> mod (a * 2019 + b) 10007) . composeCmds 10007 . lines

    -- part B
    let ln = 119315717514047
    AM a b <- power ln 101741582076661 .
              composeCmds ln . lines <$>
              readFile "i22.txt"

    -- fuck, it's backwards.  must solve for p:
    -- 2020 == a * p + b   mod ln
    let (1,s,t) = extEuclid a ln
    -- now 1 == a*s + t*ln, or a*s == 1 mod ln

    -- (2020-b)*s == p  mod ln
    print $ mod (s * (2020 - b)) ln

composeCmds :: Integer -> [String] -> AM
composeCmds ln = foldl' (compose ln) (AM 1 0) . map (from_shuf . words)

power :: Integer -> Int64 -> AM -> AM
power ln n am | n == 1     =  am
              | odd n      =  compose ln am (power ln (div n 2) (compose ln am am))
              | otherwise  =  power ln (div n 2) (compose ln am am)



extEuclid a b = go a b 1 0 0 1
  where
    go  r  0  s  _  t  _ = (r,s,t)
    go rp rn sp sn tp tn = go rn (rp - q * rn) sn (sp - q * sn) tn (tp - q * tn)
      where
        q = rp `div` rn
