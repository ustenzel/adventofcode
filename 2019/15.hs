import Data.Bifunctor
import Data.Foldable
import Intcode
import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.State.Strict

import qualified Data.Map.Strict as M
import qualified Data.Set as S

main = do
    prg <- readProg <$> readFile "i15.txt"
    clrscr
    z <- scan (vm prg)
    gotoXY (0,60)
    case s_oxy z of
        Just (p,d) -> do
            print d     -- part A, only correct if the area does not have loops
            print $ flood (s_map z) (S.singleton p) 0       -- part B


clrscr = putStr "\27[1;1H\27[J"
clreol = putStr "\27[K"
gotoXY (x,y) = putStr $ "\27[" ++ show (1+y) ++ ";" ++ show (1+x) ++ "H"

data Z = Z { s_prg :: P IO ()
           , s_map :: M.Map (Int,Int) Int
           , s_pos :: (Int,Int)
           , s_oxy :: Maybe ((Int,Int),Int) }

scan :: P IO () -> IO Z
scan prg = execStateT (mapM_ (go 1) [1..4]) (Z prg (M.singleton (0,0) 1) (0,0) Nothing)
  where
    go z d = do
              m <- gets s_map
              p <- gets s_pos
              case M.lookup (move p d) m of
                Just  _ -> pure ()
                Nothing -> do rc <- fromIntegral <$> step d
                              lift $ gotoXY $ bimap (+30) (+40) (move p d)
                              lift $ putChar $ "#.@" !! rc
                              modify $ \s -> s { s_map = M.insert (move p d) rc m }
                              when (rc==2) $ modify $ \s -> s { s_oxy = Just (p,z) }
                              case rc of
                                0 -> pure ()
                                _ -> do modify $ \s -> s { s_pos = move p d }
                                        go (z+1) (leftof d)
                                        go (z+1) d
                                        go (z+1) (rightof d)
                                        step (leftof (leftof d))
                                        modify $ \s -> s { s_pos = p }
    leftof 1 = 3
    leftof 2 = 4
    leftof 3 = 2
    leftof 4 = 1

    rightof 1 = 4
    rightof 2 = 3
    rightof 3 = 1
    rightof 4 = 2

    move (x,y) 1 = (x,y-1)
    move (x,y) 2 = (x,y+1)
    move (x,y) 3 = (x-1,y)
    move (x,y) 4 = (x+1,y)

    step d = do p <- gets s_prg
                (o,p') <- lift $ loop d p
                modify $ \s -> s { s_prg = p' }
                pure o

    loop d (I f) = loop2 (f d)
    loop d (M m) = m >>= loop d

    loop2 (O o p) = pure (o,p)
    loop2 (M   m) = m >>= loop2


flood :: M.Map (Int,Int) Int -> S.Set (Int,Int) -> Int -> Int
flood closed open time
    | S.null open = time
    | otherwise =
        let closed' = foldl' (\m p -> M.insert p 2 m) closed open
            open' = S.fromList [ (x,y)
                               | (x0,y0) <- S.toList open
                               , (x,y) <- [(x0+1,y0),(x0-1,y0),(x0,y0-1),(x0,y0+1)]
                               , M.lookup (x,y) closed' == Just 1 ]
        in flood closed' open' (time+1)


