import Intcode

main = do
    let prg1 = readProg "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"
    let prg1' = pogo [] (vm prg1)
    print $ prg1 == prg1'

    let prg2 = readProg "1102,34915192,34915192,7,4,7,99,0"
    let res2 = pogo [] (vm prg2)
    print $ length res2 == 1 && length (show (head res2)) == 16

    let prg3 = readProg "104,1125899906842624,99"
    print $ pogo [] (vm prg3) == take 1 (drop 1 prg3)


    prg <- readProg <$> readFile "input9.txt"
    mapM_ print $ pogo [1] (vm prg)     -- part A
    mapM_ print $ pogo [2] (vm prg)     -- part B

