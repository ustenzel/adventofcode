{-# LANGUAGE BangPatterns #-}
import qualified Data.Sequence as Z
import Text.Printf

readInts s = case break (','==) s of
    (l,',':r) -> read l : readInts r
    _         -> []

main = do
    mem <- Z.fromList . readInts <$> readFile "input2.txt"
    -- part A
    print $ runPrg 12 2 mem
    -- part B
    print $ [ 100*noun + verb
            | noun <- [0..99], verb <- [0..99]
            , runPrg noun verb mem == 19690720 ]
    -- non-boring part B
    let r@(Cell x y z) = flip Z.index 0 $ runIC' 0 $
                         Z.update 2 (Cell 0 1 0) $ Z.update 1 (Cell 1 0 0) $
                         fmap (Cell 0 0) mem
    print r
    -- solve x*N + y*V + z == 19690720
    -- Euclid's algorithm, but for now, leave it to the user
    printf "%d*N + %d*V = %d\n" x y (19690720-z)

runPrg :: Int -> Int -> Z.Seq Int -> Int
runPrg noun verb mem = flip Z.index 0 $ runIC 0 $ Z.update 2 verb $ Z.update 1 noun mem


runIC :: Int -> Z.Seq Int -> Z.Seq Int
runIC !pc !mem = case mem `Z.index` pc of
    99 -> mem
    1  -> let x = mem `Z.index` (mem `Z.index` (pc+1))
              y = mem `Z.index` (mem `Z.index` (pc+2))
          in runIC (pc+4) $ Z.update (mem `Z.index` (pc+3)) (x+y) mem
    2  -> let x = mem `Z.index` (mem `Z.index` (pc+1))
              y = mem `Z.index` (mem `Z.index` (pc+2))
          in runIC (pc+4) $ Z.update (mem `Z.index` (pc+3)) (x*y) mem


-- Note on part B:  too bad the search space is so small, this makes it
-- boring.  An exciting solution would be if we tried symbolic
-- evaluation.  All we have is addition, multiplication, constants, and
-- the two input values.  The result therefore is a polynomial in N and
-- V, and we could solve symbolically for N and V.

data Cell = Cell Int Int Int        -- Cell x y z ~ xN + yV + z
  deriving Show

runIC' :: Int -> Z.Seq Cell -> Z.Seq Cell
runIC' !pc !mem = case mem `Z.index` pc of
    Cell 0 0 99 -> mem
    Cell 0 0 1  -> let x = mem ! (mem `Z.index` (pc+1))
                       y = mem ! (mem `Z.index` (pc+2))
                   in runIC' (pc+4) $ update (mem `Z.index` (pc+3)) (plus x y) mem
    Cell 0 0 2  -> let x = mem ! (mem `Z.index` (pc+1))
                       y = mem ! (mem `Z.index` (pc+2))
                   in runIC' (pc+4) $ update (mem `Z.index` (pc+3)) (mult x y) mem
  where
    (!) m (Cell 0 0 i) = Z.index m i
    update (Cell 0 0 i) z m = Z.update i z m

    Cell a b c `plus` Cell x y z = Cell (a+x) (b+y) (c+z)

    Cell 0 0 a `mult` Cell x y z = Cell (a*x) (a*y) (a*z)
    Cell x y z `mult` Cell 0 0 a = Cell (a*x) (a*y) (a*z)
    -- can't have mixed terms, so the other cases must error


