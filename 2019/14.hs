{-# LANGUAGE BangPatterns #-}

import BasePrelude
import Prelude ()

import qualified Data.HashMap.Strict as M

data Reaction = R !Int64                  -- how much product
                  [(String, Int64)]       -- educts
  deriving Show

readReaction :: [String] -> (String, Reaction)
readReaction ["=>", n, x] = (x, R (read n) [])
readReaction (n:x:ws) =
    let (y, R m es) = readReaction ws
    in (y, R m ((filter (/= ',') x,read n):es))


type Bill = M.HashMap String Int64

main = do
    inp <- M.fromList . map (readReaction . words) . lines <$> readFile "i14.txt"
    print $ oreForFuel inp 1        -- part A

    -- part B:  not elegant, just exponential+binary search
    print $ search (oreForFuel inp) 1000000000000


oreForFuel :: M.HashMap String Reaction -> Int64 -> Int64
oreForFuel inp =
    snd . head . M.toList . head . dropWhile ((>1) . M.size) . drop 1 . iterate (make inp) . M.singleton "FUEL"

make :: M.HashMap String Reaction -> Bill -> Bill
make rs b0 = case M.toList b0 of (p0:ps0) -> go [] p0 ps0
  where
    go acc mx (p:ps)
        | fst p `higher` fst mx = go (mx:acc) p ps
        | otherwise             = go (p:acc) mx ps

    go acc (x,n) [] = case M.lookup x rs of
        Just (R m es) -> let k = (n+m-1) `div` m
                         in M.fromListWith (+) $ acc ++ [ (e,k*i) | (e,i) <- es ]
        Nothing -> b0

    a `higher` b =
        case M.lookup a rs of
            Just (R _ es) -> any (\e -> b == e || e `higher` b) $ map fst es
            Nothing       -> False

-- search for largest x such that (f x <= y)
search :: (Integral a, Show a, Ord b) => (a -> b) -> b -> a
search f y = sexp 1
  where
    sexp x | f (2*x) <= y = sexp (2*x)
           | otherwise    = sbin x (2*x)

    sbin a b | a == b    = a
             | f u <= y  = sbin u b
             | otherwise = sbin a (u-1)
      where
        u = div (a+b+1) 2

