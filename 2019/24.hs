{-# LANGUAGE BangPatterns #-}
import BasePrelude
import Prelude ()

import qualified Data.Set as S

ex1 = [ "....#"
      , "#..#."
      , "#..##"
      , "..#.."
      , "#...." ]

bugs = [ ".###."
       , "##..."
       , "...##"
       , ".#.#."
       , "#.#.#" ]


step grid =
    zipWith5
        (zipWith5 (\c n e s w -> cell c (sum $ map fromEnum [n,e,s,w])))
        grid
        (repeat False : grid)
        (map (tail . (++ [False])) grid)
        (tail grid ++ [repeat False])
        (map (False:) grid)
  where
    cell True  n  =  n == 1
    cell False n  =  n == 1 || n == 2


firstDup :: Ord a => [a] -> a
firstDup = go S.empty
  where
    go s (x:xs) | S.member x s = x
                | otherwise    = go (S.insert x s) xs

biodiv :: [[Bool]] -> Integer
biodiv = sum . zipWith (\i -> bool 0 (1 `shiftL` i)) [0..] . concat

test1 = mapM_ printGrid $ take 5 $ iterate step $ map (map (=='#')) ex1
test2 = print $ biodiv $ firstDup $ iterate step $ map (map (=='#')) ex1

printGrid = putStrLn . unlines . map (map (bool '.' '#'))

partA = print $ biodiv $ firstDup $ iterate step $ map (map (=='#')) bugs



newtype Grid = Grid Int

at :: Grid -> Int -> Int -> Bool
at (Grid w) x y = testBit w (y * 5 + x)

neighbors :: Grid -> Grid -> Grid -> Int -> Int -> Int
neighbors up here down x y = sum $ map fromEnum $
    [ if y == 0 then at up 2 1 else False
    , if y == 4 then at up 2 3 else False
    , if x == 0 then at up 1 2 else False
    , if x == 4 then at up 3 2 else False ] ++
    [ at here u v
    | (u,v) <- [(x,y-1),(x,y+1),(x-1,y),(x+1,y)]
    , 0 <= u && u < 5
    , 0 <= v && v < 5
    , u /= 2 || v /= 2 ] ++
    case (x,y) of
        (2,1) -> [ at down i 0 | i <- [0..4] ]
        (2,3) -> [ at down i 4 | i <- [0..4] ]
        (1,2) -> [ at down 0 i | i <- [0..4] ]
        (3,2) -> [ at down 4 i | i <- [0..4] ]
        _     -> []

emptyGrid :: Grid
emptyGrid = Grid 0

newGrid :: [String] -> Grid
newGrid ss | length ss == 5 && all (==5) (map length ss)
    = Grid . foldl' (\w b -> shiftL w 1 .|. fromEnum b) 0 . map (=='#') . concat . reverse $ map reverse ss

stepB1 :: Grid -> Grid -> Grid -> Grid
stepB1 up here down
    = Grid . foldl' (\w b -> shiftL w 1 .|. fromEnum b) 0 $
        [ if x == 2 && y == 2 then False else if self then n == 1 else n == 1 || n == 2
        | y <- [4,3,2,1,0]
        , x <- [4,3,2,1,0]
        , let n = neighbors up here down x y
        , let self = at here x y ]

stepB gs = zipWith3 stepB1 (emptyGrid:emptyGrid:gs) (emptyGrid:gs++[emptyGrid]) (gs++[emptyGrid,emptyGrid])

showGrid g = unlines [ [ if at g x y then '#' else '.' | x <- [0..4] ] | y <- [0..4] ]
showGrids = intercalate "\n" . map showGrid

partB = print . sum . map (\(Grid w) -> popCount w) . head . drop 200 $ iterate stepB [newGrid bugs]



