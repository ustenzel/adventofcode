{-# LANGUAGE BangPatterns #-}
import Data.Foldable

data M = M !Int !Int !Int !Int !Int !Int
  deriving Show

ex1 = [ M (-1) 0 2 0 0 0
      , M 2 (-10) (-7) 0 0 0
      , M 4 (-8) 8 0 0 0
      , M 3 5 (-1) 0 0 0 ]

moons = [ M 7 10 17 0 0 0
        , M (-2) 7 0 0 0 0
        , M 12 5 12 0 0 0
        , M 5 (-8) 6 0 0 0 ]

grav :: [M] -> [M]
grav ms = map (flip (foldl' accel) ms) ms
  where
    accel (M x y z u v w) (M x' y' z' _ _ _) =
        M x y z (u + signum (x'-x)) (v + signum (y'-y)) (w + signum (z'-z))

vel :: M -> M
vel (M x y z u v w) = M (x+u) (y+v) (z+w) u v w

erg :: [M] -> Int
erg = sum . map e1
  where
    e1 (M x y z u v w) = (abs x + abs y + abs z) * (abs u + abs v + abs w)

partA = erg $ head $ drop 1000 $ iterate (map vel . grav) moons

partB ms = do let xx = find_period [ x | M x y z _ _ _ <- ms ]
                  yy = find_period [ y | M x y z _ _ _ <- ms ]
                  zz = find_period [ z | M x y z _ _ _ <- ms ]
              print [xx,yy,zz]
              print $ xx `lcm` yy `lcm` zz

find_period :: [Int] -> Integer
find_period pps = fromIntegral $ (+1) $ length $ takeWhile (/= init_state) $ drop 1 $ iterate step init_state
  where
    init_state = [ (x,0) | x <- pps ]
    step ps = map vee $ map (flip (foldl' accel) ps) ps
    accel (x,v) (y,_) = (x, v + signum (y-x))
    vee (x,v) = (x+v,v)

main = print partA >> partB moons
