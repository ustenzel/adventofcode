{-# LANGUAGE BangPatterns, DeriveFunctor #-}
module Intcode where

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.State.Strict
import Data.Bool
import Data.Functor.Identity
import Data.Int

import qualified Data.IntMap as M

type MWord = Integer

readProg :: String -> [MWord]
readProg s = case break (','==) s of
    (l,',':r) -> read l : readProg r
    (l,"")    -> [read l]
    _         -> []



type Mem = M.IntMap MWord
data Regs = Regs { mem :: !Mem, pc :: !Int, base :: !MWord }

data P m a = I (MWord -> P m a)
           | O !MWord (P m a)
           | S !a
           | M (m (P m a))
  deriving Functor

instance Monad m => Applicative (P m) where
    pure = S

    M   m <*> M   n = M (liftM2 (<*>) m n)
    M   m <*>     b = M ((<*> b) <$> m)
    a     <*> M   m = M ((a <*>) <$> m)
    I   f <*>     k = I (\i -> f i <*> k)
    O o a <*>     k = O o (a <*> k)
    a     <*> I   f = I (\i -> a <*> f i)
    a     <*> O o m = O o (a <*> m)
    S a   <*> S   b = S (a b)

instance Monad m => Monad (P m) where
    return = pure

    I   f >>= k = I (\i -> f i >>= k)
    O o m >>= k = O o (m >>= k)
    S a   >>= k = k a
    M m   >>= k = M ((>>= k) <$> m)

instance MonadTrans P where
    lift = M . fmap S


vm :: Monad m => [MWord] -> P m ()
vm prg = go (Regs (M.fromList $ zip [0..] prg) 0 0)
  where
    go = runStateT stepVM >=> uncurry (bool go (const $ pure ()))

traceVM :: [MWord] -> P IO ()
traceVM = genVM (\regs -> print (pc regs, base regs, [ M.lookup i (mem regs) | i <- [pc regs..pc regs +3] ], M.size (mem regs)))

genVM :: Monad m => (Regs -> m ()) -> [MWord] -> P m ()
genVM m prg = go (Regs (M.fromList $ zip [0..] prg) 0 0)
  where
    go regs = lift (m regs) >> runStateT stepVM regs >>= uncurry (bool go (const $ pure ()))

stepVM :: Monad m => StateT Regs (P m) Bool
stepVM = do
    opcode <- readMem =<< gets pc
    case opcode `mod` 100 of
        99 -> pure ()

        1  -> do x <- readOp 1
                 y <- readOp 2
                 writeOp 3 $ x+y
                 modify $ \regs -> regs { pc = pc regs + 4 }

        2  -> do x <- readOp 1
                 y <- readOp 2
                 writeOp 3 $ x*y
                 modify $ \regs -> regs { pc = pc regs + 4 }

        3  -> do x <- lift (I S)
                 writeOp 1 x
                 modify $ \regs -> regs { pc = pc regs + 2 }

        4  -> do x <- readOp 1
                 modify $ \regs -> regs { pc = pc regs + 2 }
                 lift (O x (S ()))

        5  -> do x <- readOp 1
                 y <- readOp 2
                 modify $ \regs -> regs { pc = if x /= 0 then fromIntegral y else pc regs + 3 }

        6  -> do x <- readOp 1
                 y <- readOp 2
                 modify $ \regs -> regs { pc = if x == 0 then fromIntegral y else pc regs + 3 }

        7  -> do x <- readOp 1
                 y <- readOp 2
                 writeOp 3 $ if x < y then 1 else 0
                 modify $ \regs -> regs { pc = pc regs + 4 }

        8  -> do x <- readOp 1
                 y <- readOp 2
                 writeOp 3 $ if x == y then 1 else 0
                 modify $ \regs -> regs { pc = pc regs + 4 }

        9  -> do b <- readOp 1
                 modify $ \regs -> regs { base = base regs + b, pc = pc regs + 2 }

        op -> error $ "illegal opcode " ++ show op

    pure $ opcode `mod` 100 == 99


readMem :: Monad m => Int -> StateT Regs m MWord
readMem i | i < 0     = error $ "invalid_index " ++ show i
          | otherwise = gets $ M.findWithDefault 0 i . mem

writeMem :: Monad m => Int -> MWord -> StateT Regs m ()
writeMem i x = modify $ \regs -> regs { mem = M.insert i x (mem regs) }

readOp :: Monad m => Int -> StateT Regs m MWord
readOp i = do
    p <- gets pc
    b <- gets base
    opcode <- readMem p
    let mode = (opcode `div` (10^(i+1))) `mod` 10
    case mode of
        0 -> readMem (p+i) >>= readMem . fromIntegral
        1 -> readMem (p+i)
        2 -> readMem (p+i) >>= readMem . fromIntegral . (+) b
        _ -> error "nope"

writeOp :: Monad m => Int -> MWord -> StateT Regs m ()
writeOp i x = do
    p <- gets pc
    b <- gets base
    opcode <- readMem p
    let mode = (opcode `div` (10^(i+1))) `mod` 10
    case mode of
        0 -> readMem (p+i) >>= flip writeMem x . fromIntegral
        2 -> readMem (p+i) >>= flip writeMem x . fromIntegral . (+) b
        _ -> error "nope"


chain :: (Monad m, Foldable f) => f (P m r) -> P m r
chain = foldr (>->) cat

(>->) :: Monad m => P m r -> P m r -> P m r
M   m >-> M   n = M (liftM2 (>->) m n)
M   m >->     b = M ((>-> b) <$> m)
a     >-> M   m = M ((a >->) <$> m)
p     >-> O o q = O o (p >-> q)
_     >-> S   r = S r
O x p >-> I   g = p >-> g x
I   f >-> I   g = I (\x -> f x >-> I g)
S   r >-> I   _ = S r

cat :: Monad m => P m a
cat = I (\x -> O x cat)

pogo :: [MWord] -> P Identity r -> [MWord]
pogo (i:is) (I   f) = pogo is (f i)
pogo [    ] (I   _) = error "input exhausted"
pogo    is  (O o p) = o : pogo is p
pogo     _  (S   _) = []
pogo    is  (M   m) = pogo is (runIdentity m)

pogoIO :: [MWord] -> P IO r -> IO r
pogoIO (i:is) (I   f) = pogoIO is (f i)
pogoIO [    ] (I   _) = error "input exhausted"
pogoIO    is  (O o p) = print o >> pogoIO is p
pogoIO     _  (S   r) = pure r
pogoIO    is  (M   m) = m >>= pogoIO is

