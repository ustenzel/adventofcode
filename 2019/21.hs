import Intcode
import BasePrelude
import Prelude ()

main = do
    prg <- readProg <$> readFile "i21.txt"

    -- part A
    {- runJumpy prg >=> print $
        [ "NOT C J"
        , "AND D J"
        , "NOT A T"
        , "OR T J"
        , "WALK" ] -}
        -- J := (D & ~C) | ~A

    -- part B
    getContents >>= runJumpy prg . cleanPrg . lines >>= print

runJumpy prg txt = do
    let out = pogo (map (fromIntegral . ord) $ unlines txt) (vm prg)
    putStrLn $ map (chr . fromIntegral) $ takeWhile (<128) out
    pure $ last out

cleanPrg = filter (not . null) . map (reverse . dropWhile isSpace . dropCmt [])
  where
    dropCmt acc ('-':'-':_) = acc
    dropCmt acc (c:s) = dropCmt (c:acc) s
    dropCmt acc [] = acc


