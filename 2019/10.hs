import Data.Function
import Data.Ord
import Data.List
import Data.Semigroup
import qualified Data.Map.Strict as M
import qualified Data.Set as S

main :: IO ()
main = do
    roids <- concat . zipWith (\y xs -> [ (x,y) | (x,'#') <- zip [0..] xs ]) [0..] . lines <$> readFile "input10.txt"
    print roids

    let xs = [ (S.size s,x,y)
             | (x,y) <- roids
             , let s = S.fromList [ dir (u-x) (v-y)
                                  | (u,v) <- roids
                                  , x /= u || y /= v ] ]

    let (nmax, x, y) = maximum xs  -- station is now at (x,y)
    print (nmax, x, y)                     -- part A


    let rays = M.toList $ M.fromListWith (++)
                    [ (dir (u-x) (v-y), [(u,v)])
                    | (u,v) <- roids
                    , x /= u || y /= v ]

    mapM_ print rays

    let scanned =scan [] $ map (sortBy (comparing $ \(u,v) -> abs (u-x) + abs (v-x)) . snd) rays
    print scanned

    let (um,vm) = scanned !! 199
    print $ 100*um+vm  -- part B

dir :: Int -> Int -> D
dir a b = D (div a g) (div b g) where g = gcd a b

data D = D Int Int deriving (Eq, Show)

-- ordering directions:  positive Y is smaller than negative Y (first
-- and second quadrants), then order by Y/X, but don't divide
-- (because Y may be zero).
instance Ord D where
    D x y `compare` D u v =
        compare (x < 0) (u < 0) <> compare (y*u) (x*v)


scan :: [[a]] -> [[a]] -> [a]
scan [] [          ] = [ ]
scan ys [          ] =     scan     [] (reverse ys)
scan ys ([    ]:xss) =     scan     ys  xss
scan ys ((x:xs):xss) = x : scan (xs:ys) xss


