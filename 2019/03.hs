import qualified Data.Map.Strict as M
import Data.Bifunctor
import Data.Foldable
import Data.Ord

main = do
    [w1,w2] <- map readWire . lines <$> readFile "input3.txt"

    let combo_trace = M.intersectionWith (+)
                          (M.fromListWith (const id) (wireTrace 0 0 0 w1))
                          (M.fromListWith (const id) (wireTrace 0 0 0 w2))

    -- part A: closest point
    print $ minimum $ map (uncurry (+) . bimap abs abs) $ M.keys combo_trace

    -- part B: minimal combined distance
    print $ minimum combo_trace


data Wire = L Int | R Int | U Int | D Int deriving Show

readWire :: String -> [Wire]
readWire (c:s) = case reads s of
    [(i,',':t)] ->  d c i : readWire t
    [(i,[   ])] -> [d c i]
  where
    d 'L' = L
    d 'R' = R
    d 'U' = U
    d 'D' = D

wireTrace :: Int -> Int -> Int -> [Wire] -> [((Int,Int),Int)]
wireTrace x y d (R n : w) = [ ((x+i,y),d+i) | i <- [1..n] ] ++ wireTrace (x+n) y (d+n) w
wireTrace x y d (L n : w) = [ ((x-i,y),d+i) | i <- [1..n] ] ++ wireTrace (x-n) y (d+n) w
wireTrace x y d (U n : w) = [ ((x,y+i),d+i) | i <- [1..n] ] ++ wireTrace x (y+n) (d+n) w
wireTrace x y d (D n : w) = [ ((x,y-i),d+i) | i <- [1..n] ] ++ wireTrace x (y-n) (d+n) w
wireTrace _ _ _ [       ] = [ ]
