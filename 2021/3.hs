{-# LANGUAGE BangPatterns #-}
import Data.Bool
import Data.Bifunctor
import Control.Applicative
import Data.List

mainA = print . uncurry (*) . two_from_binary . map get_most_common . transpose . lines =<< getContents
mainB = print . (liftA2 (*) getO getC) . map (\x -> (x,x)) . lines =<< getContents

get_most_common :: String -> Bool
get_most_common xs = length (filter (=='0') xs) <= length (filter (=='1') xs)

two_from_binary :: [Bool] -> (Int,Int)
two_from_binary = foldl' (\(!a,!b) x -> if x then (2*a+1,2*b) else (2*a,2*b+1)) (0,0)


getO, getC :: [(String,String)] -> Int
getO = get (==)
getC = get (/=)

get :: (Char -> Char -> Bool) -> [(String,String)] -> Int
get f [(_,x)] = foldl' (\a c -> 2*a + fromEnum (c=='1')) 0 x
get f xs = get f $ map (bimap tail id) $ filter (f c . head . fst) xs
  where
    c = bool '0' '1' $ get_most_common $ map (head . fst) xs

