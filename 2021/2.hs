{-# LANGUAGE BangPatterns #-}
import Data.List

main = -- main' (\(x,y) -> x*y) runA (0,0)
       main' (\(_,x,y) -> x*y) runB (0,0,0)

main' fmt run nil = print . fmt . foldl' run nil . map words . lines =<< getContents

runA :: (Int,Int) -> [String] -> (Int,Int)
runA (!x,!y) ["forward",n] = (x+read n,y)
runA (!x,!y) ["down",   n] = (x,y+read n)
runA (!x,!y) ["up",     n] = (x,y-read n)

runB :: (Int,Int,Int) -> [String] -> (Int,Int,Int)
runB (a,!x,!y) ["forward",n] = (a,x+read n,y+a*read n)
runB (a,!x,!y) ["down",   n] = (a+read n,x,y)
runB (a,!x,!y) ["up",     n] = (a-read n,x,y)
