import           Data.Char              ( isUpper )
import qualified Data.HashSet        as S
import qualified Data.HashMap.Strict as M

parse_line s = case break (== '-') s of
    (l, '-':r) -> [ (l,[r]), (r,[l]) ]

main :: IO ()
main = do
    neighbors <- M.fromListWith (++) . concatMap parse_line . lines <$> getContents
    print $ length $ paths_from neighbors False (S.singleton "start") "start"
    print $ length $ paths_from neighbors True  (S.singleton "start") "start"


paths_from :: M.HashMap String [String] -> Bool -> S.HashSet String -> String -> [[String]]
paths_from _neighbors _ _visited "end" = [[]]
paths_from  neighbors d  visited    p  = map (p:) $ concatMap follow $ M.findWithDefault [] p neighbors
  where
    visited' = S.insert p visited

    follow q | all isUpper q            = paths_from neighbors     d visited' q
             | not (S.member q visited) = paths_from neighbors     d visited' q
             | d && q /= "start"        = paths_from neighbors False visited' q
             | otherwise                = []




