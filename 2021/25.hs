import Data.Array.Unboxed
import Data.Word
import qualified Data.ByteString.Char8 as B

read_arr :: [B.ByteString] -> UArray (Int,Int) Word8
read_arr ss = listArray ((0,0),(h-1,w-1)) $ map from_char $ concatMap B.unpack ss
  where
    w = B.length (head ss)
    h = length ss

    from_char '.' = 0
    from_char '>' = 1
    from_char 'v' = 2


stepH :: UArray (Int,Int) Word8 -> UArray (Int,Int) Word8
stepH arr = listArray b $ map next $ range b
  where
    b@((0,0),(h,w)) = bounds arr

    next (y,x) = case arr ! (y,x) of
        0 -> if arr ! (y,p x) == 1 then 1 else 0
        1 -> if arr ! (y,s x) == 0 then 0 else 1
        2 -> 2

    s x = if x == w then 0 else x+1
    p x = if x == 0 then w else x-1

stepV :: UArray (Int,Int) Word8 -> UArray (Int,Int) Word8
stepV arr = listArray b $ map next $ range b
  where
    b@((0,0),(h,w)) = bounds arr

    next (y,x) = case arr ! (y,x) of
        0 -> if arr ! (p y,x) == 2 then 2 else 0
        2 -> if arr ! (s y,x) == 0 then 0 else 2
        1 -> 1

    s y = if y == h then 0 else y+1
    p y = if y == 0 then h else y-1

main = do
    arr0 <- read_arr . B.lines <$> B.getContents
    let arrs = iterate (stepV . stepH) arr0
    print . succ . length . takeWhile not $ zipWith (==) arrs (tail arrs)

