import Data.Char                ( digitToInt )
import Data.List                ( sortBy, zipWith5 )
import Control.Monad.ST
import Data.Array.ST
import Data.Array.Unboxed
import Data.Word                ( Word8 )

main = mapM_ print . partB . pad . lines =<< getContents

pad :: [String] -> [String]
pad xs = replicate w '9' : map (\x -> '9' : x ++ "9") xs ++ [replicate w '9']
  where
    w = length (head xs) + 2

-- very Haskellish solution.  works well.
partA (x1:x2:xs) = sum $ zipWith3 partA' (x1:x2:xs) (x2:xs) xs

partA' (_:top) (h1:h2:here) (_:bot) = sum $ zipWith5 partA'' top (h1:h2:here) (h2:here) here bot

partA'' t l h r b
    | t > h && l > h && r > h && b > h  =  1 + digitToInt h
    | otherwise                         =  0


-- meh.  got to reimplement part A, because here I really need an array
partB :: [String] -> [Int]
partB lns | all ((==) w . length) lns = [ sum $ map risk low_points, product $ take 3 $ sortBy (flip compare) sizes ]
  where
    w = length (head lns)
    h = length lns

    grid :: UArray (Int,Int) Word8
    grid = listArray ((0,0),(h-1,w-1)) $ map (fromIntegral . digitToInt) $ concat lns

    sizes = runST $ thaw grid >>= \g -> mapM (flood g) low_points

    low_points = filter is_low $ range ((1,1),(h-2,w-2))
    risk p = 1 + fromIntegral (grid ! p)

    is_low (x,y) = and [ grid ! p > grid ! (x,y) | p <- neighbors x y ]

    neighbors x y = [(x-1,y),(x+1,y),(x,y-1),(x,y+1)]

    flood :: STUArray s (Int,Int) Word8 -> (Int,Int) -> ST s Int
    flood g (x,y) = do lv <- readArray g (x,y)
                       writeArray g (x,y) 9
                       (+1) . sum <$> sequence [ do lv' <- readArray g p
                                                    if lv' > lv && lv' < 9
                                                      then flood g p
                                                      else pure 0
                                               | p <- neighbors x y ]

