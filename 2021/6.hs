import Util
import qualified Data.ByteString.Char8 as B
import qualified Data.IntSet as S

mainA, mainB, main :: IO ()

-- exponential complexity.  clearly no good, except for testing
mainA = print . length . (!! 80) . iterate step_slow . parse_numbers =<< B.getContents

step_slow :: [Int] -> [Int]
step_slow = concatMap spawn
  where
    spawn 0 = [6,8]
    spawn n = [n-1]


-- linear complexity.  acceptable, but unsatisfying
mainB = print . sum . (!! 256) . iterate step . iniCount . parse_numbers =<< B.getContents


iniCount :: [Int] -> [Int]
iniCount xs = [ length (filter (== i) xs) | i <- [0..8] ]

step :: [Int] -> [Int]
step [x0,x1,x2,x3,x4,x5,x6,x7,x8] = [x1,x2,x3,x4,x5,x6,xn,x8,x0]
  where
    !xn = x7 + x0


-- logarithmic number of operations.  very satisfying.
-- only makes sense for __very__ large numbers, hence the use of Integer
--
-- Here is the last example from https://the-tk.com/project/aoc2021-bigboys.html
-- It completes in 36s on my i5-7500T.

main = print . sum . vmul (pow step_mat 67108864) . map fromIntegral . iniCount $ [3,4,3,1,2]


-- always 9x9
newtype Matrix = M [[Integer]]

mul :: Matrix -> Matrix -> Matrix
mul (M xs) (M ys) = M [ [ sum $ zipWith (*) row col | col <- transpose ys ] | row <- xs ]

-- e must be strictly positive
pow :: Matrix -> Int -> Matrix
pow m 1 = m
pow m e | even e  =  pow (mul m m) (div e 2)
        | odd e   =  pow (mul m m) (div e 2) `mul` m

vmul :: Matrix -> [Integer] -> [Integer]
vmul (M xs) v = map (sum . zipWith (*) v) xs

step_mat :: Matrix
step_mat = M [ [ 0,1,0,0,0,0,0,0,0 ]
             , [ 0,0,1,0,0,0,0,0,0 ]
             , [ 0,0,0,1,0,0,0,0,0 ]
             , [ 0,0,0,0,1,0,0,0,0 ]
             , [ 0,0,0,0,0,1,0,0,0 ]
             , [ 0,0,0,0,0,0,1,0,0 ]
             , [ 1,0,0,0,0,0,0,1,0 ]
             , [ 0,0,0,0,0,0,0,0,1 ]
             , [ 1,0,0,0,0,0,0,0,0 ] ]

