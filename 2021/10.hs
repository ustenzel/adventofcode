import Data.List
import Data.Maybe

mainA = print . sum . map (either scoreA (const 0) . parse) . lines =<< getContents
main  = print . median . mapMaybe (either (const Nothing) (Just . scoreB) . parse) . lines =<< getContents

scoreA :: Char -> Int
scoreA ')' = 3
scoreA ']' = 57
scoreA '}' = 1197
scoreA '>' = 25137

scoreB :: String -> Int
scoreB = foldl (\a c -> 5*a + s c) 0
  where
    s ')' = 1
    s ']' = 2
    s '}' = 3
    s '>' = 4

-- Left c: erroneaus character c
-- Right s: the missing characters
-- Right "": a correct line
parse :: String -> Either Char String
parse = go []
  where
    go a ('(':s) = go (')':a) s
    go a ('[':s) = go (']':a) s
    go a ('{':s) = go ('}':a) s
    go a ('<':s) = go ('>':a) s
    go (b:a) (c:s) | b == c = go a s
    go a [   ] = Right a
    go _ (c:_) = Left c

median xs = sort xs !! div (length xs) 2
