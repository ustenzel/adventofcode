{-# LANGUAGE TypeFamilies, OverloadedStrings #-}
import Control.Applicative
import Control.Monad
import Data.Bifunctor
import Data.List
import Data.String
import qualified Data.ByteString.Char8 as B
import qualified Data.Set as S

-- parser combinators.  should have done this earlier...
data Cuboid = Cuboid !Int !Int !Int !Int !Int !Int
    deriving Show

data P a = P { runP :: B.ByteString -> [(a,B.ByteString)] }

instance Functor P where
    fmap f p = P $ map (first f) . runP p

instance Applicative P where
    pure a = P $ \s -> [(a,s)]
    a <*> b = P $ \s -> [ (f x, s'') | (f,s') <- runP a s, (x,s'') <- runP b s' ]

instance a ~ () => IsString (P a) where
    fromString w = P $ maybe [] (pure . (,) ()) . B.stripPrefix (fromString w)

instance Alternative P where
    empty = P $ \_ -> []
    a <|> b = P $ \s -> runP a s ++ runP b s

parse :: P a -> B.ByteString -> a
parse p s = case runP p s of
    [] -> error $ "no parse " ++ show s
    [(x,s)] | B.null s -> x
            | otherwise -> error $ "incomplete parse " ++ show s
    _ -> error $ "multiple parses " ++ show s

readCuboid :: P (Either Cuboid Cuboid)
readCuboid =
    cuboid <$> onoff
           <*  " x="
           <*> range
           <*  ",y="
           <*> range
           <*  ",z="
           <*> range
  where
    cuboid f (x1,x2) (y1,y2) (z1,z2) = f $ Cuboid x1 x2 y1 y2 z1 z2

onoff :: P (a -> Either a a)
onoff = Left <$ "off" <|> Right <$ "on"

range :: P (Int,Int)
range = (,) <$> int <* ".." <*> int

int :: P Int
int = P $ maybe [] pure . B.readInt


-- part 1.  straight forward
applyA :: S.Set (Int,Int,Int) -> Either Cuboid Cuboid -> S.Set (Int,Int,Int)
applyA s ec = case ec of
    Right c -> foldl' (flip S.insert) s $ rng c
    Left  c -> foldl' (flip S.delete) s $ rng c
  where
    rng (Cuboid x1 x2 y1 y2 z1 z2) = (,,) <$> init_rgn x1 x2 <*> init_rgn y1 y2 <*> init_rgn z1 z2
    init_rgn a b = enumFromTo (max (-50) a) (min 50 b)


-- part 2.  as expected, far too big to do it by counting cubes
applyB :: [Cuboid] -> Either Cuboid Cuboid -> [Cuboid]
applyB cs (Right c) = c : remove_overlaps c cs
applyB cs (Left  c) =     remove_overlaps c cs

-- Remove the "new" cuboid from an old one, then recurse.  This leaves a
-- list of cuboids that have no overlap with the "new" one.  If were
-- turning cubes off, this implements the operation.  If were turning
-- cubes on, this ensures that the resulting list remains free of
-- overlaps; only at the very end is the new cuboid turned on.
remove_overlaps :: Cuboid -> [Cuboid] -> [Cuboid]
remove_overlaps c (b1:bs) = deleteFrom b1 c ++ remove_overlaps c bs
remove_overlaps c [     ] = []

deleteFrom :: Cuboid -> Cuboid -> [Cuboid]
deleteFrom a@(Cuboid x1 x2 y1 y2 z1 z2) b@(Cuboid u1 u2 v1 v2 w1 w2)
    -- quick check for no overlap
    | u1 > x2 || v1 > y2 || w1 > z2 = [a]
    | u2 < x1 || v2 < y1 || w2 < z1 = [a]
    -- cut slabs off along every possible dimension
    | x1 < u1   = Cuboid x1 (u1-1) y1 y2 z1 z2 : deleteFrom (Cuboid u1 x2 y1 y2 z1 z2) b
    | x2 > u2   = Cuboid (u2+1) x2 y1 y2 z1 z2 : deleteFrom (Cuboid x1 u2 y1 y2 z1 z2) b
    | y1 < v1   = Cuboid x1 x2 y1 (v1-1) z1 z2 : deleteFrom (Cuboid x1 x2 v1 y2 z1 z2) b
    | y2 > v2   = Cuboid x1 x2 (v2+1) y2 z1 z2 : deleteFrom (Cuboid x1 x2 y1 v2 z1 z2) b
    | z1 < w1   = Cuboid x1 x2 y1 y2 z1 (w1-1) : deleteFrom (Cuboid x1 x2 y1 y2 w1 z2) b
    | z2 > w2   = Cuboid x1 x2 y1 y2 (w2+1) z2 : deleteFrom (Cuboid x1 x2 y1 y2 z1 w2) b
    -- at this point, 'a' is fully contained in 'b'
    | otherwise = []


volume :: Cuboid -> Integer
volume (Cuboid x1 x2 y1 y2 z1 z2) =
    fromIntegral (x2-x1+1) * fromIntegral (y2-y1+1) * fromIntegral (z2-z1+1)

main = do
    cbs <- map (parse readCuboid) . B.lines <$> B.getContents
    print $ S.size $ foldl' applyA S.empty cbs
    print $ sum $ map volume $ foldl' applyB [] cbs
