import Data.Array.Unboxed
import Data.Bits
import Data.Bool
import Data.List

data Pic = Pic Bool                         -- color of pixels not represented
               (UArray (Int,Int) Bool)      -- explicit representation of central portion
  deriving Show

type Algo = UArray Int Bool

read_input :: [String] -> (Algo, Pic)
read_input (algo:"":arr) = (toAlgo algo, Pic False pic)
  where
    w = length (head arr)
    h = length arr
    pic = listArray ((0,0),(h-1,w-1)) $ map (== '#') $ concat arr
    toAlgo = listArray (0,511) . map (== '#')

get :: Pic -> (Int, Int) -> Bool
get (Pic bg arr) p
    | bounds arr `inRange` p = arr ! p
    | otherwise              = bg

enhance :: Algo -> Pic -> Pic
enhance algo p@(Pic bg arr) = Pic bg' arr'
  where
    bg' = if bg then algo ! 511 else algo ! 0
    ((top,left),(bot,right)) = bounds arr
    bounds' = ((top-1,left-1),(bot+1,right+1))
    arr' = listArray bounds' $ map pixel $ range bounds'

    pixel (y0,x0) = (!) algo $ foldl' (\a b -> 2*a + fromEnum b) 0 $
                    [ get p (y,x) | y <- [y0-1..y0+1], x <- [x0-1..x0+1] ]

show_pic :: Pic -> String
show_pic (Pic _ arr) = unlines $ map (map toChar) $ groupBy same_line $ assocs arr
  where
    toChar (_,False) = '.'
    toChar (_, True) = '#'

    same_line ((y,_),_) ((v,_),_)  =  y == v

count :: Pic -> Int
count (Pic _ arr) = sum $ map fromEnum $ elems arr

main = do
    (algo, pic) <- read_input . lines <$> getContents
    let pics = iterate (enhance algo) pic

    -- putStrLn . show_pic       $ pic
    -- putStrLn . show_pic       $ enhance algo pic
    -- putStrLn . show_pic       $ enhance algo $ enhance algo pic

    print $ count $ pics !! 2
    print $ count $ pics !! 50
