use std::io ;
use std::io::BufRead;

fn main() {
    // xs is something implementing the Iterator trait
    let mut xs = std::io::BufReader::new( io::stdin() ).lines().map( 
                    |x| i64::from_str_radix( &x.unwrap(), 10 ).unwrap() ) ;

    let mut n = 0 ;

    /* part a 
    let mut x0 = xs.next().unwrap() ;
    for x1 in xs {
        if x1 > x0 { n += 1 ; }
        x0 = x1 ;
    } */

    // part b
    let mut x0 = xs.next().unwrap() ;
    let mut x1 = xs.next().unwrap() ;
    let mut x2 = xs.next().unwrap() ;
    for x3 in xs {
        if x3 > x0 { n += 1 ; }
        x0 = x1 ;
        x1 = x2 ;
        x2 = x3 ;
    }
    //

    println!( "{}", n ) ;
}

