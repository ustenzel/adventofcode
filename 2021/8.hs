import Util

mainA, main :: IO ()
mainA = print . length . filter good_length . map length . concatMap (words . drop 1 .dropWhile (/='|')) . lines =<< getContents

main = print . sum . map (uncurry decode . bimap (map fromList . words) (map fromList . words . drop 1) . break (=='|'))
             . lines =<< getContents
  where
    fromList = sum . map (\c -> shiftL 1 (ord c - ord 'a'))

good_length 2 = True
good_length 3 = True
good_length 4 = True
good_length 7 = True
good_length _ = False

decode :: [Word] -> [Word] -> Int
decode digits = foldl' (\a d -> fromJust (findIndex (== d) vs) + 10 * a) 0
  where
    pick p = case filter p digits of [d] -> d

    vs = map pick [ \d -> size d == 6 && one `isSubsetOf` d && not (four `isSubsetOf` d)
                  , \d -> size d == 2
                  , \d -> size d == 5 && not (d `isSubsetOf` nine) && not (one `isSubsetOf` d)
                  , \d -> size d == 5 && one `isSubsetOf` d
                  , \d -> size d == 4
                  , \d -> size d == 5 && d `isSubsetOf` nine && not (one `isSubsetOf` d)
                  , \d -> size d == 6 && not (one `isSubsetOf` d)
                  , \d -> size d == 3
                  , \d -> size d == 7
                  , \d -> size d == 6 && four `isSubsetOf` d ]

    one  = vs !! 1
    four = vs !! 4
    nine = vs !! 9

    size = popCount
    isSubsetOf x y = x .&. y == x
