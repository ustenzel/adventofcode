import Control.Applicative
import Control.Monad
import Data.List

-- Big Idea™:  build a /symbolic/ evaluator, and "just read off" the
-- result.  Unfortunately, it got complicated quickly and I ended up
-- reverse engineering the program manually.
--
-- Days later, this plan actually worked:  We try to simplify arithmetic
-- expressions as well a possible.  A few comparisons remain, which will
-- complicate everything.  These are also the solution to the puzzle.
--
-- We now act non-deterministically on those comparisons.  Everytime a
-- comparison doesn't evaluate to a constant, we split the universe and
-- track what happened.  In one of them, the comparison is true, in the
-- other it is false.  This results in many further simplifications, and
-- there are just 128 different universes to track.   In one universe,
-- the z register always equals zero, in all others, it cannot possibly
-- be zero.  Reading off the constraints for the single solution and
-- turning them into the puzzle answer is easily done manually.

-- | Expressions built by the ALU.
data Expr = Lit !Int
          | Var !Char
          | SumE [Expr]
          | ProdE [Expr]
          | Expr :/ Expr
          | Expr :% Expr
  deriving (Eq, Ord)

instance Show Expr where
    showsPrec _ (Lit a) | a < 0     = (:) '(' . shows a . (:) ')'
                        | otherwise = shows a
    showsPrec _ (Var c) = (:) c

    showsPrec _ (SumE [    ]) = (:) '0'
    showsPrec _ (SumE [  e ]) = shows e
    showsPrec _ (SumE (e:es)) = (:) '(' . shows e . (:) '+' . shows (SumE es) . (:) ')'

    showsPrec _ (ProdE [    ]) = (:) '1'
    showsPrec _ (ProdE [  e ]) = shows e
    showsPrec _ (ProdE (e:es)) = (:) '(' . shows e . (:) '*' . shows (ProdE es) . (:) ')'

    showsPrec _ (e :/ f) = (:) '(' . shows e . (:) '/' . shows f . (:) ')'
    showsPrec _ (e :% f) = (:) '(' . shows e . (:) '%' . shows f . (:) ')'

data ALU = ALU { v :: [Expr], x :: Expr, y ::Expr, z :: Expr, w :: Expr }
    deriving Show

-- | Constraints we will collect during execution.
data Constraint = Expr :== Expr
                | Expr :/= Expr

instance Show Constraint where
    showsPrec _ (e :== f) = (:) '(' . shows e . (++) "==" . shows f . (:) ')'
    showsPrec _ (e :/= f) = (:) '(' . shows e . (++) "/=" . shows f . (:) ')'


-- | A Monad that allows for non-determinism while collecting Constraints.
newtype M a = M { runM :: [([Constraint], a)] }

instance Functor M where
    fmap f = M . map (fmap f) . runM

instance Applicative M where
    pure a = M [([],a)]
    u <*> v = M [ (c1++c2, f x)
                | (c1,f) <- runM u
                , (c2,x) <- runM v ]

instance Monad M where
    return = pure
    m >>= k = M [ (c1++c2,r)
                | (c1,a) <- runM m
                , (c2,r) <- runM (k a) ]

-- | Direct implementation of the ALU.  Calls out to smart constructors,
-- these simplify expressions as far as possible.  'equals' is a bit
-- special.
step :: ALU -> [String] -> M ALU
step alu ("inp":r:_) = pure $ uncurry (set r) (nextVar alu)
step alu ("add":r1:r2:_) = pure $ set r1 (get r1 alu .+ get r2 alu) alu
step alu ("mul":r1:r2:_) = pure $ set r1 (get r1 alu .* get r2 alu) alu
step alu ("div":r1:r2:_) = pure $ set r1 (get r1 alu ./ get r2 alu) alu
step alu ("mod":r1:r2:_) = pure $ set r1 (get r1 alu .% get r2 alu) alu
step alu ("eql":r1:r2:_) = flip (set r1) alu . Lit <$> equals (get r1 alu) (get r2 alu)
step alu _               = pure $ alu

nextVar :: ALU -> (Expr, ALU)
nextVar (ALU (i:is) x y z w) = (i, ALU is x y z w)

get :: String -> ALU -> Expr
get "x" (ALU _ x _ _ _) = x
get "y" (ALU _ _ y _ _) = y
get "z" (ALU _ _ _ z _) = z
get "w" (ALU _ _ _ _ w) = w
get  n  _               = Lit (read n)

set :: String -> Expr -> ALU -> ALU
set "x" e (ALU c _ y z w) = ALU c e y z w
set "y" e (ALU c x _ z w) = ALU c x e z w
set "z" e (ALU c x y _ w) = ALU c x y e w
set "w" e (ALU c x y z _) = ALU c x y z e

-- Computes bounds for an expression.  This is possible, because we know
-- that every variable is bounded to (1,9).  Given the bounds, many more
-- expressions simplify, most notably many conditionals.
bounds :: Expr -> (Int, Int)
bounds (Lit a) = (a,a)
bounds (Var _) = (1,9)

bounds (SumE  es) = foldl (\(x,y) (u,v) -> (x+u,y+v)) (0,0) $ map bounds es
bounds (ProdE es) = foldl (\(x,y) (u,v) -> let ex = liftA2 (*) [x,y] [u,v] in (minimum ex, maximum ex)) (1,1) $ map bounds es

bounds (e :/ f) = (minimum extremes, maximum extremes)
  where
    (x,y) = bounds e; (u,v) = bounds f
    extremes = liftA2 quot [x,y] [u,v]

bounds (e :% f) | x > 0 && y < max u (-v) = (x,y)
                | otherwise               = (0,max u (-v) - 1)
  where
    (x,y) = bounds e; (u,v) = bounds f

(.+) :: Expr -> Expr -> Expr
SumE xs .+ SumE ys  =  sumE $ sort $ xs ++ ys     -- reassociate and normalize order
e       .+ SumE ys  =  sumE $ sort $ e : ys
SumE xs .+ f        =  sumE $ sort $ f : xs
e       .+ f        =  sumE $ sort [e,f]

sumE :: [Expr] -> Expr
sumE (Lit a : Lit b : es)  =  sumE (Lit (a+b) : es)           -- collect literals
sumE [e]                   =  e
sumE []                    =  Lit 0
sumE (Lit 0 : es)          =  sumE es
sumE es                    =  SumE es

(.*) :: Expr -> Expr -> Expr
ProdE xs .* ProdE ys  =  prodE $ sort $ xs ++ ys        -- reassociate and normalize order
e        .* ProdE ys  =  prodE $ sort $ e : ys
ProdE xs .* f         =  prodE $ sort $ f : xs
e        .* f         =  prodE $ sort [e,f]

prodE :: [Expr] -> Expr
prodE (Lit a : Lit b : es) =  prodE (Lit (a*b) : es)      -- collect literals
prodE [e]                  =  e
prodE []                   =  Lit 1
prodE (Lit 1 : es)         =  prodE es
prodE (Lit 0 : _)          =  Lit 0
prodE es                   =  ProdE es


-- Split in two, because sumE is likely to return exactly what we
-- started with, and we will recurse forever.
(./) :: Expr -> Expr -> Expr
SumE es ./ f  =  sumE (map (./ f) es)
e       ./ f  =  e ../ f

(../) :: Expr -> Expr -> Expr
Lit 0              ../ _                        =  Lit 0
e                  ../ Lit 1                    =  e
e                  ../ Lit a | 0 <= x && y < a  =  Lit 0                        where (x,y) = bounds e
Lit x              ../ Lit y                    =  Lit (quot x y)
ProdE (Lit x : es) ../ Lit y | mod x y == 0     =  Lit (quot x y) .* ProdE es
e                  ../ f                        =  e :/ f

-- Split in two, for the same reason.
-- This might result in expression full of nested modulo operations.
-- While not strictly /wrong/, it's ugly.  But in practice, all
-- expressions simplify nicely and we end up with very clean results.
(.%) :: Expr -> Expr -> Expr
ProdE es .% f  =  prodE (map (.% f) es) ..% f
SumE  es .% f  =  sumE  (map (.% f) es) ..% f
e        .% f  =  e ..% f

(..%) :: Expr -> Expr -> Expr
Lit 0    ..% _                        =  Lit 0
Lit 1    ..% _                        =  Lit 1
_        ..% Lit 1                    =  Lit 0
e        ..% Lit a | 0 <= x && y < a  =  e                                         where (x,y) = bounds e
Lit x    ..% Lit y                    =  Lit (rem x y)
(e :% f) ..% g | f == g               =  e :% f
e        ..% f                        =  e :% f

-- | Tries to evaluate a condition to a constant.  If it can't, it
-- splits the universe:  In one of them, we assume the condition to be
-- true, in the other we assume it to be false.  Those two constraints
-- are tracked.
equals :: Expr -> Expr -> M Int
equals e f
    | y < u || x > v = pure 0
    | e == f         = pure 1
    | otherwise      = M [ ([e :== f], 1), ([e :/= f], 0) ]
  where
    (x,y) = bounds e
    (u,v) = bounds f


main :: IO ()
main = print . eval =<< getContents

-- | Parses the input, runs a non-deterministic ALU on it, drops all the
-- universes in which z cannot possibly be zero.
--
-- It turns out, this is good enough:  only one result remains, in which
-- z is always zero.  The constraints are easy enough to read, and
-- manually creating the puzzle solution is straight forward.

eval :: String -> [ ([Constraint], Expr) ]
eval = filter good . runM . fmap (get "z") .
       foldM step (ALU vars0 (Lit 0) (Lit 0) (Lit 0) (Lit 0)) .
       map words . filter (not . null) . lines
  where
    vars0 = [ Var c | c <- ['a'..'n'] ]

    good :: ([Constraint], Expr) -> Bool
    good (_, e) = x <= 0 && y >= 0
      where
        (x,y) = bounds e

