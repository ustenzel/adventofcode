import Data.List
import qualified Data.Map.Strict as M

parse_rule :: String -> ((Char,Char),Char)
parse_rule (a:b:' ':'-':'>':' ':c:[]) = ((a,b),c)

main :: IO ()
main = do
    ini:"":rules_ <- lines <$> getContents
    let rules = M.fromList $ map parse_rule rules_

    -- part 1.  easy enough.
    print $ partA rules 10 ini

    -- this better be the same
    print $ partB rules 10 ini

    -- for realz now
    print $ partB rules 40 ini


type Rules = M.Map (Char,Char) Char

partA :: Rules -> Int -> String -> Int
partA rules nrounds = score . (!! nrounds) . iterate (next rules)
  where
    next :: M.Map (Char,Char) Char -> String -> String
    next rules (a:b:cs) = a : rules M.! (a,b) : next rules (b:cs)
    next rules [c] = [c]

    score :: String -> Int
    score s = maximum m - minimum m
      where
        m = M.fromListWith (+) $ map (flip (,) 1) s

-- part 2.  Obviously, there is no time to construct the string.  A few observations:
--
-- - The string isn't needed, only character counts.
-- - Summarizing characters is awkward, but summarizing pairs should be easy,
-- - Each rule maps one pair to two pairs.  That remains true, even if
--   we don't know the order of the pairs.
-- - When translating pairs to characters, say, by taking the second, we miss the
--   first character.  Easy enough to correct, because it never changes.

partB :: Rules -> Int -> String -> Int
partB rules nrounds ini_chars = maximum fin_counts - minimum fin_counts
  where
    ini_counts = M.fromListWith (+) $ map (flip (,) 1) $ zip ini_chars (tail ini_chars)

    next = M.fromListWith (+) . M.foldrWithKey
                (\(a,b) n -> let c = rules M.! (a,b) in (:) ((a,c),n) . (:) ((c,b),n)) []

    fin_counts = M.fromListWith (+) . (:) (head ini_chars,1) .
                 M.foldrWithKey (\(a,b) n -> (:) (b,n)) [] $
                 iterate next ini_counts !! nrounds
