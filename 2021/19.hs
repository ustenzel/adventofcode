{-# LANGUAGE BangPatterns, OverloadedStrings #-}
import Data.List
import Text.Printf
import qualified Data.ByteString.Char8 as B
import qualified Data.Set as S
import qualified Data.Map.Strict as M

data V3 = V3 { fst3 :: !Int, snd3 :: !Int, trd3 :: !Int }
    deriving (Ord, Eq)

instance Show V3 where
    show (V3 x y z) = printf "%d,%d,%d" x y z

plus :: V3 -> V3 -> V3
plus (V3 a b c) (V3 u v w) = V3 (a+u) (b+v) (c+w)

minus :: V3 -> V3 -> V3
minus (V3 a b c) (V3 u v w) = V3 (a-u) (b-v) (c-w)

type Scan = S.Set V3

read_input :: [B.ByteString] -> [Scan]
read_input [] = []
read_input (h:ls) | is_header h = case break is_header ls of
    (u,v) -> S.fromList (map read_point (filter (not . B.null) u)) : read_input v
  where
    is_header = B.isPrefixOf "---"      -- totally ignoring the scanner number

    read_point s0 | "," `B.isPrefixOf` s1 && "," `B.isPrefixOf` s2 && B.null s3 = V3 x y z
      where
        Just (x,s1) = B.readInt s0
        Just (y,s2) = B.readInt $ B.tail s1
        Just (z,s3) = B.readInt $ B.tail s2

-- Find one scan that can be joined.  There is always at least one
-- joinable scan, and there should be exactly one way to do it.
join_all :: [V3] -> Scan -> [Scan] -> ([V3], Scan)
join_all ps !master [   ] = (ps, master)
join_all ps !master scans = head
    [ join_all (d:ps) (S.union master scan_xformed) (lefts ++ rights)
    | (lefts, scan:rights) <- zip (inits scans) (tails scans)

    , [x_,y_,z_] <- permutations [ fst3, snd3, trd3 ]
    , x <- [x_, negate . x_]
    , y <- [y_, negate . y_]
    , z <- [z_, negate . z_]
    , let transform p = V3 (x p) (y p) (z p)

    , d <- M.keys . M.filter (>=12) $
           M.fromListWith (+) [ (a `minus` transform b, 1)
                              | a <- S.toList master
                              , b <- S.toList scan ]

    , let scan_xformed = S.map (plus d . transform) scan ]


l1 :: V3 -> V3 -> Int
l1 (V3 a b c) (V3 u v w) = abs (a-u) + abs (b-v) + abs (c-w)

main = do
    scan0:scans <- read_input . B.lines <$> B.getContents
    let (posns, master) = join_all [V3 0 0 0] scan0 scans
    print $ S.size master
    print $ maximum [ l1 x y | x <- posns, y <- posns ]



