{-# LANGUAGE OverloadedStrings #-}
module Util
    (
    parse_numbers,
    module X,
    Heap, singletonH, insertH, viewMinH
    ) where

import Control.Applicative as X
import Control.Monad as X
import Data.Bifunctor as X
import Data.Bits as X
import Data.Bool as X
import Data.Char as X
import Data.List as X
import Data.Maybe as X

import qualified Data.ByteString.Char8 as B
import qualified Data.IntMap.Strict    as M

parse_numbers :: B.ByteString -> [Int]
parse_numbers = unfoldr (\s -> do (i,s') <- B.readInt s
                                  pure (i, maybe s' id $ B.stripPrefix "," s'))


type Heap a = M.IntMap [a]

singletonH :: Int -> a -> Heap a
singletonH p v = M.insert p [v] M.empty

insertH :: Heap a -> Int -> a -> Heap a
insertH h p v = M.insertWith (++) p [v] h

viewMinH :: Heap a -> (Int, a, Heap a)
viewMinH h = case M.minViewWithKey h of
    Just ((p, v : []), h') -> (p,v,h')
    Just ((p, v : vs), h') -> (p,v, M.insert p vs h')
