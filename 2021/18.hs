import Control.Monad

data N = N Int | P N N

instance Show N where
    showsPrec _ (N i) = shows i
    showsPrec _ (P a b) = (:) '[' . shows a . (:) ',' . shows b . (:) ']'

instance Read N where
    readsPrec _ ('[':s) = [ (P a b, s'') | (a,',':s') <- reads s , (b,']':s'') <- reads s' ]
    readsPrec _ s = [ (N i, s') | (i,s') <- reads s ]

add :: N -> N -> N
add a b = reduce $ P a b

reduce :: N -> N
reduce n = case try_explode n of
    Just n' -> reduce n'
    Nothing -> case try_split n of
        Just n' -> reduce n'
        Nothing -> n

try_split :: N -> Maybe N
try_split (N n) | n  >=  10 = Just $ P (N $ div n 2) (N $ div (n+1) 2)
                | otherwise = Nothing
try_split (P a b) = case try_split a of
    Just a' -> Just $ P a' b
    Nothing -> case try_split b of
        Just b' -> Just $ P a b'
        Nothing -> Nothing

try_explode :: N -> Maybe N
try_explode = fmap (uncurry to_num) . go 0 []
  where
    go 4 p (P (N a) (N b)) = Just . (,) (N 0) . add_rightmost_left a . add_leftmost_right b $ p
    go 4 p (P   _     _  ) = error "You promised!!"

    go d p (P a b) = go (d+1) (Left b:p) a `mplus` go (d+1) (Right a:p) b
    go _ p (N   n) = Nothing

    to_num n [         ] = n
    to_num n (Left  b:p) = to_num (P n b) p
    to_num n (Right a:p) = to_num (P a n) p

add_rightmost_left :: Int -> [Either N N] -> [Either N N]
add_rightmost_left n [] = []
add_rightmost_left n (Left  b:p) = Left b : add_rightmost_left n p
add_rightmost_left n (Right a:p) = Right (go a) : p
  where
    go (N m) = N (n+m)
    go (P a b) = P a (go b)

add_leftmost_right :: Int -> [Either N N] -> [Either N N]
add_leftmost_right n [] = []
add_leftmost_right n (Right a:p) = Right a : add_leftmost_right n p
add_leftmost_right n (Left  b:p) = Left (go b) : p
  where
    go (N m) = N (n+m)
    go (P a b) = P (go a) b

magnitude :: N -> Int
magnitude (N n) = n
magnitude (P a b) = 3 * magnitude a + 2 * magnitude b

main :: IO ()
main = do
    ns <- mapM readIO . lines =<< getContents
    print . magnitude $ foldl1 add ns                           -- part 1
    print $ maximum [ magnitude $ add x y | x <- ns, y <- ns ]  -- part 2
