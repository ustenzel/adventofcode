import Data.Array.Unboxed
import Util
import qualified Data.Set as S

type Grid = UArray (Int,Int) Int

read_arr :: [String] -> Grid
read_arr ls = listArray ((0,0),(h-1,w-1)) $ map digitToInt $ concat ls
  where
    w = length (head ls)
    h = length ls

neighbors :: Grid -> (Int,Int) -> [(Int, (Int,Int))]
neighbors arr (x,y) = [ (arr ! p, p)
                      | p <- [ (x-1,y), (x,y-1), (x+1,y), (x,y+1) ]
                      , inRange (bounds arr) p ]

dijkstra :: ((Int,Int) -> [(Int,(Int,Int))]) -> (Int,Int) -> (Int,Int) -> Int
dijkstra nbrs start end = go S.empty (singletonH 0 start)
  where
    go !closed !open | p  == end = c
                     | otherwise = go (S.insert p closed) open''
      where
        (c,p,open') = viewMinH open
        open'' = foldl' (uncurry . insertH) open' [ (c+c',p') | (c',p') <- nbrs p, not (S.member p closed) ]


embiggen :: Grid -> Grid
embiggen arr = listArray ((0,0),(5*h+4,5*w+4))
                    [ if c > 9 then c - 9 else c
                    | y <- [0..4]
                    , j <- [0..h]
                    , x <- [0..4]
                    , i <- [0..w]
                    , let c = arr ! (j,i) + x + y ]
  where
    ((0,0),(h,w)) = bounds arr

main :: IO ()
main = do
    arr <- read_arr . lines <$> getContents
    print $ dijkstra (neighbors arr) (0,0) (snd (bounds arr))

    let bigarr = embiggen arr
    print $ dijkstra (neighbors bigarr) (0,0) (snd (bounds bigarr))
