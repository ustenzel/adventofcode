import Data.Time.Clock
import Data.Hashable
import Util

import qualified Data.HashSet as S

-- #############
-- #01x2x3x4x56#
-- ###0#1#2#3###
--   #0#1#2#3#
--   #########

-- | We could pick a more compact representation here; everything could be
-- packed into just two 'Word64's with little effort.  Then it could be
-- unpacked into the 'Heap' below and we could use two nested 'IntSet's
-- instead of a 'HashSet'.  Fewer indirections, better performance.
--
-- But frankly, at this point I'm to lazy to mess with it further.

data Burrow = Burrow { hall :: String, rooms :: [Room] }
  deriving (Eq, Ord, Show)

type Room = String

instance Hashable Burrow where
    hashWithSalt s (Burrow h rs) = hashWithSalt s (h,rs)

-- path from room c to spot x, not including x
path :: Char -> Int -> [Int]
path 'A' 0 = [ 1 ]
path 'A' 1 = [ ]
path 'A' 2 = [ ]
path 'A' 3 = [ 2 ]
path 'A' 4 = [ 2,3 ]
path 'A' 5 = [ 2,3,4 ]
path 'A' 6 = [ 2,3,4,5 ]
path 'B' 0 = [ 1,2 ]
path 'B' 1 = [ 2 ]
path 'B' 2 = [ ]
path 'B' 3 = [ ]
path 'B' 4 = [ 3 ]
path 'B' 5 = [ 3,4 ]
path 'B' 6 = [ 3,4,5 ]
path 'C' 0 = [ 1,2,3 ]
path 'C' 1 = [ 2,3 ]
path 'C' 2 = [ 3 ]
path 'C' 3 = [ ]
path 'C' 4 = [ ]
path 'C' 5 = [ 4 ]
path 'C' 6 = [ 4,5 ]
path 'D' 0 = [ 1,2,3,4 ]
path 'D' 1 = [ 2,3,4 ]
path 'D' 2 = [ 3,4 ]
path 'D' 3 = [ 4 ]
path 'D' 4 = [ ]
path 'D' 5 = [ ]
path 'D' 6 = [ 5 ]

dist :: Char -> Int -> Int
dist 'A' 0 = 3
dist 'A' 1 = 2
dist 'A' 2 = 2
dist 'A' 3 = 4
dist 'A' 4 = 6
dist 'A' 5 = 8
dist 'A' 6 = 9
dist 'B' 0 = 5
dist 'B' 1 = 4
dist 'B' 2 = 2
dist 'B' 3 = 2
dist 'B' 4 = 4
dist 'B' 5 = 6
dist 'B' 6 = 7
dist 'C' 0 = 7
dist 'C' 1 = 6
dist 'C' 2 = 4
dist 'C' 3 = 2
dist 'C' 4 = 2
dist 'C' 5 = 4
dist 'C' 6 = 5
dist 'D' 0 = 9
dist 'D' 1 = 8
dist 'D' 2 = 6
dist 'D' 3 = 4
dist 'D' 4 = 2
dist 'D' 5 = 2
dist 'D' 6 = 3


-- my input encoded:
-- #############
-- #...........#
-- ###D#C#B#C###
--   #D#A#A#B#
--   #########

my_burrow, my_burrow2 :: Burrow
my_burrow = Burrow "......." ["DD","CA","BA","CB"]

-- for part 2, add these lines in the middle:
--   #D#C#B#A#
--   #D#B#A#C#
my_burrow2 = Burrow "......." ["DDDD","CCBA","BBAA","CACB"]

-- #############
-- #...........#
-- ###B#C#B#D###
--   #A#D#C#A#
--   #########

example, example2 :: Burrow
example = Burrow "......." ["BA","CD","BC","DA"]
example2 = Burrow "......." ["BDDA","CCBD","BBAC","DACA"]


-- is the position a legal final position?
final :: Burrow -> Bool
final b = all (=='.') (hall b) &&
          and (zipWith (all . (==)) "ABCD" (rooms b))

-- base cost for one step
cost 'A' = 1
cost 'B' = 10
cost 'C' = 100
cost 'D' = 1000

-- list all legal moves:  cost and new configuration
legal :: Burrow -> [(Int, Burrow)]
legal b0 = concat $ zipWith move_in "ABCD" (rooms b0) ++ zipWith move_out "ABCD" (rooms b0)
  where
    move_out :: Char -> Room -> [(Int, Burrow)]
    move_out c s
        -- if everyone here is in the right room, don't move at all
        | all (liftA2 (||) (== c) (== '.')) s = []
        -- move to any hallway position
        | otherwise = [ ( cost d * (length p0 + dist c x)
                        , set_hallway x d $ set_room c (p0 ++ '.' : s') b0 )
                      | x <- [0..6]
                      , get_hallway x == '.'
                      , path_clear c x ]
      where
        (p0,d:s') = span (=='.') s

    move_in :: Char -> Room -> [(Int, Burrow)]
    move_in c s
        -- can't move into a full room
        | null p0 = []
        -- can't move in if we'd block someone
        | any (liftA2 (&&) (/= c) (/= '.')) s = []
        -- anyone of the correct color currently in the hallway can move in
        | otherwise = [ ( cost c * (length p0 + dist c x -1)
                        , set_hallway x '.' $ set_room c (tail p0 ++ c : s') b0 )
                      | x <- [0..6]
                      , get_hallway x == c
                      , path_clear c x ]
      where
        (p0,s') = span (=='.') s


    -- path_clear c x returns whether the path from room c to spot x is
    -- clear, excluding spot x
    path_clear :: Char -> Int -> Bool
    path_clear c x = all (=='.') $ map get_hallway $ path c x

    get_hallway :: Int -> Char
    get_hallway = (hall b0 !!)

    set_hallway :: Int -> Char -> Burrow -> Burrow
    set_hallway x c (Burrow h rs) = Burrow h' rs
      where
        h' = take x h ++ c : drop (x+1) h

    set_room :: Char -> String -> Burrow -> Burrow
    set_room c r (Burrow h rs) = Burrow h rs'
      where
        i = ord c - ord 'A'
        rs' = take i rs ++ r : drop (i+1) rs


-- A* algorithm
search :: Burrow -> (Int, [Burrow])
search b0 = go S.empty (singletonH (est b0) (0, [b0]))
  where
    go !closed !open
        | final (head b_min) = (c_min, b_min)
        | otherwise   = go (S.insert (head b_min) closed) open'
      where
        (_, (c_min, b_min), open_tail) = viewMinH open
        open' = foldl' (uncurry . insertH) open_tail
                [ (c_min + c + est b, (c_min+c, b:b_min)) | (c,b) <- legal (head b_min), not (S.member b closed) ]


-- optimistic cost estimate
-- (there may be some untapped potential here)
est :: Burrow -> Int
est (Burrow h rs) = sum $ zipWith hall_cost [0..] h ++ zipWith room_cost "ABCD" rs
  where
    hall_cost _ '.' = 0
    hall_cost x  c  = cost c * dist c x

    room_cost c = sum . zipWith (room_cost1 c) [0..] . tails

    room_cost1 c n (d:ds)
        | d == '.'      = 0
        | c /= d        = cost d * (abs (ord d - ord c) * 2 + 2 + n)    -- cost to move to correct room
        | any (/= c) ds = cost d * (n + 4)                              -- cost to move out and back in (to unblock someone)
    room_cost1 _ _ _    = 0


-- debugging helpers
pr :: [(Int, Burrow)] -> IO ()
pr = mapM_ $ \(c,b) -> putStrLn (show c ++ " + ~" ++ show (est b)) >> putStrLn (disp b)

disp :: Burrow -> String
disp (Burrow [h0,h1,h2,h3,h4,h5,h6] rs) =
    unlines $ "#############" : hallway : transpose cols
  where
    cols = "#  " : "#  " : "###" :
           intersperse "###" (map (++ "#") rs) ++
           ["###", "#  ", "#  "]

    hallway = ['#',h0,h1,'.',h2,'.',h3,'.',h4,'.',h5,h6,'#']



timed :: IO a -> IO a
timed k = do t <- getCurrentTime
             r <- k
             t' <- getCurrentTime
             print $ diffUTCTime t' t
             pure r

main :: IO ()
main = do timed $ print $ search example         -- 00.04 sec       (originally 0.4 sec)
          timed $ print $ search example2        -- 1.2 sec         (originally 43 sec)
          timed $ print $ search my_burrow       -- 0.2 sec         (originally 3 sec)
          timed $ print $ search my_burrow2      -- 1.4 sec         (originally 10 min 41 sec)

