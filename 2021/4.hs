import Util
import qualified Data.ByteString.Char8 as B
import qualified Data.IntSet as S

type Card = [[Int]]

parse_cards :: [B.ByteString] -> [Card]
parse_cards [] = []
parse_cards ls = map (unfoldr (B.readInt . B.dropSpace)) u : parse_cards v
  where
    (u,v) = break B.null $ dropWhile B.null ls


is_won :: S.IntSet -> Card -> Maybe Int
is_won ns c
    | any (all (`S.member` ns)) c || any (all (`S.member` ns)) (transpose c)
        = Just $ sum $ filter (not . (`S.member` ns)) $ concat c
    | otherwise
        = Nothing

good :: Card -> Bool
good c = length c == 5 && all ((==) 5 . length) c

main :: IO ()
main = do
    numbers_:cards_ <- B.lines <$> B.getContents

    let numbers = parse_numbers numbers_
        cards   = parse_cards cards_

    True <- pure $ all good cards

    print . uncurry (*) $ partA cards numbers
    print . uncurry (*) $ partB cards numbers


partA :: [Card] -> [Int] -> (Int,Int)
partA cs = go S.empty
  where
    go s (n:ns) = case mapMaybe (is_won s') cs of
        x:_ -> (x,n)
        [ ] -> go s' ns
      where
        s' = S.insert n s


partB :: [Card] -> [Int] -> (Int,Int)
partB = go S.empty
  where
    go s cs (n:ns)
        | length solved == length cs  =  (last solved, n)
        | otherwise                   =  go s' cs' ns
      where
        s' = S.insert n s
        solved = mapMaybe (is_won s') cs
        cs' = filter (isNothing . is_won s') cs
