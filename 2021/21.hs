{-# LANGUAGE BangPatterns, DeriveGeneric, TypeFamilies #-}
import GHC.Generics
import Data.List
import Data.MemoTrie

-- my input:
-- Player 1 starting position: 6
-- Player 2 starting position: 7

my_game s = Game s s 5 6 0
example_game s = Game s s 3 7 0

-- Part 1:  straight forward simulation
data Game = Game { score1 :: !Int           -- counts down
                 , score2 :: !Int
                 , pawn1  :: !Int           -- [0..9]
                 , pawn2  :: !Int           -- [0..9]
                 , rolls  :: !Int }
  deriving (Show, Generic)

turn :: (Game, [Int]) -> (Game, [Int])
turn (!g, a:b:c:ds) =
    let p = (pawn1 g + a + b + c) `mod` 10
    in ( Game (score2 g) (score1 g - p - 1) (pawn2 g) p (rolls g + 3), ds )

det_die = cycle [1..100]

practice = let g = head . filter ((<= 0) . score2) . map fst $ iterate turn (my_game 1000, det_die)
           in (1000 - score1 g) * rolls g


-- Part 2: straight forward recursion and summation, almost.  To finish
-- in acceptable time, we need to memoize intermediate results (aka.
-- dynamic programming.)  Let's use the MemoTrie package for this, just
-- to show off what lazy evaluation can do!
--
instance HasTrie Game where
    newtype (Game :->: b) = GameTrie { unGameTrie :: Reg Game :->: b }
    trie = trieGeneric GameTrie
    untrie = untrieGeneric unGameTrie
    enumerate = enumerateGeneric unGameTrie

dirac = uncurry max $ memoFix all_plays (my_game 21)

-- Returns number of universes in which the first (second) player wins.
all_plays :: (Game -> (Integer, Integer)) -> Game -> (Integer, Integer)
all_plays self g
    | score2 g <= 0 = (0,1)         -- player two wins
    | otherwise =
        -- cast the die thrice:
        -- 1x3, 3x4, 6*5, 7x6, 6*7, 3x8, 1x9
        uncurry (flip (,)) $ sum2 $ zipWith mul2 [1,3,6,7,6,3,1] $
            [ self $ Game (score2 g) (score1 g - p - 1) (pawn2 g) p 0
            | d <- [3..9]
            , let p = (pawn1 g + d) `mod` 10 ]

  where
    mul2 :: Integer -> (Integer, Integer) -> (Integer, Integer)
    mul2 a (b,c) = (a*b,a*c)

    sum2 :: [(Integer, Integer)] -> (Integer, Integer)
    sum2 = foldl' (\(!a,!b) (c,d) -> (a+c,b+d)) (0,0)

