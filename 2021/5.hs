{-# LANGUAGE OverloadedStrings #-}
import Control.Monad
import Data.List
import qualified Data.ByteString.Char8 as B
import qualified Data.Map.Strict as M

parse_segment :: B.ByteString -> IO ((Int,Int),(Int,Int))
parse_segment s0 =
    maybe (fail $ "parse error in " ++ show s0) pure $ do
        (x1,s1) <- B.readInt s0
        (y1,s2) <- B.readInt =<< B.stripPrefix "," s1
        (x2,s3) <- B.readInt . B.dropSpace =<< B.stripPrefix "->" (B.dropSpace s2)
        (y2,s4) <- B.readInt =<< B.stripPrefix "," s3
        guard $ B.null s4
        Just $ ((x1,y1),(x2,y2))

main :: IO ()
main = print . solve True =<< mapM parse_segment . B.lines =<< B.getContents

solve :: Bool -> [((Int,Int),(Int,Int))] -> Int
solve part_b = M.size . M.filter (>1) . foldl' go M.empty
  where
    go seen ((x1,y1),(x2,y2))
        | x1 == x2                =  M.unionWith (+) seen $ M.fromList [ ((x1,y),1) | y <- [min y1 y2 .. max y1 y2] ]
        | y1 == y2                =  M.unionWith (+) seen $ M.fromList [ ((x,y1),1) | x <- [min x1 x2 .. max x1 x2] ]
        | not part_b              =  seen
        | (x1 > x2) == (y1 > y2)  =  M.unionWith (+) seen $ M.fromList [ ((min x1 x2 + d,min y1 y2 + d),1) | d <- [0 .. abs (x2-x1)] ]
        | otherwise               =  M.unionWith (+) seen $ M.fromList [ ((min x1 x2 + d,max y1 y2 - d),1) | d <- [0 .. abs (x2-x1)] ]
