import Control.Applicative
import Control.Monad
import Data.Array.IO
import Data.Char

main = print =<< mainB =<< newListArray universe . map digitToInt . concat . lines =<< getContents

mainA :: IOUArray (Int,Int) Int -> IO Int
mainA arr = sum <$> replicateM 100 (step arr)

mainB :: IOUArray (Int,Int) Int -> IO Int
mainB arr = loop 1
  where
    loop i = do n <- step arr
                if n == 100 then pure i
                            else loop $! i+1

universe :: ((Int,Int),(Int,Int))
universe = ((0,0),(9,9))

step :: IOUArray (Int,Int) Int -> IO Int
step arr = do
    forM_ (range universe) $ \p -> readArray arr p >>= writeArray arr p . (+) 1
    sum <$> forM (range universe) tryflash

  where
    tryflash :: (Int,Int) -> IO Int
    tryflash (x,y) = do
        e <- readArray arr (x,y)
        if e > 9 then do
            writeArray arr (x,y) 0
            fmap ((+) 1 . sum) . forM
                (filter (inRange universe) (liftA2 (,) [x-1,x,x+1] [y-1,y,y+1])) $ \p' -> do
                    e' <- readArray arr p'
                    if e' > 0 then do
                        writeArray arr p' (e' + 1)
                        tryflash p'
                      else pure 0
          else pure 0

