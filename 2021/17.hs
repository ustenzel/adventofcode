{-
# Part 1 without programming

Movement along the x and y axes happens independently from each other.

## X axis

If launched at velocity v, the probe travels a total distance of v +
(v-1) + (v-2) + ... + 1 to the right and then stays there.  After
sufficiently many steps, x will then be $ v (v+1) / 2 $.  Therefore, if
there is any such number within the target area, it is easy to hit.  I'm
not going to bother and just assume such a number exists.

## Y axis

If launched upwards at velocity v, the probe travels to a height of $ v
(v+1) / 2 $.  It then falls back and will again reach a height of 0.  At
that moment, the vertical velocity will be (-(v+1)).  (Velocity is
updated after position.)

At the next moment, y will be (-(v+1)).  We want this to be as fast as
possible, so we make it hit the lower boundary of the target area:
v := - y\_min - 1

That makes the maximum height equal to $ -y\_min * (-y\_min - 1) / 2 $.
-}

part1 ((_,_),(ymin,_)) = ymin * (ymin+1) `div` 2

-- Part 2: too complicated to solve by thinking.

target_area = ((81,129),(-150,-108))
example_area = ((20,30),(-10,-5))

step (x,y,u,v) = (x+u,y+v,max 0 (u-1), v-1)

hits ((xmin,xmax),(ymin,ymax)) = go
  where
    go ((x,y,u,v):ps)
        | xmin <= x && x <= xmax && ymin <= y && y <= ymax = True
        | u == 0 && x < xmin = False
        | x > xmax = False
        | y < ymin = False
        | otherwise = go ps

part2 a@((xmin,xmax),(ymin,ymax)) = [ (u,v) | u <- [0..xmax], v <- [ymin..(-ymin)], hits a (iterate step (0,0,u,v)) ]

main = do
    print $ part1 target_area
    print $ length $ part2 target_area
