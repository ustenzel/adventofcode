{-# LANGUAGE BangPatterns #-}

import Control.Monad
import Control.Monad.Trans.State.Strict
import Data.Bits
import Data.Bool
import Data.Char
import Data.List

toBits :: String -> [Bool]
toBits = concatMap (\c -> [ testBit c 3, testBit c 2, testBit c 1, testBit c 0 ]) . map digitToInt

data Packet = Packet Word Packet' deriving Show

data Packet' = Literal Word
             | Operator Word [Packet]
  deriving Show

type P = State [Bool]

getBit :: P Bool
getBit = state $ \(b:bs) -> (b,bs)

getBits :: Int -> P Word
getBits = go 0
  where
    go !acc  0 = pure acc
    go !acc !n = do b <- getBit
                    go (shiftL acc 1 .|. bool 0 1 b) (n-1)

exhaust :: P a -> P [a]
exhaust p = do bs <- get
               if null bs then pure [] else liftM2 (:) p (exhaust p)

getLiteral :: P Word
getLiteral = go 0
  where
    go !acc = do e <- getBit
                 w <- getBits 4
                 bool pure go e $ shiftL acc 4 .|. w

getOperator :: P [Packet]
getOperator = getBit >>= getPackets
  where
    getPackets False = do bitlen <- getBits 15
                          (sub,rest) <- gets (splitAt (fromIntegral bitlen))
                          put sub
                          ps <- exhaust getPacket
                          put rest
                          pure ps

    getPackets True  = do plen <- getBits 11
                          replicateM (fromIntegral plen) getPacket


getPacket :: P Packet
getPacket = do
    vrsn <- getBits 3
    tid  <- getBits 3
    Packet vrsn <$> case tid of
        4 -> Literal      <$> getLiteral
        _ -> Operator tid <$> getOperator

sumVersions :: Packet -> Word
sumVersions = go 0
  where
    go !acc (Packet vrsn (Literal     _)) = acc + vrsn
    go !acc (Packet vrsn (Operator _ ps)) = foldl' go (acc+vrsn) ps

runProg (Packet _ (Literal w)) = w
runProg (Packet _ (Operator o ps)) = op o $ map runProg ps
  where
    op 0 = sum
    op 1 = product
    op 2 = minimum
    op 3 = maximum
    op 5 = bop (>)
    op 6 = bop (<)
    op 7 = bop (==)

    bop f [p,q] = bool 0 1 $ f p q


main = do
    pack <- evalState getPacket . toBits <$> getContents
    print $ sumVersions pack
    print $ runProg pack
    print pack
