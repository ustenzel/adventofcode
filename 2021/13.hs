import Data.List
import qualified Data.Set as S

-- store (y,x) instead of the more common (x,y), because then the
-- ascending order is convenient for printing
read_dot :: String -> (Int,Int)
read_dot s = head [ (y,x)
                  | (x, ',':s') <- reads s
                  , (y, "")     <- reads s' ]

data Fold = X Int | Y Int deriving Show

read_fold :: String -> Fold
read_fold s = head [ axis i
                   | ("fold along ", x:'=':s') <- pure (splitAt 11 s)
                   , axis <- if x == 'x' then [X] else if x == 'y' then [Y] else []
                   , (i,"") <- reads s' ]

apply_fold :: Fold -> S.Set (Int,Int) -> S.Set (Int,Int)
apply_fold (X z) = S.map (\(y,x) -> (y, min x (2*z-x)))
apply_fold (Y z) = S.map (\(y,x) -> (min y (2*z-y), x))

print_grid :: S.Set (Int,Int) -> [String]
print_grid = go (0,0) . S.toAscList
  where
    go _ [] = [[]]
    go (j,i) ((y,x):ps) | j == y     =  let l1:ls = go (j,x+1) ps in (replicate (x-i) ' ' ++ '#' : l1) : ls
                        | otherwise  =  [] : go (j+1,0) ((y,x):ps)

main :: IO ()
main = do
    (dots_, "":folds_) <- break null . lines <$> getContents

    let dots  = S.fromList $ map read_dot dots_
    let folds = map read_fold folds_

    -- part 1
    print $ S.size $ apply_fold (head folds) dots

    -- part 2
    putStr . unlines . print_grid . foldl' (flip apply_fold) dots $ folds
