main  = -- main' countA
        main' countB

main' :: ([Int] -> [Bool]) -> IO ()
main' f = print . sum . map fromEnum . f . map read . lines =<< getContents

countA :: [Int] -> [Bool]
countA xs = zipWith (<) xs (drop 1 xs)

countB :: [Int] -> [Bool]
countB xs = zipWith (<) xs (drop 3 xs)

