import Util
import qualified Data.ByteString.Char8 as B

main :: IO ()
main = print . partB . parse_numbers =<< B.getContents

partA :: [Int] -> Int
partA xs = sum $ map abs $ map (subtract median) xs
  where
    median = sort xs !! div (length xs) 2

partB :: [Int] -> Int
partB xs = total_fuel floor_mean `min` total_fuel ceiling_mean
  where
    fuel m x = let d = abs (x - m) in div (d * (d+1)) 2
    total_fuel m = sum $ map (fuel m) xs
    floor_mean = sum xs `div` length xs
    ceiling_mean = floor_mean + 1
