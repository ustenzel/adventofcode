{-# Language BangPatterns #-}
module AoC.Dijkstra where

import           Data.Foldable
import qualified Data.Set               as S

-- | Dijkstra' algorithm is its common form.  Only one cheapest path to
-- any node is considered.
dijkstra ::
    (Ord cost, Eq node, Ord node )
    => (cost -> node -> [(cost,node)])   -- ^ list neighbors of a node
    -> (node -> Bool)                    -- ^ determine whether a target has been reached
    -> [ (cost,node) ]                   -- ^ initial set of nodes
    -> Maybe (cost,node)
dijkstra = dijkstra' const

-- | Dijkstra's algorithm, as general as I could make it.  If a node can
-- be reached by multiple cheapest paths, their cost is combined with a
-- custom function.
dijkstra' ::
    (Ord cost, Eq node, Ord node )
    => (cost -> cost -> cost)            -- ^ combine cost of two paths
    -> (cost -> node -> [(cost,node)])   -- ^ list neighbors of a node
    -> (node -> Bool)                    -- ^ determine whether a target has been reached
    -> [ (cost,node) ]                   -- ^ initial set of nodes
    -> Maybe (cost,node)

dijkstra' (?) neighbors finished = go S.empty . foldl' (\pq (c,n) -> insertPQ (?) c n pq) EmptyPQ
  where
    go !closed !open = case viewMinPQ (?) open of
        Nothing -> Nothing
        Just ((c,n), pq) | finished n        -> Just (c,n)
                         | S.member n closed -> go closed pq
                         | otherwise         -> go (S.insert n closed) open'
            where
                open' = foldl' (\q (c',n') -> insertPQ (?) c' n' q) pq $
                        filter (not . flip S.member closed . snd) (neighbors c n)

-- | A minimum skew heap.  Orders by k first, then by v for equal k.
data PQ k v = EmptyPQ | NodePQ (PQ k v) !k !v (PQ k v) deriving Show

insertPQ :: (Ord k, Ord v) => (k->k->k) -> k -> v -> PQ k v -> PQ k v
insertPQ (?) !k !v = unionPQ (?) (NodePQ EmptyPQ k v EmptyPQ)

viewMinPQ :: (Ord k, Ord v) => (k->k->k) -> PQ k v -> Maybe ((k,v), PQ k v)
viewMinPQ  _   EmptyPQ         = Nothing
viewMinPQ (?) (NodePQ l k v r) = Just ((k,v), unionPQ (?) l r)

unionPQ :: (Ord k, Ord v) => (k->k->k) -> PQ k v -> PQ k v -> PQ k v
unionPQ (?) = go
  where
    go EmptyPQ pq = pq
    go pq EmptyPQ = pq
    go q1@(NodePQ l1 k1 v1 r1) q2@(NodePQ l2 k2 v2 r2)
        | (k1,v1) < (k2,v2)  =  NodePQ (go r1 q2) k1 v1 l1
        | (k1,v1) > (k2,v2)  =  NodePQ (go q1 r2) k2 v2 l2
        | otherwise          =  go (NodePQ EmptyPQ (k1 ? k2) v1 EmptyPQ) $ go l1 r1 `go` go l2 r2

