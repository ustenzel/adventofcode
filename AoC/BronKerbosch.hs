module AoC.BronKerbosch where

import           Data.List
import           Data.Ord
import           Data.Set       ( Set )
import qualified Data.Set       as S

type Neighborhood node = node -> Set node

-- | A function to pick a pivot.  It is called as @pivot p x@ from the
-- inner loop of 'bronKerbosch' and should pick some node from
-- @p `union` x@.
type Pivot node = Set node -> Set node -> node

-- | Bron Kerbosch algorithm.  Takes a graph in the form of a set of
-- nodes and a neighborhood function, and a pivoting function.  Returns
-- a list of maximal cliques in no particular order.
bronKerbosch :: Ord node => Set node -> Neighborhood node -> Pivot node -> [ Set node ]
bronKerbosch all_nodes neighbors pivot = go S.empty all_nodes S.empty []
  where
    go !r !p !x
        | S.null p  = if S.null x then (:) r else id
        | otherwise = loop r p x (S.toList $ p S.\\ (neighbors $ pivot p x))

    loop !r !p !x [    ] = id
    loop !r !p !x (v:vs) =
        go (S.insert v r) (S.intersection p (neighbors v)) (S.intersection x (neighbors v)) .
        loop r (S.delete v p) (S.insert v x) vs

-- | Picks the node with the biggest neighborhood from p.
greedyPivot :: Ord node => Neighborhood node -> Pivot node
greedyPivot neighbors p _ = snd $ S.foldl' step (0, error "empty set") p
  where
    step (m,u) v = let n = S.size (neighbors v) in if n >= m then (n, v) else (m,u)

-- | Picks node u from @union p x@ with the biggest
-- @intersection p (neighborhood u)@.  This is more expensive than
-- 'greedyPivot', but supposedly better on certains pathological graphs.
tamitaPivot :: Ord node => Neighborhood node -> Pivot node
tamitaPivot neighbors p x =
    snd $ S.foldl' step (S.foldl' step (0, error "empty sets") p) x
  where
    step (m,u) v = let n = S.size (p `S.intersection` neighbors v) in if n >= m then (n, v) else (m,u)

