import Control.Monad
import Data.Char
import Data.List
import Data.Maybe

validA assocs = all (`elem` keys) mand_keys
  where
    keys = map fst assocs
    mand_keys = words "byr iyr eyr hgt hcl ecl pid"

validB assocs = isJust $ do
    -- byr (Birth Year) - four digits; at least 1920 and at most 2002.
    [(byr,"")] <- reads <$> lookup "byr" assocs
    guard $ 1920 <= byr && byr <= (2002 :: Int)

    -- iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    [(iyr,"")] <- reads <$> lookup "iyr" assocs
    guard $ 2010 <= iyr && iyr <= (2020 :: Int)

    -- eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    [(eyr,"")] <- reads <$> lookup "eyr" assocs
    guard $ 2020 <= eyr && eyr <= (2030 :: Int)

    -- hgt (Height) - a number followed by either cm or in:
    -- If cm, the number must be at least 150 and at most 193.
    -- If in, the number must be at least 59 and at most 76.
    [(hgt,unit)] <- reads <$> lookup "hgt" assocs
    guard $ case unit of
        "cm" -> 150 <= hgt && hgt <= 193
        "in" -> 59 <= hgt && hgt <= 76
        _    -> False

    -- hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    '#' : hcl <- lookup "hcl" assocs
    guard $ length hcl == 6 && all isHexDigit hcl

    -- ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    ecl <- lookup "ecl" assocs
    guard $ ecl `elem` words "amb blu brn gry grn hzl oth"

    -- pid (Passport ID) - a nine-digit number, including leading zeroes.
    pid <- lookup "pid" assocs
    guard $ length pid == 9 && all isDigit pid

    -- cid (Country ID) - ignored, missing or not.
    pure ()


main = do
    let split1 w = case break (==':') w of (l,':':r) -> (l,r)
    pps <- map (map split1 . concatMap words) . groupBy (\_ -> not . all isSpace) . lines <$> getContents

    print $ length $ filter validA pps
    print $ length $ filter validB pps
