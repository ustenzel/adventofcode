import Data.List
import Data.Maybe

invalid xs = if null [ () | x <- ys, y <- ys, x+y == x0 ] then Just x0 else Nothing
  where
    ys = init xs
    x0 = last xs

tryhit y = go 0 []
  where
    go acc rng _ | acc == y = Just rng
                 | acc >  y = Nothing
    go acc rng []           = Nothing
    go acc rng (x:xs)       = go (acc+x) (x:rng) xs

main = do
    inp <- map read . lines <$> readFile "input09.txt"

    let tgt : _ = mapMaybe invalid $ map (take 26) $ tails inp
    print tgt

    print . (\rng -> minimum rng + maximum rng) . head . filter ((>1) . length) . mapMaybe (tryhit tgt) $ tails inp
