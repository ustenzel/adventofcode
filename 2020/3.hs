import qualified Data.ByteString.Char8 as C

get_at l i = C.index l $ i `mod` C.length l

partA grid step = length $ filter (=='#') $ zipWith (\l i -> get_at l (step*i)) grid [0..]

partB grid = length $ filter (=='#') $ zipWith (\l i -> if even i then get_at l (i `div` 2) else ' ') grid [0..]

main = do
    grid <- C.lines <$> C.getContents

    print $ partA grid 3

    print . product . map (fromIntegral :: Int -> Integer) $ partB grid : map (partA grid) [1,3,5,7]
