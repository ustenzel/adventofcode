import Control.Arrow
import Data.List
import Data.Int

-- element i of (numWays xs) is the number of ways in which (drop i xs)
-- can be connected in series
numWays :: [Int64] -> [Int64]
numWays [_] = [1]
numWays (x:xs) = sum (map snd $ takeWhile ((<= x+3) . fst) $ zip xs ys) : ys
  where
    ys = numWays xs


main = do
    xs <- map read . lines <$> readFile "input10.txt"

    let ys = sort xs
        ds = zipWith (flip (-)) (0:ys) ys ++ [3]

    -- partA
    let n1 = length $ filter (== 1) ds
        n3 = length $ filter (== 1) ds
    print $ n1 * n3

    -- partB, w/ sanity check
    print . head $ numWays (0:sort ex1)
    print . head $ numWays (0:sort ex2)
    print . head $ numWays (0:ys)


ex1 = map read $ words "16 10 15 5 1 11 7 19 6 12 4"
ex2 = map read $ words "28 33 18 42 31 14 46 20 48 47 24 23 49 45 19 38 39 11 1 32 25 35 8 17 7 9 4 2 34 10 3"

