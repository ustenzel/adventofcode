{-# LANGUAGE BangPatterns #-}
import Prelude ()
import BasePrelude
import qualified Data.Map.Strict as M

type Grid = M.Map (Int,Int,Int)
type GridB = M.Map (Int,Int,Int,Int)

my_input =
    [ ".###.###"
    , ".#.#...#"
    , "..##.#.."
    , "..##..##"
    , "........"
    , "##.#.#.#"
    , "..###..."
    , ".####..." ]


fromInput :: [String] -> Grid ()
fromInput ls = M.fromList [ ((x,y,0), ())
                          | (y,l) <- zip [0..] ls
                          , (x,'#') <- zip [0..] l ]

fromInputB :: [String] -> GridB ()
fromInputB ls = M.fromList [ ((x,y,0,0), ())
                           | (y,l) <- zip [0..] ls
                           , (x,'#') <- zip [0..] l ]

step :: Grid a -> Grid ()
step grd = void $ M.filterWithKey good nbrs
  where
    nbrs = M.fromListWith (+)
                [ ((x+u,y+v,z+w), 1)
                | (x,y,z) <- M.keys grd
                , u <- [-1,0,1]
                , v <- [-1,0,1]
                , w <- [-1,0,1]
                , u /= 0 || v /= 0 || w /= 0 ]

    good k n = n == 3 || n == 2 && M.member k grd

stepB :: GridB a -> GridB ()
stepB grd = void $ M.filterWithKey good nbrs
  where
    nbrs = M.fromListWith (+)
                [ ((x+u,y+v,z+w,a+t), 1)
                | (x,y,z,a) <- M.keys grd
                , u <- [-1,0,1]
                , v <- [-1,0,1]
                , w <- [-1,0,1]
                , t <- [-1,0,1]
                , u /= 0 || v /= 0 || w /= 0 || t /= 0]

    good k n = n == 3 || n == 2 && M.member k grd


main = do
    print . M.size . (!! 6) . iterate step $ fromInput my_input
    print . M.size . (!! 6) . iterate stepB $ fromInputB my_input

