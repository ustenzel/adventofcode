import Data.List

parse acc [    ] = acc
parse acc (c:cs) = parse (2*acc+v) cs
  where
    v = case c of 'F' -> 0
                  'B' -> 1
                  'L' -> 0
                  'R' -> 1

me (a:b:cs) = if a + 2 == b then a + 1 else me (b:cs)

main = do
    seats <- sort . map (parse 0) . lines <$> getContents
    print $ last seats
    print $ me seats
