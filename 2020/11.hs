import Control.Monad
import Data.Bifunctor
import qualified Data.Vector as V
import qualified Data.Vector.Unboxed as U
import Data.Bits
import Data.List
import Data.Word

type Arr = V.Vector (U.Vector Word8)

c2w :: Char -> Word8
c2w '.' = 0
c2w 'L' = 1
c2w '#' = 2

w2c 0 = '.'
w2c 1 = 'L'
w2c 2 = '#'

count :: Arr -> Int
count = V.sum . V.map (U.sum . U.map (fromIntegral . flip shiftR 1))

showGrid :: Arr -> String
showGrid = concatMap ((++ "\n") . map w2c . U.toList) . V.toList

raw0 =
    "L.LL.LL.LL\n\
    \LLLLLLL.LL\n\
    \L.L.L..L..\n\
    \LLLL.LL.LL\n\
    \L.LL.LL.LL\n\
    \L.LLLLL.LL\n\
    \..L.L.....\n\
    \LLLLLLLLLL\n\
    \L.LLLLLL.L\n\
    \L.LLLLL.LL\n"

step :: Arr -> Arr
step arr = V.imap (U.imap . step1) arr
  where
    step1 i j 0 = 0
    step1 i j 1 = if occ i j == 0 then 2 else 1
    step1 i j 2 = if occ i j >= 4 then 1 else 2

    occ i j = sum [ maybe 0 (flip shiftR 1) c
                  | u <- [-1,0,1]
                  , v <- [-1,0,1]
                  , u /= 0 || v /= 0
                  , let c = arr V.!? (i+u) >>= (U.!? (j+v)) ]

stepB :: Arr -> Arr
stepB arr = V.imap (U.imap . step1) arr
  where
    step1 i j 0 = 0
    step1 i j 1 = if occ i j == 0 then 2 else 1
    step1 i j 2 = if occ i j >= 5 then 1 else 2

    occ i j = sum [ 1
                  | u <- [-1,0,1]
                  , v <- [-1,0,1]
                  , u /= 0 || v /= 0
                  , Just 2 <- take 1 $
                              dropWhile (== Just 0) $
                              map (\(a,b) -> arr V.!? a >>= (U.!? b)) $
                              tail $
                              iterate (bimap (+u) (+v)) (i,j) ]

main = do
    raw <- readFile "input11.txt"

    let arr0  = V.fromList . map (U.fromList . map c2w) .  lines $ raw
        arrsA = iterate step arr0
        arrsB = iterate stepB arr0

    -- mapM_ putStrLn . map showGrid $ take 3 arrsB

    print $ count $ fst $ head $ filter (uncurry (==)) $ zip arrsA (tail arrsA)
    print $ count $ fst $ head $ filter (uncurry (==)) $ zip arrsB (tail arrsB)
