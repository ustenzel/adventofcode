import Data.List
import Data.Int

card_pubkey = 9232416
door_pubkey = 14144084

step subj val = val * subj `mod` modulus


subj = 7
modulus = 20201227 :: Int64

-- card_pubkey = subj ^ card_loop_size `mod` modulus
-- door_pubkey = subj ^ door_loop_size `mod` modulus


Just card_loop_size = elemIndex card_pubkey $ iterate (step subj) 1
Just door_loop_size = elemIndex door_pubkey $ iterate (step subj) 1

-- subj ^ (card_loop_size + door_loop_size) `mod` modulus
{- enc_key = door_pubkey ^ card_loop_size `mod` modulus
        = (subj ^ door_loop_size) ^ card_loop_size `mod` modulus
        = (subj ^ card_loop_size) ^ door_loop_size `mod` modulus
        = card_pubkey ^ door_loop_size `mod` modulus-}

-- transform subj num = subj ^ num `mod` modulus

transform s 0 = 1
transform s n | even n = (y*y) `mod` modulus
              | odd  n = (((y*y) `mod` modulus) * s) `mod` modulus
  where
    y = transform s (div n 2)

enc_key = transform card_pubkey door_loop_size

