{-# LANGUAGE BangPatterns, LambdaCase #-}
import Prelude ()
import BasePrelude

data Expr = Num Integer | Add Expr Expr | Mul Expr Expr
  deriving Show

runParser :: P a -> String -> a
runParser (P p) s = case filter (null . snd) . p $ s of
    [(a,"")] -> a


newtype P a = P { runP :: String -> [(a, String)] }

instance Functor P where
    fmap f (P p) = P $ map (first f) . p

instance Applicative P where
    pure a = P $ \s -> [(a,s)]
    P a <*> P b = P $ \s -> [ (u v,s'') | (u,s') <- a s, (v,s'') <- b s' ]

instance Monad P where
    return = pure
    P a >>= k = P $ \s -> concat [ runP (k u) s' | (u,s') <- a s ]

instance Alternative P where
    empty = P $ const []
    P a <|> P b = P $ \s -> a s ++ b s


p_expr_A = chain_l p_op p_atom_A

p_expr_B = chain_l (Mul <$ char '*' <* p_space) p_sum
p_sum = chain_l (Add <$ char '+' <* p_space) p_atom_B

p_atom_A = Num <$> p_int <|> p_open *> p_expr_A <* p_close
p_atom_B = Num <$> p_int <|> p_open *> p_expr_B <* p_close

p_open = char '(' *> p_space
p_close = char ')' *> p_space
p_space = P $ \s -> [((), dropWhile isSpace s)]

p_int :: P Integer
p_int = P $ map (second (dropWhile isSpace)) . reads

p_op :: P (Expr -> Expr -> Expr)
p_op = Add <$ char '+' <* p_space <|>
       Mul <$ char '*' <* p_space

char c = P $ \case c':cs | c == c' -> [(c,cs)] ; _ -> []

chain_l sep p = do a <- p
                   go a
  where
    go a = do f <- sep
              b <- p
              go (f a b)
           <|> pure a

eval (Num a) = a
eval (Add a b) = eval a + eval b
eval (Mul a b) = eval a * eval b

main = do
    es <- map (runParser p_expr_A) . lines <$> readFile "input18.txt"
    print . sum $ map eval es

    es <- map (runParser p_expr_B) . lines <$> readFile "input18.txt"
    print . sum $ map eval es
