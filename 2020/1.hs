import Prelude ()
import BasePrelude
import qualified Data.ByteString.Char8 as C
import qualified Data.IntSet as S

ints :: C.ByteString -> [Int]
ints s = case C.readInt s of
    _ | C.null s -> []
    Nothing      -> error "parse error"
    Just (x,t)   -> x : ints (C.dropWhile isSpace t)


main = do
    is <- ints <$> C.getContents
    print $ foldM (\s x -> if S.member (2020 - x) s
                           then Left (x * (2020-x))
                           else pure (S.insert x s)) S.empty is

-- main' = do
    --is <- S.fromList . ints <$> C.getContents
    print $ goA (S.fromList is)
    print $ goB (S.fromList is)

goA :: S.IntSet -> [Int]
goA s = S.foldr (\x -> if S.member (2020 - x) s then (:) (x * (2020-x)) else id) [] s

goB :: S.IntSet -> [Int]
goB s = S.foldr (\x nil ->
            S.foldr (\y -> if S.member (2020 - x - y) s then (:) (x * y * (2020-x-y)) else id)
                    nil s) [] s
