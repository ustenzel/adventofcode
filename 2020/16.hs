{-# LANGUAGE BangPatterns #-}
import Data.Bifunctor
import Data.List
import Data.Ord

runAll ls = -- part A:  sum $ filter (surely_invalid constraints) $ concat other_tickets
            map (bimap id (map c_name)) $ sortBy (comparing (length . snd)) $ zip my_ticket valid_fields
            -- did the rest manually.  YOLO
  where
    (constraints, [] : "your ticket:" : tk0 : [] : "nearby tickets:" : tks)
        = bimap (map to_constraint) id $ break null ls
    my_ticket = read_ticket tk0
    other_tickets = map read_ticket tks
    good_tickets = filter (not . any (surely_invalid constraints)) other_tickets

    valid_fields = map (\vs -> filter (\c -> all (valid c) vs) constraints) $ transpose (my_ticket : good_tickets)

data Con = Con { c_name :: String, c_r1 :: (Int, Int), c_r2 :: (Int, Int) }
  deriving Show

surely_invalid :: [Con] -> Int -> Bool
surely_invalid cs v = not $ any (`valid` v) cs

valid :: Con -> Int -> Bool
valid (Con _ (a,b) (c,d)) v = a <= v && v <= b || c <= v && v <= d

to_constraint s = Con name (to_range r1) (to_range r2)
  where
    (name, ':':' ':s1) = break (==':') s
    [r1, "or", r2] = words s1

to_range :: String -> (Int, Int)
to_range s = (read w1, read w2)
  where
    (w1, '-':w2) = break (=='-') s

read_ticket :: String -> [Int]
read_ticket s = case break (==',') s of
    (l,',':r) -> read l : read_ticket r
    (l,"") -> [read l]

main = readFile "input16.txt" >>= mapM_ print . runAll . lines
