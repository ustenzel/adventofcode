import Data.Bits
import Data.Bool
import Data.List
import Data.Word
import qualified Data.IntMap.Strict as I

data Cmd = Mask !Word64 !Word64 !Word64
         | Store !Int !Word64

rd_cmd ["mask", "=", m ] = Mask set (complement clear) float
  where
    set = foldl' (\a c -> shiftL a 1 .|. bool 0 1 (c == '1')) 0 m
    clear = foldl' (\a c -> shiftL a 1 .|. bool 0 1 (c == '0')) 0 m
    float = foldl' (\a c -> shiftL a 1 .|. bool 0 1 (c == 'X')) 0 m

rd_cmd [m, "=", v] = Store (read $ init $ drop 4 m) (read v)

run = foldl' step (I.empty, 0, complement 0)
  where
    step (mem, set, clear) (Mask s c _) = (mem, s, c)
    step (mem, set, clear) (Store a v) = (mem', set, clear)
     where
        mem' = I.insert a (v .&. clear .|. set) mem

-- w/ hashmap:        0.74user 0.05system 0:00.84elapsed 95%CPU (0avgtext+0avgdata 17584maxresident)k
-- w/ nested intmaps: 0.48user 0.03system 0:00.52elapsed 98%CPU (0avgtext+0avgdata 10580maxresident)k


runB = foldl' step (I.empty, 0, 0)
  where
    step (mem, set, float) (Mask s _ f) = (mem, s, f)
    step (mem, set, float) (Store a0 v) = (mem', set, float)
     where
        mem' = foldr (\a -> I.alter (maybe (Just $ I.singleton (fromIntegral $ (a .|. set) .&. 0xFFFFFF) v)
                                           (Just . I.insert ((fromIntegral $ a .|. set) .&. 0xFFFFFF) v))
                                    (fromIntegral $ (a .|. set) `shiftR` 24))
                     mem (addrs 0)

        addrs 36 = [fromIntegral a0]
        addrs n | testBit float n = map (`setBit` n) (addrs (n+1)) ++ map (`clearBit` n) (addrs (n+1))
                | otherwise       = addrs (n+1)


main = do
    prg <- map (rd_cmd . words) . lines <$> readFile "input14.txt"

    case run prg of (mem, _, _) -> print $ sum mem
    case runB prg of (mem, _, _) -> print $ foldl' (\a m -> a + sum m) 0 mem

