{-# LANGUAGE BangPatterns #-}
import Data.List

stepA (!x,!y,!h) c d = case c of
    'E' -> (x+d,y,h)
    'N' -> (x,y+d,h)
    'W' -> (x-d,y,h)
    'S' -> (x,y-d,h)
    'L' -> (x,y,(h+d) `rem` 360)
    'R' -> (x,y,(360+h-d) `rem` 360)
    'F' -> case h of
        0   -> (x+d,y,h)
        90  -> (x,y+d,h)
        180 -> (x-d,y,h)
        270 -> (x,y-d,h)


stepB (!x,!y,!u,!v) c d = case c of
    'E' -> (x,y,u+d,v)
    'N' -> (x,y,u,v+d)
    'W' -> (x,y,u-d,v)
    'S' -> (x,y,u,v-d)
    'L' -> case d of 0 -> (x,y,u,v)
                     90 -> (x,y,-v,u)
                     180 -> (x,y,-u,-v)
                     270 -> (x,y,v,-u)
    'R' -> case d of 0 -> (x,y,u,v)
                     90 -> (x,y,v,-u)
                     180 -> (x,y,-u,-v)
                     270 -> (x,y,-v,u)
    'F' -> (x+d*u,y+d*v,u,v)


main = do
    (x,y,h) <- foldl' (\p s -> stepA p (head s) (read $ tail s)) (0,0,0) . lines <$> readFile "input12.txt"
    print $ abs x + abs y

    (x,y,u,v) <- foldl' (\p s -> stepB p (head s) (read $ tail s)) (0,0,10,1) . lines <$> readFile "input12.txt"
    print $ abs x + abs y
