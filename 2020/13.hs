import Data.Ord
import Data.List
import Data.Int

t0 = 1005595
ps = [41,37,557,29,13,17,23,419,19]

partA = uncurry (*) . minimumBy (comparing snd) . zip ps $
        [ p - rem t0 p | p <- ps ]


-- partB
-- Find the smallest t such that t `mod` 41 == 0, t `mod` 37 == 1, etc

cs :: (Eq a, Num a, Enum a) => [(a,a)]
cs = filter ((/=) 0 . snd) $ zip [0..]
        -- [7,13,x,x,59,x,31,19]
        [41,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
        ,x,x,37,x,x,x,x,x,557,x,29,x,x,x,x,x,x,x,x,x,x,13,x,x,x,17,x,x,x,x
        ,x,23,x,x,x,x,x,x,x,419,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,19]
    where x = 0


allValid t = all (\(r,p) -> 0 == rem (t+r) p) cs

-- each constraint (r,p) meant t === -r mod p; therefore, we have to
-- negate the solution, then find the remainder to get the canonical
-- answer
partB :: Integer
partB = mod (-r) p
  where
    (r,p) = foldl1 (~~) cs

partB' :: Int64
partB' = solve_chinese_remainder [ (-a,p) | (a,p) <- cs ]

main = do print partA
          putStrLn $ show partB ++ if allValid partB then " OK" else " fail"
          putStrLn $ show partB' ++ if allValid partB' then " OK" else " fail"


-- direct solution of simultaneous congruences as per wikipedia
-- While this should work even if the ps are not mutually prime, it
-- won't fit into a 64 bit integer.  Not a problem if 'Integer' is
-- available, still interesting.
(a,n) ~~ (b,m) = (mod (a - y `mul` div n d `mul` (a-b)) l, l)
  where
    (d,y,z) = ext_euclid n m
    l = n * div m d
    mul u v = (u*v) `mod` l

-- Only works if all the ps are relatively prime.  They are, though.
solve_chinese_remainder cs =
    foldl' (\a b -> mod (a+b) mm) 0
    [ mod (a * mod (s * div mm m) mm) mm | (a,m) <- cs, let (1,r,s) = ext_euclid m (div mm m) ]
  where
    -- check reletive primeness and compute master modulus
    mm = foldl1 (\m n -> let (1,_,_) = ext_euclid m n in m * n) $ map snd cs

-- ext_euclid a b computes (d,s,t) such that d == gcd(a,b) and d == s*a + t*b
ext_euclid a b
    | b == 0 = (a,1,0)
    | otherwise = let (d',s',t') = ext_euclid b (mod a b)
                  in  (d',t',s' - (div a b) * t')
