{-# LANGUAGE OverloadedStrings #-}
import qualified Data.ByteString.Char8 as C
import qualified Data.HashMap.Lazy as H

type Col = (C.ByteString, C.ByteString)

parseLine :: [C.ByteString] -> (Col, H.HashMap Col Int)
parseLine (c1:c2:"bags":"contain":"no":"other":"bags.":[]) = ((c1,c2), H.empty)
parseLine (c1:c2:"bags":"contain":contents) = ((c1,c2), H.fromList $ go contents)
  where
    go (n:c3:c4:_:rest) = let Just (n',"") = C.readInt n in ((c3,c4),n') : go rest
    go [] = []

main = do
    -- bags directly contain other bags
    raw <- H.fromList . map (parseLine . C.words) . C.lines <$> C.readFile "input07.txt"

    -- bags indirectly contain other bags, including themselves
    let acc :: H.HashMap Col (H.HashMap Col Int)
        acc = H.mapWithKey (\k vs -> foldr (H.unionWith (+)) (H.singleton k 1) $
                                     H.mapWithKey (\k1 n -> H.map (*n) $ H.lookupDefault H.empty k1 acc) vs) raw

    -- how many contain at least one shiny gold bag?  subtract one,
    -- because the shiny gold bag itself doesn't count.
    print . subtract 1 . H.size $ H.filter ((> 0) . H.lookupDefault 0 ("shiny","gold")) acc

    -- how many bags are contained in a single shiny gold bag?  subtract one,
    -- because the shiny gold bag itself doesn't count.
    print . subtract 1 . sum $ H.lookupDefault H.empty ("shiny","gold") acc
