{-# LANGUAGE RecordWildCards, BangPatterns #-}

import Prelude ()
import BasePrelude hiding ( left, right )

import Data.Array.IO
import Data.Array.MArray
import Control.Monad.Trans.Class
import Control.Monad.Trans.State.Strict


data Tile = Tile { num :: Int
                 , bdy :: [String]
                 , top_row :: Word              -- left-to-right
                 , left_col :: Word             -- bottom-up
                 , bottom_row :: Word           -- right-to-left
                 , right_col :: Word }          -- top-down
  deriving Show

left :: Tile -> Tile
left (Tile n p t l b r) = Tile n (reverse $ transpose p) r t l b

right :: Tile -> Tile
right (Tile n p t l b r) = Tile n (transpose $ reverse p) l b r t

invert :: Tile -> Tile
invert (Tile n p t l b r) = Tile n (reverse $ map reverse p) b r t l

mirror :: Tile -> Tile
mirror (Tile n p t l b r) = Tile n (map reverse p) (rev t) (rev r) (rev b) (rev l)


-- assumes 10x10 tiles
readTiles :: String -> [Tile]
readTiles = unfoldr readTile . lines
  where
    readTile [] = Nothing
    readTile ls = case break null ls of
        (hd:bdy, rest) | "Tile " `isPrefixOf` hd ->
            let [(num,":")] = reads $ drop 5 hd
                top_row = readWord (head bdy)
                bottom_row = rev $ readWord (last bdy)
                right_col = readWord (map last bdy)
                left_col = rev $ readWord (map head bdy)
            in Just (Tile {..}, drop 1 rest)

    readWord cs | length cs == 10 = fromIntegral $ foldl (\a c -> shiftL a 1 .|. fromEnum (c == '#')) 0 cs

-- assumes 10x10 tiles
rev :: Word -> Word
rev = foldl (\a b -> shiftL a 1 .|. b .&. 1) 0 . take 10 . iterate (`shiftR` 1)

pick0 :: StateT [Tile] [] Tile
pick0 = StateT $ go []
  where
    go acc [    ] = []
    go acc (t:ts) = (t, acc ++ ts) : go (t:acc) ts

pick :: StateT [Tile] [] Tile
pick = do t <- pick0
          f <- lift [ id, left, invert, right ]
          g <- lift [ id, mirror ]
          pure . g . f $ t

puzzle :: StateT [Tile] [] [[Tile]]
puzzle = do sz <- round . sqrt . fromIntegral . length <$> get
            r0 <- firstRow sz
            rs <- scanM nextRow r0 [1..sz-1]
            pure (r0:rs)

firstRow :: Int -> StateT [Tile] [] [Tile]
firstRow sz = do t0 <- pick
                 ts <- scanM nextTile t0 [1..sz-1]
                 pure (t0:ts)

nextTile :: Tile -> a -> StateT [Tile] [] Tile
nextTile prev _ = do t <- pick
                     guard (left_col t == rev (right_col prev))
                     pure t

nextRow :: [Tile] -> a -> StateT [Tile] [] [Tile]
nextRow (p0:prev) _ = do t0 <- pick
                         guard (top_row t0 == rev (bottom_row p0))
                         ts <- scanM innerTile t0 prev
                         pure (t0:ts)

innerTile :: Tile -> Tile -> StateT [Tile] [] Tile
innerTile left top = do t <- pick
                        guard (left_col t == rev (right_col left))
                        guard (top_row t == rev (bottom_row top))
                        pure t

scanM :: Monad m => (a -> b -> m a) -> a -> [b] -> m [a]
scanM f z [] = pure []
scanM f z (x:xs) = do y <- f z x
                      ys <- scanM f y xs
                      pure (y:ys)

checksum :: [[Tile]] -> Integer
checksum sqr = product $ map (fromIntegral . num)
    [ head (head sqr), head (last sqr), last (head sqr), last (last sqr) ]

monster :: [(Int,Int)]
monster = map fst . filter ((==) '#' . snd) . concat .
          zipWith (\y -> map (\(x,c) -> ((x,y),c))) [0..] . map (zip [0..]) $
    [ "                  # "
    , "#    ##    ##    ###"
    , " #  #  #  #  #  #   " ]

findMonsters :: (Int -> Int -> (Int,Int)) -> IOUArray (Int,Int) Char -> IO ()
findMonsters f arr = do
    ((0,0),(xm,ym)) <- getBounds arr

    forM_ [0 .. xm-mwidth] $ \x ->
        forM_ [0 .. ym-mheight] $ \y -> do
            is_monster <- all (/= '.') <$> mapM (\(u,v) -> readArray arr (f (x+u) (y+v))) monster
            when is_monster $
                mapM_ (\(u,v) -> writeArray arr (f (x+u) (y+v)) 'O') monster
  where
    mwidth = maximum $ map fst monster
    mheight = maximum $ map snd monster

main = do
    tiles <- readTiles <$> readFile
                    -- "example20.txt"
                    "input20.txt"

    let (soln,[]) : _ = runStateT puzzle tiles
    print $ checksum soln


    let eff_bdy = init . tail . map init . map tail . bdy
    pict <- newListArray ((0,0),(95,95)) . concat $ concatMap (map concat . transpose . map eff_bdy) soln

    -- great... there aren't any flipped or rotates sea monsters...
    -- however, there is also no guarantee that the map comes out
    -- oriented correctly
    findMonsters (\x y -> (x,y)) pict
    findMonsters (\x y -> (95-y,x)) pict
    findMonsters (\x y -> (95-x,95-y)) pict
    findMonsters (\x y -> (y,95-x)) pict

    findMonsters (\x y -> (95-x,y)) pict
    findMonsters (\x y -> (y,x)) pict
    findMonsters (\x y -> (x,95-y)) pict
    findMonsters (\x y -> (95-y,95-x)) pict

    print . length . filter (=='#') =<< getElems pict

    do ((0,0),(xm,ym)) <- getBounds pict
       forM_ [0..ym] $ \y -> do
           forM_ [0..xm] $ \x -> do
               readArray pict (x,y) >>= putChar
           putChar '\n'
       putChar '\n'



