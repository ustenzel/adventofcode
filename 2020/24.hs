{-# LANGUAGE BangPatterns #-}
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Data.List

main = do
    let grid0 = foldl' flop S.empty . map (walk 0 0) $ example
    print $ S.size grid0 -- example part A
    mapM_ print . filter (\(i,_) -> i<=10 || i`mod`10==0) . zip [0..] .
        map S.size . take 101 $ iterate advance grid0 -- example part B

    putStrLn "---"

    grid1 <- foldl' flop S.empty . map (walk 0 0) . lines <$>
             readFile "input24.txt"
    print $ S.size grid1 -- part A
    print . S.size . (!! 100) $ iterate advance grid1 -- part B

type Pos = (Int,Int)
type Grid = S.Set Pos

flop :: Grid -> Pos -> Grid
flop grid pos | pos `S.member` grid = S.delete pos grid
              | otherwise           = S.insert pos grid

walk :: Int -> Int -> String -> Pos
walk !x !y [         ] = (x,y)
walk !x !y     ('e':s) = walk (x+1)   y   s
walk !x !y ('n':'e':s) = walk (x+1) (y+1) s
walk !x !y ('s':'e':s) = walk   x   (y-1) s
walk !x !y     ('w':s) = walk (x-1)   y   s
walk !x !y ('n':'w':s) = walk   x   (y+1) s
walk !x !y ('s':'w':s) = walk (x-1) (y-1) s

advance :: Grid -> Grid
advance grid = S.union
    -- everything with two black neighbors becomes or stays black
    (S.fromList [ pos | (pos,2) <- M.toList nbrs ])
    -- everything with one black neighbor stays black
    (S.fromList [ pos | (pos,1) <- M.toList nbrs ] `S.intersection` grid)
  where
    nbrs = M.fromListWith (+)
           [ ((x+u, y+v), 1)
           | (x,y) <- S.toList grid
           , (u,v) <- [ (1,0), (1,1), (0,1), (-1,0), (-1,-1), (0,-1) ] ]


example =   [ "sesenwnenenewseeswwswswwnenewsewsw"
            , "neeenesenwnwwswnenewnwwsewnenwseswesw"
            , "seswneswswsenwwnwse"
            , "nwnwneseeswswnenewneswwnewseswneseene"
            , "swweswneswnenwsewnwneneseenw"
            , "eesenwseswswnenwswnwnwsewwnwsene"
            , "sewnenenenesenwsewnenwwwse"
            , "wenwwweseeeweswwwnwwe"
            , "wsweesenenewnwwnwsenewsenwwsesesenwne"
            , "neeswseenwwswnwswswnw"
            , "nenwswwsewswnenenewsenwsenwnesesenew"
            , "enewnwewneswsewnwswenweswnenwsenwsw"
            , "sweneswneswneneenwnewenewwneswswnese"
            , "swwesenesewenwneswnwwneseswwne"
            , "enesenwswwswneneswsenwnewswseenwsese"
            , "wnwnesenesenenwwnenwsewesewsesesew"
            , "nenewswnwewswnenesenwnesewesw"
            , "eneswnwswnwsenenwnwnwwseeswneewsenese"
            , "neswnwewnwnwseenwseesewsenwsweewe"
            , "wseweeenwnesenwwwswnew" ]
