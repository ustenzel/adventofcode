{-# LANGUAGE RecordWildCards #-}

import Prelude ()
import BasePrelude
import qualified Data.ByteString.Char8 as C
import qualified Data.IntSet as S

data Case = Case { c_min :: Int, c_max :: Int, c_chr :: Char, c_pw :: String }
  deriving Show

parse :: String -> Case
parse s = head [ Case lo hi c pw
               | (lo,'-':s1) <- reads s
               , (hi,s2) <- reads s1
               , let [ c:':':[], pw ] = words s2 ]

validA :: Case -> Bool
validA Case{..} = let n = length $ filter (== c_chr) c_pw
                  in c_min <= n && n <= c_max

validB :: Case -> Bool
validB Case{..} = let a = c_pw !! (c_min-1) == c_chr
                      b = c_pw !! (c_max-1) == c_chr
                  in a /= b


main = do
    cases <- map parse . lines <$> getContents
    print $ length $ filter validA $ cases
    print $ length $ filter validB $ cases
