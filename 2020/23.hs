{-# LANGUAGE BangPatterns, TypeFamilies #-}
import BasePrelude
import Prelude ()

import qualified Data.Vector.Unboxed.Mutable as W

example = map digitToInt "389125467"
my_input = map digitToInt "193467258"
my_input_b = my_input ++ [10 .. 1000000]

-- 'linked list':  if (cups U.! a == b), then b directly follows a
type Cups = W.IOVector Int

arrange :: [Int] -> IO Cups
arrange xs0 = do
    cups <- W.new (length xs0 + 1)
    let go (x1:x2:xs) = do W.write cups x1 x2
                           go (x2:xs)
        go [x] = W.write cups x (head xs0)

    go xs0
    pure cups


move :: Int -> Cups -> Int -> IO Int
move !m !cups !cur = do
    !cup1 <- W.read cups cur
    !cup2 <- W.read cups cup1
    !cup3 <- W.read cups cup2
    !next <- W.read cups cup3

    let (!dest) : _ = filter (not . (`elem` [cup1,cup2,cup3])) $ map wrap [ cur - 1, cur - 2 .. ]

    W.write cups cur next                       -- cur is followed by next
    -- cup1 is still followed by cup2
    -- cup2 is still followed by cup3
    W.read cups dest >>= W.write cups cup3      -- cup3 is now followed by what followed dest
    W.write cups dest cup1                      -- dest is followed by cup1
    pure next                                   -- cur becomes what followed cup3
  where
    wrap x = if x < 1 then m + x else x

soln :: Cups -> IO String
soln cups = go 1
  where
    go i = do j <- W.read cups i
              if j == 1
                then pure []
                else (intToDigit j :) <$> go j

solnB :: Cups -> IO Integer
solnB cups = do i <- W.read cups 1
                j <- W.read cups i
                pure $ fromIntegral i * fromIntegral j

iterateM :: Int -> (a -> IO a) -> a -> IO a
iterateM 0 _ x = pure x
iterateM n f x = iterateM (n-1) f =<< f x

run :: Int -> [Int] -> IO Cups
run n xs = do cups <- arrange xs
              let !m = maximum xs
              iterateM n (move m cups) (head xs)
              pure cups

main = do
    putStrLn =<< soln =<< run 10 example            -- test
    putStrLn =<< soln =<< run 100 example            -- test
    putStrLn =<< soln =<< run 100 my_input            -- part A
    print =<< solnB =<< run 10000000 my_input_b            -- part B

