import qualified Data.Set as S
import Data.List

mainA =
    print . sum .
    map (S.size . S.fromList . concat) .
    groupBy (\_ -> not . null) . lines
    =<< readFile "input06.txt"


mainB =
    print . sum .
    map (S.size . foldl1' S.intersection . map S.fromList . filter (not . null)) .
    groupBy (\_ -> not . null) . lines
    =<< readFile "input06.txt"
