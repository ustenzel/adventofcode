{-# LANGUAGE BangPatterns #-}
import qualified Data.Vector.Unboxed.Mutable as U

game_to :: Int -> [Int] -> IO Int
game_to n xs = do
    h <- U.replicate n 0

    let go !i !y (!x:xs) = U.write h y i >> go (i+1) x xs
        go !i !y [     ] = cont i y

        cont !i !y
            | i == n    = pure y
            | otherwise = do j <- U.read h y
                             U.write h y i
                             cont (i+1) $ if j == 0 then 0 else i-j
    go 0 0 xs

-- Brute force.  Not very cleaver, but runs in less than a second.

main :: IO ()
main = do game_to     2020 [8,11,0,19,1,2] >>= print
          game_to 30000000 [8,11,0,19,1,2] >>= print

