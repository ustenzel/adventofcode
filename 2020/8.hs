{-# LANGUAGE OverloadedStrings, BangPatterns #-}

import qualified Data.ByteString.Char8 as C
import qualified Data.IntSet as S
import qualified Data.Vector as V

data Ins = Acc Int | Jmp Int | Nop Int

readInsn [ cmd, arg ] = cmd' arg'
  where
    cmd' = case cmd of "acc" -> Acc
                       "jmp" -> Jmp
                       "nop" -> Nop
    Just (arg', "") = C.readInt arg

main = do
    prg <- V.fromList . map (readInsn . C.words) . C.lines <$> C.readFile "input08.txt"

    print $ go prg False S.empty 0 0
    print [ r | Right r <- go prg True S.empty 0 0 ]

go prg patch visited !acc !pc
    | pc `S.member` visited = [Left acc]
    | otherwise = case prg V.! pc of
        Acc x -> go prg patch visited' (acc+x) (pc+1)
        Jmp x
            | pc+x == V.length prg  -> [Right (acc,pc)]
            | patch                 -> concat [ go prg patch visited' acc (pc+x), go prg False visited' acc (pc+1) ]
            | otherwise             -> go prg False visited' acc (pc+x)
        Nop x
            | patch && pc+x == V.length prg  -> [Right (acc,pc)]
            | patch                 -> concat [ go prg False visited' acc (pc+x), go prg patch visited' acc (pc+1) ]
            | otherwise             -> go prg patch visited' acc (pc+1)
  where
    visited' = S.insert pc visited

