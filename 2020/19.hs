{-# LANGUAGE LambdaCase #-}

import Prelude ()
import BasePrelude
import qualified Data.IntMap.Strict as I
import qualified Text.Megaparsec as P
import qualified Text.Megaparsec.Char as P
import qualified Text.Megaparsec.Char.Lexer as P ( decimal )

type Rule = String -> [String]

matches :: Rule -> String -> Bool
matches r s = any null (r s)

parseRule :: I.IntMap Rule -> String -> (Int, Rule)
parseRule m = either (error . show) id . P.parse (p_rule m) ""


p_rule m = do
    n <- P.decimal
    P.char ':'
    P.space

    f <- literal_rule <|>
         alternatives <$> P.sepBy1 (chain m <$> P.some (P.decimal <* P.space))
                                   (P.char '|' <* P.space)
    pure (n, f)

literal_rule :: P.Parsec () String Rule
literal_rule =
    (\c -> \case c':cs | c == c' -> [ cs ] ; _ -> [])
    <$ P.char '"' <*> P.anySingle <* P.char '"'

chain m = foldr (\i k -> let Just f = I.lookup i m in f >=> k) pure

alternatives rs = \s -> concatMap ($ s) rs

main = do
    (ruledef, "":worddef) <- break null . lines <$> readFile "input19.txt"
    let rules = I.fromList $ map (parseRule rules) ruledef
        Just rule0 = I.lookup 0 rules

    print $ length $ filter (matches rule0) worddef
