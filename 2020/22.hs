{-# LANGUAGE BangPatterns #-}
import qualified Data.Set as S
import qualified Data.Vector.Unboxed as U
import Data.Word

type Game = (U.Vector Word8, U.Vector Word8)

play :: Game -> Either Int Int
play (as,bs)
    | U.null as = Right $ score bs
    | U.null bs = Left  $ score as

    | a > b = play (U.tail as `U.snoc` a `U.snoc` b, U.tail bs)
    | a < b = play (U.tail as, U.tail bs `U.snoc` b `U.snoc` a)
    | otherwise = error $ show (as,bs)
  where
    a = U.head as
    b = U.head bs

playRec :: S.Set Game -> Game -> Either Int Int
playRec !h g@(as, bs)
    | U.null    as = Right $ score bs
    | U.null    bs = Left  $ score as
    | S.member g h = Left  $ score as

    | U.length as > fromIntegral a && U.length bs > fromIntegral b =
        let as' = U.take (fromIntegral a) (U.tail as)
            bs' = U.take (fromIntegral b) (U.tail bs)
        in if U.maximum as' > U.maximum bs' && U.maximum as' >= a
           then left_wins
           else either (const left_wins) (const right_wins) $ playRec S.empty (as',bs')

    | a > b = left_wins
    | a < b = right_wins
  where
    left_wins =  playRec h' (U.tail as `U.snoc` a `U.snoc` b, U.tail bs)
    right_wins = playRec h' (U.tail as, U.tail bs `U.snoc` b `U.snoc` a)
    a = U.head as
    b = U.head bs
    h' = S.insert g h


score = sum . zipWith (*) [1..] . reverse . map fromIntegral . U.toList

readGame :: [String] -> Game
readGame ("Player 1:":ls) = case break null ls of
    (as, "":"Player 2:":bs) -> (U.fromList $ map read as, U.fromList $ map read bs)

example = "Player 1:\n9\n2\n6\n3\n1\n\nPlayer 2:\n5\n8\n4\n7\n10\n"

main = do
    print . play . readGame $ lines example
    print . play . readGame . lines =<< readFile "input22.txt"

    print . playRec S.empty . readGame $ lines example
    print . playRec S.empty . readGame . lines =<< readFile "input22.txt"
