{-# LANGUAGE BangPatterns, OverloadedStrings #-}
import qualified Data.ByteString.Char8 as B
import qualified Data.Set as S

newtype Ingredient = Ingredient B.ByteString deriving (Eq, Ord, Show)
newtype Allergen = Allergen B.ByteString deriving (Eq, Ord, Show)

data Food = Food { ingredients :: S.Set Ingredient, allergens :: S.Set Allergen }

readFood :: B.ByteString -> Food
readFood s = case break (== "(contains") $ B.words s of
    (ings, _:algs) -> Food (S.fromList $ map Ingredient ings)
                           (S.fromList $ map (Allergen . B.init) algs)


main = do
    foods <- map readFood . B.lines <$> B.readFile "input21.txt"

    let all_ingredients = S.unions $ map ingredients foods
        all_allergens = S.unions $ map allergens foods

    -- for an ingredient to contain an allergen, it has to be in every
    -- food that contains the allergen
    let might_contain = S.fromList [ (ing,allg)
                                   | ing <- S.toList all_ingredients
                                   , allg <- S.toList all_allergens
                                   , all (\f -> not (allg `S.member` allergens f) || ing `S.member` ingredients f) foods ]

    let might_contain_any = S.filter (\i -> any (\a -> (i,a) `S.member` might_contain) all_allergens) all_ingredients

    print might_contain_any
    print $ sum $ map (S.size . S.filter (`S.notMember` might_contain_any) . ingredients) foods

    mapM_ print $ S.toList might_contain
    -- did part B manually from that output, yields:
    -- bcdgf,xhrdsl,vndrb,dhbxtb,lbnmsr,scxxn,bvcrrfbr,xcgtv
