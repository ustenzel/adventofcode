import Data.Char
import Data.List

main :: IO ()
main = do
    grid <- map (map digitToInt) . lines <$> getContents

    print . sum . map fromEnum $ mapreduce (vis_left (-1)) (||) grid
    print . maximum $ mapreduce view_dist (*) grid

mapreduce :: ([a] -> [b]) -> (b -> b -> b) -> [[a]] -> [b]
mapreduce m r grid = zipWith r (zipWith r (concat vleft) (concat vright))
                               (zipWith r (concat vtop) (concat vbot))
  where
    vleft = map m grid
    vright = map reverse . map m . map reverse $ grid
    vtop = transpose . map m . transpose $ grid
    vbot = reverse . transpose . map m . transpose . reverse $ grid


vis_left :: Int -> [Int] -> [Bool]
vis_left l (x:xs) = (x > l) : vis_left (max x l) xs
vis_left _ [    ] = []

view_dist :: [Int] -> [Int]
view_dist [    ] = []
view_dist (x:xs)
    | all (<x) xs = length xs : view_dist xs
    | otherwise   = 1 + length (takeWhile (<x) xs) : view_dist xs

