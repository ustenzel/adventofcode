import Control.Applicative
import Data.Char
import Data.List
import Text.ParserCombinators.ReadP

data List = Int Int | List [List] deriving Show

pList = Int <$> pInt <|> List <$> between (char '[') (char ']') (sepBy pList (char ','))

pInt = foldl' (\a c -> 10 * a + digitToInt c) 0 <$> munch1 isDigit


to_list :: String -> List
to_list s = case readP_to_S (pList <* eof) s of
    [(l,"")] -> l
    [] -> error $ "no parse: " ++ show s
    _  -> error $ "multiple parses: " ++ show s

to_pairs :: [a] -> [(a,a)]
to_pairs (l1:l2:ls) = (l1,l2) : to_pairs ls
to_pairs [] = []


instance Eq List where
    a == b = compare a b == EQ

instance Ord List where
    compare (Int   a) (Int   b) = compare a b
    compare (Int   a) (List ys) = compare (List [Int a]) (List ys)
    compare (List xs) (Int   b) = compare (List xs) (List [Int b])

    compare (List [    ]) (List [    ]) = EQ
    compare (List [    ]) (List   _   ) = LT
    compare (List   _   ) (List [    ]) = GT
    compare (List (x:xs)) (List (y:ys)) = compare x y <> compare (List xs) (List ys)


main = do
    inputs <- map to_list . filter (not . null) . lines <$> getContents

    -- part 1
    print . sum $ zipWith (\i (l1,l2) -> if l1 < l2 then i else 0) [1..] (to_pairs inputs)

    -- part 2
    let divider1 = List [List [Int 2]]
        divider2 = List [List [Int 6]]

    print $ product $ map fst $
        filter (\(i,l) -> l == divider1 || l == divider2) $
        zip [(1::Int)..] $ sort (divider1 : divider2 : inputs)
