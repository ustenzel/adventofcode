scoreA :: String -> Int
scoreA "A X" = 3 + 1
scoreA "A Y" = 6 + 2
scoreA "A Z" = 0 + 3
scoreA "B X" = 0 + 1
scoreA "B Y" = 3 + 2
scoreA "B Z" = 6 + 3
scoreA "C X" = 6 + 1
scoreA "C Y" = 0 + 2
scoreA "C Z" = 3 + 3

scoreB :: String -> Int
scoreB "A X" = 0 + 3
scoreB "A Y" = 3 + 1
scoreB "A Z" = 6 + 2
scoreB "B X" = 0 + 1
scoreB "B Y" = 3 + 2
scoreB "B Z" = 6 + 3
scoreB "C X" = 0 + 2
scoreB "C Y" = 3 + 3
scoreB "C Z" = 6 + 1

mainA = interact $ show . sum . map scoreA . lines

main = interact $ show . sum . map scoreB . lines
