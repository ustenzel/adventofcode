import Data.Array.IO
import Data.Array.Unboxed
import Data.Char
import Data.Word
import Debug.Trace

pairs (x:y:zs) = (x,y) : pairs zs
pairs [] = []

clean c = if isDigit c then c else ' '

trace_line :: (Int,Int) -> (Int,Int) -> [(Int,Int)]
trace_line (x1,y1) (x2,y2)
    | y1 == y2 && x1 < x2  =  [ (x,y1) | x <- [x1..x2] ]
    | y1 == y2             =  [ (x,y1) | x <- [x2..x1] ]
    | y1 < y2              =  [ (x1,y) | y <- [y1..y2] ]
    | otherwise            =  [ (x1,y) | y <- [y2..y1] ]

type Grid = IOUArray (Int,Int) Word8

-- returns True if the grain fell off the bottom
trace_grain :: Grid -> Int -> Int -> IO Bool
trace_grain grid x y = do
    ((left,_top),(right,bot)) <- getBounds grid
    if y >= bot || x == left || x == right
      then pure True
      else do
        below <- readArray grid (x,y+1)
        below_left <- readArray grid (x-1,y+1)
        below_right <- readArray grid (x+1,y+1)
        case () of
            _ | below == 0 -> trace_grain grid x (y+1)
              | below_left == 0 -> trace_grain grid (x-1) (y+1)
              | below_right == 0 -> trace_grain grid (x+1) (y+1)
              | otherwise -> do writeArray grid (x,y) 2
                                pure False

measure_sand :: Grid -> IO Int
measure_sand grid = go 0
  where
    go n = do
        full <- readArray grid (500,0)
        done <- trace_grain grid 500 0
        if done || full > 0
            then pure n
            else go $! n+1


main = do
    input <- map (pairs . map read . words . map clean) . lines <$> getContents

    let left  = minimum (map fst $ concat input) - 1
        right = maximum (map fst $ concat input) + 1
        top   = minimum (0 : map snd (concat input))
        bot   = maximum (map snd $ concat input)

    do  grid <- newArray ((left,top),(right,bot)) 0 :: IO Grid
        mapM_ (\p -> writeArray grid p 1) $ concatMap (\ps -> concat $ zipWith trace_line ps (drop 1 ps)) input

        print =<< getBounds grid
        print =<< measure_sand grid

    do  let h = bot-top+2
        gridB <- newArray ((left-h,top),(right+h,bot+2)) 0 :: IO Grid
        mapM_ (\p -> writeArray gridB p 1) $ concatMap (\ps -> concat $ zipWith trace_line ps (drop 1 ps)) input
        mapM_ (\x -> writeArray gridB (x,bot+2) 1) [left-h..right+h]

        print =<< getBounds gridB
        print =<< measure_sand gridB
