{-# Language BangPatterns #-}
import Data.Bits
import Data.List
import Data.Word

import qualified Data.HashMap.Strict as M

type Rock = [ Word ]

rocks :: Rocks
rocks = zip [0..] $
        [ [ 0x3c ] -- ...####..

        , [ 0x10   -- ....#....
          , 0x38   -- ...###...
          , 0x10 ] -- ....#....

        , [ 0x08   -- .....#...
          , 0x08   -- .....#...
          , 0x38 ] -- ...###...

        , [ 0x20   -- ...#.....
          , 0x20   -- ...#.....
          , 0x20   -- ...#.....
          , 0x20 ] -- ...#.....

        , [ 0x30   -- ...##....
          , 0x30 ] -- ...##....
        ]

newtype Shaft = Shaft [ Word ]

empty_shaft :: Shaft
empty_shaft = Shaft [ 0x1ff ]

shaft_extension :: Word
shaft_extension = 0x101

-- make room for the rock
room_on_top :: Shaft -> Rock -> Shaft
room_on_top (Shaft shaft) rock = Shaft $ replicate (length rock + 3) shaft_extension ++ shaft

-- remove empty space
reduce :: Shaft -> Shaft
reduce (Shaft ws) = Shaft $ (dropWhile (== shaft_extension)) ws

fits :: Rock -> Shaft -> Bool
fits r (Shaft s) = all (== 0) $ zipWith (.&.) r s


drop_rock :: Rock -> Gas -> Shaft -> (Gas, Shaft)
drop_rock r g s = fmap reduce $ try_shift r g $ room_on_top s r

try_shift :: Rock -> Gas -> Shaft -> (Gas, Shaft)
try_shift r ((_,op):gas) s = if fits r' s then try_drop r' gas s else try_drop r gas s
  where
    r' = if op == '<' then map (`shiftL` 1) r else map (`shiftR` 1) r

try_drop :: Rock -> Gas -> Shaft -> (Gas, Shaft)
try_drop r gas (Shaft s) = if fits (0:r) (Shaft s)
                           then try_shift (0:r) gas (Shaft s)
                           else (gas, Shaft $ longZipWith (.|.) r s)

longZipWith :: (a -> a -> a) -> [a] -> [a] -> [a]
longZipWith f (x:xs) (y:ys) = f x y : longZipWith f xs ys
longZipWith _ [    ]    ys  = ys
longZipWith _    xs  [    ] = xs

type Gas = [(Int, Char)]
type Rocks = [(Int,Rock)]

drop_rocks :: Rocks -> Gas -> Shaft -> [(Shaft,Int,Int)]
drop_rocks ((i,r):rs) gas@((j,_):_) s = (s,i,j) : drop_rocks rs gas' s' where (gas',s') = drop_rock r gas s


main = do
    gas <- cycle . zip [0..] . filter (\c -> c == '<' || c == '>') <$> getContents

    -- part 1, rock and gas indices ignored
    let (Shaft filled_shaft,_,_) : _ = drop 2022 $ drop_rocks (cycle rocks) gas empty_shaft
    print $ length filled_shaft - 1

    -- part 2: try to find a cycle.
    -- After that, simulate until the cycle has started and an integral
    -- number of cycles remains, add the height gain per cycle times the
    -- number of remaining cycles to the height accumulated so far.

    let (t0,t1) = find_cycle $ drop_rocks (cycle rocks) gas empty_shaft
        (ncycles, nrocks) = (1000000000000 - t0) `divMod` (t1-t0)
        (Shaft shaft1,_,_) : shafts = drop (t0+nrocks) $ drop_rocks (cycle rocks) gas empty_shaft
        (Shaft shaft2,_,_) : _      = drop (t1-t0-1) shafts

    print $ (length shaft2 - length shaft1) * ncycles + length shaft1 - 1

-- Returns (t0,t1) such that the states at both times match for the
-- first time.
-- Despite the huge state space (10k input, times 5 rocks, times lots of
-- shapes the shaft can have), the cycle is pretty short (only 1705
-- rocks).  That was easier than anticipated!

find_cycle :: [(Shaft, Int, Int)] -> (Int, Int)
find_cycle = go 0 M.empty
  where
    go !t1 !seen ((s,i,j):ss) = case M.lookup (signature s,i,j) seen of
        Just t0 -> (t0,t1)
        Nothing -> go (t1+1) (M.insert (signature s,i,j) t1 seen) ss

-- Defines when two shafts "look effectively the same".  we'll take the
-- top eight lines and assemble them into a Word.  Should be good
-- enough, shouldn't it?  (Indeed, it is.)

signature :: Shaft -> Word
signature (Shaft ws) = foldl' (\a w -> shiftL a 8 .|. (w .&. 0xfe)) 0
                     $ take 8 $ ws ++ repeat 0

