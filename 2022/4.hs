

mainA = main' $ \(a,b) -> contains a b || contains b a

main = main' (uncurry overlaps)

main' p = interact $
    show .
    length .
    filter p .
    map parse_ranges .
    lines


type Range = (Int,Int)

contains :: Range -> Range -> Bool
contains (a,b) (u,v) = a <= u && b >= v

overlaps :: Range -> Range -> Bool
overlaps (a,b) (u,v) = u <= b && a <= v

parse_ranges :: String -> (Range, Range)
parse_ranges s = ((a,b),(c,d))
  where
    [a,b,c,d] = map read $ words $ map clean s
    clean c | '0' <= c && c <= '9' = c
            | otherwise            = ' '
