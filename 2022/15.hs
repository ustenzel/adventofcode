import Data.Bifunctor
import Data.Char
import Data.List
import qualified Data.Set as S

data Sensor = Sensor !Int !Int !Int !Int !Int deriving Show

clean c | isDigit c = c
        | c == '-'  = c
        | otherwise = ' '

to_sensor [sx,sy,bx,by] = Sensor sx sy bx by d
  where
    d = abs (sx-bx) + abs (sy - by)

enum_impossible :: Int -> Sensor -> [(Int,Int)]
enum_impossible y (Sensor sx sy _ _ d)
    | dx  <   0 = []
    | otherwise = [(sx-dx, sx+dx+1)]
  where
    dx = d - abs (y-sy)

union_ranges ((x1,y1):(x2,y2):rs)
    | x2 <= y1 && y1 <= y2 = union_ranges ((x1,y2):rs)
    | x2 <= y1             = union_ranges ((x1,y1):rs)
    | otherwise            = (x1,y1) : union_ranges ((x2,y2):rs)
union_ranges rs = rs

remove_points ((x1,y1):rs) (z:zs)
    | y1 <= z = (x1,y1) : remove_points rs (z:zs)
    | z < x1  = remove_points ((x1,y1):rs) zs
    | otherwise = remove_points ((x1,z):(z+1,y1):rs) zs

remove_points rs [] = rs
remove_points []  _ = []

remove_ranges :: [(Int,Int)] -> [(Int,Int)] -> [(Int,Int)]
remove_ranges ((x1,y1):xs) ((x2,y2):ys)
    | y1 <= x2              = (x1,y1) : remove_ranges xs ((x2,y2):ys)
    | x1 <= x2 && y1 <= y2  = (x1,x2) : remove_ranges xs ((x2,y2):ys)
    | x1 <= x2              = (x1,x2) : remove_ranges ((y2,y1):xs) ys
    | y1 <= y2              = remove_ranges xs ((x2,y2):ys)
    | x1 < y2               = remove_ranges ((y2,y1):xs) ys
    | otherwise             = remove_ranges ((x1,y1):xs) ys

remove_ranges rs [] = rs
remove_ranges []  _ = []


main = do
    sensors <- map (to_sensor . map read . words . map clean) . lines <$> getContents

    -- part 1
    do  let impossible = union_ranges $ sort $ concatMap (enum_impossible 2000000) sensors
            taken      = sort [ x | Sensor _ _ x 2000000 _ <- sensors ]

        print $ impossible `remove_points` taken
        print . negate . sum . map (uncurry (-)) $ impossible `remove_points` taken

    -- part 2
    do  let impossible = [ (imp, y) | y <- [ 0..4000000 ]
                                    , let imp = union_ranges $ sort $ concatMap (enum_impossible y) sensors ]

        let [([(x,xe)],y)] = filter (not . null . fst) $ map (first (remove_ranges [(0,4000000)])) $ impossible
        print $ (x,xe,y)
        print $ x * 4000000 + y
