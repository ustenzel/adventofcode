import Data.Char

run :: Int -> [[String]] -> [Int]
run r (["noop"]  :ops) =  r : run r ops
run r (["addx",n]:ops) =  r : r : run (r + read n) ops
run r [              ] = [r]

block n [] = []
block n xs = u : block n v where (u,v) = splitAt n xs

mainA =
    print . sum .
    zipWith (*) [20,60..] .
    map head . block 40 . drop 19 .
    run 1 .
    map words . lines =<<
    getContents


main =
    putStrLn . unlines .
    map (zipWith (\col pos -> if abs (col-pos) <= 1 then chr 0x2588 else ' ') [0..]) .
    block 40 .
    take 240 .
    run 1 .
    map words . lines =<<
    getContents
