import qualified Data.Set as S

data Move = R Int | U Int | L Int | D Int deriving Read

data V2 = Int :+ Int deriving (Eq, Ord)

(a :+ b) +^ (c :+ d) = (a+c) :+ (b+d)
(a :+ b) -^ (c :+ d) = (a-c) :+ (b-d)

main :: IO ()
main =
    print .
    S.size .
    S.fromList .
    -- trace_rope [0 :+ 0, 0 :+ 0] .                   -- part 1
    trace_rope_multi (replicate 10 (0 :+ 0)) .      -- part 2
    map read . lines
    =<< getContents

dir :: Move -> (V2, Int)
dir (R n) = (1 :+ 0, n)
dir (U n) = (0 :+ 1, n)
dir (L n) = ((-1) :+ 0, n)
dir (D n) = (0 :+ (-1), n)

drag :: V2 -> V2
drag (dx :+ dy) | -1 <= dx && dx <= 1 && -1 <= dy && dy <= 1 = 0 :+ 0
                | otherwise                                  = signum dx :+ signum dy

trace_rope_multi :: [V2] -> [Move] -> [V2]
trace_rope_multi knots [    ] = [last knots]
trace_rope_multi knots (m:ms) = single_step_multi knots (dir m) ms

single_step_multi :: [V2] -> (V2, Int) -> [Move] -> [V2]
single_step_multi knots (d,0) ms = trace_rope_multi knots ms
single_step_multi knots (d,n) ms = last knots : single_step_multi knots' (d,n-1) ms
  where
    knots' = scanl drag1 (head knots +^ d) (tail knots)
    drag1 hd tl = tl +^ drag (hd-^tl)

