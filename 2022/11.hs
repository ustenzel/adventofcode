{-# Language BangPatterns, LambdaCase #-}
import Control.Applicative
import Control.Monad
import Data.Char
import Data.List
import Text.ParserCombinators.ReadP
import Text.Printf

import qualified Data.Map.Strict as M

type Monkeys = M.Map Int Monkey

data Monkey = Monkey
    { ntot :: !Int
    , items :: [Int] -- in reverse order
    , op :: Int -> Int
    , tst :: !Int
    , throw_true :: !Int
    , throw_false :: !Int }


pMonkey :: ReadP (Int, Monkey)
pMonkey =
    (,) <$  string "Monkey "
        <*> int
        <*  char ':'
        <*  skipSpaces
        <*> (Monkey 0 <$> (reverse <$ string "Starting items: " <*> sepBy int (char ',' *> skipSpaces) <* skipSpaces)
                      <*> (string "Operation: new = " *> pExpr <* skipSpaces)
                      <*> (string "Test: divisible by " *> int <* skipSpaces)
                      <*> (string "If true: throw to monkey " *> int <* skipSpaces)
                      <*> (string "If false: throw to monkey " *> int <* skipSpaces))

int :: Integral a => ReadP a
int = foldl' (\a c -> 10 * a + fromIntegral (digitToInt c)) 0 <$> munch1 isDigit

pExpr :: ReadP (Int -> Int)
pExpr = pAtom <|> (\f op g x -> op (f x) (g x)) <$> pAtom <*> pOperation <* skipSpaces <*> pAtom

pAtom :: ReadP (Int -> Int)
pAtom = id <$ string "old" <* skipSpaces
    <|> const <$> int <* skipSpaces

pOperation :: ReadP (Int -> Int -> Int)
pOperation = (+) <$ char '+'
         <|> (-) <$ char '-'
         <|> (*) <$ char '*'

play_round :: (Int -> Int) -> Monkeys -> IO Monkeys
play_round reduce ms = foldM (play_turn reduce) ms (map fst $ M.toAscList ms)

play_turn :: (Int -> Int) -> Monkeys -> Int -> IO Monkeys
play_turn reduce ms0 n = do
    -- printf "Monkey %d:\n" n
    let Just m = M.lookup n ms0
    foldM (\ms i -> do
            -- printf "  inspects item at worry level %d\n" i
            let i' = reduce $ op m i
            -- printf "  worry level updated to %d\n" i'
            let dest = if i' `mod` fromIntegral (tst m) == 0 then throw_true m else throw_false m
            -- printf "  throw to monkey %d\n" dest
            pure $ M.adjust (\m' -> m' { items = i' : items m' }) dest ms)
        (M.insert n m { ntot = ntot m + length (items m), items = [] } ms0)
        (reverse $ items m)


monkey_business :: (Int -> Int) -> Int -> Monkeys -> IO ()
monkey_business reduce nrounds monkeys = do
    top:top2:_ <- reverse . sort . map ntot . M.elems <$>
                  foldM (\m _ -> play_round reduce m) monkeys [1..nrounds]
    print $ top * top2


main :: IO ()
main = do
    [monkeys] <- map (M.fromList . fst) . readP_to_S (many1 pMonkey <* eof) <$> getContents

    -- part 1
    monkey_business (`div` 3) 20 monkeys

    -- part 2
    let m = product $ map tst $ M.elems monkeys
    monkey_business (`mod` m) 10000 monkeys

