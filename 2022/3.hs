import Data.Bifunctor
import Data.Char
import qualified Data.Set as S

mainA = print
        . sum
        . map toPrio
        . map fromSingletonSet
        . map (uncurry S.intersection)
        . map (bimap S.fromList S.fromList)
        . map halves
        . lines
        =<< getContents


halves :: String -> (String, String)
halves s | even (length s) = splitAt (length s `div` 2) s
         | otherwise       = error "odd sized rucksack"

fromSingletonSet :: S.Set Char -> Char
fromSingletonSet s = case S.toList s of
    [a] -> a
    [ ] -> error "no common item"
    _   -> error "multiple common items"

toPrio :: Char -> Int
toPrio c | 'a' <= c && c <= 'z' = ord c - ord 'a' + 1
         | 'A' <= c && c <= 'Z' = ord c - ord 'A' + 27
         | otherwise            = error "unexpected item"



mainB = print
        . sum
        . map toPrio
        . map fromSingletonSet
        . intersect3
        . lines
        =<< getContents


intersect3 :: [String] -> [S.Set Char]
intersect3 (a:b:c:xs) = S.fromList a `S.intersection` S.fromList b `S.intersection` S.fromList c : intersect3 xs
intersect3 [        ] = []
intersect3 _          = error "not a multiple of three"
