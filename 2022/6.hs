{-# Language BangPatterns #-}
import Data.List

mainA = print . find_marker  4 0 =<< getContents
main  = print . find_marker 14 0 =<< getContents

find_marker !n !i xs
    | all_different (take n xs) = n + i
    | otherwise                 = find_marker n (i+1) (drop 1 xs)

all_different xs = and [ x /= y | x:ys <- tails xs, y <- ys ]
