import AoC.Dijkstra
import Control.Monad
import Data.Array.Unboxed
import Data.Char
import Data.Monoid
import Data.Word

to_height :: Char -> Int
to_height 'S' = 0
to_height 'E' = 25
to_height c | 'a' <= c && c <= 'z' = fromIntegral (ord c - ord 'a')

main :: IO ()
main = do
    raw_input <- lines <$> getContents
    let height = length raw_input
        width : ws = map length raw_input
    unless (all (==width) ws) $ error "not a rectangle"

    let grid = listArray ((1,1),(height,width)) $ concat raw_input :: UArray (Int,Int) Char

        neighbors c (x,y) = [ (c+1,(x',y'))
                            | (x',y') <- [(x+1,y), (x-1,y), (x,y+1), (x,y-1)]
                            , inRange ((1,1),(height,width)) (x',y')
                            , let d = to_height (grid ! (x',y')) - to_height (grid ! (x,y))
                            , d <= 1 ]

        finished (x,y) = grid ! (x,y) == 'E'
        initial = [ (0,n) | (n,'S') <- assocs grid ] ++ [ (0,n) | (n,'a') <- assocs grid ]

    forM_ (dijkstra neighbors finished initial) $ print . fst
