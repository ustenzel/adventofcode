{-# Language BangPatterns, LambdaCase #-}

import Data.Char
import Data.List
import qualified Data.Map.Strict as M


data Tok = Cd String | CdUp | CdRoot | Dir String | File Int String | Ls
  deriving Show

token :: [String] -> Tok
token ["$","cd",".."] = CdUp
token ["$","cd","/"] = CdRoot
token ["$","cd",nm] = Cd nm
token ["$","ls"] = Ls
token ["dir",nm] = Dir nm
token [sz,nm] | all isDigit sz = File (read sz) nm


data FS = ListedDir (M.Map String FS) | UnlistedDir | SizedFile Int
  deriving Show

type Path = [String]

walk :: (Path,FS) -> Tok -> (Path,FS)
walk (p,fs) = \case
    Cd      nm -> (p ++ [nm],fs)
    CdUp       -> (init p,fs)
    CdRoot     -> ([],fs)
    Ls         -> (p,fs)
    Dir     nm -> (p,update id (p ++ [nm]) fs)
    File sz nm -> (p,update (add_file sz) (p ++ [nm]) fs)

update :: (FS -> FS) -> Path -> FS -> FS
update f [] fs = f fs
update f (p:ps) UnlistedDir = ListedDir (M.singleton p (update f ps UnlistedDir))
update f (p:ps) (SizedFile s) = SizedFile s
update f (p:ps) (ListedDir m) = ListedDir (M.insert p (update f ps $ M.findWithDefault UnlistedDir p m) m)

add_file sz UnlistedDir = SizedFile sz
add_file sz fs          = fs


-- Traverses the file system once and computes both results.  The
-- argument is the minimum amount to free (for part 2), the three
-- results are the total size of the FS, the sum total of small
-- directories (for part 1), and the smallest directory that's big
-- enough (for part 2).

both :: Int -> FS -> (Int,Int,Int)
both k  UnlistedDir   = (0,0,maxBound)
both k (SizedFile sz) = (sz,0,maxBound)
both k (ListedDir m) = let (t,r1,r2) = foldl' add (0,0,maxBound) $ fmap (both k) m
                       in (t, if t <= 100000 then r1+t else r1, if t >= k then min r2 t else r2)
  where
    -- my_r2, r2 have to be matched lazily, otherwise we wouldn't be
    -- able to accept our own result as argument
    add (!my_total,!my_r1,my_r2) (t,r1,r2) = (my_total + t, my_r1 + r1, my_r2 `min` r2)

main :: IO ()
main = do
    fs <- snd . foldl' walk ([],UnlistedDir) . map (token . words) . lines <$> getContents

    let (total_used, result1, result2) = both min_del fs
        min_del = 30000000 - (70000000 - total_used)

    print result1
    print result2
