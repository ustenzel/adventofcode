import Data.Array.Unboxed
import Data.Array.IArray
import Data.Int
import Debug.Trace


data ComeFrom = Nobody | N | E | W | S | Bounce
  deriving Show

instance Semigroup ComeFrom where
    Nobody <> x = x
    x <> Nobody = x
    _ <> _      = Bounce

type Grid =  UArray (Int,Int) Int8

app_round :: Grid -> Directions -> Maybe Grid
app_round grid dirs
    | null movements = Nothing
    | otherwise      = Just $ accumArray (+) 0 (bounds proposals) $
                            filter (inRange (bounds proposals) . fst) (assocs grid) ++ movements
  where
    ((y1,x1),(y2,x2)) = bbox grid

    proposals :: Array (Int,Int) ComeFrom
    proposals = accumArray (<>) Nobody ((y1-1,x1-1),(y2+1,x2+1))
                [ r | pos <- range (bounds grid), grid ! pos > 0, r <- take 1 $ propose pos ]

    propose :: (Int,Int) -> [ ((Int,Int),ComeFrom) ]
    propose (y,x)
        | all (\(v,u) -> empty (y+v,x+u)) neighbors = []
        | otherwise                                 = [ (ps !! 1,d) | (vs,d) <- dirs
                                                                    , let ps = map (\(v,u) -> (y+v,x+u)) vs
                                                                    , all empty ps ]

    empty p = not (inRange (bounds grid) p) || grid ! p == 0

    neighbors = [ (-1,-1), (-1,0), (-1,1), (0,-1), (0,1), (1,-1), (1,0), (1,1) ]

    movements = concat [ app_come_from p (proposals ! p) | p <- range (bounds proposals) ]


-- app_come_from (y,x) cf | trace (show (y,x,cf)) False = undefined
app_come_from (y,x) Nobody = []
app_come_from (y,x) Bounce = []
app_come_from (y,x) N = [ ((y,x),1), ((y-1,x),-1) ]
app_come_from (y,x) E = [ ((y,x),1), ((y,x+1),-1) ]
app_come_from (y,x) S = [ ((y,x),1), ((y+1,x),-1) ]
app_come_from (y,x) W = [ ((y,x),1), ((y,x-1),-1) ]

print_grid :: Grid -> String
print_grid grid = unlines $ [ [ if grid ! (y,x) == 0 then '.' else '#' | x <- [xmin..xmax] ]
                              | y <- [ymin..ymax] ]
  where
    ((ymin,xmin),(ymax,xmax)) = bounds grid

type Directions = [ ( [(Int,Int)], ComeFrom ) ]

-- order of directions in round 0
dirs0 :: Directions
dirs0 = [ ( [ (-1,-1), (-1,0), (-1,1) ], S )
        , ( [ (1,-1), (1,0), (1,1) ], N )
        , ( [ (-1,-1), (0,-1), (1,-1) ], E )
        , ( [ (-1,1), (0,1), (1,1) ], W ) ]

full_round :: Maybe (Grid, Directions) -> Maybe (Grid, Directions)
full_round Nothing = Nothing
full_round (Just (grid, dirs)) = flip (,) dirs' <$> app_round grid dirs
  where dirs' = tail dirs ++ [head dirs]

measure_area :: Grid -> Int
measure_area grid = sum [ 1 - fromIntegral (grid ! (y,x)) | x <- [xmin..xmax], y <- [ymin..ymax] ]
  where
    ((ymin,xmin),(ymax,xmax)) = bbox grid


bbox grid = ((ymin,xmin),(ymax,xmax))
  where
    ((y1,x1),(y2,x2)) = bounds grid
    ymin = head [ y | y <- [y1     ..y2], x <- [x1..x2], grid ! (y,x) /= 0 ]
    ymax = head [ y | y <- [y2,y2-1..y1], x <- [x1..x2], grid ! (y,x) /= 0 ]
    xmin = head [ x | x <- [x1     ..x2], y <- [ymin..ymax], grid ! (y,x) /= 0 ]
    xmax = head [ x | x <- [x2,x2-1..x1], y <- [ymin..ymax], grid ! (y,x) /= 0 ]


main = do
    raw <- lines <$> getContents
    let h = length raw
        w = length $ head raw

    let grid = listArray ((1,1),(h,w)) $ map (\c -> if c == '#' then 1 else 0) $ concat raw

    -- mapM_ (putStrLn . print_grid . fst) $ take 11 $ iterate full_round (grid, dirs0)
    mapM_ (print . measure_area . fst) $ head $ drop 10 $ iterate full_round (Just (grid, dirs0))   -- part 1
    print $ length $ takeWhile (maybe False (const True)) $ iterate full_round (Just (grid, dirs0)) -- part 2
