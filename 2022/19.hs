{-# Language BangPatterns, RecordWildCards #-}
import Data.Char
import Data.List
import System.Environment

data Res = Res { r_ore  :: !Int
               , r_clay :: !Int
               , r_obs  :: !Int
               , r_geo  :: !Int }
  deriving Show

data Bots = Bots { b_ore  :: !Int
                 , b_clay :: !Int
                 , b_obs  :: !Int
                 , b_geo  :: !Int }
  deriving Show

data BP = BP { ore_for_ore  :: !Int
             , ore_for_clay :: !Int
             , ore_for_obs  :: !Int
             , clay_for_obs :: !Int
             , ore_for_geo  :: !Int
             , obs_for_geo  :: !Int }
  deriving Show


read_blueprint :: String -> BP
read_blueprint s = BP{..}
  where
    [_, ore_for_ore, ore_for_clay, ore_for_obs, clay_for_obs, ore_for_geo, obs_for_geo ]
        = map read . words . map (\c -> if isDigit c then c else ' ') $ s


-- Given remaining time, returns the maximum number of cracked geodes.
optimize :: BP -> Int -> Int
optimize BP{..} time = go time (Res 0 0 0 0) (Bots 1 0 0 0) 0
  where
    !max_ore = maximum [ ore_for_ore, ore_for_clay, ore_for_obs, ore_for_geo ]

    go !t !res_ !bots !curmax
        | curmax >= bound                                     = curmax                     -- hopeless
        | t  ==  0                                            = max curmax (r_geo res_)    -- done
        | otherwise                                           = foldr ($) curmax $
            [ go (t-1) res' bots ] ++
            [ go (t-1) res' { r_ore  = r_ore  res'  - ore_for_ore }
                       bots { b_ore  = b_ore  bots + 1 }                      | b_ore bots < max_ore
                                                                              , r_ore res_ >= ore_for_ore ] ++
            [ go (t-1) res' { r_ore  = r_ore  res'  - ore_for_clay }
                       bots { b_clay = b_clay bots + 1 }                      | b_clay bots < clay_for_obs
                                                                              , r_ore res_ >= ore_for_clay ] ++
            [ go (t-1) res' { r_ore  = r_ore  res'  - ore_for_obs
                            , r_clay = r_clay res'  - clay_for_obs }
                       bots { b_obs  = b_obs  bots + 1 }                      | b_obs bots < obs_for_geo
                                                                              , r_ore res_ >= ore_for_obs
                                                                              , r_clay res_ >= clay_for_obs ] ++
            [ go (t-1) res' { r_ore  = r_ore  res'  - ore_for_geo
                            , r_obs  = r_obs  res'  - obs_for_geo }
                       bots { b_geo  = b_geo  bots + 1 }                      | r_ore res_ >= ore_for_geo
                                                                              , r_obs res_ >= obs_for_geo ]
      where
        res' = Res { r_ore  = r_ore  res_ + b_ore  bots
                   , r_clay = r_clay res_ + b_clay bots
                   , r_obs  = r_obs  res_ + b_obs  bots
                   , r_geo  = r_geo  res_ + b_geo  bots }


        -- The better the bound, the faster the search...  this is
        -- crude, but finishes part 1 in ~13s and part 2 in ~80s.
        -- Not worth bothering with a better bound.
        bound = r_geo res_ + b_geo bots * t + t*(t-1) `div` 2

main :: IO ()
main = do
    bps <- map read_blueprint . lines <$> getContents
    print . sum . zipWith (*) [1..] $ map (flip optimize 24) bps        -- part 1
    print . product $ map (flip optimize 32) $ take 3 bps               -- part 2

