use std::io ;
use std::io::BufRead ;

fn main() 
{
    let reader = io::BufReader::new(io::stdin()) ;
    // println!( "{}", reader.lines().map( score_a ).sum::<u32>() ) ;
    println!( "{}", reader.lines().map( score_b ).sum::<u32>() ) ;
}


fn score_a( ms : io::Result<String> ) -> u32
{
    let s = ms.expect("I/O error") ;
    if      s == "A X" { 3 + 1 }
    else if s == "A Y" { 6 + 2 }
    else if s == "A Z" { 0 + 3 }
    else if s == "B X" { 0 + 1 }
    else if s == "B Y" { 3 + 2 }
    else if s == "B Z" { 6 + 3 }
    else if s == "C X" { 6 + 1 }
    else if s == "C Y" { 0 + 2 }
    else if s == "C Z" { 3 + 3 }
    else { panic!("unexpected input") }
}

fn score_b( ms : io::Result<String> ) -> u32
{
    let s = ms.expect("I/O error") ;
    if      s == "A X" { 0 + 3 }
    else if s == "A Y" { 3 + 1 }
    else if s == "A Z" { 6 + 2 }
    else if s == "B X" { 0 + 1 }
    else if s == "B Y" { 3 + 2 }
    else if s == "B Z" { 6 + 3 }
    else if s == "C X" { 0 + 2 }
    else if s == "C Y" { 3 + 3 }
    else if s == "C Z" { 6 + 1 }
    else { panic!("unexpected input") }
}

