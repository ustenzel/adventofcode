use std::io ;
use std::io::BufRead ;

fn main() 
{
    let reader = io::BufReader::new(io::stdin()) ;

    let mut acc : u32 = 0 ;
    let mut v: Vec<u32> = Vec::new();

    for line in reader.lines() {
        match line {
            Ok(s) => 
                if s == "" 
                {
                    v.push(acc) ;
                    acc = 0 ;
                }
                else
                {
                    let x : u32 = s.parse().expect("not an int") ;
                    acc += x ;
                },
            Err(e) => panic!(e) 
        }
    }
    v.push(acc) ;

    println!("{}", v.iter().max().expect("need at least one input")) ;    // solves first part

    // If I had Rust >= 1.49.0, I could cleverly use select_nth_unstable.  I don't.
    v.sort() ;
    let b : u32 =
        v.get( v.len()-1 ).expect("need three blocks") +
        v.get( v.len()-2 ).expect("need three blocks") +
        v.get( v.len()-3 ).expect("need three blocks") ;
    println!("{}", b) ;                                             // solves second part
}

