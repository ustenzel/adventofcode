{-# Language TypeApplications #-}

import Data.List

split :: (a -> Bool) -> [a] -> [[a]]
split _ [] = []
split p xs = u : split p (drop 1 v)
  where
    (u,v) = break p xs

main :: IO ()
main = do
    xs <- mapM (fmap sum . mapM readIO) . split null . lines =<< getContents
    print (maximum xs :: Int)                   -- part 1
    print . sum . take 3 . reverse $ sort xs    -- part 2

