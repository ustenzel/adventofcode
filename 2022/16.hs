{-# Language BangPatterns #-}
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Data.Array
import Data.Maybe
import Data.Bits
import Data.Char
import Data.List
import Debug.Trace


read_line (_:v:r:vs) = (read v,(read r, map read vs))
read_line ws = error $ "no parse: " ++ show ws

newtype Valve = Valve Int deriving (Eq, Ord)

instance Read Valve where readsPrec _ (a:b:s) = [ (Valve $ 26 * (ord a - 65) + ord b - 65,s) ]
instance Show Valve where show (Valve v) = [ chr (65 + v `div` 26), chr (65 + v `mod` 26) ]

type CaveMap = M.Map Valve (Int, [Valve])

-- Only the location AA and the valves with positive rate are
-- interesting from here on.  We can even rename the valves, and we do:
-- node 0 is AA, the valves with positive rate get succesive numbers.

-- maps node numbers back to valves and rates
type ValveList = Array Int (Int, Valve)

collect_valves :: CaveMap -> ValveList
collect_valves cave = listArray (0,num) $ (fst . fromJust $ M.lookup (Valve 0) cave, Valve 0) : valves
  where
    valves = [ (rate, nm) | (nm, (rate,_)) <- M.toList cave
                          , rate > 0 && nm /= Valve 0 ]
    num = length valves

-- We need to compute the shortest distances between valves:
-- (from,to) -> number of minutes
type Distances = Array (Int,Int) Int

measure_distances :: CaveMap -> ValveList -> Distances
measure_distances cave valves =
    array ((lo,lo),(hi,hi)) [ ((fro,tho), dist) | fro <- range (lo,hi)
                                                , let valve0 = snd $ valves ! fro
                                                , (valve1, dist) <- walk_everywhere cave S.empty (S.singleton valve0)
                                                , tho <- maybe [] pure $ M.lookup valve1 valveidx ]
  where
    (lo,hi) = bounds valves
    valveidx = M.fromList [ (v,i) | (i,(_,v)) <- assocs valves ]


walk_everywhere :: CaveMap -> S.Set Valve -> S.Set Valve -> [ (Valve,Int) ]
walk_everywhere cave = go 0
  where
    go !dist !closed !open
        | S.null open = []
        | otherwise   = [ (n,dist) | n <- S.toList open ] ++ go (dist+1) closed' open'
      where
        closed' = S.union closed open
        open'   = S.fromList [ n2 | n1 <- S.toList open
                                  , n2 <- maybe [] snd $ M.lookup n1 cave
                                  , not $ S.member n2 closed' ]

-- Search the sequences of valves to visit within a time limit.  Returns
-- the set of valves opened (a bitffield) and the total flow for every
-- solution.
--
-- Unfortunately, there doesn't seem to be any way to prune this
-- search.  Fortunately, the depth is fairly limited and the search
-- finishes quickly.

search :: Distances -> ValveList -> Int -> [(Word, Int)]
search !dists !valves = go 0 0 0
  where
    (0,maxvalve) = bounds valves

    go :: Int -> Word -> Int -> Int -> [(Word,Int)]
    go !pos !opened !acc !time =
        (opened, acc) :
        [ r | next <- [1..maxvalve]
            , let t = dists ! (pos,next)
            , not $ testBit opened next
            , let t' = time - t - 1
            , t' >= 0
            , let flow = t' * fst (valves ! next)
            , r <- go next (setBit opened next) (acc+flow) t' ]



main :: IO ()
main = do
    cave_map <- M.fromList . map (read_line . words . filter (\c -> isDigit c || isUpper c || isSpace c)) . lines <$> getContents

    let valves = collect_valves cave_map
        dists  = measure_distances cave_map valves

    -- part 1:  search with time limit 30, pick maximum
    print . maximum . map snd $ search dists valves 30

    -- part 2:  search with time limit 26, keep best solution for
    --          every distinct set of valves opened
    let (0,maxvalve) = bounds valves
        solns = accumArray max 0 (0, shiftL 2 maxvalve -1) $ search dists valves 26

    -- Now pick the best pair of solutions that have nothing in common.
    -- This has quadratic runtime and could possibly be improved, but
    -- the problem turns out small enough that this finishes in less
    -- than a second anyway.
    print $ maximum [ flow1 + flow2
                    | (vs1, flow1) : solns' <- tails $ assocs solns
                    , flow1 > 0
                    , (vs2, flow2) <- solns'
                    , flow2 > 0
                    , vs1 .&. vs2 == 0 ]


