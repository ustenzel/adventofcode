import Data.List

main :: IO ()
main = putStrLn . toSNAFU . foldl' (+) 0 . map fromSNAFU . lines =<< getContents

fromSNAFU :: String -> Int
fromSNAFU = foldl' (\a c -> a * 5 + snafuToInt c) 0
  where
    snafuToInt '-' = -1
    snafuToInt '=' = -2
    snafuToInt '0' = 0
    snafuToInt '1' = 1
    snafuToInt '2' = 2

toSNAFU :: Int -> String
toSNAFU = reverse . go
  where
    go 0 = ""
    go x = let r = mod (x + 2) 5 - 2
               q = div (x - r) 5
           in intToSnafu r : go q

    intToSnafu (-1) = '-'
    intToSnafu (-2) = '='
    intToSnafu   0  = '0'
    intToSnafu   1  = '1'
    intToSnafu   2  = '2'
