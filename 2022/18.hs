import Control.Monad
import Control.Monad.Trans.State
import Data.Char
import qualified Data.Set as S

read_line :: String -> (Int,Int,Int)
read_line s = case map read $ words $ map clean s of [a,b,c] -> (a,b,c)
  where
    clean c = if isDigit c then c else ' '

surface :: S.Set (Int,Int,Int) -> Int
surface ps = sum [ if S.member b ps then 0 else 1
                 | (x,y,z) <- S.toList ps
                 , b <- [ (x,y,z+1)
                        , (x,y,z-1)
                        , (x,y+1,z)
                        , (x,y-1,z)
                        , (x+1,y,z)
                        , (x-1,y,z) ] ]

outerface :: S.Set (Int,Int,Int) -> Int
outerface blocked = fst $ execState (flood (xlo,ylo,zlo)) (0,S.empty)
  where
    xlo = minimum [ x | (x,y,z) <- S.toList blocked ] - 1
    xhi = maximum [ x | (x,y,z) <- S.toList blocked ] + 1
    ylo = minimum [ y | (x,y,z) <- S.toList blocked ] - 1
    yhi = maximum [ y | (x,y,z) <- S.toList blocked ] + 1
    zlo = minimum [ z | (x,y,z) <- S.toList blocked ] - 1
    zhi = maximum [ z | (x,y,z) <- S.toList blocked ] + 1

    flood (x,y,z) | x < xlo || x > xhi        =  pure ()
                  | y < ylo || y > yhi        =  pure ()
                  | z < zlo || z > zhi        =  pure ()
                  | S.member (x,y,z) blocked  =  modify $ \(n,ps) -> (n+1,ps)
                  | otherwise = do
                        (n,visited) <- get
                        put $ (n,S.insert (x,y,z) visited)
                        unless (S.member (x,y,z) visited) $
                            mapM_ flood [ (x+1,y,z), (x-1,y,z), (x,y+1,z), (x,y-1,z), (x,y,z+1), (x,y,z-1) ]

main :: IO ()
main = do
    ps <- S.fromList . map read_line . lines <$> getContents
    print $ surface ps
    print $ outerface ps
