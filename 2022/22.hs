import Data.Array.Unboxed
import Data.List

data Op = L | R | M deriving Show

type Grid = UArray (Int,Int) Char

parse_ops :: String -> [Op]
parse_ops [] = []
parse_ops ('L':s) = L : parse_ops s
parse_ops ('R':s) = R : parse_ops s
parse_ops s = case reads s of [(i,s')] -> replicate i M ++ parse_ops s'


-- Makes one step for each M, then invokes the 'next' function.  For
-- part 2, 'next' must be able to change the direction.
walk :: Grid -> (Int -> Int -> Int -> Int -> (Int,Int,Int,Int))
     -> (Int,Int,Int,Int) -> Op -> (Int,Int,Int,Int)
walk grid next (y,x,v,u) L = (y,x,-u,v)
walk grid next (y,x,v,u) R = (y,x,u,-v)
walk grid next (y,x,v,u) M
    | grid ! (nexty,nextx) == '.'  = (nexty,nextx,nextv,nextu)
    | grid ! (nexty,nextx) == '#'  = (y,x,v,u)
    | otherwise                    = error "what?"
  where
    (nexty,nextx,nextv,nextu) = next (y+v) (x+u) v u



next1 :: Grid -> Int -> Int -> Int -> Int -> (Int,Int,Int,Int)
next1 grid y0 x0 v u = go y0 x0
  where
    ((1,1),(h,w)) = bounds grid
    go y x | x > w               = go y 1
           | x < 1               = go y w
           | y > h               = go 1 x
           | y < 1               = go h x
           | grid ! (y,x) == ' ' = go (y+v) (x+u)
           | otherwise           = (y,x,v,u)

main :: IO ()
main = do
    raw <- lines <$> getContents

    let height = length raw - 2
        width = maximum $ map length $ init $ init raw

        grid :: UArray (Int,Int) Char
        grid = listArray ((1,1),(height,width)) $
               concatMap (\l -> take width $ l ++ repeat ' ') $
               init $ init raw

        ops = parse_ops $ last raw

        Just x0 = findIndex (=='.') $ head raw

    print $ encode $ foldl' (walk grid (next1 grid)) (1,x0+1,0,1) ops
    print $ encode $ foldl' (walk grid next2) (1,x0+1,0,1) ops


encode (y,x,v,u) = 1000 * y + 4 * x + case (v,u) of
        (0,1)  -> 0
        (1,0)  -> 1
        (0,-1) -> 2
        (-1,0) -> 3


-- no use making part 2 generic...  my input has these six areas:
--
--  12
--  3
-- 45
-- 6
--
-- with edge length 50

next2 :: Int -> Int -> Int -> Int -> (Int,Int,Int,Int)
next2 y x v u
    | y ==   0 &&  51 <= x && x <= 100  =  (x+100,    1,  0, 1)               -- leave 1 at top, enter 6 from left
    | y ==   0 && 101 <= x && x <= 150  =  (  200,x-100, -1, 0)               -- leave 2 at top, enter 6 from below
    | x ==  50 &&   1 <= y && y <=  50  =  (151-y,    1,  0, 1)               -- leave 1 left, enter 4 from left
    | x ==  50 &&  51 <= y && y <= 100  =  (  101,y- 50,  1, 0)               -- leave 3 left, enter 4 from above
    | x ==   0 && 100 <= y && y <= 150  =  (151-y,   51,  0, 1)               -- leave 4 left, enter 1 from left
    | x ==   0 && 151 <= y && y <= 200  =  (    1,y-100,  1, 0)               -- leave 6 left, enter 1 from above
    | y == 100 &&   1 <= x && x <=  50  =  (x+ 50,   51,  0, 1)               -- leave 4 at top, enter 3 left
    | y == 201 &&   1 <= x && x <=  50  =  (    1,x+100,  1, 0)               -- leave 6 at bottom, enter 2 from above
    | x ==  51 && 151 <= y && y <= 200  =  (  150,y-100, -1, 0)               -- leave 6 right, enter 5 below
    | x == 101 && 101 <= y && y <= 150  =  (151-y,  150,  0,-1)               -- leave 5 right, enter 2 right
    | x == 101 &&  51 <= y && y <= 100  =  (   50,y+ 50, -1, 0)               -- leave 3 right, enter 2 below
    | x == 151 &&   1 <= y && y <=  50  =  (151-y,  100,  0,-1)               -- leave 2 right, enter 5 right
    | y == 151 &&  51 <= x && x <= 100  =  (x+100,   50,  0,-1)               -- leave 5 below, enter 6 right
    | y ==  51 && 101 <= x && x <= 150  =  (x- 50,  100,  0,-1)               -- leave 2 below, enter 3 right
    | otherwise                         =  (y,x,v,u)

