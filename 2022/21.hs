import qualified Data.Map.Lazy as M

type Table = M.Map String Expr

read_line :: Table -> Bool -> String -> (String, Expr)
read_line tbl p2 s = case words s of
    "humn:" : _ | p2 -> ("humn", X)
    [ nm, num ] -> (init nm, I $ read num)
    [ nm, nm1, op, nm2 ] -> (init nm, getop op (M.findWithDefault undefined nm1 tbl)
                                               (M.findWithDefault undefined nm2 tbl))

      where
        getop _ | p2 && nm == "root:" = (:=)
        getop "+" = plus
        getop "-" = minus
        getop "*" = mult
        getop "/" = divide

main = do
    raw <- lines <$> getContents
    let tbl = M.fromList $ map (read_line tbl False) raw
    print . fmap (eval undefined) $ M.lookup "root" tbl                     -- part 1, easy.

    -- part 2:
    --
    -- - the operation of 'root' becomes '='
    -- - the number of 'humn' becomes different
    -- - pick the number such that root detects equality
    let tbl2 = M.fromList $ map (read_line tbl2 True) raw

    let x = case M.lookup "root" tbl2 of
                    Just (u := I v) -> solve u v
                    Just (I u := v) -> solve v u

    print x                                                             -- prints the solution
    print . fmap (eval x) $ M.lookup "root" tbl2                        -- check, should print "Just 0"




data Expr = I Int | X | Expr :+ Expr | Expr :- Expr | Expr :* Expr | Expr :/ Expr | Expr := Expr
  deriving Show

eval :: Int -> Expr -> Int
eval x (I i) = i
eval x  X    = x
eval x (e :+ f) = eval x e + eval x f
eval x (e :- f) = eval x e - eval x f
eval x (e :* f) = eval x e * eval x f
eval x (e :/ f) = eval x e `div` eval x f
eval x (e := f) = eval x e - eval x f



plus (I a) (I b) = I $ a + b
plus a b = a :+ b

minus (I a) (I b) = I $ a - b
minus a b = a :- b

mult (I a) (I b) = I $ a * b
mult a b = a :* b

divide (I a) (I b) = I $ a `div` b
divide a b = a :/ b



-- Find an assignment for X that makes the expressions to the int.
-- Works as long as X is used only once.
solve :: Expr -> Int -> Int
solve (u :+ I a) i = solve u (i - a)
solve (u :- I a) i = solve u (i + a)
solve (u :* I a) i = solve u (i `div` a)
solve (u :/ I a) i = solve u (i * a)
solve (I a :+ v) i = solve v (i - a)
solve (I a :- v) i = solve v (a - i)
solve (I a :* v) i = solve v (i `div` a)
solve (I a :/ v) i = solve v (a `div` i)
solve X i = i
solve e i = error $ show (e,i)

