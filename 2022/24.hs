{-# Language BangPatterns #-}
import Data.Array.Unboxed
import Data.Bits
import Data.List
import Data.Word

import qualified Data.Set as S

type Grid = UArray (Int,Int) Word8

data Dir = N | E| S | W deriving Enum

from_line = map (maybe 0 (setBit 0) . fmap fromEnum . to_dir) . init . tail
  where
    to_dir '^' = Just N
    to_dir '>' = Just E
    to_dir 'v' = Just S
    to_dir '<' = Just W
    to_dir  _  = Nothing

evolve :: Grid -> Grid
evolve g = accumArray setBit 0 bnds $
           [ (step pos (toEnum b), b)
           | (pos, bits) <- assocs g
           , b <- [0..3]
           , testBit bits b ]
  where
    bnds@((1,1),(w,h)) = bounds g

    step :: (Int,Int) -> Dir -> (Int,Int)
    step (x,y) W = if y == 1 then (x,h) else (x,y-1)
    step (x,y) S = if x == w then (1,y) else (x+1,y)
    step (x,y) E = if y == h then (x,1) else (x,y+1)
    step (x,y) N = if x == 1 then (w,y) else (x-1,y)


-- Naive BFS.  Takes intial point(s), returns shortest distance to exit.
-- If this blows up, I'll need A* instead.  Okay, finished part 1 in
-- 0.11s and part 2 in 0.33s.  Too bad, no cleverness is needed at all.
search :: (Int,Int) -> S.Set (Int,Int) -> Grid -> (Int, Grid)
search dest pts0 grid0 = go 0 grid0 pts0
  where
    ((1,1),(h,w)) = bounds grid0

    go !steps !grid !pts
        | S.null pts = error "can't get through"
        | dest `S.member` pts = (steps+1, grid')         -- one extra step at the end
        | otherwise = go (steps+1) grid' $ S.fromList
                        [ (x',y')
                        | (x,y) <- S.toList pts
                        , (x',y') <- [ (x+1,y), (x,y+1), (x-1,y), (x,y-1), (x,y) ]
                        , (1 <= x' && x' <= h && 1 <= y' && y' <= w)
                          || (x',y')==(0,1)  || (x',y')==(h+1,w)
                        , (x',y')==(0,1) || (x',y')==(h+1,w) || grid' ! (x',y') == 0 ]
          where
            grid' = evolve grid


main = do
    raw <- lines <$> getContents

    let height = length raw - 2
        width  = length (head raw) - 2
        grid = listArray ((1,1),(height, width)) $ concatMap from_line $ init $ tail raw :: Grid

        (steps1,  grid1) = search (height,width) (S.singleton (0,1)) grid
        (steps2,  grid2) = search (1,1) (S.singleton (height+1,width)) grid1
        (steps3, _grid3) = search (height,width) (S.singleton (0,1)) grid2

    print steps1                            -- part 1
    print $ steps1 + steps2 + steps3        -- part 2



