import Control.Monad
import Data.Bifunctor
import qualified Data.Sequence as Z

-- Apparently, numbers have an identity separate from their value...
step :: Z.Seq (Int,Int) -> Int -> Z.Seq (Int,Int)
step z a
    | 0 <= i' && i' < Z.length z - 1 && a == a'   =   Z.insertAt i' (a,v) $ Z.deleteAt i z
  where
    Just i = Z.findIndexL ((==) a . fst) z
    (a',v) = Z.index z i
    i'     = (i + v) `mod` (Z.length z - 1)

full_mix :: Z.Seq (Int,Int) -> Z.Seq (Int,Int)
full_mix z = foldl step z [1 .. Z.length z]


coords :: Z.Seq (Int,Int) -> Int
coords z = a + b + c
  where
    len = Z.length z
    Just i = Z.findIndexL ((==) 0 . snd) z
    (_,a)  = Z.index z ((i+1000) `mod` len)
    (_,b)  = Z.index z ((i+2000) `mod` len)
    (_,c)  = Z.index z ((i+3000) `mod` len)


main :: IO ()
main = do
    orig <- zip [1..] . map read . lines <$> getContents

    print . coords . full_mix $ Z.fromList orig                                                     -- part 1
    print . coords . (!! 10) . iterate full_mix . Z.fromList $ map (second (* 811589153)) orig      -- part 2




