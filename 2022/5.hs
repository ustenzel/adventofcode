import Data.List

main = do
    (hd,"":tl) <- break null . lines <$> getContents
    let stacks = parse_stacks hd
        moves  = map (parse_move . words) tl

    putStrLn $ map head $ foldl app_1move stacks (concatMap move1 moves)
    putStrLn $ map head $ foldl app_move stacks moves


data Move = Move Int Int Int

parse_move :: [String] -> Move
parse_move ["move",n,"from",f,"to",t] = Move (read n) (read f) (read t)

move1 :: Move -> [(Int,Int)]
move1 (Move n f t) = replicate n (f,t)

type Stack = String

parse_stacks :: [String] -> [Stack]
parse_stacks =
    map (dropWhile (==' ')) .
    map reverse .
    go .
    transpose .
    drop 1 .
    reverse
  where
    go (_:x:_:xs) = x : go (drop 1 xs)
    go _ = []


app_1move :: [Stack] -> (Int,Int) -> [Stack]
app_1move ss (f,t) = at t (x:) . at f tail $ ss
  where
    x = head $ ss !! (f-1)

app_move :: [Stack] -> Move -> [Stack]
app_move ss (Move n f t) = at t (xs++) . at f (drop n) $ ss
  where
    xs = take n $ ss !! (f-1)

at :: Int -> (a -> a) -> [a] -> [a]
at 1 f (a:as) = f a : as
at n f (a:as) = a : at (n-1) f as

