import Data.Bifunctor
import Data.List
import Data.Char

type Hand = [Int]

parse :: [String] -> (String, Int)
parse [cards, bid] = (cards, read bid)

to_strengthA :: Char -> Int
to_strengthA c = maybe (error "nope") id $ findIndex (== c) "23456789TJQKA"

to_strengthB :: Char -> Int
to_strengthB c = maybe (error "nope") id $ findIndex (== c) "J23456789TQKA"

type Type = [Int]

to_typeA :: Hand -> Type
to_typeA = reverse . sort . map length . group . sort

to_typeB :: Hand -> Type
to_typeB cs = g0 + nJ : gs          -- jokers beecome part of the biggest group
  where
    -- got to append an empty group, in case all cards are jokers
    g0:gs = (++ [0]) . reverse . sort . map length . group . sort . filter (/=0) $ cs      -- type without jokers
    nJ    = length $ filter (==0) cs

main = do
    hs <- map parse . map words . lines <$> getContents
    print . sum . zipWith (*) [1..] . map snd . sort . map (first ((\cs -> (to_typeA cs, cs)) . map to_strengthA)) $ hs
    print . sum . zipWith (*) [1..] . map snd . sort . map (first ((\cs -> (to_typeB cs, cs)) . map to_strengthB)) $ hs


