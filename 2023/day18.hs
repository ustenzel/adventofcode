import Data.Char
import Data.List

step :: (Int,Int) -> (String, Int) -> (Int,Int)
step (x,y) ("U",n)  =  (x,y-n)
step (x,y) ("D",n)  =  (x,y+n)
step (x,y) ("L",n)  =  (x-n,y)
step (x,y) ("R",n)  =  (x+n,y)

insnsA [a,b,_] = (a,read b)
insnsB [_,_,'(':'#':ds] = (dir, dist)
  where
    dir  = case last (init ds) of '0' -> "R" ; '1' -> "D" ; '2' -> "L" ; '3' -> "U"
    dist = foldl' (\a c -> 16*a + digitToInt c) 0 (init (init ds))

main = do
    inp <- map words . lines <$> getContents

    print $ area2 (map insnsA inp) `div` 2 + peri (map insnsA inp) `div` 2 + 1
    print $ area2 (map insnsB inp) `div` 2 + peri (map insnsB inp) `div` 2 + 1

area2 is = let ps@((x0,y0):_) = scanl step (0,0) is
           in sum $ zipWith (\(x1,y1) (x2,y2) -> (x1-x0)*(y2-y0) - (x2-x0)*(y1-y0)) ps (tail ps)

peri = sum . map snd
