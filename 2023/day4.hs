{-# Language OverloadedStrings #-}
import Control.Applicative
import Data.List

import qualified Data.ByteString.Char8 as C
import qualified Data.Attoparsec.ByteString.Char8 as A

pCard :: A.Parser ([Int], [Int])
pCard = (,) <$  A.string "Card"
            <*  A.skipSpace
            <*  A.decimal
            <*  A.char ':'
            <*  A.skipSpace
            <*> some (A.decimal <* A.skipSpace)
            <*  A.char '|'
            <*  A.skipSpace
            <*> some (A.decimal <* A.skipSpace)


main = do
    cs <- either fail pure . mapM (A.parseOnly pCard) . C.lines =<< C.getContents
    print $ sum $ map (uncurry score) cs
    print $ sum $ win_copies $ map ((,) 1) cs


score :: [Int] -> [Int] -> Int
score win have = case length (intersect win have) of
    0 -> 0
    n -> 2^(n-1)


win_copies :: [(Int, ([Int], [Int]))] -> [Int]
win_copies [] = []
win_copies ((n, (win,have)):cs) = n : win_copies (add_copies n m cs)
  where
    m = length $ intersect win have


add_copies n 0 cs = cs
add_copies n m ((n1,c):cs) = (n1+n,c) : add_copies n (m-1) cs
