{-# Language OverloadedStrings #-}
import Control.Applicative

import qualified Data.Attoparsec.ByteString.Char8   as A
import qualified Data.ByteString.Char8              as B

data Bag = Bag { nred, ngreen, nblue :: !Int } deriving Show

instance Semigroup Bag where
    Bag r g b <> Bag r' g' b' = Bag (r+r') (g+g') (b+b')

instance Monoid Bag where
    mempty = Bag 0 0 0


data Game = Game { gid :: !Int, hands :: [Bag] } deriving Show

pGame :: A.Parser Game
pGame =
    Game <$  A.string "Game"
         <*  A.skipSpace
         <*> A.decimal
         <*  A.skipSpace
         <*  A.char ':'
         <*  A.skipSpace
         <*> pHand `A.sepBy1` (A.char ';' *> A.skipSpace)

  where
    pHand :: A.Parser Bag
    pHand  = mconcat <$> pBalls `A.sepBy1` (A.char ',' *> A.skipSpace)

    pBalls :: A.Parser Bag
    pBalls = A.decimal <* A.skipSpace <**> pColor <* A.skipSpace

    pColor :: A.Parser (Int -> Bag)
    pColor = A.choice [ (\n -> Bag n 0 0) <$ A.string "red"
                      , (\n -> Bag 0 n 0) <$ A.string "green"
                      , (\n -> Bag 0 0 n) <$ A.string "blue" ]


possible :: Bag -> Game -> Bool
possible (Bag r0 g0 b0) game = all smaller (hands game)
  where
    smaller (Bag r g b) = r <= r0 && g <= g0 && b <= b0

maxBag :: Bag -> Bag -> Bag
maxBag (Bag r g b) (Bag r' g' b') = Bag (max r r') (max g g') (max b b')

minGame :: Game -> Bag
minGame = foldr maxBag mempty . hands

powerBag :: Bag -> Int
powerBag (Bag r g b) = r*g*b

main = do
    Right gs <- mapM (A.parseOnly pGame) . B.lines <$> B.getContents

    -- part 1
    print . sum . map gid . filter (possible (Bag 12 13 14)) $ gs

    -- part 2
    print . sum . map powerBag . map minGame $ gs
