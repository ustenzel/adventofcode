#! /usr/bin/env cabal
{- cabal:
   build-depends: base, containers
-}

import Data.List
import Data.Char
import qualified Data.IntMap.Strict as I

hash :: Int -> Char -> Int
hash acc c = ((acc + fromIntegral (ord c)) * 17) `mod` 256

hashString = foldl' hash 0

split [] = []
split s = case break (==',') s of (l,r) -> l : split (drop 1 r)

mainA =
    print . sum . map hashString . split . filter (/='\n') =<< getContents



data Lens = Lens !String !Int           -- label, focal length

type Box = [Lens]

step :: I.IntMap Box -> String -> I.IntMap Box
step bs s = case break (`elem` "=-") s of
    (lbl, "-") -> let b = I.findWithDefault [] (hashString lbl) bs :: Box
                      b' = filter (\(Lens l _) -> l /= lbl) b :: Box
                  in I.insert (hashString lbl) b' bs

    (lbl, '=':num_) -> let b = go $ I.findWithDefault [] (hashString lbl) bs
                           n = read num_
                           go :: [Lens] -> [Lens]
                           go [             ]             = [Lens lbl n]
                           go (Lens l f : ls) | l == lbl  = Lens lbl n : ls
                                              | otherwise = Lens l f : go ls
                       in I.insert (hashString lbl) b bs

power :: I.IntMap Box -> Int
power = sum . map (\(i,ls) -> sum $ zipWith (\j (Lens _ f) -> (i+1)*j*f) [1..] ls) . I.toList

main =
    print . power . foldl' step I.empty . split . filter (/='\n') =<< getContents

