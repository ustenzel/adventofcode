{-# Language BangPatterns, RecordWildCards #-}

import Data.List
import Data.Ratio
import Debug.Trace

data Hail = Hail { x,y,z, u,v,w :: Integer }
 deriving Show

parse :: String -> Hail
parse s = case map read $ words $ map clean s of
    [x,y,z,u,v,w] -> Hail{..}
  where
    clean ',' = ' '
    clean '@' = ' '
    clean  c  =  c

part1 :: [Hail] -> Int
part1 hails = length [ () | (Hail x1 y1 _ u1 v1 _):hs <- tails hails
                          , (Hail x2 y2 _ u2 v2 _)    <- hs
                          , u1*v2 - v1*u2 /= 0
                          , let t1 = (v2*(x2-x1) - u2*(y2-y1)) % (u1*v2-v1*u2)
                          , t1 >= 0
                          , let t2 = (v1*(x1-x2) - u1*(y1-y2)) % (u2*v1-v2*u1)
                          , t2 >= 0
                          , let x = fromIntegral x1 + t1 * fromIntegral u1
                          , low <= x && x <= high
                          , let y = fromIntegral y1 + t1 * fromIntegral v1
                          , low <= y && y <= high ]
  where
    low  = 200000000000000
    high = 400000000000000


is_parallel h1 h2  =  v h1 * u h2 == u h1 * v h2 && w h1 * u h2 == u h1 * w h2


do_intersect (Hail x1 y1 z1  u1 v1 w1) (Hail x2 y2 z2  u2 v2 w2)
    | u1*v2 - v1*u2 == 0                                                                =  Nothing
    | fromIntegral z1 + t1 * fromIntegral w1 /= fromIntegral z2 + t2 * fromIntegral w2  =  Nothing
    | otherwise                                                                         =  Just (t1,t2)
  where
    t1 = (v2*(x2-x1) - u2*(y2-y1)) % (u1*v2-v1*u2)
    t2 = (v1*(x1-x2) - u1*(y1-y2)) % (u2*v1-v2*u1)


-- Part 2, the first plan:
--
-- - If two hail stones' trajectories are parallel, they define a plane,
--   and the rock's trajectory must lie in that plane.
-- - If two hail stones' trajectories intersect, they define a plane and
--   either both hail stones and the rock meet each other
--   simultaneously, or the rock's trajectory must lie in that plane.
--
-- All answers are zero.  That didn't work at all.

part2_failure :: [Hail] -> String
part2_failure hails = unlines [ "number of parallel trajectories: " ++ show n_parallel
                              , "number of coplanar trajectories: " ++ show n_coplanar
                              , "number of colliding hail stones: " ++ show n_collisions ]
  where
    n_parallel   = sum [ fromEnum $ is_parallel h1 h2 | h1:hs <- tails hails, h2 <- hs ]
    n_coplanar   = sum [ 1                            | h1:hs <- tails hails, h2 <- hs, Just    _    <- [do_intersect h1 h2] ]
    n_collisions = sum [ fromEnum (t1 == t2)          | h1:hs <- tails hails, h2 <- hs, Just (t1,t2) <- [do_intersect h1 h2] ]

-- Part 2, second plan:  algebraic solution inspired by reddit.
--
-- Let p0={x0,y0,z0} be the initial position of the rock and
-- s0={u0,v0,w0} its speed.  Let pi and si be initial position and speed
-- of hailstone i.  For rock and hailstone i to collide at time ti, we
-- must have
--
-- p0 + ti s0 == pi + ti si
--
-- p0 - pi ==  ti (si - s0)
--
-- and therefore, these vectors are parallel:
--
-- (p0 - pi) × (s0 - si) == 0
--
-- p0×s0 - pi×s0 - p0×si + pi×si == 0
--
-- Only the p0×s0 term is not linear.  Subtract the same equation for another hail stone:
--
--   p0×s0 - pi×s0 - p0×si + pi×si - p0×s0 + pj×s0 + p0×sj - pj×sj == 0
--   (pj-pi)×s0 + p0×(sj-si) = pj×sj - pi×si
--
-- This expands to three linear(!) equations in x0,y0,z0,u0,v0,w0.
-- In matrix form:
--                                                          (u0)
-- (   0     (zi-zj)  (yj-yi)     0     (wj-wi)  (vi-vj))   (v0)   ( yj wj - zj vj - yi wi + zi vi )
-- ((zj-zi)     0     (xi-xj)  (wi-wj)     0     (uj-ui)) * (w0) = ( zj uj - xj wj - zi ui + xi wi )
-- ((yi-yj)  (xj-xi)     0     (vj-vi)  (ui-uj)     0   )   (x0)   ( xj vj - yj uj - xi vi + yi ui )
--                                                          (y0)
--                                                          (z0)
--
-- Adding the same equations for a third hail stone should give a system with exactly one solution.

part2_the_second (h1:h2:h3:_) = gaussElim $ map (map fromIntegral) $ mk_matrix h1 h2 ++ mk_matrix h2 h3

mk_matrix (Hail xi yi zi ui vi wi) (Hail xj yj zj uj vj wj) =
    [[   0  , zi-zj, yj-yi,   0  , wj-wi, vi-vj,   yj*wj - zj*vj - yi*wi + zi*vi ]
    ,[ zj-zi,   0  , xi-xj, wi-wj,   0  , uj-ui,   zj*uj - xj*wj - zi*ui + xi*wi ]
    ,[ yi-yj, xj-xi,   0  , vj-vi, ui-uj,   0  ,   xj*vj - yj*uj - xi*vi + yi*ui ]]


format_table :: [[String]] -> String
format_table cells = unlines $ map (unwords . zipWith pad widths) cells
  where
    widths  = map maximum $ transpose $ map (map length) cells
    pad n s = replicate (n + 1 - length s) ' ' ++ s

main = do
    hails <- map parse . lines <$> getContents
    print $ part1 hails
    putStrLn $ part2_failure hails
    let rock@[ur,vr,wr,xr,yr,zr] = part2_the_second hails :: [Rational]
    print rock
    print $ xr+yr+zr


gaussElim :: (Num a, Ord a, Fractional a, Show a) => [[a]] -> [a]
gaussElim [] = []
gaussElim ls = go $ sortOn (negate . abs . head) ls
  where
    go mat@((a0:as):li) = x0:xs
      where
        xs  = gaussElim [ zipWith (\a y -> y - a*y0/a0) as ys | y0:ys <- li ]
        !x0 = (last as - sum (zipWith (*) as xs)) / a0


