{-# Language BangPatterns, OverloadedStrings #-}
import Control.Applicative

import qualified Data.ByteString.Char8 as C
import qualified Data.HashMap.Strict as H
import qualified Data.Attoparsec.ByteString.Char8 as A

type Name = C.ByteString

data Part a = Part { x, m, a, s :: a } deriving (Show, Read)

data Var = X | M | A | S
data Op = Lt | Gt

rating (Part x m a s) = x + m + a + s

combinations p = product [ h-l+1 | f <- [x,m,a,s], let (l,h) = f p ]

data Rule = Accept | Reject | Jump Name | Cond Var Op Int Rule

parseLn :: A.Parser (Name, [Rule])
parseLn = (,) <$> A.takeWhile1 A.isAlpha_ascii <* A.char '{' <*> parseRule `A.sepBy1` A.char ',' <* A.char '}'

parseRule = A.choice [ Accept <$  A.char 'A'
                     , Reject <$  A.char 'R'
                     , parseCond
                     , Jump   <$> A.takeWhile1 A.isAlpha_ascii ]

parseCond = Cond <$> A.choice [ f <$ A.char c | (f,c) <- zip [X,M,A,S] "xmas" ]
                 <*> A.choice [ Lt <$ A.char '<', Gt <$ A.char '>' ]
                 <*> A.decimal <* A.char ':'
                 <*> parseRule


main = do
    (workflows_, _:parts_) <- break C.null . C.lines <$> C.getContents

    Right workflows <- pure $ fmap H.fromList $ mapM (A.parseOnly parseLn) workflows_
    let parts = map read $ map ("Part " ++) $ map C.unpack parts_ :: [Part Int]

    print $ sum $ map rating $ filter (work workflows) parts
    print $ sum $ map combinations $ workGen workflows

work :: H.HashMap Name [Rule] -> Part Int -> Bool
work wfs part = go "in"
  where
    go wf = case H.lookup wf wfs of
        Just rs -> go' rs

    go' (Accept        :  _)  =  True
    go' (Reject        :  _)  =  False
    go' (Jump  nm      :  _)  =  go nm
    go' (Cond v op c r : rs)  =  if v' part `op'` c then go' (r:rs) else go' rs
      where
        v' = case v of X -> x ; M -> m ; A -> a ; S -> s
        op' = case op of Lt -> (<) ; Gt -> (>)

workGen :: H.HashMap Name [Rule] -> [Part (Int,Int)]
workGen wfs = go "in" (Part full full full full) []
  where
    full = (1,4000)

    go wf = case H.lookup wf wfs of
        Just rs -> go' rs

    go' _ p | any (\(l,h) -> l > h) $ map ($ p) [x,m,a,s] = id
    go' (Accept        :  _) p  =  (:) p
    go' (Reject        :  _) p  =  id
    go' (Jump  nm      :  _) p  =  go nm p
    go' (Cond v op c r : rs) p  =  go' (r:rs) (p' acc) . go' rs (p' rej)
      where
        acc (l,h) = case op of Lt -> (l,c-1) ; Gt -> (c+1,h)
        rej (l,h) = case op of Lt -> (c,h) ; Gt -> (l,c)

        p' f = case v of X -> p { x = f $ x p }
                         M -> p { m = f $ m p }
                         A -> p { a = f $ a p }
                         S -> p { s = f $ s p }

