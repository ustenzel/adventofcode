import           Control.Monad
import           Data.Array.Unboxed
import qualified Data.HashSet as S
import           Data.List

neighborset grid = foldl' S.union S.empty . S.map (S.fromList . neighbors grid)

neighbors grid (y,x) = [ p | p <- [(y+1,x),(y-1,x),(y,x+1),(y,x-1)]
                           , inRange (bounds grid) p
                           , grid ! p == '.' || grid ! p == 'S' ]

main = do
    inp <- lines <$> getContents
    let w     = length (head inp)
        h     = length inp
        grid1 = listArray ((0,0),(h-1,w-1)) $ concat inp :: UArray (Int,Int) Char
        ini   = S.fromList [ p | (p,'S') <- assocs grid1 ]

    print $ S.size $ iterate (neighborset grid1) ini !! 64           -- part 1, brute force

    -- part 2:
    -- We combine brute force with assumptions about the input and a few
    -- spoilers from the intertubes.  We are asked to do a great many
    -- steps, which is exactly enough to reach the edge of the initial
    -- grid, and then traverse an even number of copies.
    --
    -- Assume that there is a pattern to the reachable areas, and that
    -- the pattern repeats for every two copies of the initial grid we
    -- traverse further (which it does, because the input is nice), we
    -- will traverse (1) just to the edge, (2) to the edge of the second
    -- copy in every direction, and (3) to the fourth copy in every
    -- direction.  Then we fit a degree 2 polynomial and plug in a large
    -- number.

    -- First, we need 9 copies of the initial grid, with S in the same
    -- position.
    let inp2 = concat . replicate 9 $ map (concat . replicate 9) inp
        grid2 = listArray ((-4*h,-4*w),(5*h-1,5*w-1)) $ concat inp2 :: UArray (Int,Int) Char

    -- This could be done more cleverly, but I can't be bothered.  The
    -- stupid code takes less than 5 minutes, but comsumed 14GiB.
    let area0 : areas0 = drop (w `div` 2) $ map S.size $ iterate (neighborset grid2) ini
        area1 : areas1 = drop (2*w-1) areas0
        area2 : _      = drop (2*w-1) areas1

    print area0
    print area1
    print area2

    -- now set
    -- area0 = areaN 0 =  0*A + 0*B + C
    -- area1 = areaN 2 =  4*A + 2*B + C
    -- area2 = areaN 4 = 16*A + 4*B + C

    -- solve for A,B,C and back-substitute:
    let areaN n = n^2 * (2*area2 - 4*area1 + 2*area0 ) `div` 16 + n * (4*area1 - 3*area0 - area2 ) `div` 4 + area0

    let (ntiles, remainder) = 26501365 `divMod` w
    guard $ remainder == w `div` 2 && even ntiles
    print $ areaN ntiles

