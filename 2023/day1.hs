import Data.Char ( isDigit, digitToInt )
import Data.List ( isPrefixOf, tails )
import Data.Maybe ( listToMaybe, mapMaybe )

mainA = print . sum . map ((\ds -> 10 * head ds + last ds) . map digitToInt . filter isDigit) . lines =<< getContents
main  = print . sum . map ((\ds -> 10 * head ds + last ds) . mapMaybe wordToInt . tails) . lines =<< getContents

wordToInt (c:_) | isDigit c = Just (digitToInt c)
wordToInt s = listToMaybe [ i | (i,w) <- zip [1..] numwords, w `isPrefixOf` s ]
  where
    numwords = words "one two three four five six seven eight nine"

