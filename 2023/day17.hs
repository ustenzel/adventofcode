import Data.Array.Unboxed
import Data.List
import Data.Char
import qualified Data.PQueue.Prio.Min as PQ
import qualified Data.Set as S

data Dir = N | E | S | W deriving (Ord, Eq, Show, Enum)

main = do
    inp <- lines <$> getContents
    let xmax = length (head inp)
        ymax = length inp
        grid = listArray ((1,1), (ymax,xmax)) $ map digitToInt $ concat inp

    let done (y,x,_) = y == ymax && x == xmax

    print $ dijkstra done (next' grid (1,3))  S.empty (PQ.insert 0 (1,1,E) $ PQ.insert 0 (1,1,S) $ PQ.empty)
    print $ dijkstra done (next' grid (4,10)) S.empty (PQ.insert 0 (1,1,E) $ PQ.insert 0 (1,1,S) $ PQ.empty)

    print $ astar done (next grid (1,3))  S.empty (PQ.insert 0 (0,(1,1,E)) $ PQ.insert 0 (0,(1,1,S)) $ PQ.empty)
    print $ astar done (next grid (4,10)) S.empty (PQ.insert 0 (0,(1,1,E)) $ PQ.insert 0 (0,(1,1,S)) $ PQ.empty)




dijkstra :: (Ord k, Ord s, Eq s) => (s -> Bool) -> (k -> s -> [(k,s)]) -> S.Set s -> PQ.MinPQueue k s -> k
dijkstra done next closed open
    | done p              = k
    | p `S.member` closed = dijkstra done next closed open'
    | otherwise           = dijkstra done next
                                     (S.insert p closed)
                                     (foldl' (\pq (k,p) -> PQ.insert k p pq) open (next k p))
  where
    ((k,p), open') = PQ.deleteFindMin open


astar :: (Ord k, Ord s, Eq s) => (s -> Bool) -> (k -> s -> [(k,(k,s))]) -> S.Set s -> PQ.MinPQueue k (k,s) -> k
astar done next closed open
    | done p              = k
    | p `S.member` closed = astar done next closed open'
    | otherwise           = astar done next
                                     (S.insert p closed)
                                     (foldl' (\pq (ek,(ak,p)) -> PQ.insert ek (ak,p) pq) open (next k p))
  where
    ((_,(k,p)), open') = PQ.deleteFindMin open

next' :: UArray (Int,Int) Int -> (Int,Int) -> Int -> (Int,Int,Dir) -> [(Int, (Int,Int,Dir))]
next' grid lim k s = snd <$> next grid lim k s

next :: UArray (Int,Int) Int -> (Int,Int) -> Int -> (Int,Int,Dir) -> [(Int, (Int, (Int,Int,Dir)))]
next grid (nmin,nmax) k (y,x,d) = [ (ek, (ak, (y',x',d')))
                                  | n <- [nmin..nmax]
                                  , let y' = y+n*dy
                                  , let x' = x+n*dx
                                  , inRange (bounds grid) (y',x')
                                  , d' <- ds
                                  , let ak = k + sum [ grid ! (y+i*dy, x+i*dx) | i <- [1..n] ]
                                  , let ek = ak + abs (ymax-y') + abs (xmax-x') ]
  where
    ds = case d of N -> [E,W] ; S -> [E,W] ; E -> [N,S] ; W -> [N,S]
    (dy,dx) = case d of N -> (-1,0) ; E -> (0,1) ; S -> (1,0) ; W -> (0,-1)
    (_,(ymax,xmax)) = bounds grid

