import Control.Monad
import Data.Char
import qualified Data.HashMap.Strict as M

parse str = case words $ filter (\c -> isAlpha c || c == ' ') str of
    [x,y,z] -> (x,(y,z))

step nodes n dir = if dir == 'L' then fst ns else if dir == 'R' then snd ns else error "huh?"
  where
    Just ns = M.lookup n nodes

main = do
    dirs : _ : nodes_ <- lines <$> getContents

    print $ length dirs

    let nodes = M.fromList $ map parse nodes_
    print $ length $ takeWhile (/="ZZZ") $ scanl (step nodes) "AAA" (cycle dirs)

    let anodes = filter ((=='A') . last) $ M.keys nodes

    -- the solution is the product of cycle lengths, even though there
    -- is no reason for this to work
    let ls = map (\n0 -> length $ takeWhile ((/='Z').last) $ scanl (step nodes) n0 (cycle dirs)) anodes
    print $ length dirs * product (map (`div` length dirs) ls)

