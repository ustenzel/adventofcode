import Data.List

h_reflections :: Eq a => Int -> [[a]] -> [Int]
h_reflections n ls = [ i | (i,l,r) <- zip3 [0..] (inits ls) (tails ls)
                       , not (null l || null r)
                       , sum (zipWith diff (reverse l) r) == n ]
  where
    diff :: Eq a => [a] -> [a] -> Int
    diff = fmap sum . fmap (map fromEnum) . zipWith (/=)

main = do
    patterns <- map (filter (not . null)) . groupBy (\_ a -> not (null a)) . lines <$> getContents

    let go n = let hh = concatMap (h_reflections n) patterns
                   vv = concatMap (h_reflections n . transpose) patterns
               in print $ 100 * sum hh + sum vv

    go 0        -- part 1, match without smudges
    go 1        -- part 2, match with exactly one smudge


