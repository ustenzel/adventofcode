import Data.Char
import Data.List

data Range = R { r_start :: !Int, r_len :: !Int }
type Map = Range -> [Range]     -- mapping can split ranges

parse :: String -> ([Int], [Map])
parse inp = ( map read . drop 1 $ words hd
            , map (to_map . map (map read . words) . filter is_clean) . groupBy (\_ -> not . null) $ tl )
  where
    hd:tl = lines inp
    is_clean s = not (null s) && all (\c -> isDigit c || c == ' ') s

to_map :: [[Int]] -> Map
to_map [          ] (R x l) = [R x l]
to_map ([d,s,m]:ts) (R x l)
    | x+l <= s                =  to_map ts (R x l)
    | x < s                   =  to_map ts (R x (s-x)) ++ to_map ([d,s,m]:ts) (R s (x+l-s))
    | x+l <= s+m              =  R (x-s+d) l : []
    | x < s+m                 =  R (x-s+d) (s+m-x) : to_map ts (R (s+m) (x+l-s-m))
    | otherwise               =  to_map ts (R x l)

compose :: [Map] -> Map
compose ms r = foldl' (\rs m -> concatMap m rs) [r] ms

to_ranges (x:l:xs) = R x l : to_ranges xs
to_ranges _        = []

main = do
    (seeds, maps) <- parse <$> getContents

    print $ minimum $ map r_start $ concatMap (compose maps) $ map (`R` 1) seeds
    print $ minimum $ map r_start $ concatMap (compose maps) $ to_ranges seeds

