import Data.Array.IArray
import Data.List

main = do
    inp <- map parse . lines <$> getContents
    print . sum $ map (uncurry (ways 0)) inp            -- part 1
    print . sum $ map (uncurry mways) inp               -- this better be the same
    print . sum $ map (uncurry mways . fatten) inp      -- part 2

parse l = case words l of
    [str, ints] -> (str, parse_ints ints)

parse_ints :: String -> [Int]
parse_ints [] = []
parse_ints s = case reads s of
    [(i,s')] -> i : parse_ints (drop 1 s')

fatten (s,ls) = (intercalate "?" [s,s,s,s,s], concat $ replicate 5 ls)

ways :: Int -> String -> [Int] -> Int
ways 0 [     ] [ ] = 1
ways 0 ('.':s) [ ] = ways 0 s []
ways 0 ('?':s) [ ] = ways 0 s []
ways _     _   [ ] = 0
ways n [     ] [l] = if n == l then 1 else 0
ways n [     ]  _  = 0

ways 0 ('.':s)    ls  = ways 0 s ls
ways n ('.':s) (l:ls) = if n == l then ways 0 s ls else 0

ways 0 ('#':s)    ls  = ways 1 s ls
ways n ('#':s) (l:ls) = if n < l then ways (n+1) s (l:ls) else 0

ways 0 ('?':s)    ls           =  ways 0 s ls + ways 1 s ls
ways n ('?':s) (l:ls) | n < l  =  ways (n+1) s (l:ls)
                      | n == l =  ways 0 s ls
                      | n > l  =  0


-- memo table
-- current group length n
-- index into s
-- s
-- index into ls
-- length list ls
--
mways_ :: Array (Int,Int,Int) Int -> Int -> Int -> String -> Int -> [Int] -> Int
mways_ tab 0 i [     ] j [ ] = 1
mways_ tab 0 i ('.':s) j [ ] = tab ! (0,i+1,j)           -- ways 0 s []
mways_ tab 0 i ('?':s) j [ ] = tab ! (0,i+1,j)           -- ways 0 s []
mways_ tab _ i     _   j [ ] = 0
mways_ tab n i [     ] j [l] = if n == l then 1 else 0
mways_ tab n i [     ] j  _  = 0

mways_ tab 0 i ('.':s) j    ls  = tab ! (0,i+1,j)        -- ways 0 s ls
mways_ tab n i ('.':s) j (l:ls) = if n == l then tab ! (0,i+1,j+1) else 0        -- ways 0 s ls else 0

mways_ tab 0 i ('#':s) j    ls  = tab ! (1,i+1,j)        -- ways 1 s ls
mways_ tab n i ('#':s) j (l:ls) = if n < l then tab ! (n+1,i+1,j) else 0       -- ways (n+1) s (l:ls) else 0

mways_ tab 0 i ('?':s) j    ls           =  tab ! (0,i+1,j) + tab ! (1,i+1,j)    -- ways 0 s ls + ways 1 s ls
mways_ tab n i ('?':s) j (l:ls) | n < l  =  tab ! (n+1,i+1,j)                    -- ways (n+1) s (l:ls)
                                | n == l =  tab ! (0,i+1,j+1)                    -- ways 0 s ls
                                | n > l  =  0


tabulate :: String -> [Int] -> Array (Int,Int,Int) Int
tabulate s ls = self
  where
    bnds = ((0,0,0), (maximum ls, length s, length ls))
    self = listArray bnds [ mways_ self n i (drop i s) j (drop j ls) | (n,i,j) <- range bnds ]

mways s ls = tabulate s ls ! (0,0,0)
