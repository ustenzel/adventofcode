import qualified Data.HashMap.Strict as H
import Data.List

roll [     ] = [ ]
roll ('#':s) = '#' : roll s
roll ('O':s) = 'O' : roll s
roll s | null boulders  =  space ++ roll s2
       | otherwise      =  boulders ++ roll (space ++ s2)
  where
    (space, s1) = span (=='.') s
    (boulders,s2) = span (=='O') s1

rollW = map roll
rollN = transpose . rollW . transpose
rollE = map reverse . rollW . map reverse
rollS = reverse . transpose . rollW . transpose . reverse

load = sum . zipWith (\w c -> if c == 'O' then w else 0) [1..] . reverse

spin = rollE . rollS . rollW . rollN

main = do
    ini <- lines <$> getContents
    print . sum . map load . map roll $ transpose ini       -- part 1

    let (ls, x0, p) = find_cycle $ iterate spin ini         -- part 2
        n = (1000000000-x0) `mod` p
        fin = iterate spin ls !! n

    print . sum . map load $ transpose fin


find_cycle :: [[String]] -> ([String],Int,Int)
find_cycle = go 0 H.empty
  where
    go !i m (x:xs) = case H.lookup x m of
        Nothing -> go (i+1) (H.insert x i m) xs
        Just  j -> (x, j, i-j)
