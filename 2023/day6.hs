
-- hold button for b ms, total time is t ms
dist :: Int -> Int -> Int
dist b t = b * (t-b)

-- ways to win:  number of integers such that 'dist b t' is greater than s.
-- bt - b^2 > s
-- b^2 - bt + s < 0
-- b between -t/2 ± sqrt( t^2/4-s )
-- Strange rounding is necessary, because bmin/bmax must not be included
-- in the result, even if they happen to be integral.

ways :: Int -> Int -> Int
ways t s = bmax-bmin-1
  where
    t2 = fromIntegral t / 2
    bmin = floor   $ -t2 - sqrt( t2*t2 - fromIntegral s )
    bmax = ceiling $ -t2 + sqrt( t2*t2 - fromIntegral s )


mainA = do
    times:scores:_ <- map (map read . drop 1 . words) . lines <$> getContents
    print $ product $ zipWith ways times scores

main = do
    time:score:_ <- map (read . concat . drop 1 . words) . lines <$> getContents
    print $ ways time score
