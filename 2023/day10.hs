import Data.Array.Unboxed
import Data.Array.IArray
import Data.List
import Data.Maybe
import Data.Char

to_grid :: [String] -> UArray (Int,Int) Char
to_grid ls = listArray ((0,0),(h-1,w-1)) $ concat ls
  where
    h = length ls
    w = length (head ls)

data Dir = N | E | S | W

step :: UArray (Int,Int) Char -> ((Int,Int),Dir) -> Maybe ((Int,Int),Dir)
step grid ((y,x),dir) | inRange (bounds grid) next = (,) next <$> dir'
                      | otherwise                  = Nothing
  where
    next = case dir of N -> (y-1,x)
                       E -> (y,x+1)
                       S -> (y+1,x)
                       W -> (y,x-1)

    dir' = case (dir, grid ! next) of (N,'7') -> Just W
                                      (N,'F') -> Just E
                                      (N,'|') -> Just N
                                      (E,'7') -> Just S
                                      (E,'J') -> Just N
                                      (E,'-') -> Just E
                                      (S,'L') -> Just E
                                      (S,'J') -> Just W
                                      (S,'|') -> Just S
                                      (W,'L') -> Just N
                                      (W,'F') -> Just S
                                      (W,'-') -> Just W
                                      _       -> Nothing

step' g = fmap (\q->(q,q)) . step g

main = do
    grid <- to_grid . lines <$> getContents
    let (y0,x0):_ = [ p | (p,'S') <- assocs grid ]
        [p1,p2] = mapMaybe (step grid) . map ((,) (y0,x0)) $ [N,E,S,W]

    -- part 1
    let dmax = (+) 2 $ length $ takeWhile (\(a,b) -> fst a /= fst b) $ zip (unfoldr (step' grid) p1) (unfoldr (step' grid) p2)
    print dmax

    -- part 2
    -- twice the area enclosed by the _center line_ of the loop
    let area2 d0 = let ps = map fst $ unfoldr (step' grid) d0
                   in abs $ sum $ zipWith (\(y1,x1) (y2,x2) -> (y2-y1)*(x1-x0) - (y1-y0)*(x2-x1)) ps (tail ps)

    -- the tiles around the perimeter (2*dmax) contribute one half tile
    -- each to the are which we aren't supposed to count.  corners
    -- cancel each other out, except for effectively four corners.
    -- those contributed a quarter tile each (total 1), but we already subtracted
    -- half a tile each (total 2), so add one tile back in.
    print $ area2 p1 `div` 2 - dmax + 1

