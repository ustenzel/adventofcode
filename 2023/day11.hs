import Data.List

-- this was feasible only in part 1
-- expand :: [String] -> [String]
-- expand = concatMap (\l -> if all (=='.') l then [l,l] else [l])

expand n = go 0 0
  where
    go _ _ [        ]              =  []
    go i o ((y,x):ps) | y == i     =  (o,x) : go i o ps
                      | otherwise  =  go y (o + 1 + n*(y-i-1)) ((y,x):ps)

total_dist ps = sum [ abs (x-u) + abs (y-v)
                    | (x,y):ps' <- tails ps
                    , (u,v) <- ps' ]

expand_twice n = expand n . sort . map (uncurry (flip (,))) . expand n

main = do
    field <- lines <$> getContents
    let gs = [ (x,y) | (x,l) <- zip [0..] field
             , (y,c) <- zip [0..] l
             , c == '#' ]

    -- part 1
    print $ total_dist $ expand_twice 2 gs

    -- examples given for part 2
    print $ total_dist $ expand_twice 10 gs
    print $ total_dist $ expand_twice 100 gs

    -- part 2
    print $ total_dist $ expand_twice 1000000 gs


