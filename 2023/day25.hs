{-# OPTIONS_GHC -Wno-incomplete-uni-patterns #-}
import Control.Arrow                    ( (&&&) )
import Data.Bifunctor                   ( first, second )
import Data.Foldable                    ( foldl' )
import Data.Hashable                    ( Hashable(..) )
import qualified Data.ByteString.Char8 as C
import qualified Data.HashMap.Strict   as M
import qualified Data.HashSet          as S
import qualified Data.HashPSQ          as P

main :: IO ()
main = do
    g <- mkGraph . map (C.init . head &&& tail)  . map C.words . C.lines <$> C.getContents

    let size1 = length $ fst $ minCut g
    print $ size1 * (P.size g - size1)


-- | Graph represented as weighted adjacency lists.  Priorities should
-- always be zero and are used itnernally in the Stoer-Wagner algorithm.
-- Keys (node names) are lists, because we have to merge them.

type Graph k w = P.HashPSQ [k] w (M.HashMap [k] w)

-- | Constructs a graph from the list of adjacency list, i.e. the input.
-- Makes sure the graph is undirected, sets all edge wights to one and
-- all priorities to zero.

mkGraph :: (Foldable f, Hashable k, Ord k) => f (k,[k]) -> Graph k Int
mkGraph = foldl' (\m1 (a,bs) -> foldl' (\m b -> p'alter (to b) [a] $! p'alter (to a) [b] m) m1 bs) P.empty
  where
    to y  Nothing     = Just (0, M.singleton [y] 1)
    to y (Just (_,v)) = Just (0, M.insert    [y] 1 v)


-- | Given a the weighted adjacency map of a symmetric graph, returns a
-- minimum cut and the weight of the cut.  Uses Stoer-Wagner algorithm.

minCut :: (Hashable k, Num w, Ord k, Ord w) => Graph k w -> ([k], w)
minCut adj
    | P.size adj' < 2      =  (cut_of_phase, w_of_phase)
    | w_of_phase < best_w  =  (cut_of_phase, w_of_phase)
    | otherwise            =  (best_cut, best_w)
  where
    (adj', cut_of_phase, w_of_phase) = minCutPhase adj
    (best_cut, best_w) = minCut adj'


-- Utility functions, because the API of HashPSQ is a bit impoverished.
p'alter  :: (Hashable k, Ord k, Ord p) => (Maybe (p,v) -> Maybe (p,v)) -> k -> P.HashPSQ k p v -> P.HashPSQ k p v
p'alter f = fmap snd . P.alter ((,) () . f)

p'adjust :: (Hashable k, Ord k, Ord p) =>       ((p,v) ->       (p,v)) -> k -> P.HashPSQ k p v -> P.HashPSQ k p v
p'adjust = p'alter . fmap


-- | Runs one 'phase' of Stoer-Wagner.  The input graph must be
-- undirected, connected, and have at least two nodes.

minCutPhase :: (Hashable k, Num w, Ord k, Ord w) => Graph k w -> (Graph k w, [k], w)
minCutPhase adj =
    let -- start with arbitrary node 'a'
        Just (a,_pa, a_nbrs,rest1) = P.minView adj
        Just (z,_pz, z_nbrs,rest2) = P.minView rest1

        -- recursively add maximally connected node until only one is left
        -- Make priority queue.  The prio is the negative of the
        -- connectivity to node 'a'.  Easy enough!
        (s,t,w_cut) = minCutPhaseIter (S.singleton a) $
                      M.foldlWithKey' (\m b w -> p'adjust (first (subtract w)) b m) (P.delete a adj) a_nbrs

        -- merge the leftover node with the one added last
        adj' = merge_nodes s t adj

    in if P.null rest2

       -- only two nodes left: the cut is obvious
       then (merge_nodes a z adj, z, M.findWithDefault 0 a z_nbrs)

       -- the cut is the left over node, its weight is the connection weight of that node
       else (adj', t, w_cut)


-- | Recursive addition of nodes to an \"in\" set used in Stoer-Wagner
-- algorithm.  Implemented with a full traversal of nodes_out in every iteration,
-- runtime is O(|V|²), and consequently O(|V|³) for 'minCut'.  For
-- 2023-12-15, it runs in <7', which is not nice, but tolerable.  Using the
-- priority search queue, we get it down to O(|V|² |E| log |V|) or
-- something like that, which works out to 4".

minCutPhaseIter :: (Hashable k, Num w, Ord w, Ord k) => S.HashSet [k] -> Graph k w -> ([k], [k], w)
minCutPhaseIter nodes_in nodes_out
    | P.null rest2  =  (s, t, sum t_nbrs)
    | otherwise     =  minCutPhaseIter (S.insert s nodes_in) $ M.foldlWithKey update_nbr (P.delete s nodes_out) s_nbrs
  where
    Just (s,_,s_nbrs,rest1)  =  P.minView nodes_out
    Just (t,_,t_nbrs,rest2)  =  P.minView rest1

    -- must update nodes_out:  every neighbor of s needs its prio
    -- decreased by the connection strength to s
    update_nbr m n w = p'adjust (first (subtract w)) n m


-- | Merges two nodes.  Dealing with priorities in here does not make
-- much sense, and they should all be zero anyway.  We give the new node
-- the sum of the priorities of the nodes it is created from; the
-- priorities of its neighbors are unchanged.

merge_nodes :: (Hashable k, Num w, Ord w, Ord k) => [k] -> [k] -> Graph k w -> Graph k w
merge_nodes s t adj = flip (M.foldlWithKey' adjust_nbr) st_nbrs $
                      P.insert (s++t) (s_prio+t_prio) st_nbrs $
                      P.delete s $
                      P.delete t adj
  where
    (s_prio, s_nbrs)  =  maybe (0, M.empty) id $ P.lookup s adj
    (t_prio, t_nbrs)  =  maybe (0, M.empty) id $ P.lookup t adj
    st_nbrs           =  M.delete s $ M.delete t $ M.unionWith (+) s_nbrs t_nbrs
    adjust_nbr m a w  = p'adjust (second (M.delete s . M.delete t . M.insert (s++t) w)) a m


