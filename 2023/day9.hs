ext :: [Int] -> Int
ext xs | all (==0) ys = last xs
       | otherwise    = last xs + ext ys
  where
    ys = zipWith (-) (tail xs) xs


prev :: [Int] -> Int
prev xs | all (==0) ys = head xs
       | otherwise     = head xs - prev ys
  where
    ys = zipWith (-) (tail xs) xs


main = do
    series <- map (map read . words) . lines <$> getContents
    print $ sum $ map ext series
    print $ sum $ map prev series
