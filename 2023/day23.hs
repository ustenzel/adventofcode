import Control.Exception
import Data.Array.IO
import Text.Printf
import qualified Data.HashMap.Strict as M
import qualified Data.HashSet        as S

type Pos = (Int,Int)

dfs :: IOUArray (Int,Int) Char -> Int -> Pos -> IO [Int]
dfs grid goal p0 = go 1 p0 []
  where
    go !l (y,x) acc = do c <- readArray grid (y,x)
                         bracket (writeArray grid (y,x) '#')
                                 (\_ -> writeArray grid (y,x) c)
                                 (\_ -> case c of
                                            '#' -> pure acc
                                            '.' | y == goal -> pure (l:acc)
                                                | otherwise -> go (l+1) (y+1,x) acc >>=
                                                               go (l+1) (y-1,x) >>=
                                                               go (l+1) (y,x+1) >>=
                                                               go (l+1) (y,x-1)
                                            '>' -> go (l+1) (y,x+1) acc
                                            '<' -> go (l+1) (y,x-1) acc
                                            '^' -> go (l+1) (y-1,x) acc
                                            'v' -> go (l+1) (y+1,x) acc)


walk2 :: IOUArray (Int,Int) Char -> Int -> IO [(Pos,Pos,Int)]
walk2 grid goal = go (0,1) 0 (0,1) (1,0)
  where
    go start !dist (y0,x0) (dy,dx) = do
        let x = x0+dx ; y = y0+dy
        c <- readArray grid (y,x)
        case c of
            '#'             -> pure []
            '*' | dist == 0 -> pure []
                | otherwise -> pure [(start,(y,x),dist+1)]
            _   | y == goal -> pure [(start,(y,x),dist+1)]
            _               -> do
                writeArray grid (y,x) '*'
                let dirs = [ (y+v,x+u) | (v,u) <- [(0,1),(0,-1),(1,0),(-1,0)]
                                       , (y+v, x+u) /= (y0,x0) ]
                walls <- mapM (readArray grid) dirs

                case map snd . filter ((/='#') . fst) $ zip walls dirs of
                    [(y',x')] -> go start (dist+1) (y,x) (y'-y,x'-x)
                    ps        -> (:) (start,(y,x),dist+1) . concat <$>
                                 mapM (\(y',x') -> go (y,x) 0 (y,x) (y'-y,x'-x)) ps

dfs2 :: M.HashMap Pos [(Pos,Int)] -> Int -> Pos -> [Int] -> [Int]
dfs2 !net !goal = go S.empty 0
  where
    go !closed !dist !pos | fst pos == goal  =  (:) dist
                          | otherwise        =  foldr (.) id [ go (S.insert p' closed) (dist+k) p'
                                                             | (p',k) <- M.findWithDefault [] pos net
                                                             , not $ S.member p' closed ]

main :: IO ()
main = do
    inp <- lines <$> getContents
    let w = length (head inp)
        h = length inp

    grid <- newListArray ((0,0),(h-1,w-1)) $ concat inp

    -- part 1
    bracket (writeArray grid (0,1) '#') (\_ -> writeArray grid (0,1) '.') $ \_ -> do
        walks <- dfs grid (h-1) (1,1)
        printf "%d distinct walks\n" (length walks)
        printf "longest is %d\n\n" (maximum walks)

    -- part 2
    do net <- M.fromListWith (++) . concatMap (\(a,b,k) -> [(a,[(b,k)]),(b,[(a,k)])]) <$> walk2 grid (h-1)
       printf "network has %d edges.\n" (sum (fmap length net) `div` 2)
       let walks = dfs2 net (h-1) (0,1) []
       printf "%d distinct walks\n" (length walks)
       printf "longest is %d\n\n" (maximum walks)
