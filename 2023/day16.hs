import Data.Bits
import qualified Data.HashMap.Strict as M
import Data.Array.Unboxed
import Data.List

data Dir = N | E| S | W deriving (Enum, Show)

main = do
    inp <- lines <$> getContents
    let xmax = length (head inp)
        ymax = length inp
        grid = listArray ((1,1),(ymax,xmax)) $ concat inp

    let energized = walk grid M.empty (1,1,E)
    print $ M.size energized

    print $ maximum $ [ M.size $ walk grid M.empty (y,  1 ,E) | y <- [1..ymax] ] ++
                      [ M.size $ walk grid M.empty (y,xmax,W) | y <- [1..ymax] ] ++
                      [ M.size $ walk grid M.empty (  1 ,x,S) | x <- [1..xmax] ] ++
                      [ M.size $ walk grid M.empty (ymax,x,N) | x <- [1..xmax] ]

walk :: UArray (Int,Int) Char -> M.HashMap (Int,Int) Int -> (Int,Int,Dir) -> M.HashMap (Int,Int) Int
walk grid en (y,x,d)
    | not $ inRange (bounds grid) (y,x) = en
    | M.findWithDefault 0 (y,x) en `testBit` (fromEnum d) = en
    | otherwise = foldl' (\e d' -> walk grid e (step y x d')) en' ds
  where
    en' = M.insertWith (.|.) (y,x) (0 `setBit` fromEnum d) en
    ds = case (grid ! (y,x), d) of
        ('.', _) -> [d]
        ('\\',E) -> [S]
        ('\\',S) -> [E]
        ('\\',W) -> [N]
        ('\\',N) -> [W]
        ('/',E) -> [N]
        ('/',N) -> [E]
        ('/',W) -> [S]
        ('/',S) -> [W]
        ('|',E) -> [N,S]
        ('|',W) -> [N,S]
        ('|',_) -> [d]
        ('-',N) -> [W,E]
        ('-',S) -> [W,E]
        ('-',_) -> [d]


step y x N = (y-1,x,N)
step y x S = (y+1,x,S)
step y x E = (y,x+1,E)
step y x W = (y,x-1,W)

