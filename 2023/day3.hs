import Data.Char
import Data.List

mainA = print . sum . map to_int . filter is_part . find_numbers . adjoin . pad . lines =<< getContents
main = print . sum . map find_gears . concat . adjoinB . map make_fingers . pad . lines =<< getContents

pad xss@(xs:_) = map (\ys -> '.' : ys ++ ".") $ replicate (length xs) '.' : xss ++ [replicate (length xs) '.']

adjoin :: [[a]] -> [[( (a,a,a), (a,a,a), (a,a,a) )]]
adjoin xss = map (\xs -> zip3 xs (drop 1 xs) (drop 2 xs)) $ zipWith3 zip3 xss (drop 1 xss) (drop 2 xss)

find_numbers = filter (is_digit . head) . concatMap (groupBy (\x y -> is_digit x == is_digit y))

is_digit (_,(_,x,_),_) = isDigit x

is_part = any has_symbol
has_symbol ((tl,tc,tr),(left,_,right),(bl,bc,br)) = any is_symbol [tl,tc,tr,left,right,bl,bc,br]
is_symbol c = c /= '.' && not (isDigit c)

to_int xs = read (map (\(_,(_,x,_),_) -> x) xs) :: Int


data F = F [Char] Char [Char]

step_right :: F -> Maybe F
step_right (F _ _ []) = Nothing
step_right (F l m (r1:rs)) = Just (F (m:l) r1 rs)

make_fingers :: [Char] -> [F]
make_fingers (x:xs) = unfoldr (fmap (\a->(a,a)) . step_right) (F [] x xs)

adjoinB ls = zipWith3 zip3 ls (drop 1 ls) (drop 2 ls)

find_gears (top, mid@(F _ x _), bot) =
    case numbers of _ | x /= '*'  ->  0
                    [a,b]         ->  a*b
                    _             ->  0
  where
    numbers = map read $ filter (not . null) $ nof top ++ nof mid ++ nof bot

    nof (F l m r) | isDigit m  =  [ reverse (takeWhile isDigit l) ++ m : takeWhile isDigit r ]
                  | otherwise  =  [ reverse (takeWhile isDigit l), takeWhile isDigit r ]
