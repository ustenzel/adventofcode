{-# Language BangPatterns, LambdaCase, OverloadedStrings #-}
import Control.Monad
import Data.List
import qualified Data.ByteString.Char8 as C
import qualified Data.HashMap.Strict as M

type Name = C.ByteString

data Pulse = Low | High
  deriving (Eq, Ord, Show)

flop :: Pulse -> Pulse
flop Low = High
flop High = Low

data Module = Broadcast { dests :: [Name] }
            | FlipFlop  { dests :: [Name], state :: Pulse }
            | Conj      { dests :: [Name], inputs :: M.HashMap Name Pulse }
  deriving Show

parseLn :: M.HashMap Name [Name] -> C.ByteString -> (Name, Module)
parseLn sourcemap str
    | C.head w0 == '%'     = (C.tail w0, FlipFlop (reverse ws) Low)
    | C.head w0 == '&'     = (C.tail w0, Conj (reverse ws) $ M.fromList $ map (flip (,) Low) $ M.findWithDefault [] (C.tail w0) sourcemap)
    | otherwise            = (w0, Broadcast (reverse ws))
  where
    w0:_:ws = C.words $ C.filter (/= ',') str

parse :: [C.ByteString] -> M.HashMap Name Module
parse lns = fwd
  where
    (rev, fwd) = foldl' (\(r,f) (nm,mdl) -> (insertall nm r (dests mdl), M.insert nm mdl f)) (M.empty, M.empty) $ map (parseLn rev) lns

    insertall :: Name -> M.HashMap Name [Name] -> [Name] -> M.HashMap Name [Name]
    insertall y = foldl' (\m x -> M.insertWith (flip (++)) x [y] m)

spark :: M.HashMap Name Module -> IO (Int,Int, M.HashMap Name Module, Int)
spark = go 0 0 [("button","broadcaster", Low)] [] 0
  where
    go !nlow !nhigh [] [] !rx ms = pure (nlow,nhigh,ms,rx)
    go !nlow !nhigh [] ps !rx ms = go nlow nhigh (reverse ps) [] rx ms
    go !nlow !nhigh ((fro,tho,lv):ps1) ps2 !rx ms = case M.lookup tho ms of
        Just (Broadcast ds) -> k ([(tho,d,lv) | d <- ds] ++ ps2) rx ms
        Just (FlipFlop ds st) -> case lv of High -> k ps2 rx ms
                                            Low  -> k ([(tho,d,flop st) | d <- ds] ++ ps2) rx (M.insert tho (FlipFlop ds (flop st)) ms)
        Just (Conj ds inps) -> do let inps' = M.insert fro lv inps
                                      lv' = if all (== High) inps' then Low else High
                                      rx' = if tho == "sl" && lv' == Low then rx+1 else rx
                                  k ([(tho,d,lv') | d <- ds] ++ ps2) rx' (M.insert tho (Conj ds inps') ms)
        Nothing -> k ps2 (rx+1) ms
      where
        k = case lv of Low  -> go (nlow+1) nhigh ps1
                       High -> go nlow (nhigh+1) ps1

sparkN :: Int -> M.HashMap Name Module -> IO (Int,Int)
sparkN = go 0 0
  where
    go nlow nhigh 0 _ = pure (nlow,nhigh)
    go nlow nhigh nrounds ms = spark ms >>= \case (u,v,ms',_) -> go (nlow+u) (nhigh+v) (nrounds-1) ms'

sparkRx :: M.HashMap Name Module -> IO Int
sparkRx = go 1
  where
    go !nrounds ms = spark ms >>= \case (_,_,ms',1) -> pure nrounds
                                        (_,_,ms',n) -> print (nrounds, n) >> go (nrounds+1) ms'

main = do
    modules <- parse . C.lines <$> C.getContents

    sparkN 1000 modules >>= \case (u,v) -> print $ u*v
    -- sparkRx modules  -- doesn't help


-- to attack part 2, visualize the network using 'dot':
main2 = do
    modules <- parse . C.lines <$> C.getContents

    putStrLn $ unlines $
        "graph foo {" :
        [ C.unpack nm ++ " [ shape = " ++ shape ++ " ];"
        | (nm, m) <- M.toList modules
        , let shape = case m of FlipFlop{} -> "box" ; _ -> "ellipse" ] ++
        [ C.unpack nm ++ " -- " ++ C.unpack nm' ++ " [dir=forward];"
        | (nm, m) <- M.toList modules
        , nm' <- dests m ] ++
        [ "}" ]

-- Inspection reveals 4x12 bit counters.  They reset every 0xFAD, 0xF31,
-- 0xF47, 0xF6B input pulses, respectively, and pass a low pulse on.
-- Only if all four reset simultaneously is the low pulse propagated to
-- rx.  The answer is therefore @lcm [0xFAD, 0xF31, 0xF47, 0xF6B].

