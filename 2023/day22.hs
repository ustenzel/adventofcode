{-# Language BangPatterns, OverloadedStrings, RecordWildCards #-}
import Data.List
import Data.Maybe

data Brick = Brick { xmin, ymin, zmin, xmax, ymax, zmax :: !Int }
  deriving (Eq, Show)

parse :: String -> Brick
parse s = case map read $ words $ map clean s of
    [xmin,ymin,zmin,xmax,ymax,zmax] -> Brick{..}
  where
    clean c = if c == ',' || c == '~' then ' ' else c

-- Brick A can fall at most @can_fall A B@ units onto brick B.  If B
-- isn't in the way, A can drop to the floor.
can_fall_onto :: Brick -> Brick -> Int
can_fall_onto a b | zmax b >= zmin a                   = zmin a - 1
                  | xmin a > xmax b || xmax a < xmin b = zmin a - 1
                  | ymin a > ymax b || ymax a < ymin b = zmin a - 1
                  | otherwise                          = zmin a - zmax b - 1

-- Apply gravity.  All bricks drop as far as possible.
gravity :: [Brick] -> [Brick]
gravity = go [] . sortOn zmin
  where
    go low [      ] = reverse low
    go [ ] (b:high) = let hmax = zmin b - 1
                      in  go [lower (zmin b - 1) b] high
    go low (b:high) = let hmax = minimum $ map (b `can_fall_onto`) low
                      in  go (lower hmax  b : low) high

lower :: Int -> Brick -> Brick
lower d b = b { zmin = zmin b - d, zmax = zmax b - d }

-- Check each block whether it can be disintegrated without anything
-- falling.  Quadratic runtime, but still fast enough.
can_disintegrate :: [Brick] -> Int
can_disintegrate = go 0 [] . sortOn zmin
  where
    go !n low [      ] = n
    go !n low (b:high) = let n' = n + fromEnum (test_integrity low high)
                         in go n' (b:low) high

    test_integrity low [      ] = True
    test_integrity [ ] (b:high) = zmin b == 1 && test_integrity [b] high
    test_integrity low (b:high) = any (== 0) (map (b `can_fall_onto`) low) && test_integrity (b:low) high

-- Cubic runtime?  Never mind, it completes in 30 seconds and debugging
-- it took way longer.
num_falling :: [Brick] -> [Int]
num_falling = go [] [] . sortOn zmin
  where
    go ns low [      ] = ns
    go ns low (b:high) = go (test_integrity 0 low high : ns) (b:low) high

    test_integrity !n low [      ] = n
    test_integrity !n [ ] (b:high) = test_integrity (if zmin b == 1 then n else n+1) [lower (zmin b - 1) b] high
    test_integrity !n low (b:high) = let hmax = minimum $ map (b `can_fall_onto`) low
                                     in test_integrity (if hmax == 0 then n else n+1) (lower hmax b : low) high


main = do
    bricks <- gravity . map parse . lines <$> getContents
    print $ can_disintegrate bricks
    print $ sum $ num_falling bricks
