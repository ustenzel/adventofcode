import Data.Char

unescape ('"':cs) = unescape' cs
unescape (c:cs) = unescape cs
unescape [] = []

unescape' ('"':cs) = unescape cs
unescape' ('\\':'\\':cs) = '\\' : unescape' cs
unescape' ('\\':'"':cs) = '"' : unescape' cs
unescape' ('\\':'x':a:b:cs) = chr (digitToInt a * 16 + digitToInt b) : unescape' cs
unescape' (c:cs) = c : unescape' cs

encode cs = '"' : encode' cs ++ "\""
encode' ('"':cs) = '\\' : '"' : encode' cs
encode' ('\\':cs) = '\\' : '\\' : encode' cs
encode' (c:cs) = c : encode' cs
encode' [] = []

main = do raw <- lines <$> getContents
          print $ sum (map length raw) - sum (map (length . unescape) raw)
          print $ sum (map (length . encode) raw) - sum (map length raw)

