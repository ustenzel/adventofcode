{-# LANGUAGE BangPatterns #-}
data Ingredient = I { capacity, durability, flavor, texture, calories :: !Int }

amount :: Int -> Ingredient -> Ingredient
amount n (I a b c d e) = I (n*a) (n*b) (n*c) (n*d) (n*e)

mix :: Ingredient -> Ingredient -> Ingredient
mix (I a b c d e) (I a' b' c' d' e') = I (a+a') (b+b') (c+c') (d+d') (e+e')

ingredients = [("Frosting", I { capacity = 4, durability = -2, flavor = 0, texture = 0, calories = 5 })
              ,("Candy", I { capacity = 0, durability = 5, flavor = -1, texture = 0, calories = 8 })
              ,("Butterscotch", I { capacity = -1, durability = 0, flavor = 5, texture = 0, calories = 6 })
              ,("Sugar", I { capacity = 0, durability = 0, flavor = -2, texture = 2, calories = 1 })]

score :: Ingredient -> Int
score (I a b c d _) | a>0 && b>0 && c>0 && d>0 = a*b*c*d
                    | otherwise                = 0

mixes !total [i] = [ amount total i ]
mixes !total (i:is) = [ amount a i `mix` rest
                      | a <- [0..total]
                      , rest <- mixes (total-a) is ]

is_meal :: Ingredient -> Bool
is_meal = (==) 500 . calories

main = do print $ maximum $ map score $ mixes 100 $ map snd ingredients
          print $ maximum $ map score $ filter is_meal $ mixes 100 $ map snd ingredients
