is_naughty ('a':'b':_) = True
is_naughty ('c':'d':_) = True
is_naughty ('p':'q':_) = True
is_naughty ('x':'y':_) = True
is_naughty (c:cs) = is_naughty cs
is_naughty [    ] = False

has_double (a:b:cs) | a == b = True
has_double (c:cs) = has_double cs
has_double [    ] = False

has_vowels 0   _    = True
has_vowels n (c:cs) | c `elem` "aeiou" = has_vowels (n-1) cs
                    | otherwise        = has_vowels   n   cs
has_vowels _ [    ] = False

nice s = has_double s && has_vowels 3 s && not (is_naughty s)

mainA = print . length . filter nice . lines =<< getContents


has_rep (a:_:c:_) | a == c = True
has_rep (a:cs) = has_rep cs
has_rep [    ] = False

has_pair (a:b:cs) | contains a b cs = True
has_pair (c:cs) = has_pair cs
has_pair [    ] = False

contains a b (x:y:zs) | a == x && b == y = True
contains a b (z:zs) = contains a b zs
contains a b [    ] = False

niceB s = has_rep s && has_pair s

main = print . length . filter niceB . lines =<< getContents
