import Data.List

mainA = do
    s <- getContents
    let up = length $ filter (=='(') s
        dn = length $ filter (==')') s
    print $ up-dn

main =
    print . head .
    filter ((<0) . snd) .
    zip [0..] .
    scanl (\p c -> if c == '(' then p+1 else p-1) 0
        =<< getContents
