import Prelude ()
import BasePrelude

import qualified Data.Map.Lazy as M

parse [ a, "RSHIFT", b, "->", c ] = ( c, liftA2 shiftR (value a) (value b))
parse [ a, "LSHIFT", b, "->", c ] = ( c, liftA2 shiftL (value a) (value b))
parse [ a, "OR", b, "->", c ] = ( c, liftA2 (.|.) (value a) (value b))
parse [ a, "AND", b, "->", c ] = ( c, liftA2 (.&.) (value a) (value b))
parse [ "NOT", b, "->", c ] = ( c, fmap complement (value b))
parse [ a, "->", c ] = ( c, value a)

value x | all isDigit x = const (read x)
        | otherwise     = fromJust . M.lookup x

wire prims = net
  where
    net = M.fromList [ (x,y net) | (x,y) <- prims ]

main = print . M.lookup "a" . wire . map (parse . words) . lines =<< getContents

