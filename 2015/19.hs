import Prelude ()
import BasePrelude

import qualified Data.HashSet as S

readSubst [ x, "=>", y ] = (x,y)

try_subst :: (String,String) -> String -> [String]
try_subst (u,v) [] = []
try_subst (u,v) s | u `isPrefixOf` s = ( v ++ drop (length u) s ) : (map (head s :) (try_subst (u,v) (tail s)))
                  | otherwise = map (head s :) (try_subst (u,v) (tail s))

count a b c ('R':'n':s) = count (a+1) (b+1) c s
count a b c ('Y':s)     = count (a+1) b (c+1) s
count a b c (x:y:s) | isLower y = count (a+1) b c s
count a b c (x:s) = count (a+1) b c s
count a b c [   ] = (a,b,c)

main = do ( part1, _:part2:_ ) <- break null . lines <$> getContents
          let substs = map (readSubst . words) part1

          print . S.size . S.fromList $ concatMap (flip try_subst part2) substs

          -- Part B:  This is evil, it needs an "aha" moment to see the
          -- efficient solution:  Every rule that doesn't involve Ar, Rn
          -- or Y introduces exactly one symbol.  Rn and Ar are always
          -- introduced together, and those rules introduce exactly
          -- three symbols, unless they also involve Y.  Rules that
          -- involve Y always introduce Y and one more symbol.
          --
          -- Therefore, after applying k rules and getting B times Rn,
          -- and C times Y, we have in total A = 1+k+2B+2C symbols.
          -- Inverting it, we simply count symbols (A), Rns (B) and Ys
          -- (C), then compute k = A - 2B - 2C -1.

          let (a,b,c) = count 0 0 0 part2
          print $ a - 2*b - 2*c - 1
