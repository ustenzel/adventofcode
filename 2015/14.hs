import BasePrelude
import Prelude ()

read1 [ n, _, _, v, _, _, t, _, _, _, _, _, _, r, _ ] = ( n, read v, read t, read r)

dist te (n, v, t, r) = let (ncycles, tr) = te `divMod` (t+r)
                       in v * (ncycles * t + min tr t)

mainA = do lst <- map (read1 . words) . lines <$> getContents
           print $ maximum $ map (dist 2503) lst

main = do lst <- map (read1 . words) . lines <$> getContents
          print $
               maximum $
               map (length &&& head) $ group $ sort $
               map (\(_,(n,_,_,_)) -> n) $
               [ maximum $ map (dist t &&& id) lst | t <- [ 1..2503 ] ]

