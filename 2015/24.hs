{-# LANGUAGE BangPatterns #-}
import Prelude ()
import BasePrelude

input :: [Int]
input = map read $ words
    -- "1 2 3 4 5 7 8 9 10 11"
    "1 2 3 7 11 13 17 19 23 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113"

takeW :: Int -> [Int] -> [ ( [Int], [Int] ) ]
takeW 0    xs  = [( [], xs )]
takeW _ [    ] = []
takeW w (x:xs) = map (second (x:)) (takeW w xs)  ++
                 if x <= w then map (first (x:)) (takeW (w-x) xs) else []

entang :: [Int] -> Integer
entang = product . map fromIntegral


mainA = do let total = sum input
               (third,0) = divMod total 3

           print . entang . fst . head $ do
                -- create the first group, sort the best groups to the beginning
                (xs,rest) <- sortOn ((length &&& entang) . fst) $ takeW third input
                -- create the other groups, details are not really important
                (ys,zs) <- takeW third rest
                return (xs,(ys,zs))

main  = do let total = sum input
               (quarter,0) = divMod total 4

           print . entang . fst . head $ do
                -- create the first group, sort the best groups to the beginning
                (xs,rest) <- sortOn ((length &&& entang) . fst) $ takeW quarter input
                -- create the other groups, details are not really important
                (ys,rest') <- takeW quarter rest
                (zs,ws) <- takeW quarter rest
                return (xs,(ys,zs,ws))

