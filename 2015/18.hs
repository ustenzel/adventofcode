{-# LANGUAGE BangPatterns #-}
import Prelude ()
import BasePrelude

import Data.Array.Unboxed

type Grid = UArray (Int,Int) Bool

read_arr :: [String] -> Grid
read_arr ls = listArray ((0,0),(h-1,w-1)) $ map (=='#') $ concat ls
  where
    h = length ls
    w = length $ head ls

count_neighbors :: Grid -> Int -> Int -> Int
count_neighbors grd y x = sum [ fromEnum (grd ! (y+v,x+u))
                              | (v,u) <- [(-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)]
                              , inRange (bounds grd) (y+v,x+u) ]

step :: Grid -> Grid
step grd = listArray (bounds grd) [ or [ n == 3, z && n == 2
                                       , (x==0 || x==w) && (y==0 || y==h) ]
                                  | ((y,x),z) <- assocs grd
                                  , let n = count_neighbors grd y x ]
  where
    ((0,0),(h,w)) = bounds grd

print_raw :: Grid -> IO ()
print_raw arr = do
    putStrLn $ "\27[1;1H" ++ unlines [ [ if arr ! (y,x) then '#' else '.' | x <- [0..w] ] | y <- [0..min 50 h] ]
    -- threadDelay 20000
  where
    ((0,0),(h,w)) = bounds arr


main = do grid <- read_arr . lines <$> getContents

          -- print $ sum $ map fromEnum $ elems $ head $ drop 100 $ iterate step grid

          putStrLn "\27[1;1H\27[2J"
          mapM_ print_raw $ iterate step grid


