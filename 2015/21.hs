import Prelude ()
import BasePrelude

tryall = do
{-  Weapons:    Cost  Damage  Armor
    Dagger        8     4       0
    Shortsword   10     5       0
    Warhammer    25     6       0
    Longsword    40     7       0
    Greataxe     74     8       0  -}

    (c1, d1) <- [ (8,4), (10,5), (25,6), (40,7), (74,8) ]


{-  Armor:      Cost  Damage  Armor
    Leather      13     0       1
    Chainmail    31     0       2
    Splintmail   53     0       3
    Bandedmail   75     0       4
    Platemail   102     0       5  -}
    (c2, a2) <- [ (0,0), (13,1), (31,2), (53,3), (75,4), (102,5) ]

{-  Rings:      Cost  Damage  Armor
    Damage +1    25     1       0
    Damage +2    50     2       0
    Damage +3   100     3       0
    Defense +1   20     0       1
    Defense +2   40     0       2
    Defense +3   80     0       3  -}
    (c3,d3,a3) : rs <- tails [ (0,0,0), (0,0,0), (25,1,0), (50,2,0), (100,3,0), (20,0,1), (40,0,2), (80,0,3) ]
    (c4,d4,a4) <- rs

    -- My Hit Points: 100
    return (c1+c2+c3+c4, P 'H' 100 (a2+a3+a4) (d1+d3+d4))

{-  Boss Hit Points: 104
    Boss Damage: 8
    Boss Armor: 1   -}
test1 = minimumBy (comparing fst) $ filter (\(_, P c _ _ _) -> c == 'H') $ map (second (`playout` P 'B' 104 1 8)) $ tryall
test2 = maximumBy (comparing fst) $ filter (\(_, P c _ _ _) -> c /= 'H') $ map (second (`playout` P 'B' 104 1 8)) $ tryall

data Player = P !Char !Int !Int !Int      -- hit pts, armor, dmg
  deriving Show

-- returns the winner
playout :: Player -> Player -> Player
playout (P c1 h1 a1 d1) (P c2 h2 a2 d2)
    | h2' <= 0  = P c1 h1 a1 d1
    | otherwise = playout (P c2 h2' a2 d2) (P c1 h1 a1 d1)
  where
    de = (d1 - a2) `max` 1
    h2' = h2 - de

test0 = playout (P 'H' 8 5 5) (P 'B' 12 2 7)
