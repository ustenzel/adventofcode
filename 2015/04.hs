import Data.Bits
import qualified Crypto.Hash.MD5 as MD5
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C

secret = "yzbqklnj"

hash n = MD5.hash $ C.pack $ secret ++ show n

goodA s = B.index s 0 == 0 && B.index s 1 == 0 && (B.index s 2 .&. 0xf0) == 0
goodB s = B.index s 0 == 0 && B.index s 1 == 0 && B.index s 2 == 0

main = print . head $ filter (goodB . snd) [ (i, hash i) | i <- [1..] ]
