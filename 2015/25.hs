import Data.Int (Int64)

hash :: Int -> Int
hash x = fromIntegral $ (fromIntegral x * (252533 :: Int64)) `mod` 33554393

index :: Int -> Int -> Int
index x y = let diag = x+y-2
                complete = (diag+1) * diag `div` 2
            in complete + x

hashN :: Int -> Int -> Int
hashN h 1 = h
hashN h n = hashN (hash h) (n-1)

-- "To continue, please consult the code grid in the manual.  Enter the code at row 2981, column 3075."
main = print $ hashN 20151125 (index 3075 2981)
