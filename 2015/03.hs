import Data.List
import qualified Data.Set as S

track s0 =
        foldl' (\s (x,y) -> S.insert (x,y) s) s0 .
        scanl (\(x,y) c -> if c == '>' then (x+1,y) else
                           if c == '<' then (x-1,y) else
                           if c == '^' then (x,y-1) else
                           if c == 'v' then (x,y+1) else (x,y)) (0,0)

mainA =  print .
         S.size .
         track S.empty
         =<< getContents

split [    ] = ([],[])
split (c:cs) = (c:r,l) where (l,r) = split cs

main = do (santa, robo) <- split <$> getContents
          print . S.size $ track (track S.empty santa) robo


