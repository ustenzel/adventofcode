{-# LANGUAGE BangPatterns #-}
import Prelude ()
import BasePrelude

import Control.Monad.State.Strict

-- Ugly code ahead.  The 'GameM' type wants a way to exit early, and I
-- thought 'ExceptT' would do that.  But that drags in a weird instance
-- of 'Applicative' and doesn't feel right.  'FinishT' from
-- "transformers-abort" looks better.  It conveys the idea clearly, and
-- it doesn't come with an 'Alternative' instance at all.  (Operations
-- could be lifted or it could be defined locally.)

{-
* Magic Missile costs 53 mana. It instantly does 4 damage.
* Drain costs 73 mana. It instantly does 2 damage and heals you for 2
  hit points.
* Shield costs 113 mana. It starts an effect that lasts for 6 turns.
  While it is active, your armor is increased by 7.
* Poison costs 173 mana. It starts an effect that lasts for 6 turns. At
  the start of each turn while it is active, it deals the boss 3 damage.
* Recharge costs 229 mana. It starts an effect that lasts for 5 turns.
  At the start of each turn while it is active, it gives you 101 new mana.
-}

-- start with 50 hp, 500 mana
-- boss has 55hp, 8 dmg

data Game = Game { tshield    :: !Int
                 , tpoison    :: !Int
                 , tcharge    :: !Int
                 , mana       :: !Int
                 , my_hp      :: !Int
                 , boss_hp    :: !Int
                 , boss_dmg   :: !Int
                 , total_mana :: !Int
                 , ktrace     :: String }
  deriving Show

data Winner = Wizard | Boss
  deriving (Eq, Show)

app_effects :: GameM Winner -> GameM Winner
app_effects k = do
    modify $ \g -> g {
        tshield = (tshield g - 1) `max` 0,
        tpoison = (tpoison g - 1) `max` 0,
        tcharge = (tcharge g - 1) `max` 0,
        mana    = mana g + if tcharge g > 0 then 101 else 0,
        boss_hp = boss_hp g - if tpoison g > 0 then 3 else 0 }
    h <- gets boss_hp
    if h <= 0 then return Wizard else k


missile, drain, shield, poison, charge :: GameM Winner
missile = do h <- gets boss_hp
             if h > 4 then do modify $ \g -> g { boss_hp = h - 4 }
                              boss_turn
                      else return Wizard

drain = do h <- gets boss_hp
           if h > 2 then do modify $ \g -> g { boss_hp = h - 2, my_hp = my_hp g + 2 }
                            boss_turn
                    else return Wizard

shield = do t <- gets tshield
            guard $ t==0
            modify $ \g -> g { tshield = 6 }
            boss_turn

poison = do t <- gets tpoison
            guard $ t==0
            modify $ \g -> g { tpoison = 6 }
            boss_turn

charge = do t <- gets tcharge
            guard $ t==0
            modify $ \g -> g { tcharge = 5 }
            boss_turn

kast :: Int -> Char -> GameM Winner -> GameM Winner
kast cost label action = do
          mana1 <- gets mana
          if cost > mana1
            then mzero
            else do
                modify $ \g -> g { mana = mana1 - cost
                                 , ktrace = label : ktrace g
                                 , total_mana = total_mana g + cost }
                guard . (<= 1400) =<< gets total_mana
                action

player_turn :: GameM Winner
player_turn = do
    modify $ \g -> g { my_hp = my_hp g - 1 }
    h <- gets my_hp
    if h <= 0
      then return Boss
      else do app_effects $ msum [ kast  53 'M' missile
                                 , kast 113 'S' shield
                                 , kast  73 'D' drain
                                 , kast 173 'P' poison
                                 , kast 229 'C' charge]

boss_turn :: GameM Winner
boss_turn = app_effects $ do
          tshield1 <- gets tshield
          bd <- gets boss_dmg
          let dmg = (bd - if tshield1 > 0 then 7 else 0) `max` 1
          hp <- gets my_hp
          if hp <= dmg
            then return Boss
            else do modify $ \g -> g { my_hp = hp - dmg }
                    player_turn

type GameM a = StateT Game [] a


main = print $ minimumBy (comparing (total_mana . snd)) $ filter ((==) Wizard . fst) $ runStateT player_turn game0
-- main = mapM_ print $ filter ((==) Wizard . fst) $ runStateT player_turn game0

game0 = Game { tshield = 0
             , tpoison = 0
             , tcharge = 0
             , mana = 500
             , my_hp = 50
             , boss_hp = 55
             , boss_dmg = 8
             , ktrace = []
             , total_mana = 0 }

