
houseA :: Int -> Int
houseA n = sum $ map (\i -> i + n `div` i)
               $ filter (\i -> n `mod` i == 0)
               $ takeWhile (\i -> i*i < n) [1..]

houseB :: Int -> Int
houseB n = sum $ map (\i -> (if i <= 50 then n `div` i else 0)
                          + (if n `div` i <= 50 then i else 0))
               $ filter (\i -> n `mod` i == 0)
               $ takeWhile (\i -> i*i < n) [1..]

mainA :: IO ()
mainA = print $ head [ i | i <- [1..], houseA i >= 2900000 ]

main  :: IO ()
main  = print $ head [ i | i <- [1..], houseB i > 29000000 `div` 11 ]
