{-# LANGUAGE BangPatterns, OverloadedStrings #-}
import Prelude ()
import BasePrelude

import qualified Data.ByteString.Char8 as C
import qualified Data.HashMap.Strict as M
import qualified Data.HashSet as S

read1 [ a, _, "gain", n, _, _, _, _, _, _, b ] = (a,C.init b,x) where Just (x,"") = C.readInt n
read1 [ a, _, "lose", n, _, _, _, _, _, _, b ] = (a,C.init b,-x) where Just (x,"") = C.readInt n

score tab (x0:xs) = go 0 x0 xs
  where
    go !a x (y:ys) = go (tab M.! (x,y) + tab M.! (y,x) + a) y ys
    go !a x [    ] = a -- tab M.! (x,x0) + tab M.! (x0,x) + a

main = do lst <- map (read1 . C.words) . C.lines <$> C.getContents
          let tab = M.fromList $ [ ((a,b),n) | (a,b,n) <- lst ]
              nms = S.fromList $ [ a | (a,_,_) <- lst ]

          print $ maximum $ map (score tab) $ permutations $ S.toList nms
