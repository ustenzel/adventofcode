matchesA :: String -> Bool
matchesA = go . drop 2 . words . filter (/=',')
  where
    go ("children:"    : n : xs) = read n == 3 && go xs
    go ("cats:"        : n : xs) = read n == 7 && go xs
    go ("samoyeds:"    : n : xs) = read n == 2 && go xs
    go ("pomeranians:" : n : xs) = read n == 3 && go xs
    go ("akitas:"      : n : xs) = read n == 0 && go xs
    go ("vizslas:"     : n : xs) = read n == 0 && go xs
    go ("goldfish:"    : n : xs) = read n == 5 && go xs
    go ("trees:"       : n : xs) = read n == 3 && go xs
    go ("cars:"        : n : xs) = read n == 2 && go xs
    go ("perfumes:"    : n : xs) = read n == 1 && go xs
    go [                       ] = True

matchesB :: String -> Bool
matchesB = go . drop 2 . words . filter (/=',')
  where
    go ("children:"    : n : xs) = read n == 3 && go xs
    go ("cats:"        : n : xs) = read n  > 7 && go xs
    go ("samoyeds:"    : n : xs) = read n == 2 && go xs
    go ("pomeranians:" : n : xs) = read n  < 3 && go xs
    go ("akitas:"      : n : xs) = read n == 0 && go xs
    go ("vizslas:"     : n : xs) = read n == 0 && go xs
    go ("goldfish:"    : n : xs) = read n  < 5 && go xs
    go ("trees:"       : n : xs) = read n  > 3 && go xs
    go ("cars:"        : n : xs) = read n == 2 && go xs
    go ("perfumes:"    : n : xs) = read n == 1 && go xs
    go [                       ] = True

main = mapM_ putStrLn . filter matchesB . lines =<< getContents
