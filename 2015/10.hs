import Prelude ()
import BasePrelude

look'n'say = concatMap (\l -> [intToDigit (length l),head l]) . group

main = do [ini] <- getArgs
          mapM_ (print . length) . take 51 $ iterate look'n'say ini
          -- ~40 seconds
