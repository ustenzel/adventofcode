import Prelude ()
import BasePrelude

cts :: [Int]
cts = map read $ words "50 44 11 49 42 46 18 32 26 40 21 7 18 43 10 47 36 24 22 40"

combos   0   _             = [ [] ]
combos total _ | total < 0 = []
combos total (c:cs) = combos total cs ++ map (c:) (combos (total-c) cs)
combos   _   [    ] = []

partA = length $ combos 150 cts
partB = head &&& length $ head $ group $ sort $ map length $ combos 150 cts

