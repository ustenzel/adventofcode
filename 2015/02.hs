import Data.List

area [a,b,c] = 3*a*b + 2*b*c + 2*a*c

ribbon [a,b,c] = 2*(a+b) + a*b*c

main =  print .
        sum .
        -- map area .
        map ribbon .
        map sort .
        map (map read) .
        map words .
        lines .
        map (\c -> if c == 'x' then ' ' else c)
        =<< getContents
