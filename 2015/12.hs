{-# LANGUAGE OverloadedStrings, BangPatterns #-}
import Data.Aeson
import Data.Maybe
import Data.Scientific
import qualified Data.ByteString.Char8 as B


-- Part A: nice shortcut, no parsing needed.
do_sum a s = case B.readInt s of
    _ | B.null s -> a
    Just (i,s')  -> do_sum (a+i) s'
    Nothing      -> do_sum a (B.tail s)

mainA = print . do_sum 0 =<< B.getContents


-- Part B: false sense of security, we do need aeson after all.
do_sum_B :: Value -> Int
do_sum_B (Object obj) | any (== String "red") obj = 0
                      | otherwise = foldl (\a -> (+) a . do_sum_B) 0 obj
do_sum_B (Array arr) = foldl (\a -> (+) a . do_sum_B) 0 arr
do_sum_B (String _) = 0
do_sum_B (Number n) = fromJust $ toBoundedInteger n
do_sum_B (Bool _) = 0
do_sum_B Null = 0


main = print . fmap do_sum_B . decodeStrict =<< B.getContents
