import Prelude ()
import BasePrelude

incr :: String -> String
incr ('z':cs) = 'a' : incr cs
incr ('h':cs) = 'j' : cs
incr ('n':cs) = 'p' : cs
incr ('k':cs) = 'm' : cs
incr ( c :cs) = chr (ord c + 1) : cs

has_straight (a:b:c:cs) | ord a == ord b +1 && ord b == ord c +1 = True
has_straight (c:cs) = has_straight cs
has_straight [] = False

has_pairs (a:b:cs) | a == b = has_pair cs
has_pairs (c:cs) = has_pairs cs
has_pairs [] = False

has_pair (a:b:cs) | a == b = True
has_pair (c:cs) = has_pair cs
has_pair [] = False


main = do [cur] <- getArgs
          print . reverse . head . filter has_straight . filter has_pairs . tail . iterate incr $ reverse cur
