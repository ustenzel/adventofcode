import Data.Array.IO
import Data.Word

parseA [ "turn", "off", x, y, _, u, v ] = \arr -> sequence_ [ writeArray arr (a,b) 0 | a <- [read x..read u], b <- [read y..read v] ]
parseA [ "turn", "on",  x, y, _, u, v ] = \arr -> sequence_ [ writeArray arr (a,b) 1 | a <- [read x..read u], b <- [read y..read v] ]
parseA [ "toggle", x, y, _, u, v ] = \arr -> sequence_ [ readArray arr (a,b) >>= writeArray arr (a,b) . (1-) | a <- [read x..read u], b <- [read y..read v] ]

parseB [ "turn", "on", x, y, _, u, v ] = \arr -> sequence_ [ readArray arr (a,b) >>= writeArray arr (a,b) . (+1) | a <- [read x..read u], b <- [read y..read v] ]
parseB [ "turn", "off",  x, y, _, u, v ] = \arr -> sequence_ [ readArray arr (a,b) >>= writeArray arr (a,b) . max 0 . (subtract 1) | a <- [read x..read u], b <- [read y..read v] ]
parseB [ "toggle", x, y, _, u, v ] = \arr -> sequence_ [ readArray arr (a,b) >>= writeArray arr (a,b) . (+2) | a <- [read x..read u], b <- [read y..read v] ]

main = do
    grid <- newArray ((0,0),(999,999)) 0 :: IO (IOUArray (Int,Int) Int)
    mapM_ (($ grid) . parseB . words) . lines . map (\c -> if c == ',' then ' ' else c) =<< getContents
    print . sum =<< getElems grid

