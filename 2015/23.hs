import Control.Monad
import Data.Int
import qualified Data.Vector as V

-- Reverse engineering the program reveals that it contains two
-- constants and counts how many steps the Collatz sequence for them
-- take to arrive at one.  But who cares about this clever insight when
-- a straight forward simulation is plenty fast?

data Regs = Regs { reg_a :: !Int64, reg_b :: !Int64, ip :: !Int }
  deriving Show

type Insn = Regs -> Regs

parse :: [String] -> Insn
parse [ "hlf", "a" ] (Regs a b ip) = Regs (a `div` 2) b (ip + 1)
parse [ "hlf", "b" ] (Regs a b ip) = Regs a (b `div` 2) (ip + 1)
parse [ "tpl", "a" ] (Regs a b ip) = Regs (a * 3) b (ip + 1)
parse [ "tpl", "b" ] (Regs a b ip) = Regs a (b * 3) (ip + 1)
parse [ "inc", "a" ] (Regs a b ip) = Regs (a + 1) b (ip + 1)
parse [ "inc", "b" ] (Regs a b ip) = Regs a (b + 1) (ip + 1)
parse [ "jmp",  d  ] (Regs a b ip) = Regs a b (ip + read d)
parse [ "jie", "a,", d  ] (Regs a b ip) = Regs a b $ if even a then ip + read d else ip + 1
parse [ "jie", "b,", d  ] (Regs a b ip) = Regs a b $ if even b then ip + read d else ip + 1
parse [ "jio", "a,", d  ] (Regs a b ip) = Regs a b $ if a == 1 then ip + read d else ip + 1
parse [ "jio", "b,", d  ] (Regs a b ip) = Regs a b $ if b == 1 then ip + read d else ip + 1

run :: V.Vector Insn -> Regs -> Regs
run v r@(Regs a b ip) = do
    case v V.!? ip of
        Nothing -> r
        Just  f -> run v $! f r

main = do prg <- V.fromList . map (parse . words) . lines . filter (/='+') <$> getContents
          print $ run prg (Regs 0 0 0)
          print $ run prg (Regs 1 0 0)

