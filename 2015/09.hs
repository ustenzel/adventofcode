import Prelude ()
import BasePrelude

import qualified Data.HashMap.Strict as M
import qualified Data.HashSet as S
import qualified Data.ByteString.Char8 as C

parse [a,"to",b,"=",c] = ((C.pack a, C.pack b), read c)

trip :: M.HashMap (C.ByteString,C.ByteString) Int -> [C.ByteString] -> Int
trip m (a:b:cs) = case (M.lookup (a,b) m, M.lookup (b,a) m) of
    (Just d, _) -> d + trip m (b:cs)
    (_, Just d) -> d + trip m (b:cs)
    _ -> error $ show (a,b)
trip _ _ = 0

main = do dists <- M.fromList . map (parse . words) . lines <$> getContents
          let places = S.toList $ S.fromList [ z | (x,y) <- M.keys dists, z <- [x,y] ]
          print $ length places
          print . minimum . map (trip dists) $ permutations places
          print . maximum . map (trip dists) $ permutations places
