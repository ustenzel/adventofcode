import Prelude ()
import BasePrelude

parse,parse' :: String -> [Either String String]
parse [      ] = []
parse      cs  = case break (== '[') cs of
    (l,r) -> Left l : parse' (drop 1 r)

parse' [      ] = []
parse'      cs  = case break (== ']') cs of
    (l,r) -> Right l : parse (drop 1 r)

hasAbba :: String -> Bool
hasAbba (a:b:c:d:s)
    | a == d && b == c && a /= b = True
    | otherwise                  = hasAbba (b:c:d:s)
hasAbba _                        = False

hasTLS :: [Either String String] -> Bool
hasTLS = liftA2 (&&)
                (any $ either hasAbba (const False))
                (all $ either (const True) (not . hasAbba))

findAba :: String -> [String]
findAba (a:b:c:s) = (if a == c && a /= b then (:) [b,a,b] else id) (findAba (b:c:s))
findAba _ = []

findAbas :: [Either String String] -> [String]
findAbas = concatMap (either findAba (const []))

hasSSL ss = any (\aba -> any (either (const False) (aba `isInfixOf`)) ss) $ findAbas ss

-- main = print . length . filter (hasTLS . parse) . lines =<< getContents
main = print . length . filter (hasSSL . parse) . lines =<< getContents
