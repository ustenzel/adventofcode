import Prelude ()
import BasePrelude

import Data.Sequence ( (|>), ViewL(..) )
import qualified Data.Sequence as Z

-- Part A is easy, pretty deterministic.
who :: Int -> Int
who 1 = 0                                   -- one guy left, he gets everything
who n | even n = 2 * who (div n 2)          -- even number:  the even guys are left after one round
      | odd  n = 2 + 2 * who (div n 2)      -- odd number:  the first guy loses everything, too


-- They start counting at one.  (Idiots.)
partA = who 3001330 + 1

-- Part B:  representation of the table
-- Each elf has a number (starting from 1), the table is a sequence of
-- elf numbers and the Int points to the elf whose turn it is.

type Table = (Int, Z.Seq Int)

step :: Table -> Table
step (i,s) = (succ i `mod` l, Z.deleteAt ((i + l `div` 2) `mod` l) s)
  where l = Z.length s

is_done (_,s) = case Z.viewl s of
    h :< t | Z.null t -> Just h
    _                 -> Nothing

partB = head $ mapMaybe is_done $ iterate step (0, Z.fromList [1..3001330])

main = print partA >> print partB
