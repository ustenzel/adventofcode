# The Input

The first floor contains a strontium generator, a strontium-compatible microchip, a plutonium generator, and a plutonium-compatible microchip.
The second floor contains a thulium generator, a ruthenium generator, a ruthenium-compatible microchip, a curium generator, and a curium-compatible microchip.
The third floor contains a thulium-compatible microchip.
The fourth floor contains nothing relevant.


# The Solution

This was a dumb game.  Assuming every conflict has a trivial solution, I
can always move n+2 items one floor up in 2n+1 steps.  Manual(!) counting
gives the answers 37 and 61 steps, which turn out to be correct.
