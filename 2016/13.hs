{-# LANGUAGE BangPatterns #-}
import Prelude ()
import BasePrelude

import qualified Data.HashSet as S

-- return whether (x,y) is passable
cubi :: Int -> Int -> Bool
cubi x y
    | x < 0 || y < 0 = False
    | otherwise      = even $ popCount $ x*x + 3*x + 2*x*y + y + y*y + favnum

dijkstra dist closed open | S.member (1,1) open = dist
dijkstra dist closed open = dijkstra (succ dist) (S.union closed open) $
                                flip S.difference closed $
                                flip S.difference open $
                                S.fromList $
                                filter (uncurry cubi) $
                                [ (x+u,y+v) | (x,y) <- S.toList open, (u,v) <- [(0,1),(1,0),(-1,0),(0,-1)] ]

favnum = 1358

partA = dijkstra 0 S.empty (S.singleton (31,39))

dijkstraB    0 closed open = S.size $ S.union closed open
dijkstraB dist closed open = dijkstraB (pred dist) (S.union closed open) $
                                flip S.difference closed $
                                flip S.difference open $
                                S.fromList $
                                filter (uncurry cubi) $
                                [ (x+u,y+v) | (x,y) <- S.toList open, (u,v) <- [(0,1),(1,0),(-1,0),(0,-1)] ]

partB = dijkstraB 50 S.empty (S.singleton (1,1))
