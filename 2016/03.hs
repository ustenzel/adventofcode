import Prelude ()
import BasePrelude

mainA = print . length . filter isPossible . map (map read . words) . lines =<< getContents

isPossible [x,y,z] = x+y > z && x+z > y && y+z > x
isPossible xs = error (show xs)

main = print . length . filter isPossible . map (take 3) . takeWhile (not . null) . iterate (drop 3)
             . concat . transpose . map (map read . words) . lines =<< getContents
