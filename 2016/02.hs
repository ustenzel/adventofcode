import Prelude ()
import BasePrelude

step 1 'R' = 2
step 2 'R' = 3
step 4 'R' = 5
step 5 'R' = 6
step 7 'R' = 8
step 8 'R' = 9

step 1 'D' = 4
step 2 'D' = 5
step 3 'D' = 6
step 4 'D' = 7
step 5 'D' = 8
step 6 'D' = 9

step 2 'L' = 1
step 3 'L' = 2
step 5 'L' = 4
step 6 'L' = 5
step 8 'L' = 7
step 9 'L' = 8

step 4 'U' = 1
step 5 'U' = 2
step 6 'U' = 3
step 7 'U' = 4
step 8 'U' = 5
step 9 'U' = 6

step p  _  = p

mainA = print . map intToDigit . tail . scanl (foldl' step) 5 . lines =<< getContents

--     1
--   2 3 4
-- 5 6 7 8 9
--   A B C
--     D

stepB  6 'L' =  5
stepB  3 'L' =  2
stepB  7 'L' =  6
stepB 11 'L' = 10
stepB  4 'L' =  3
stepB  8 'L' =  7
stepB 12 'L' = 11
stepB  9 'L' =  8

stepB  5 'R' =  6
stepB  2 'R' =  3
stepB  6 'R' =  7
stepB 10 'R' = 11
stepB  3 'R' =  4
stepB  7 'R' =  8
stepB 11 'R' = 12
stepB  8 'R' =  9

stepB  3 'U' =  1
stepB  6 'U' =  2
stepB  7 'U' =  3
stepB  8 'U' =  4
stepB 10 'U' =  6
stepB 11 'U' =  7
stepB 12 'U' =  8
stepB 13 'U' = 11

stepB  1 'D' =  3
stepB  2 'D' =  6
stepB  3 'D' =  7
stepB  4 'D' =  8
stepB  6 'D' = 10
stepB  7 'D' = 11
stepB  8 'D' = 12
stepB 11 'D' = 13

stepB  p  _  = p

main = print . map (toUpper . intToDigit) . tail . scanl (foldl' stepB) 5 . lines =<< getContents
