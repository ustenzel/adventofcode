{-# LANGUAGE BangPatterns #-}

import BasePrelude
import Data.Array.Unboxed
import Prelude ()

data Cmd = Rect !Int !Int
         | Row !Int !Int
         | Col !Int !Int

parse s = case words $ map clean s of
    ["rect",w,h] -> Rect (read w) (read h)
    ["rotate","row",y,"b",n] -> Row (read y) (read n)
    ["rotate","column",x,"b",n] -> Col (read x) (read n)
    ws -> error $ show ws
  where
    clean '=' = ' '
    clean 'x' = ' '
    clean 'y' = ' '
    clean  c  =  c

type Grid = UArray (Int,Int) Word8

exec :: Grid -> Cmd -> Grid
exec arr cmd = case cmd of
    Rect u v -> arr // [ ((x,y),1) | x <- [0..u-1], y <- [0..v-1] ]
    Row  y n -> arr // [ ((x',y), arr ! (x,y)) | x <- [0..w], let x' = (x + n) `mod` (w+1) ]
    Col  x n -> arr // [ ((x,y'), arr ! (x,y)) | y <- [0..h], let y' = (y + n) `mod` (h+1) ]
  where
    ((0,0),(w,h)) = bounds arr

printA arr = putStrLn $ unlines $ [ unwords [ [ if arr ! (x,y) > 0 then '#' else '.' | x <- [xx..xx+4] ]
                                            | xx <- [0,5..w] ]
                                  | y <- [0..h] ]
  where
    ((0,0),(w,h)) = bounds arr

test = printA . foldl' exec (listArray ((0,0),(6,2)) (repeat 0)) . map parse $
    [ "rect 3x2"
    , "rotate column x=1 by 1"
    , "rotate row y=0 by 4" ]

main = do arr <- foldl' exec (listArray ((0,0),(49,5)) (repeat 0)) . map parse . lines <$> getContents
          print . sum $ elems arr
          printA arr
