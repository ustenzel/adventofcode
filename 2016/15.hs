{-
Disc #1 has 7 positions; at time=0, it is at position 0.
Disc #2 has 13 positions; at time=0, it is at position 0.
Disc #3 has 3 positions; at time=0, it is at position 2.
Disc #4 has 5 positions; at time=0, it is at position 2.
Disc #5 has 17 positions; at time=0, it is at position 0.
Disc #6 has 19 positions; at time=0, it is at position 7.
-}

data Disc = Disc { dsize :: !Int, doff :: !Int }

discsA =
    [ Disc  7 0
    , Disc 13 0
    , Disc  3 2
    , Disc  5 2
    , Disc 17 0
    , Disc 19 7 ]

discsB = discsA ++ [ Disc 11 0 ]


-- Ball started at t0 reaches disc n (counting starts at 1!) at time (t0+n), when it is in position ((t0+n+o) `mod` s)
pass :: Int -> Int -> Disc -> Bool
pass t0 n (Disc s o)  =  (t0+n+o) `mod` s == 0

pass_all :: Int -> [Disc] -> Bool
pass_all t0  =  and . zipWith (pass t0) [1..]

-- Brute force is good enough.  Chinese remainder theorem would be
-- elegant.
main = print $ head $ filter (flip pass_all discsB) [0..]
