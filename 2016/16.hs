import Prelude ()
import BasePrelude

import qualified Data.Vector.Unboxed as U

expand :: U.Vector Word8 -> U.Vector Word8
expand v = v <> U.singleton 0 <> U.reverse (U.map (1-) v)

cksum :: U.Vector Word8 -> U.Vector Word8
cksum v | odd (U.length v) = v
        | otherwise        = cksum v'
  where
    v' = U.generate (U.length v `div` 2) $ \i ->
            let x = v U.! (2*i)
                y = v U.! (2*i+1)
            in (x+y+1) .&. 1


initial :: U.Vector Word8
initial = U.fromList $ map (fromIntegral . digitToInt) "11011110011011101"

showV :: U.Vector Word8 -> String
showV = map (intToDigit . fromIntegral) . U.toList

puzzle :: Int -> IO ()
puzzle l = print . showV . cksum . U.take l . head . dropWhile ((< l) . U.length) $ iterate expand initial

main = do puzzle 272
          puzzle 35651584
