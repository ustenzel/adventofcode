import Prelude ()
import BasePrelude

import qualified Data.Vector.Unboxed as V

type OP = V.Vector Word8 -> V.Vector Word8

c2w :: Char -> Word8
c2w = fromIntegral . ord

w2c :: Word8 -> Char
w2c = chr . fromIntegral

parse :: [String] -> (OP, OP)
parse ["rotate","based","on","position","of","letter",[a]] = do_rot_letter2   (c2w  a)
parse ["swap","letter",[a],"with","letter",[b]]            = do_swap_letters2 (c2w  a) (c2w  b)
parse ["move","position",x,"to","position",y]              = do_move2         (read x) (read y)
parse ["swap","position",x,"with","position",y]            = do_swap_pos2     (read x) (read y)
parse ["rotate","right","1","step"]                        = do_rot_right2    1
parse ["rotate","right",n,"steps"]                         = do_rot_right2    (read n)
parse ["rotate","left" ,"1","step"]                        = do_rot_left2     1
parse ["rotate","left" ,n,"steps"]                         = do_rot_left2     (read n)
parse ["reverse","positions",x,"through",y]                = do_reverse2      (read x) (read y)
parse ws                                                   = error $ "Huh? " ++ show ws


do_rot_letter2     a = (do_rot_letter a,     do_rot_letter_inv a)
do_swap_letters2 a b = (do_swap_letters a b, do_swap_letters a b)
do_move2         x y = (do_move x y,         do_move y x)
do_swap_pos2     x y = (do_swap_pos x y,     do_swap_pos x y)
do_rot_right2      n = (do_rot_right n,      do_rot_left n)
do_rot_left2       n = (do_rot_left  n,      do_rot_right n)
do_reverse2      x y = (do_reverse x y,      do_reverse x y)


do_rot_right m v = V.drop (l-n) v <> V.take (l-n) v where l = V.length v ; n = m `mod` l
do_rot_left  m v = V.drop   n   v <> V.take   n   v where l = V.length v ; n = m `mod` l
do_swap_pos x y v = v V.// [(x, v V.! y), (y, v V.! x)]
do_swap_letters a b v = V.map (\c -> if a == c then b else if b == c then a else c) v
do_reverse x y v = V.take x v <> V.reverse (V.drop x (V.take (y+1) v)) <> V.drop (y+1) v
do_move x y v = V.take y v' <> V.singleton (v V.! x) <> V.drop y v'
  where v' = V.take x v <> V.drop (x+1) v

-- What is this shit?!
do_rot_letter a v = do_rot_right j v
  where
    Just i = V.elemIndex a v
    j = if i >= 4 then i+2 else i+1

-- Going forward:  if i>=4, the letter ends up in position 2i+2, which
-- is even; else in position 2i+1, which is odd.  (Good thing the length
-- of the password is just 8, which is even and short.)
do_rot_letter_inv a v =
    case [ v' | j <- [ 0 .. V.length v - 1 ]
              , let v' = do_rot_left j v
              , do_rot_letter a v' == v ] of
        [r] -> r
        [ ] -> error "no solution"
        rs  -> error $ "more solutions: " ++ show rs

fromS :: String -> V.Vector Word8
fromS = V.fromList . map c2w

toS :: V.Vector Word8 -> String
toS = map w2c . V.toList

scramble :: V.Vector Word8 -> [(OP,OP)] -> V.Vector Word8
scramble v = foldl (flip id) v . map fst

unscramble :: V.Vector Word8 -> [(OP,OP)] -> V.Vector Word8
unscramble v = foldl (flip id) v . map snd . reverse

main = do ops <- map (parse . words) . lines <$> getContents
          let w = scramble (fromS "abcdefgh") ops
          print $ toS w
          let z = toS $ unscramble w ops
          printf "%s %s\n" (show z) (if z == "abcdefgh" then "OK" else "fail")
          print $ toS $ unscramble (fromS "fbgdceah") ops

test = print . toS . scramble (fromS "abcde") . map (parse . words) $ ex

ex = [ "swap position 4 with position 0"
     , "swap letter d with letter b"
     , "reverse positions 0 through 4"
     , "rotate left 1 step"
     , "move position 1 to position 4"
     , "move position 3 to position 0"
     , "rotate based on position of letter b"
     , "rotate based on position of letter d" ]
