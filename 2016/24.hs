import Prelude ()
import BasePrelude

import Data.Array.Unboxed

import qualified Data.HashMap.Strict as M
import qualified Data.HashSet as S

parse :: String -> UArray (Int,Int) Char
parse s = listArray ((0,0),(h-1,w-1)) $ concat $ lines s
  where
    w = length $ head $ lines s
    h = length $ lines s

all_to_all :: UArray (Int,Int) Char -> M.HashMap (Int,Int) Int
all_to_all grid =
    M.fromList $
    concatMap (\(p,c) -> map (\(c',d) -> ((digitToInt c, digitToInt c'), d))
                         $ bfs grid 0 (S.singleton p) S.empty) $
    filter (\(_,c) -> '0' <= c && c <= '9') $
    assocs grid

bfs :: UArray (Int,Int) Char -> Int -> S.HashSet (Int,Int) -> S.HashSet (Int,Int) -> [(Char, Int)]
bfs grid dist open closed | S.null open = []
bfs grid dist open closed =
    [ (c,dist) | p <- S.toList open, let c = grid ! p, '0' <= c && c <= '9' ] ++
    bfs grid (succ dist) open' closed'
  where
    open' = S.fromList [ (x+u,y+v) | (x,y) <- S.toList open
                                   , (u,v) <- [(0,1),(0,-1),(-1,0),(1,0)]
                                   , grid ! (x+u,y+v) /= '#' ]
            `S.difference` closed'
    closed' = S.union closed open

pathLen mat x (y:zs) = M.lookupDefault 999999999 (x,y) mat + pathLen mat y zs
pathLen mat x [    ] = 0

main = do mat <- all_to_all . parse <$> getContents
          let mkey = maximum $ map fst $ M.keys mat
          print $ minimum $ map (pathLen mat 0) $ permutations [1..mkey]
          print $ minimum $ map (pathLen mat 0) $ map (++[0]) $ permutations [1..mkey]

