{-# LANGUAGE BangPatterns #-}
import BasePrelude
import Prelude ()

import qualified Data.IntMap.Strict as I

data Cmd = Give { from :: Int
                , low_to :: Either Int Int          -- left is output
                , high_to :: Either Int Int }
         | Value { value :: Int
                 , bot :: Int }

parse :: String -> Cmd
parse s = case words s of
    ["bot", b, "gives", "low", "to", kl, nl, "and", "high", "to", kh, nh] -> Give (read b) (reado kl nl) (reado kh nh)
    ["value", v, "goes", "to", "bot", b]                                  -> Value (read v) (read b)
  where
    reado "bot" n = Right (read n)
    reado "output" n = Left (read n)

data Bot = Bot (Either Int Int) (Either Int Int) [Int]

type State = (I.IntMap Bot, I.IntMap [Int])

initState :: [Cmd] -> State
initState cmds = (foldl go I.empty cmds, I.empty)
  where
    go m (Give b l h) = I.insert b (Bot l h []) m
    go m (Value _  _) = m

run :: [Cmd] -> IO State
run cmds = go (initState cmds) (mapMaybe deliveries cmds)
  where
    deliveries (Value  v b) = Just (Right b,v)
    deliveries (Give _ _ _) = Nothing

    go (!bs,!os) ((Right b,v):ds) =
        let Just (Bot l h vs) = I.lookup b bs
        in case vs of [ ] -> go (I.insert b (Bot l h [v]) bs, os) ds
                      [w] -> when (sort [v,w] == [17,61]) (print b) >>
                             go (I.insert b (Bot l h []) bs, os) ((l,min v w):(h,max v w):ds)

    go (!bs,!os) ((Left o,v):ds) = go (bs, I.insertWith (++) o [v] os) ds

    go (!bs,!os) [] = return (bs,os)

main = do (bs,os) <- run . map parse . lines =<< getContents
          print os
