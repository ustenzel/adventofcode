{-# LANGUAGE OverloadedStrings, BangPatterns #-}
import BasePrelude
import Prelude ()
import Crypto.Hash.MD5
import Foreign.Marshal.Alloc

import qualified Data.ByteString as B
import qualified Data.ByteString.Unsafe as B

toHex :: B.ByteString -> B.ByteString
toHex i = unsafePerformIO $ do
    p <- mallocBytes (2 * B.length i)
    forM_ [0 .. B.length i - 1] $ \k -> do
        pokeElemOff p (2*k)   $ intToDigit' (B.index i k `shiftR` 4)
        pokeElemOff p (2*k+1) $ intToDigit' (B.index i k .&. 0xf)
    B.unsafePackMallocCStringLen (castPtr p, 2 * B.length i)

intToDigit' :: Word8 -> Word8
intToDigit' i = if i < 10 then i + 48 else i + 97 - 10

shash :: Int -> B.ByteString -> B.ByteString
shash 1 s = toHex $ hash s
shash n s = shash (n-1) $ toHex $ hash s

isGood :: [B.ByteString] -> Bool
isGood (x:xs)
    | B.length x >= 3 && B.index x 0 == B.index x 1 &&
      B.index x 0 == B.index x 2                            =  any (hasFiveOf $ B.index x 0) (take 1000 xs)
    | B.length x > 0                                        =  isGood ( B.tail x : xs )
isGood _ = False

hasFiveOf :: Word8 -> B.ByteString -> Bool
hasFiveOf x s
    | B.length s >= 5 && B.index s 0 == x && B.index s 1 == x &&
      B.index s 2 == x && B.index s 3 == x && B.index s 4 == x    =  True
    | B.length s > 0                                              =  hasFiveOf x (B.tail s)
hasFiveOf _ _ = False

pad n salt = map (id *** head) . filter (isGood . snd) . zip [0..] . tails
           $ map (shash n . (<>) salt . fromString . show) [0..]

main = do print $ pad    1 "ngcjuoqr" !! 63  -- part A, instantaneous
          print $ pad 2017 "ngcjuoqr" !! 63  -- part B, takes 2½ minutes




