import Data.Char
import qualified Data.IntMap.Strict as I

-- Adding 'Add' and 'Mul' instructions instead of the two heavily loaded
-- inner nested loops gives the result instantaneously.  The program
-- computes the factorial of a, but realizes the loop exit through a
-- jnz instruction eventually getting toggled into a cpy.  The remainder
-- of the code is also toggled wildly, but it is fast anyway, so
-- simulating the bizarre machine exactly after the core loop exits is
-- feasible.

data Ins = Cpy Val Val
         | Inc Val
         | Dec Val
         | Jnz Val Val
         | Tgl Val
         | Add Val Val
         | Mul Val Val
         | Nop
  deriving Show

data Regs = Regs !Int !Int !Int !Int
  deriving Show

data Val = Val !Int
         | Reg !Int
  deriving Show

type Prog = I.IntMap Ins

parse :: [String] -> Ins
parse ["cpy",x,y] = Cpy (pval x) (pval y)
parse ["inc",x]   = Inc (pval x)
parse ["dec",x]   = Dec (pval x)
parse ["tgl",x]   = Tgl (pval x)
parse ["jnz",x,y] = Jnz (pval x) (pval y)
parse ["nop"]     = Nop
parse ["add",x,y] = Add (pval x) (pval y)
parse ["mul",x,y] = Mul (pval x) (pval y)

pval :: String -> Val
pval "a" = Reg 0
pval "b" = Reg 1
pval "c" = Reg 2
pval "d" = Reg 3
pval s | all isDigit s = Val $ read s
pval ('-':s) | all isDigit s = Val $ - read s



run :: Prog -> Int -> Regs -> Regs
run prg ip regs = case I.lookup ip prg of
    Nothing -> regs
    Just (Cpy x y)        -> run prg (succ ip) (set regs y $ get regs x)
    Just (Add x y)        -> run prg (succ ip) (set regs y $ get regs x + get regs y)
    Just (Mul x y)        -> run prg (succ ip) (set regs y $ get regs x * get regs y)
    Just (Inc x)          -> run prg (succ ip) (set regs x $ get regs x + 1)
    Just (Dec x)          -> run prg (succ ip) (set regs x $ get regs x - 1)
    Just (Tgl x)          -> run (toggle prg $ ip + get regs x) (succ ip) regs
    Just  Nop             -> run prg (succ ip) regs
    Just (Jnz x y)
        | get regs x /= 0 -> run prg (ip + get regs y) regs
        | otherwise       -> run prg (succ ip) regs

set :: Regs -> Val -> Int -> Regs
set (Regs a b c d) (Reg 0) x = Regs x b c d
set (Regs a b c d) (Reg 1) x = Regs a x c d
set (Regs a b c d) (Reg 2) x = Regs a b x d
set (Regs a b c d) (Reg 3) x = Regs a b c x
set  regs               _  _ = regs

get :: Regs -> Val -> Int
get (Regs a b c d) (Reg 0) = a
get (Regs a b c d) (Reg 1) = b
get (Regs a b c d) (Reg 2) = c
get (Regs a b c d) (Reg 3) = d
get             _  (Val x) = x

toggle :: Prog -> Int -> Prog
toggle prg i = I.adjust go i prg
  where
    go (Inc x) = Dec x
    go (Dec x) = Inc x
    go (Tgl x) = Inc x
    go (Cpy x y) = Jnz x y
    go (Jnz x y) = Cpy x y

main :: IO ()
main = do let prg = I.fromList . zip [0..] $ map (parse . words . snd) bothPrograms
          print $ run prg 0 (Regs 7 0 0 0)
          print $ run prg 0 (Regs 12 0 0 0)


-- The left column was my original input; the right column is the same
-- program with inner loops replaced by more suped up instructions.
bothPrograms :: [(String, String)]
bothPrograms =
    [( "cpy a b",         "cpy a b"     )
    ,( "dec b",           "dec b"       )
    ,( "cpy a d",         "mul b a"     )
    ,( "cpy 0 a",         "nop"         )
    ,( "cpy b c",         "nop"         )
    ,( "inc a",           "nop"         )
    ,( "dec c",           "nop"         )
    ,( "jnz c -2",        "nop"         )
    ,( "dec d",           "nop"         )
    ,( "jnz d -5",        "nop"         )
    ,( "dec b",           "dec b"       )
    ,( "cpy b c",         "cpy b c"     )
    ,( "cpy c d",         "cpy c d"     )
    ,( "dec d",           "add d c"     )
    ,( "inc c",           "cpy 0 d"     )
    ,( "jnz d -2",        "nop"         )
    ,( "tgl c",           "tgl c"       )
    ,( "cpy -16 c",       "cpy -16 c"   )
    ,( "jnz 1 c",         "jnz 1 c"     )
    ,( "cpy 75 c",        "cpy 75 c"    )
    ,( "jnz 85 d",        "jnz 85 d"    )
    ,( "inc a",           "inc a"       )
    ,( "inc d",           "inc d"       )
    ,( "jnz d -2",        "jnz d -2"    )
    ,( "inc c",           "inc c"       )
    ,( "jnz c -5",        "jnz c -5"    )]
