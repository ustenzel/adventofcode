import Prelude ()
import BasePrelude

parse :: [String] -> (String, Int,Int)
parse [l,_,u,a,_] = (l, read (init u), read (init a))

-- Part A
main = do nodes <- map parse . map words . drop 2 . lines <$> getContents
          print $ length [ ()
                         | (l1,u1,a1) <- nodes
                         , (l2,u2,a2) <- nodes
                         , l1 /= l2
                         , u1 > 0
                         , u1 <= a2 ]


-- Part B
-- We got one empty node with lots of free space at (3,28), all other
-- nodes are full enough that they can not move anywhere, except to the
-- empty node.  32 nodes (at all y==20) are so full that they cannot
-- move at all.  All other nodes are big enough to take the data of any
-- other node.  Data is at (32,0) and needs to go to (0,0).
--
-- Put another way, we need to move the empty spot into position, then
-- move the data.  The data can move along the top row, so we do this:
--
-- (1) Move the empty spot from (3,28) to (0,20), the only gap in the
--     barrier.  [11 moves]
-- (2) Move the spot from (0,20) to (31,0).  Move the data over.
--     [52 moves]
-- (3) Move the spot down, left, left, up (around the data, from (32,0)
--     to (30,0)).  Move the data over.  [5 moves]  Repeat 31 times.
--
-- Total: 11 + 52 + 5*31 = 218 moves.
