import BasePrelude
import Prelude ()

main = print . map most_frequent . transpose . lines =<< getContents

most_frequent = snd . head {-last-} . sort . map (length &&& head) . group . sort
