import Prelude ()
import BasePrelude

import qualified Data.ByteString.Char8 as B

-- sorted list of non-overlapping intervals
type Allowed = [(Word32,Word32)]

parse :: B.ByteString -> [(Word32,Word32)]
parse s0
    | B.null s0 = []
    | otherwise = case B.readInteger s0 of { Just (l,s1) ->
                  case B.uncons s1 of      { Just ('-',s2) ->
                  case B.readInteger s2 of { Just (r,s3) ->
                  case B.uncons s3 of      { Just ('\n',s4) ->
                  (fromIntegral l, fromIntegral r) : parse s4 }}}}

block :: Allowed -> (Word32,Word32) -> Allowed
block [        ]   _   = []
block ((x,y):is) (l,r)
    | y <= l           = (x,y) : block is (l,r)
    | x < l && y <= r  = (x,l-1) : block is (l,r)
    | x < l            = (x,l-1) : (r+1,y) : is
    | y <= r           = block is (l,r)
    | x <= r           = (r+1,y) : is
    | otherwise        = (x,y) : is

printI (l,r) | l  > r    = printf "-\n"
             | l == r    = printf "%d\n" l
             | otherwise = printf "%d-%d %d\n" l r (r-l+1)

main = do allowed <- foldl' block [(0,0xffffffff)] . parse <$> B.getContents
          print . fst $ head allowed
          -- mapM_ printI allowed
          print . sum . map (\(l,r) -> r-l+1) $ allowed

