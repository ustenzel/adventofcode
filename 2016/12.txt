// reverse-engineered it, these are the notes left behind
// answers are then fib(27)+16*12 == 318003 and fib(34)+16*12 == 9227657

      	cpy 1 a                     a := 1
        cpy 1 b                     b := 1
      	cpy 26 d                    d := 26
                                    if( c ) {
      	jnz c 6:                
      	jnz 1 10:
     6:	cpy 7 c                         d += 7
     7:	inc d
      	dec c
      	jnz c 7:                   
                                    }

// a = fib(d-1) 
                                    do {
    10:	cpy a c                         
    11:	inc a                           a += b
      	dec b                      
      	jnz b 11:
      	cpy c b                         b := old_a              
      	dec d                           d-- ;
      	jnz d 10:                   } while( d ) ;

      	cpy 16 c                    c := 16
                                    {
    18:	cpy 12 d                        
    19:	inc a                           a += 12
      	dec d
      	jnz d 19:
      	dec c                           c-- 
      	jnz c 18:                   } while(c)                  a += 16*12
