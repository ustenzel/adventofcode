{-# LANGUAGE BangPatterns, OverloadedStrings #-}
import Prelude ()
import BasePrelude

import Crypto.Hash.MD5 ( hash )

import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C

data Node = Node { path :: !B.ByteString
                 , px :: !Int
                 , py :: !Int }
  deriving Show

expand :: B.ByteString -> Node -> [Node]
expand pfx (Node p x y) =
    [ Node (p `C.snoc` k) (x+u) (y+v)
    | (k,u,v,i) <- [('U',0,-1,0), ('R',1,0,3), ('D',0,1,1), ('L',-1,0,2)]
    , x+u >= 0 && y+v >= 0
    , x+u < 4 && y+v < 4
    , get i > 10 ]
  where
    h = hash (pfx <> p)
    get i | even i = B.index h (div i 2) `shiftR` 4
          | odd  i = B.index h (div i 2) .&. 0xf

search :: B.ByteString -> [Node] -> [Node] -> [ Node ]
search pfx (x:xs) ys
    | (px x, py x) == (3,3) = x : search pfx xs ys
    | otherwise = search pfx xs (expand pfx x ++ ys)
search pfx [] [] = []
search pfx [] ys = search pfx (reverse ys) []

mainA = do print . head $ search "ihgpwlah" [Node "" 0 0] []
           print . head $ search "kglvqrro" [Node "" 0 0] []
           print . head $ search "ulqzkmiv" [Node "" 0 0] []
           print . head $ search "pslxynzg" [Node "" 0 0] []

longest = maximum . map (B.length . path)

mainB = do print . longest $ search "ihgpwlah" [Node "" 0 0] []
           print . longest $ search "kglvqrro" [Node "" 0 0] []
           print . longest $ search "ulqzkmiv" [Node "" 0 0] []
           print . longest $ search "pslxynzg" [Node "" 0 0] []

