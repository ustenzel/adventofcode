{-# LANGUAGE OverloadedStrings #-}
import Prelude ()
import BasePrelude

import Crypto.Hash.MD5
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C


mkhash :: B.ByteString -> Int -> B.ByteString
mkhash input i = hash $ input <> fromString (show i)

-- if hex rep. starts with 5 zeroes, take sixth and seventh char
hash_char s | B.index s 0 /= 0 = Nothing
            | B.index s 1 /= 0 = Nothing
            | B.index s 2 .&. 0xF0 /= 0 = Nothing
            | otherwise = Just ( fromIntegral $ B.index s 2 .&. 0xF
                               , fromIntegral $ B.index s 3 `shiftR` 4 )

puzzleA x = print $ take 8 $ map (intToDigit . fst) $ mapMaybe hash_char $ map (mkhash x) [ 0.. ]

puzzleB x = mapM_ print $ build "--------" $ mapMaybe hash_char $ map (mkhash x) [ 0.. ]
  where
    build s _ | C.all (/= '-') s = [s]
    build s ((x,y):ps) | x >= 8 || C.index s x /= '-' = build s ps
                       | otherwise                    = s' : build s' ps
      where
        s' = B.take x s <> C.singleton (intToDigit y) <> B.drop (x+1) s

mainA = puzzleA "abc" >> puzzleA "ffykfhsq"
main = puzzleB "abc" >> puzzleB "ffykfhsq"


