{-# LANGUAGE OverloadedStrings, BangPatterns #-}
import Prelude ()
import BasePrelude
import System.IO

import qualified Data.Attoparsec.ByteString.Char8 as A
import qualified Data.ByteString.Char8            as B

data Rspec = Rspec { enc_name  :: [B.ByteString]
                   , sector_id :: Int
                   , cksum     :: B.ByteString }
  deriving Show

checksum :: Rspec -> B.ByteString
checksum = B.pack . map snd . take 5 . sort . map ((Down . length) &&& head)
         . group . sort . concat . map B.unpack . enc_name

p_rspec s = A.parseOnly p1 s
  where
    p1 = Rspec <$> A.many1 (A.takeWhile isLower <* A.char '-')
               <*> A.decimal
               <*  A.char '['
               <*> A.takeWhile isLower
               <*  A.char ']'
               <*  A.endOfInput

decrypt :: Rspec -> [B.ByteString]
decrypt r = map (B.map dec) $ enc_name r
  where
    dec c = chr $ (ord c - ord 'a' + sector_id r) `mod` 26 + ord 'a'

main = do rs <- either fail return . mapM p_rspec . B.lines =<< B.getContents
          print . sum . map sector_id . filter (liftA2 (==) cksum checksum) $ rs
          mapM_ (\r -> printf "%d\t%s\n" (sector_id r) (B.unpack $ B.unwords $ decrypt r)) rs

