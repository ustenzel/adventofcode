import Data.List

input = ".^^^.^.^^^^^..^^^..^..^..^^..^.^.^.^^.^^....^.^...^.^^.^^.^^..^^..^.^..^^^.^^...^...^^....^^.^^^^^^^"

step :: String -> String
step s = map go . filter ((==) 3 . length) . map (take 3) . tails $ '.' : s ++ "."
  where
    go "^^." = '^'
    go ".^^" = '^'
    go "^.." = '^'
    go "..^" = '^'
    go _ = '.'

go n = length . filter (=='.') . concat . take n $ iterate step input

-- Brute force, but runs in 5 seconds, so who cares?
main = do print $ go 40
          print $ go 400000
