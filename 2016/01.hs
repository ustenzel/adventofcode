import qualified Data.HashSet as M

clean = map (\c -> if c == ',' then ' ' else c)

step (x,y,u,v) (t:s) = (x + read s * u', y + read s * v', u', v')
  where
    (u',v') | t == 'L'  =  (-v,u)
            | t == 'R'  =  (v,-u)

step' (x,y,u,v) (t:s) = [ (x + i * u', y + i * v', u', v') | i <- [1 .. read s] ]
  where
    (u',v') | t == 'L'  =  (-v,u)
            | t == 'R'  =  (v,-u)

mainA = do (x,y,_,_) <- foldl step (0,0,0,1) . words . clean <$> getContents
           print (x,y, abs x + abs y)

find_dup m ((x,y,_,_):ps)
    | M.member (x,y) m = (x,y)
    | otherwise        = find_dup (M.insert (x,y) m) ps

main  = do (x,y) <- find_dup M.empty . concat . scanl (step' . last) [(0,0,0,1)] . words . clean <$> getContents
           print (x,y, abs x + abs y :: Int)
