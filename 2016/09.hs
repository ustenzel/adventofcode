{-# LANGUAGE BangPatterns #-}
import BasePrelude
import Prelude ()

decomp ('(':s) =
    let (ln,'x':s2) = span isDigit s
        (ct,')':s3) = span isDigit s2
        (rep,rest)  = splitAt (read ln) s3
    in concat (replicate (read ct) rep) ++ decomp rest

decomp (c:s) = c : decomp s
decomp [   ] = [ ]

decomp2L !l ('(':s) =
    let (ln,'x':s2) = span isDigit s
        (ct,')':s3) = span isDigit s2
        (rep,rest)  = splitAt (read ln) s3
        l' = read ct * decomp2L 0 rep + l
    in decomp2L l' rest

decomp2L !l (_:s) = decomp2L (succ l) s
decomp2L !l [   ] = l

main = print . decomp2L 0 . filter (not . isSpace) =<< getContents
